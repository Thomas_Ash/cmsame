<?php namespace cmsame;
	class crypto {
		static $blake_init = NULL;
		static $blake_sigma = NULL;
		static $blake_seq = NULL;
		public static function init() {
			crypto::$blake_init = [
				0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
				0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19];

			crypto::$blake_sigma = [
				[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
				[14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3],
				[11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4],
				[7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8],
				[9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13],
				[2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9],
				[12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11],
				[13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10],
				[6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5],
				[10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0]];

			crypto::$blake_seq = [
				[0, 4, 8, 12], [1, 5, 9, 13], [2, 6, 10, 14], [3, 7 ,11, 15],
				[0, 5, 10, 15], [1, 6, 11, 12], [2, 7, 8, 13], [3, 4, 9, 14]];
		}

		private static function blake_trim($word) {
			return $word & 0xFFFFFFFF;
		}

		private static function blake_rrot($word, $bits) {
			return self::blake_trim(($word < 0 ? ((~$word)>>$bits) ^ (0x7fffffff>>($bits-1)) : $word >> $bits) ^ ($word << (32-$bits)));
		}

		private static function blake_comp($ctx, $last) {
			//debug
/*				echo "State: ";
			for($i=0;$i<8;$i++) {
				printf("%08x ", $ctx->state[$i]);
			}
			echo "\n";

			echo "Buffer: ";
			for($i=0;$i<64;$i++) {
				printf("%02x ", $ctx->buf[$i]);
				if($i%16==15) echo "\n        ";

			}
*/
			//end debug
			for($i=0;$i<8;$i++) {
				$v[$i] = $ctx->state[$i];
				$v[$i+8] = self::$blake_init[$i];
			}
			$v[12] ^= $ctx->tl;
			$v[13] ^= $ctx->th;
			if($last) $v[14] = self::blake_trim(~$v[14]);

			for($i=0;$i<16;$i++) {
				$m[$i] = 0;
				for($j=0;$j<4;$j++) {
					$m[$i] = $m[$i] << 8;
					$m[$i] ^= $ctx->buf[(4*$i)+3-$j];
				}
			}

			//debug
/*			echo "\nv: ";
			for($i=0;$i<16;$i++) {
				printf("%08x ", $v[$i]);
				if($i==7) echo "\n   ";

			}
			echo "\nm: ";
			for($i=0;$i<16;$i++) {
				printf("%08x ", $m[$i]);
				if($i==7) echo "\n   ";

			}
			echo "\n";
*/
			//end debug

			for($i=0;$i<10;$i++) {
				for($j=0;$j<8;$j++) {
					$v[self::$blake_seq[$j][0]] = self::blake_trim($v[self::$blake_seq[$j][0]] + $v[self::$blake_seq[$j][1]] + $m[self::$blake_sigma[$i][$j*2]]);
					$v[self::$blake_seq[$j][3]] = self::blake_rrot($v[self::$blake_seq[$j][3]] ^ $v[self::$blake_seq[$j][0]], 16);
					$v[self::$blake_seq[$j][2]] = self::blake_trim($v[self::$blake_seq[$j][2]] + $v[self::$blake_seq[$j][3]]);
					$v[self::$blake_seq[$j][1]] = self::blake_rrot($v[self::$blake_seq[$j][1]] ^ $v[self::$blake_seq[$j][2]], 12);
					$v[self::$blake_seq[$j][0]] = self::blake_trim($v[self::$blake_seq[$j][0]] + $v[self::$blake_seq[$j][1]] + $m[self::$blake_sigma[$i][$j*2+1]]);
					$v[self::$blake_seq[$j][3]] = self::blake_rrot($v[self::$blake_seq[$j][3]] ^ $v[self::$blake_seq[$j][0]], 8);
					$v[self::$blake_seq[$j][2]] = self::blake_trim($v[self::$blake_seq[$j][2]] + $v[self::$blake_seq[$j][3]]);
					$v[self::$blake_seq[$j][1]] = self::blake_rrot($v[self::$blake_seq[$j][1]] ^ $v[self::$blake_seq[$j][2]], 7);
				}
			}
			for($i=0;$i<8;$i++) {
				$ctx->state[$i] ^= $v[$i] ^ $v[$i+8];
			}
		}

		public static function blake2s128($data) {
			$ctx = new \stdClass();
			$ctx->state = self::$blake_init;
			$ctx->state[0] ^= 0x01010010;
			$ctx->tl=0; $ctx->th=0;
			$ctx->ptr =0;
			$ctx->buf = array_fill(0, 64, 0);

			for($i=0;$i<strlen($data);$i++) {
				if($ctx->ptr==64) {
					$ctx->tl+=64;
					if($ctx->tl>0x100000000) {
						$ctx->tl-=0x100000000;//fake overflow
						$ctx->th++;
					}
					self::blake_comp($ctx, false);
					$ctx->ptr=0;
				}
				$ctx->buf[$ctx->ptr++] = ord($data[$i]);

			}
			//final
			$ctx->tl += $ctx->ptr;
			if($ctx->tl>0x100000000) {
				$ctx->tl-=0x100000000;//fake overflow
				$ctx->th++;
			}

			while($ctx->ptr<64) $ctx->buf[$ctx->ptr++] = 0;
			self::blake_comp($ctx, true);

			$hash='';
			for($i=0;$i<16;$i++) {
/*				echo "\$i = $i\n";
				echo "\$i >> 2 = " . ($i >> 2) . "\n";
				echo "\$i & 3 = " . ($i & 3) . "\n"; */
				$hash .= sprintf("%02x", ($ctx->state[$i >> 2] >> (8 * ($i & 3))) & 0xFF);
			}
			return $hash;

		}

		public static function salt($data = NULL) {
			$base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
			$salt='';
			if(is_null($data)) {
				$bytes = chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255)).
				 	chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255)).
					chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255));
				while(strlen($salt)<16) {
					$word = ord($bytes[0]) ^ (ord($bytes[1]) << 8) ^ (ord($bytes[2]) << 16);
					for($i=0;$i<4;$i++) {
						$salt .= $base64[$word & 0x3F];
						$word = $word >> 6;
					}
					$bytes = substr($bytes, 3);
				}
			} else {
				$hash = self::blake2s128($data);

				while(strlen($salt)<16) {
				//get 3 bytes/6 chars of hex
				//turn into 4 6-bit base64 chars
					$word = hexdec(substr($hash, 0, 6)); 
					for($i=0;$i<4;$i++) {
						$salt .= $base64[$word & 0x3F];
						$word = $word >> 6;
					}
					$hash = substr($hash, 4);
				}
			}
			return $salt;
		}


	} crypto::init();
?>
