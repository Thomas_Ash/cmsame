function confExit() {
	//probably will be overridden by browser, but...
	return('You have unsaved changes which will be lost if you leave. Are you sure?');
}
function zeroPad(value, len, base=10) {
	var out = value.toString(base);
	while (out.length<len) out = '0' + out;
	return out;
}
function parseTime(value) {
	valstr = value.toString();
	if(!valstr.match(/\d\d:\d\d:\d\d/i)) return 0;
	h = Number(valstr.substring(0,2));
	m = Number(valstr.substring(3,5));
	s = Number(valstr.substring(6,8));
	return h*3600+m*60+s;
}

var blake_init = [
	0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
	0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19];

var blake_sigma = [
	[0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15],
	[14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3],
	[11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4],
	[7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8],
	[9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13],
	[2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9],
	[12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11],
	[13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10],
	[6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5],
	[10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0]];

var blake_seq = [
	[0, 4, 8, 12], [1, 5, 9, 13], [2, 6, 10, 14], [3, 7 ,11, 15],
	[0, 5, 10, 15], [1, 6, 11, 12], [2, 7, 8, 13], [3, 4, 9, 14]];

function blake_rrot(word, bits) {
	return ((word >>> bits) ^ (word << (32-bits)) & 0xFFFFFFFF);
}

function blake_comp(ctx, last) {
	var v=[], m=[], i, j;
	for(i=0;i<8;i++) {
		v[i] = ctx.state[i];
		v[i+8] = blake_init[i];
	}
	v[12] ^= ctx.tl;
	v[13] ^= ctx.th;
	if(last) v[14] = ~v[14];

	for(i=0;i<16;i++) {
		m[i] = 0;
		for(j=0;j<4;j++) {
			m[i] = m[i] << 8;
			m[i] ^= ctx.buf[(4*i)+3-j];
		}
	}

	for(i=0;i<10;i++) {
		for(j=0;j<8;j++) {
			v[blake_seq[j][0]] = v[blake_seq[j][0]] + v[blake_seq[j][1]] + m[blake_sigma[i][j*2]];
			v[blake_seq[j][3]] = blake_rrot(v[blake_seq[j][3]] ^ v[blake_seq[j][0]], 16);
			v[blake_seq[j][2]] = v[blake_seq[j][2]] + v[blake_seq[j][3]];
			v[blake_seq[j][1]] = blake_rrot(v[blake_seq[j][1]] ^ v[blake_seq[j][2]], 12);
			v[blake_seq[j][0]] = v[blake_seq[j][0]] + v[blake_seq[j][1]] + m[blake_sigma[i][j*2+1]];
			v[blake_seq[j][3]] = blake_rrot(v[blake_seq[j][3]] ^ v[blake_seq[j][0]], 8);
			v[blake_seq[j][2]] = v[blake_seq[j][2]] + v[blake_seq[j][3]];
			v[blake_seq[j][1]] = blake_rrot(v[blake_seq[j][1]] ^ v[blake_seq[j][2]], 7);
		}
	}

	for(i=0;i<8;i++) {
		ctx.state[i] ^= v[i] ^ v[i+8];
	}
}

function blake2s128(data) {
	var ctx = [], i;
	ctx.state = blake_init.slice();
	ctx.state[0] ^= 0x01010010;
	ctx.tl = 0; ctx.th = 0;
	ctx.ptr = 0;
	ctx.buf = [];
	for(i=0;i<64;i++) ctx.buf[i] = 0;

	for(i=0;i<data.length;i++) {
		if(ctx.ptr==64) {
			ctx.tl+=64;
			if(ctx.tl>0x100000000) {
				ctx.tl-=0x100000000;//fake overflow
				ctx.th++;
			}
			blake_comp(ctx, false);
			ctx.ptr=0;
		}
		ctx.buf[ctx.ptr++] = data.charCodeAt(i);
	}
	//final
	ctx.tl += ctx.ptr;
	if(ctx.tl>0x100000000) {
		ctx.tl-=0x100000000;//fake overflow
		ctx.th++;
	}

	while(ctx.ptr<64) ctx.buf[ctx.ptr++] = 0;
	blake_comp(ctx, true);

	hash='';
	for(i=0;i<16;i++) {
		hash+=zeroPad(((ctx.state[i >>> 2] >>> (8 * (i & 3))) & 0xFF), 2, 16);
	}

	return hash;

}

$.widget( "ui.timespinner", $.ui.spinner, {
	options: {step:60,page:60,min:0,max:86400},
	_parse: function(value) {
		if(typeof value === "string") {
			if(Number(value) == value) {
				return Number(value);
			}
			return parseTime(value);
		}
		return value;
	},
	_format: function(value) {
		var h = Math.floor(value / 3600);
		var m = Math.floor((value-(h*3600)) / 60);
		var s = value - (h*3600+m*60);
		return zeroPad(h,2) + ':' + zeroPad(m,2) + ':' + zeroPad(s,2);
	},
	_uiSpinnerHtml: function() {
		return '<span class="ui-timespinner ui-corner-all ui-widget ui-widget-content"></span>';
	}
});

$(document).ready(function() {
	$('input.btndelete').on('click', function(e) {
		var del = confirm('This will permanently delete the item. Are you sure?');
		if(del) {
			window.onbeforeunload = null;
			$(this).after('<input type="hidden" name="_DELETE" value="_DELETE" />');
		}
		else e.preventDefault();
	});

	$('form.detail').on('submit', function(e) {
		var form = this;
		e.preventDefault();
		var data = $(form).serialize();

		$(this).find('input.uploadfile').each(function() {
			if($(this).get(0).files.length==0) return;
			var id=$(this).attr('id');
			data += '&' + id + 'ValidateSize=' + $(this).get(0).files[0].size;
			data += '&' + id + 'ValidateType=' + encodeURIComponent($(this).get(0).files[0].type);
		});

		$.post('validate.php', data, function(d) {
			var res = jQuery.parseJSON(d);
			if(res===null) {
				window.onbeforeunload = null;
				form.submit();
			} else {
				for (var field in res.updates) {
					$('#' + field).val(res.updates[field]);
				}
				if(res.errors.length==0) {
					window.onbeforeunload = null;
					form.submit();
				} else {
					for (var field in res.errors) {
						$('div#' + field + 'Error').html('<span>' + res.errors[field] + '</span>');
					}
				}
			}
		});
	});

	$('form.detail div.input').find('input.text, input.integer, input.decimal, select.select, input.boolean, input.datetime, textarea.bigtext, input.uploadfile').change(function() {
		window.onbeforeunload = confExit;
		$('div._selfpw:not(:visible)').each(function() {
			$(this).siblings('div.data').hide();
			$(this).show();
		});

	});

	$('div.input input.integer').spinner();
	$('div.input input.decimal').spinner({step:0.01,numberFormat: "n"});
	$('div.input input.timepart').timespinner();
	$('div.input input.datepart').datepicker({showOn:"button",buttonText:"Calendar",dateFormat:"dd/mm/yy"});
	$('div.input input.datetime').each(function(){
		var val = $(this).val();
		if(val) {
			var datepart = $(this).siblings('input.datepart');
			var timepart = $(this).parent().find('input.timepart');
			var dateval = val.substring(8, 10) + '/' + val.substring(5, 7) + '/' + val.substring(0, 4);
			var timeval = val.substring(11, 13) + ':' + val.substring(14, 16) + ':' + val.substring(17, 19);
			datepart.val(dateval);
			timepart.val(timeval);
		}
	});
	$('div.input input.datepart').change(function(){

		var datetime = $(this).siblings('input.datetime');
		var datepart = $(datetime).siblings('input.datepart');
		var timepart = $(datetime).parent().find('input.timepart');
		var dateval = $(datepart).val();
		var timeval = $(timepart).val();
		if(!dateval.match(/\d\d\/\d\d\/\d\d\d\d/i)) return;
		if(!timeval.match(/\d\d:\d\d:\d\d/i)) return;
		var val = dateval.substring(6, 10) + "-" +  dateval.substring(3, 5) + "-" +
			dateval.substring(0, 2) + " " + timeval.substring(0, 2) + ":" +
			timeval.substring(3, 5) + ":" + timeval.substring(6, 8);
		$(datetime).val(val).trigger('change');
	});
	$('div.input input.datepart').keyup(function(){
		$(this).trigger('change');
	});
	$('div.input a.ui-spinner-button').click(function() {
		$(this).parent().parent().find('input.datepart').trigger('change');
	});
	$('div.input input.timepart').keyup(function() {
		$(this).parent().parent().find('input.datepart').trigger('change');
	});
	$('div.input select.oneof').change(function() {
		var id=$(this).val();
		var btn = $(this).siblings('a.oneof');
		var url = $(btn).attr('href');
		$(btn).attr('href', url.substring(0, url.lastIndexOf('=') + 1) + id);
	});
	$('div.input div.tabs').tabs({
		activate: function(e, ui) {
			var cur = ui.newPanel;
			var id = cur.attr('id');
			var m = /^(.*)-tabs-prev$/.exec(id);
			if(m) {
				var col = m[1];
				var url = "Preview" + col + "Markdown.php";
				var comp = $(cur).siblings('#' + col + '-tabs-comp');
				var inp = $(comp).find('textarea');
				if(inp.length==0) inp = $(comp).find('input');
				var raw = $(inp).val();
				var data = {};
				data[col] = raw;
				$.post(url, data, function(d, s, x) {
					$(cur).children('div.preview').html(d);
				});
			}
		}
	});

	$('div.input a.btnremove').click(function(e) {
		e.preventDefault();
		window.onbeforeunload = confExit;
		$(this).siblings('input').val('');
		$(this).siblings('input.doremove').val('Remove');
		$(this).siblings('span').remove();
		if($(this).hasClass('file')) $(this).after('<span>(no file)</span>');
		else if($(this).hasClass('image')) $(this).after('<span>(no image)</span>');
		else if($(this).hasClass('password')) $(this).after('<span>(no password)</span>');
		$(this).remove();
	});

	$('a.imgpopup').click(function(e) {
		e.preventDefault();
		var href = $(this).attr('href');
		var wx = $(window).width();
		var wy = $(window).height();
		var overlay=$('body>div#overlay');
		if(overlay.length==0) {
			$('body').prepend('<div id="overlay"></div>');
			overlay=$('body>div#overlay');
		}
		$(overlay).empty().html('<div id="lightbox"><div class="image"><img src="'+href+'"/><div class="buttons"><span></span><a class="button btnclose">Close</a></div></div>');
		var scale=NaN;
		var ix = $(overlay).find('img').get(0).width;
		var iy = $(overlay).find('img').get(0).height;
		if(ix>(wx*0.8)) { scale=(wx*0.8)/ix; ix*=scale; iy*=scale; }
		if(iy>(wy*0.8)) { scale=(wy*0.8)/iy; ix*=scale; iy*=scale; }
		scale=ix/$(overlay).find('img').get(0).width;
		if(isNaN(scale)) {
			$(this).addClass('_tryagain');
			setTimeout(function() {
				$('a._tryagain').click();
			}, 500);
			return;
		} else $(this).removeClass('_tryagain');


		var left=(wx-ix)/2-16;
		var top=(wy-iy)/2-32;
		$(overlay).find('img').css({'width':ix.toString()+'px','height':iy.toString()+'px'});
		$(overlay).find('div#lightbox').css({'left':left.toString()+'px','top':top.toString()+'px'});
		$(overlay).find('span').text(Math.floor(scale*100).toString()+'%');
		$(overlay).show().animate({opacity:1}, 200, function() {
			$(overlay).find('a.btnclose').click(function(e) {
				e.preventDefault();
				$(overlay).animate({opacity:0}, 200).hide();
			});

		});
	});

	$('div.input a.btnresize').click(function(e) {
		e.preventDefault();
		var href = $(this).attr('href') + '&Width=' + $(window).width() + '&Height=' + $(window).height();
		var overlay=$('body>div#overlay');
		if(overlay.length==0) {
			$('body').prepend('<div id="overlay"></div>');
			overlay=$('body>div#overlay');
		}
		$(overlay).empty().load(href, function(r,s,x) {
			$(overlay).show().animate({opacity:1}, 200, function() {
				$(this).find('a.btnresizedone, a.btnresizecancel').click(function(e) {
					e.preventDefault();
					if($(this).hasClass('btnresizedone')) {
						var scale=parseFloat($(overlay).find('input.scale').val());
						var img=$(overlay).find('div.image');
						var cropbox=$(overlay).find('div.cropbox');
						var prefix=$(overlay).find('input.prefix').val();
						var l=parseInt($(cropbox).css('left'))/scale;
						var r=parseInt($(cropbox).css('right'))/scale;
						var t=parseInt($(cropbox).css('top'))/scale;
						var b=parseInt($(cropbox).css('bottom'))/scale;
						var w=parseInt($(img).width())/scale;
						var h=parseInt($(img).height())/scale;
						$('input#'+prefix+'L').val(Math.ceil(w-l));
						$('input#'+prefix+'R').val(Math.ceil(w-(r+l)));
						$('input#'+prefix+'T').val(Math.ceil(h-t));
						$('input#'+prefix+'B').val(Math.ceil(h-(b+t)));
					}
					$(overlay).animate({opacity:0}, 200).hide();
				});
				$(this).find('div.cropbox').each(function(){
					var aspect=parseFloat($(overlay).find('input.aspect').val());
					if(aspect>0) {
						var img=$(overlay).find('div.image');
						var nat=$(img).width()/$(img).height();
						if(aspect>nat) {
							var y=($(img).height()-$(img).width()/aspect)/2;
							$(this).css({top:y+'px',bottom:y+'px'});
						} else {
							var x=($(img).width()-$(img).height()*aspect)/2;
							$(this).css({left:x+'px',right:x+'px'});
						}
					}
				});
				$(this).find('div.cropbox, div.handle>div').on('mousedown', function(e) {
					e.stopPropagation();
					e.preventDefault();
					var cropbox=$('#overlay div.cropbox');
					var scale=parseFloat($(overlay).find('input.scale').val());
					var min_x=$(overlay).find('input.min_x').val() * scale;
					var min_y=$(overlay).find('input.min_y').val() * scale;
					var aspect=parseFloat($(overlay).find('input.aspect').val());
					var img_x=$(overlay).find('div.image').width();
					var img_y=$(overlay).find('div.image').height();
					$(this).data('sx', e.clientX).data('sy', e.clientY)
						.data('sl', parseInt($(cropbox).css('left')))
						.data('sr', parseInt($(cropbox).css('right')))
						.data('st', parseInt($(cropbox).css('top')))
						.data('sb', parseInt($(cropbox).css('bottom')))
						.addClass('dragging').on('mousemove', function(e) {
							var dx=e.clientX-$(this).data('sx');
							var dy=e.clientY-$(this).data('sy');
							var nl=$(this).data('sl');
							var nr=$(this).data('sr');
							var nt=$(this).data('st');
							var nb=$(this).data('sb');

							if($(this).is('.h_l div, .cropbox')) nl+=dx;
							if($(this).is('.h_r div, .cropbox')) nr-=dx;
							if($(this).is('.h_t div, .cropbox')) nt+=dy;
							if($(this).is('.h_b div, .cropbox')) nb-=dy;

							if(nl<0) {nr+=nl;nl=0;}
							if(nr<0) {nl+=nr;nr=0;}
							if(nt<0) {nb+=nt;nt=0;}
							if(nb<0) {nt+=nb;nb=0;}

							var nx, ny;

							if(aspect>0) {
								var ox=img_x-(nl+nr);
								var oy=img_y-(nt+nb);
								ny=(ox+oy)/(aspect+1);
								nx=aspect*ny;
							} else {
								nx=img_x-(nl+nr);
								ny=img_y-(nt+nb);
							}

							if(nx<min_x) nx=min_x;
							if(ny<min_y) ny=min_y;

							if($(this).is('.h_tl div')) {
								nl=img_x-(nx+nr);
								nt=img_y-(ny+nb);
							} else if($(this).is('.h_tc div')) {
								var dl=img_x-(nx+nr);
								nr+=(dl-nl)/2;
								nl+=(dl-nl)/2;
								nt=img_y-(ny+nb);
							} else if($(this).is('.h_tr div')) {
								nr=img_x-(nx+nl);
								nt=img_y-(ny+nb);
							} else if($(this).is('.h_cl div')) {
								var dt=img_y-(ny+nb);
								nb+=(dt-nt)/2;
								nt+=(dt-nt)/2;
								nl=img_x-(nx+nr);
							} else if($(this).is('.h_cr div')) {
								var dt=img_y-(ny+nb);
								nb+=(dt-nt)/2;
								nt+=(dt-nt)/2;
								nr=img_x-(nx+nl);
							} else if($(this).is('.h_bl div')) {
								nl=img_x-(nx+nr);
								nb=img_y-(ny+nt);
							} else if($(this).is('.h_bc div')) {
								var dl=img_x-(nx+nr);
								nr+=(dl-nl)/2;
								nl+=(dl-nl)/2;
								nb=img_y-(ny+nt);
							} else if($(this).is('.h_br div')) {
								nr=img_x-(nx+nl);
								nb=img_y-(ny+nt);
							}

							$(cropbox).css({left:nl+'px',right:nr+'px',top:nt+'px',bottom:nb+'px'});

						});
				});
				$(overlay).on('mouseup', function() {
					$('#overlay .dragging').off('mousemove').removeClass('dragging');
				});
			});
		});
	});
});
