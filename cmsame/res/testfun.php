<?php
        require_once('cmsame/example/crypto.php');
        require_once('cmsame/example/mdlite.php');
        if(isset($_POST['Ajax'])&&isset($_POST['Input'])&&isset($_POST['Func'])) {
                if($_POST['Func']=='blake2s128') {
                        echo \cmsame\crypto::blake2s128($_POST['Input']);
                } else if($_POST['Func']=='blake_rrot') {
                        $arg=explode(',',$_POST['Input']);
                        $res = \cmsame\crypto::blake_rrot($arg[0], $arg[1]);
                        echo $res;
                        printf("%16x", $res);
                } else if($_POST['Func']=='mdlite') {
					$md = new \cmsame\mdlite();
					echo $md->get_html($_POST['Input']);
                } else if($_POST['Func']=='mdlite_sl') {
					$md = new \cmsame\mdlite();
					echo $md->get_html_singleline($_POST['Input']);
                } else if($_POST['Func']=='htmlregex') {
					$md = new \cmsame\mdlite();
					echo $md->htmlregex(intval($_POST['Input']));
                }

        } else {

?>


<html>
        <head>
                <title>Tester page</title>
                <script src="https://code.jquery.com/jquery-3.1.1.min.js" integrity="sha256-hVVnYaiADRTO2PzUGmuLJr8BLUSjGIZsDYGmIJLv2b8=" crossorigin="anonymous"></script>
                <script>
                        $(document).ready(function() {
                                $('form').submit(function(e) {
                                        e.preventDefault();
                                        var fd = $(this).serialize();
                                        $.ajax({type:'POST',
                                                url:'testfun.php',
                                                data:fd,
                                                success:function(rd) {
                                                        $('textarea#Output').val(rd);

                                                        $('span#arrow').css({'opacity':'1.0'}).animate({'opacity':'0.0'}, 500);
                                                },
                                        });
                                });
                        });
                </script>
        </head>
        <body>
        <form>
                <input type="hidden" name="Ajax" />
                <label for="Input">Input</label>
                <textarea style="width:35%;height:90%;" name="Input"></textarea>
                <select name="Func">
                        <option value="blake2s128">blake2s128</option>
            <!--            <option value="blake_rrot">blake_rrot</option> -->
                        <option value="mdlite">mdlite</option>
                        <option value="mdlite_sl">mdlite_sl</option>
                        <option value="htmlregex">htmlregex</option>
                </select>
                <span id="arrow" style="font-size:36px;color:#08F;position:relative;opacity:0;">&#187;</span>
                <label for="Output">Output</label>
                <textarea style="width:35%;height:90%;" name="Output" id="Output"></textarea>
                <input type="submit" value="Go" />

        </form>
        </body>
</html>

<?php

        }
?>

