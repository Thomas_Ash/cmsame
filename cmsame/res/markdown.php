<?php namespace cmsame;
	class markdown {
		//static: () and [] regex, escape md5 hashes,
		//tabwidth, (X)HTML closer

		static $regex = [];
		static $special = [];
		static $cap_char = [];
		static $closer = " />";

		private static function cap_char($c) {
			if(key_exists($c, self::$cap_char)) return self::$cap_char[$c];
			if($c=='\\') $r = '~\\\\~';
			else if($c=='['||$c==']'||$c=='*') $r = "~\\$c~";
			else $r = "~$c~";
			self::$cap_char[$c] = $r;
			return $r;
		}

		//members: urls, titles, htmlblocks, listlevel
		//NO HTML buffer! inplace only

		public function __construct() {
			$this->hashedblocks = [];
			$this->urls = [];
			$this->titles = [];
			$this->listlevel=0;
			$this->ready = true;
		}

		public function init() {
//			self::$regex["_l"] = '(\\s{0,3}([\\*+-]|\\d+\\.)\\s+)(?s:.+?)(\\z|\\n{2,}(?=\\S)(?!\\s*([\\*+-]|\\d\\.)\\s+))';

			self::$regex["lr"] = '~^\\s{0,3}\\[([^\\[\\]\\s]+)\\]:\\s?([^\\[\\] ]+)\\s*(([\'"])([^\'"]+)\\4)?\\s*$~m';
			self::$regex["h1"] = '~^(.+)\\n=+$~m';
			self::$regex["h2"] = '~^(.+)\\n-+$~m';
			self::$regex["hx"] = '~^(#+)\\s*(.+?)\\s*#*$~m';
			self::$regex["hr"] = '~^\\s{0,3}([\\*-]\\s?){3,}\\s*~m';
			self::$regex["lx"] = '~((\r?\n){2})?^([\*+-]|\d\.)\s+(((?!\n[^\s\d\\*+-]).)+)((\r?\n){2})?~sm';
//			self::$regex["l0"] = '~(?:(?<=\\n\\n)|\\A\\n?)' . self::$regex["_l"] . '~m';
//			self::$regex["l1"] = '~^' . self::$regex["_l"] . '~m';
			//			1. leading line
			//				2. indent
			//					3. bullet or ordinal
			//							4. content
			//									5.lookahead: linebreaks
			//									followed by end of string or
			//									same indent and bullet/ordinal,
			//									and spaces?
//			self::$regex["li"] = '~(\\n)?(^ *)([\\*+-]|\\d+\\.)\\s+(.+?)\\n{1,2}(?=\n*(\\z|\\2([\\*+-]|\\d+\\.)\\s+))~m';

			self::$regex["li"] = '~((\r?\n){2})?^([\\*+-]|\\d\\.)\\s+(((?!\\n\\S).)+)((\r?\n){2})?~sm';
			self::$regex["lp"] = '~\\n.~';
			self::$regex["bq"] = '~(^\\s*>\\s?.+\\n(.+\\n)*\\n*)+~m';

			self::$regex["cs"] = '~(?<!\\\\)(`+)(.+?)(?<!`)\\1(?!`)~';
			self::$regex["ar"] = '~\\[([^\\[\\]]+)\\]\\s?\\[([^\\[\\]\\s]+)\\]~';
			self::$regex["ai"] = '~\\[([^\\[\\]]*)\\]\\s?\\(([^\\s\\(\\)]+)\\s?((["\'])([^\\s\\(\\)\'"]*)\\4)?\\)~';
			self::$regex["as"] = '~(\\[([^\\[\\]]+)\\])~';
			self::$regex["em"] = '~(?<!\\\\)([_\\*]{1,2})(.+)\\1~';

			foreach(str_split('\\`*_{}[]()>#+-.!') as $s) {
				self::$special[$s] = md5($s);
			}
		}

		private function hashblock($block, $pad = true) {
			$hash = md5($block); 
			$this->hashedblocks[$hash] = $block;
			if($pad) return "\n$hash";
			return $hash;
		}

		public function get_html($md) {

			//error_log('get_html');

			if(!$this->ready) {
				$this->hashedblocks = [];
				$this->urls = [];
				$this->titles = [];
				$this->listlevel = 0;
				$this->ready = true;
			}

			$md = $this->cleanup($md);
			//2. //hashhtmlblocks
			$md = $this->striplinkdefs($md);
			$md = $this->runblockgamut($md);
			//5. unescspecialchars
			//6. //convertcopyright

	//		var_dump($this->hashedblocks);
	//		echo "\n";

			$this->ready = false;

			return $md;
		}

		public function get_html_singleline($md) {

			if(!$this->ready) {
				$this->hashedblocks = [];
				$this->urls = [];
				$this->titles = [];
				$this->listlevel = 0;
				$this->ready = true;
			}

			$md = $this->cleanup($md);
			//2. //hashhtmlblocks
			$md = $this->striplinkdefs($md);
			$md = $this->runspangamut($md);


			$this->ready = false;

			return $md;

		}

		private function cleanup($md) {

			//error_log('cleanup');

			//1. unixify line endings
			$md = preg_replace('~\r\n~', "\n", $md);
			$md = preg_replace('~\r~', "\n", $md);

			//2. append \n\n
			$md .= "\n\n";

			//3. tabs to spaces
			$md = preg_replace('~\t~', "    ", $md);

			//4. trim empty lines
			$md = preg_replace('~\n +\n~', "\n\n", $md);

			return $md;
		}

		private function striplinkdefs($md) {

			//error_log('striplinkdefs');

			while(preg_match(self::$regex["lr"], $md, $m, PREG_OFFSET_CAPTURE)) {
				$id = strtolower($m[1][0]);
				$this->urls[$id] = trim($m[2][0]);
				if(count($m) == 6) {
					$this->titles[$id] = $m[5][0];
				}
				$md = substr_replace($md, "", $m[0][1], strlen($m[0][0]));
			}
			return $md;
		}

		private function runblockgamut($md, $wrap = true) {

			//error_log('runblockgamut');

			//=====also hashify blocks generated
			$md = $this->doheaders($md);
			$md = $this->dohrs($md);
			$md = $this->dolists($md);
			$md = $this->docode($md);
			$md = $this->doquotes($md);

			//=====unhashify and
			return $this->formparas($md, $wrap);
		}

		private function doheaders($md) {

			//error_log('doheaders');

			while (preg_match(self::$regex["h1"], $md, $m, PREG_OFFSET_CAPTURE)) {
				//error_log('matched ' . $m[1][0]);
				$block = "<h1>" . $this->runspangamut($m[1][0]) . "</h1>\n";
				$md = substr_replace($md, $this->hashblock($block), $m[0][1], strlen($m[0][0]));
			}

			while (preg_match(self::$regex["h2"], $md, $m, PREG_OFFSET_CAPTURE)) {
				//error_log('matched ' . $m[1][0]);
				$block = "<h2>" . $this->runspangamut($m[1][0]) . "</h2>\n";
				$md = substr_replace($md, $this->hashblock($block), $m[0][1], strlen($m[0][0]));
			}

			while (preg_match(self::$regex["hx"], $md, $m, PREG_OFFSET_CAPTURE)) {
				//error_log('matched ' . $m[1][0]);
				$level = strlen($m[1][0]);
				$block = "<h$level>" . $this->runspangamut($m[2][0]) . "</h$level>\n";
				$md = substr_replace($md, $this->hashblock($block), $m[0][1], strlen($m[0][0]));
			}

			return $md;
		}


		private function dohrs($md) { 

			//error_log('dohrs');

			while (preg_match(self::$regex["hr"], $md, $m, PREG_OFFSET_CAPTURE)) {
				$block = "<hr" . self::$closer . "\n";
				$md = substr_replace($md, $this->hashblock($block), $m[0][1], strlen($m[0][0]));
			}
			return $md;
		}


		private function dolists($md) {

			error_log('dolists on '. $md);
 
			while (preg_match(self::$regex["lx"], $md, $m, PREG_OFFSET_CAPTURE)) {
	//			var_dump($m);
	//			echo "\n";
				$list = $m[0][0];
				$type = preg_match('~^\\d\\.~', $m[3][0]) ? "ol" : "ul";

				//echo "CHECKING " . $m[2][0] . " : $type";

//				$list = preg_replace('~\\n{2,}~', "\n\n\n", $list);
				$block = "<$type>\n" . $this->dolistitems($list) . "\n</$type>\n";
				$md = substr_replace($md, $this->hashblock($block), $m[0][1], strlen($m[0][0]));
				//echo "LIST BLOCK:\n" . $block;


			}

			return $md;
		}

		private function dolistitems($md) {

			error_log('dolistitems on ' . $md);

			$this->listlevel++;
			$first=true;
//			$md = preg_replace('~\\n{2,}\\z~', "\n", $md);
			while (preg_match(self::$regex["li"], $md, $m, PREG_OFFSET_CAPTURE)) {
				error_log('matched ' . $m[0][0]);
				//var_dump($m);//, true));
//				foreach($m as $match) { echo $match[0] . "  |  "; }
//				echo PHP_EOL;
				if(preg_match(self::$regex["lp"], $m[4][0]) || (strlen($m[1][0]) > 0 && !$first)) {   //newlines followed by content
					error_log('blockmode');
					$block = "<li>" . $this->runblockgamut($m[4][0]) . "</li>\n";

//					$block = "<li>" . $this->runspangamut(trim($m[4][0])) . "</li>\n";
				} else {
					error_log('spanmode');
					$block = "<li>" . $this->runspangamut(trim($m[4][0])) . "</li>\n";
				}
				$hash = $this->hashblock($block);
				if(strlen($m[5][0]) > 0) $hash .= "\n";
				$md = substr_replace($md, $this->hashblock($block), $m[0][1], strlen($m[0][0]));
				$first=false;
				error_log('subject is now ' . $md);
			}


			return $md;
		}

		private function docode($md) { 

			//error_log('docode');

			return $md;
		}
		private function doquotes($md) { 

			//error_log('doquotes');

			while (preg_match(self::$regex["bq"], $md, $m, PREG_OFFSET_CAPTURE)) {
				$block = preg_replace('~^ *> ?~m', "", $m[0][0]);
				$block = preg_replace('~^ +$~m', "", $block);
				$block = "<blockquote>\n" . $this->runblockgamut($block) . "\n</blockquote>\n";

				$md = substr_replace($md, $this->hashblock($block), $m[0][1], strlen($m[0][0]));
			}


			return $md;
		}

		private function formparas($md, $wrap = true) {

			//error_log('formparas on ' . $md);

			$html = '';
			foreach(preg_split('~\n~', $md) as $p) {
				if(key_exists($p, $this->hashedblocks)) {
					$out = $this->formparas($this->hashedblocks[$p], false) . "\n";
				} else {

					$out = $this->runspangamut($p);
					if($wrap && trim($out)!='') $out = "<p>" . $out . "</p>\n";
				}
				$html .= $out;
			}
			return $html;
		}

	///span level

		private function runspangamut($md) {

			//error_log('runspangamut');

			$md = $this->docodespans($md);
			//2. escapespecialchars
			//3. doimages
			$md = $this->doanchors($md);
			//5. doautolinks
			//6. encodeampsandangles
			$md = $this->doemphasis($md);
			//8. dohardbreaks

			return $md;
		}

		private function docodespans($md) {

			//error_log('docodespans');

			while (preg_match(self::$regex["cs"], $md, $m, PREG_OFFSET_CAPTURE)) {
				$c = trim($m[2][0]);
				$c = preg_replace('~&~', "&amp;", $c);
				$c = preg_replace('~<~', "&lt;", $c);
				$c = preg_replace('~>~', "&gt;", $c);
				foreach(str_split('_{}*[]\\') as $s) {
					$c = preg_replace(self::cap_char($s), self::$special[$s], $c);

				}
				$md = substr_replace($md, "<code>$c</code>", $m[0][1], strlen($m[0][0]));
			}

			return $md;
		}


		private function escapespecialchars($md) { 

			//error_log('escapespecialchars');

			//split into tokens - html tag or text between - then iterate
			//tags - hashify * and _
			//text - hashify anything in $special but only if preced with \

		}

		private function doimages($md) { 
			//skip for ever
		}

		private function doanchors($md) { 
			//[my hyperlink][id]

			//error_log('doanchors');

			while(preg_match(self::$regex["ar"], $md, $m, PREG_OFFSET_CAPTURE)) {
				$id = strtolower($m[2][0]);
				if(key_exists($id, $this->urls)) {
					if(key_exists($id, $this->titles)) {
						$a = "<a href=\"" . $this->urls[$id] . "\" title=\"" . 
							$this->titles[$id] . "\">" . $m[1][0] . "</a>";
					} else {
						$a = "<a href=\"" . $this->urls[$id] . "\">" . $m[1][0] . "</a>";
					} 
				} else {
					$a = $m[1][0];
				}
				$md = substr_replace($md, $a, $m[0][1], strlen($m[0][0]));
			}

			//[my hyperlink](url "Optional Title")
			while(preg_match(self::$regex["ai"], $md, $m, PREG_OFFSET_CAPTURE)) {
	//			var_dump($m);
	//			echo "\n";
				$txt = $m[1][0];
				$url = $m[2][0];
				if(count($m) == 6) {
					$a = "<a href=\"" . $url . "\" title=\"" .
						$m[5][0] . "\">" . $txt . "</a>";
				} else {
					$a = "<a href=\"" . $url . "\">" . $txt . "</a>";
				}
				$md = substr_replace($md, $a, $m[0][1], strlen($m[0][0]));
			}

			//[visibleurl.com]
			while(preg_match(self::$regex["as"], $md, $m, PREG_OFFSET_CAPTURE)) {
				$a = "<a href=\"" . $m[1][0] . "\">" . $m[1][0] . "</a>";
				$md = substr_replace($md, $a, $m[0][1], strlen($m[0][0]));
			}
			return $md;
		}

		private function doautolinks($md) { }
		private function encodehtmlchars($md) { }

		private function doemphasis($md) { 

			//error_log('doemphasis');

			while(preg_match(self::$regex["em"], $md, $m, PREG_OFFSET_CAPTURE)) {
				$tag = (strlen($m[1][0]) == 2) ? "strong" : "em";
				$md = substr_replace($md, "<$tag>" . $m[2][0] . "</$tag>", $m[0][1], strlen($m[0][0]));
			}
			return $md;
		}

		private function dohardbreaks($md) { }


	} markdown::init();
?>
