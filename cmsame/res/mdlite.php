<?php namespace cmsame;

	/*
		YES:
			Code; need it for tech site
			Inline; essential
			Extra inline; strike, emdash, ellipsis
			Lists; one layer only though
			HTML; encoded inside code blocks, stripped or hashed outside
			Backslash escapes

		NO:
			Blockquotes; this isn't email
			Images; inline images are a bad idea, widgets/emoji are better
			Nested lists; silly
			Paragraph lists; silly
			Underline header; h1 and h2 don't belong in body content

	*/


	class mdlite {
		function hash($html) {
			$hash=md5($html);
			if(!array_key_exists($hash, $this->hashes)) 
				$this->hashes[$hash]=$html;
			return $hash;
		}

		function unhash($html) {
			foreach(array_keys($this->hashes) as $hash) {
				while(preg_match('~(.*)('.$hash.')(.*)~ms', $html, $m)) {
					$html=$m[1].$this->hashes[$hash].$m[3];
				}
			}
			return $html;
		}


		function specialchars(&$line) {
			while(preg_match('~^(.*)&(?!amp;)(.*)$~', $line, $m)) {
				$line = $m[1] . '&amp;' . $m[2];
			}
			while(preg_match('~^(.*)<(.*)$~', $line, $m)) {
				$line = $m[1] . '&lt;' . $m[2];
			}
			while(preg_match('~^(.*)>(.*)$~', $line, $m)) {
				$line = $m[1] . '&gt;' . $m[2];
			}
			return $line;
		}

		function doinline(&$line) {

			$line = $this->specialchars($line);

			//code
			while(preg_match('~^(.*)`(.*)`(.*)$~', $line, $m)) {
				$line = $m[1] . '<code>' . $this->hash($m[2]) . '</code>' . $m[3];
			}
			//strong emphasis
			while(preg_match('~^(.*)([_\*]{2})(.+)(\2)(.*)$~', $line, $m)) {
				$line = $m[1] . "<strong>" . $m[3] . "</strong>" . $m[5];
			}
			//emphasis
			while(preg_match('~^(.*)([_\*])(.+)(\2)(.*)$~', $line, $m)) {
				$line = $m[1] . "<em>" . $m[3] . "</em>" . $m[5];
			}
			//strikethru
			while(preg_match('@^(.*)~~(.*)~~(.*)$@', $line, $m)) {
				$line = $m[1] . "<strike>" . $m[2] . "</strike>" . $m[3];
			}
			//ref link
			while(preg_match('~^(.*)\[(.*)\]\s?\[(.*)\](.*)$~', $line, $m)) {
				$key=md5(strtolower($m[3]));
				if(key_exists($key, $this->urls)) {
					if(strlen($this->ttls[$key])>0) {
						$line = $m[1] . '<a href="' . $this->hash($this->urls[$key]) . '" title="' .
							$this->ttls[$key] . '">' . $m[2] . "</a>" . $m[4];
					} else {
						$line = $m[1] . '<a href="' . $this->hash($this->urls[$key]) . '">' . 
							$m[2] . "</a>" . $m[4];
					}
				} else { //missing ref
					$line = $m[1] . '<a href="">' . $m[2] . "</a>" . $m[4];
				}
			}
			//inline link w/ title
			//		     1      2          3     	    4      5 
			while(preg_match('~^(.*)\[(.*)\]\s?\((.*)\s{1,3}\"(.*)\"\)(.*)$~', $line, $m)) {
				$line = $m[1] . '<a href="' . $this->hash(trim($m[3])) . '" title="' . $m[4] . 
						'">' . $m[2] . '</a>' . $m[5];
			}
			//inline link, no title
			//		     1	   2	      3      4 
			while(preg_match('~^(.*)\[(.*)\]\s?\((.*)\)(.*)$~', $line, $m)) {
				$line = $m[1] . '<a href="' . $this->hash(trim($m[3])) . '">' . $m[2] . '</a>' . $m[4];
			}
			return $line;
		}

		function striprefs($line) {
			if(preg_match('~^\[(.*)\]:\s(.*)\s(\"(.*)\")$~', $line, $m)) {
				$id=md5(strtolower($m[1]));
				$this->urls[$id]=trim($this->specialchars($m[2]));
				$this->ttls[$id]=$this->specialchars($m[4]);
				return true;
			} else if(preg_match('~^\[(.*)\]:\s(.*)$~', $line, $m)) {
				$id=md5(strtolower($m[1]));
				$this->urls[$id]=trim($this->specialchars($m[2]));
				$this->ttls[$id]='';
				return true;
			} else return false;
		}

		function htmlregex($depth=1) {
			//$attr = '(\w+\s*(=\s*"[^"]+")?\s*)';
			// # and @ are placeholders to be replaced/removed later
			//         1    2       3                 4      5
			$tag = '<(\w+)(\s*\w+\s*(=\s*"[^"]+")?\s*)*>#</(\@)>';
			$regex = $tag;
			for($i=0;$i<$depth;$i++) {
				//sub out @ for matching closing tag
				$regex = substr_replace($regex, strval($i*4+1), strpos($regex, '@'), 1);
				//sub out # for inner tag or catch-all if depth limit is reached
				if($i+1 < $depth) {
					$regex = substr_replace($regex, "($tag)*", strpos($regex, '#'), 1);
				} else {
					$regex = substr_replace($regex, ".*", strpos($regex, '#'), 1);
				}
				echo "iteration $i done, regex=$regex".PHP_EOL;
			}
			return $regex;
			
		}
		
		function hashhtmlblocks($text) {
			
			
		}

		function popblock(&$line) {
			if(preg_match('~^\s*([\*\-_]\s?){3,}\s*$~', $line, $m)) {
				//'h': horizontal rule
				$line='';
				return 'h';
			} else if(preg_match('~^(#{1,6})\s(.*)$~', $line, $m)) {
				//'1-6': heading 1-6
				$line=$m[2];
				return strval(strlen($m[1]));
			} else if(preg_match('~^\d+\.\s{1,3}(.*)$~', $line, $m)) {
				//'o': ol item
				$line=$m[1];
				return 'o';
			} else if(preg_match('~^[\*\-\+]\s{1,3}(.*)$~', $line, $m)) {
				//'u': ul item
				$line=$m[1];
				return 'u';
			} else if(preg_match('~^(    )(.*)$~', $line, $m)) {
				//'_': indent (code or para of item)
				$line=$m[2];
				return '_';
			} else if(preg_match('~^\s*$~', $line, $m)){
				//' ': empty line
				return ' ';
			} else return 'p';	//p: generic text
		}

		function scan($line) {
			$text=$line;
			$n=$this->popblock($text);

			return ['blks'=>$n,'text'=>$text];
		}

		function blockstr($lines) {
			$str='';

			for($i=0;$i<count($lines);$i++) {
				$str.=$lines[$i]['blks'];
			}
			return $str;
		}

		function openblock(&$blocks) {
			return array_push($blocks, ['html'=>'', 'open'=>true, 'type'=>NULL]);
		}

		function closeblock(&$blocks) {
			$j=count($blocks)-1;
			if($blocks[$j]['type'] == 'pre') 
				$blocks[$j]['html'].='</code>';
			if(preg_match('~^[uo]l$~', $blocks[$j]['type']))
				$blocks[$j]['html'].='</li>';
			if($blocks[$j]['type'] != 'hr') 
				$blocks[$j]['html'].='</'.$blocks[$j]['type'].'>';
			$blocks[$j]['html'].=PHP_EOL;
			$blocks[$j]['open']=false;
		}

		function hasblock($blocks) {
			if(count($blocks)==0) return false;
			return $blocks[count($blocks)-1]['open'];
		}

		function doblocks($lines) {
			$blocks=[];
			$html='';
			$str=$this->blockstr($lines, 0);

			//echo "doblocks: $str".PHP_EOL;

			$j=0;
			for($i=0;$i<strlen($str);$i++) {
				$blk_0=$str[$i];
				$blk_1=($i==0?' ':$str[$i-1]);
//				$blk_2=($i<2?' ':$str[$i-2]);
				if($blk_0=='p') {
					if(preg_match('~^[ h\d]$~', $blk_1)) {		//6
						if($this->hasblock($blocks)) 
							$this->closeblock($blocks);
						$j=$this->openblock($blocks);
						$blocks[$j-1]['type']='p';
						$blocks[$j-1]['html'].='<p>'.$this->doinline($lines[$i]['text']).PHP_EOL;
					} else if(preg_match('~^[puo]$~', $blk_1)) { //1
						$j=count($blocks);
						$blocks[$j-1]['html'].=$this->doinline($lines[$i]['text']).PHP_EOL;
					}
				} else if($blk_0==' ') {
					if(preg_match('~^[puo_]$~', $blk_1)) {
						if($this->hasblock($blocks)) {
							$j=count($blocks);
							if(!preg_match('~^([uo]l|pre)$~', $blocks[$j-1]['type']))	//12
								$this->closeblock($blocks);
						}

					}
				} else if($blk_0=='h') {			//2
					if($this->hasblock($blocks)) 
						$this->closeblock($blocks);
					$j=$this->openblock($blocks);
					$blocks[$j-1]['type']='hr';
					$blocks[$j-1]['html'].='<hr />'.PHP_EOL;
					$this->closeblock($blocks);
				} else if(is_numeric($blk_0)) {			//3
					if($this->hasblock($blocks)) 
						$this->closeblock($blocks);
					$j=$this->openblock($blocks);
					$blocks[$j-1]['type'].="h$blk_0";
					$blocks[$j-1]['html'].="<h$blk_0>".$this->doinline($lines[$i]['text']);
					$this->closeblock($blocks);
				} else if($blk_0=='_') {
					if(preg_match('~^[ h\d]$~', $blk_1)) {	//4
						if($this->hasblock($blocks)) {
							$j=count($blocks);
							if($blocks[$j-1]['type']!='pre')
								$this->closeblock($blocks);
						}
						if(!$this->hasblock($blocks)) {
							$j=$this->openblock($blocks);
							$blocks[$j-1]['type']='pre';
							$blocks[$j-1]['html'].='<pre><code>'.PHP_EOL;
						}
						$blocks[$j-1]['html'].=$this->specialchars($lines[$i]['text']).PHP_EOL;
					} else if($blk_1=='p') {	//1
						$j=count($blocks);
						$blocks[$j-1]['html'].=$this->doinline($lines[$i]['text']).PHP_EOL;
					} else {		//5
						$j=count($blocks);
						if($blocks[$j-1]['type']=='pre') 
							$blocks[$j-1]['html'].=$this->specialchars($lines[$i]['text']).PHP_EOL;
						else 
							$blocks[$j-1]['html'].=$this->doinline($lines[$i]['text']).PHP_EOL;
					}
				} else if($blk_0=='u') {
					if(preg_match('~^[po _]$~', $blk_1)) { //7
						if($this->hasblock($blocks)) {
							$j=count($blocks);
							if($blocks[$j-1]['type']!='ul')
								$this->closeblock($blocks);
						}
						if($this->hasblock($blocks)) {
							$j=count($blocks);
							$blocks[$j-1]['html'].='</li>'.PHP_EOL.'<li>'.$this->doinline($lines[$i]['text']);
						} else {
							$j=$this->openblock($blocks);
							$blocks[$j-1]['type']='ul';
							$blocks[$j-1]['html'].='<ul><li>'.$this->doinline($lines[$i]['text']);
						}
					} else if($blk_1=='u') {	//11
						$j=count($blocks);
						$blocks[$j-1]['html'].='</li>'.PHP_EOL.'<li>'.$this->doinline($lines[$i]['text']);
					} else {	//8
						$j=$this->openblock($blocks);
						$blocks[$j-1]['type']='ul';
						$blocks[$j-1]['html'].='<ul><li>'.$this->doinline($lines[$i]['text']);
					}
				} else if($blk_0=='o') {
					if(preg_match('~^[pu _]$~', $blk_1)) { //9
						if($this->hasblock($blocks)) {
							$j=count($blocks);
							if($blocks[$j-1]['type']!='ol')
								$this->closeblock($blocks);
						}
						if($this->hasblock($blocks)) {
							$j=count($blocks);
							$blocks[$j-1]['html'].='</li>'.PHP_EOL.'<li>'.$this->doinline($lines[$i]['text']);
						} else {
							$j=$this->openblock($blocks);
							$blocks[$j-1]['type']='ol';
							$blocks[$j-1]['html'].='<ol><li>'.$this->doinline($lines[$i]['text']);
						}
					} else if($blk_1=='o') {	//11
						$j=count($blocks);
						$blocks[$j-1]['html'].='</li>'.PHP_EOL.'<li>'.$this->doinline($lines[$i]['text']);
					} else {	//10
						$j=$this->openblock($blocks);
						$blocks[$j-1]['type']='ol';
						$blocks[$j-1]['html'].='<ol><li>'.$this->doinline($lines[$i]['text']);
					}
				}
			}

			if($this->hasblock($blocks))
				$this->closeblock($blocks);

			foreach($blocks as $block) {
				$html.=$block['html'];
			}

			return $html;

		}

		function get_html_singleline($text) {
			return $this->doinline($text);
		}

		function get_html($text, $debug=false) {
			$lines=[];
			$this->urls=[];
			$this->ttls=[];
			$this->hashes=[];
			$i=0;

			$text=preg_replace('~\t~', '    ', $text).PHP_EOL.PHP_EOL;

			foreach(preg_split('~(\r\n|\r|\n)~', $text) as $line) {
				if(!$this->striprefs($line))
				$lines[$i++] = $line;
			}

			for($i=0;$i<count($lines);$i++) {
				$lines[$i] = $this->scan($lines[$i]);
			}
			if($debug) {
				echo 'DEBUG'.PHP_EOL.'====='.PHP_EOL.PHP_EOL;
				for($i=0;$i<count($lines);$i++) {
					echo $i . ': `' . $lines[$i]['blks'] . '` "' .
						$lines[$i]['text'] . '"' . PHP_EOL;
				}
			}
			$html = $this->doblocks($lines);
			$html = $this->unhash($html);
			return $html;
		}

		static function test($text) {
			$md=new mdlite();
			$html = $md->get_html($text, true);
			echo 'OUTPUT'.PHP_EOL.'======'.PHP_EOL.PHP_EOL;
			echo $html;
		}


	}





?>
