<?php namespace cmsame;
	class getcms {
		public static function readonly($val) {
			if(!is_a($val, 'cmsame\_var')) return '';
			if(is_a($val, 'cmsame\_bool')) return ($val->val ? 'yes' : 'no');
			if(is_a($val, 'cmsame\_text')) {
				$text = $val->val;
				if(isset($val->text) && !is_null($val->text)) $text = $val->text;
				if(strlen($text) > 100) {
					$text = substr($text, 0, 40) . " ... " . substr($text, -40);
				}
				return $text;
			}
			if(is_a($val, 'cmsame\_enum')) {
				if($val->isnull()) return '(no value)';
				return $val->fwd[$val->val];
			}
			if(is_a($val, 'cmsame\_int') || is_a($val, 'cmsame\_real')) {
				if($val->isnull()) return '(no value)';
				return strval($val->val);
			}
			if(is_a($val, 'cmsame\_password')) {
				if($val->isnull()) return '(no password)';
				return '(encrypted)';
			}
			if(is_a($val, 'cmsame\_datetime')) {
				if($val->isnull()) return '(no date/time)';
				return $val->val->format('Y-m-d H:i:s');
			}

//these are just for completeness, column.cpp generates more specific code
			if(is_a($val, 'cmsame\_pointer')) {
				if($val->isnull()) return '(no record)';
				return $val->val->getName();
			}
			if(is_a($val, 'cmsame\_list')) {
				if($val->isnull()||$val->count==0) return '(no records)';
				if($val->count==1) return '(1 record)';
				return '(' . strval($val->count) . ' records)';
			}
			if(is_a($val, 'cmsame\_file')) {
				if($val->isnull()) return '(no image)';
				$path = $val->getPath();
				if(!$path) return '(no image)';
				return $val->name . '.' . $val->ext . ' (' . $val->bytes . ')';
			}


		}

		public static function inputvalue($val) {
			if(!is_a($val, 'cmsame\_var') || $val->isnull()) return "";
			if(is_a($val, 'cmsame\_enum')) {
				return strval($val->val);
			}
			if(is_a($val, 'cmsame\_bool')) return $val->val ? "checked" : "";
			if(is_a($val, 'cmsame\_int')) return strval($val->val);
			if(is_a($val, 'cmsame\_real')) return strval($val->val);
			if(is_a($val, 'cmsame\_password')) return "";
			if(is_a($val, 'cmsame\_datetime')) return $val->val->format('Y-m-d H:i:s');
			if(is_a($val, 'cmsame\_text')) return $val->val;
			if(is_a($val, 'cmsame\_file')) return $val->val;
			if(is_a($val, 'cmsame\_image')) return $val->val;
		}



		public static function linkfile($val, $prefix="") {
			if(!is_a($val, 'cmsame\_file')) return "";
			if(is_a($val, 'cmsame\_image')) {	//_image

//				error_log('hyperlinking image: '. print_r($val, true));
				if($val->isnull()) return "<span>(no image)</span>";
				$path = $val->getPath();
				if(!$path) return "<span>(no image)</span>";
				$url = $val->getUrl($prefix);
				$link = "<span><a target=\"_blank\" class=\"imgpopup\" href=\"$url\">" . $val->name . '.' . $val->ext . 
					' (' . $val->bytes . ' bytes, ' . $val->width . ' X ' . $val->height . ')</a></span>';
				$list="";

				foreach(array_keys($val->sizes) as $size) {
					if(file_exists(_var::$home . $val->getPath($size))) {
						$url = $val->getUrl($prefix, $size);
						$list .= "<br/><span><a target=\"_blank\" class=\"imgpopup\" href=\"$url\">$size (" . $val->s_bytes[$size] . 
							" bytes, " . $val->s_width[$size] . " X " . $val->s_height[$size] . ")</a></span>";
					}
				}
				return $link . $list;
			} else {				//_file
				if($val->isnull()) return "<span>(no file)</span>";
				$path = $val->getPath();
				if(!$path) return "<span>(no file)</span>";
				$url = $val->getUrl($prefix);
				$link = "<span><a target=\"_blank\" href=\"$url\">" . $val->name . '.' . $val->ext . 
					' (' . $val->bytes . ' bytes)</a></span>';
				return $link;
			}
		}

		public static function editfile($val, $prefix="") {
			if(!is_a($val, 'cmsame\_file')) return "";
			if(is_a($val, 'cmsame\_image')) {	//_image

				//error_log('hyperlinking image: '. print_r($val, true));
				if($val->isnull()) return "<span>(no image)</span>";
				$path = $val->getPath();
				if(!$path) return "<span>(no image)</span>";
				$url = $val->getUrl($prefix);
				$link = "<span><a target=\"_blank\" class=\"imgpopup\" href=\"$url\">" . $val->name . '.' . $val->ext . 
					' (' . $val->bytes . ' bytes, ' . $val->width . ' X ' . $val->height . ')</a></span>';
				$link.="<a class=\"button btnremove image\">Remove</a>";
				$list="";

				foreach(array_keys($val->sizes) as $size) {
					if($val->hasDyn[$size]) {
						if(file_exists(_var::$home . $val->getPath($size))) {
							$url = $val->getUrl($prefix, $size);
							$list .= "<br/><span><a target=\"_blank\" class=\"imgpopup\" href=\"$url\">$size (" . $val->s_bytes[$size] . 
								" bytes, " . $val->s_width[$size] . " X " . $val->s_height[$size] . ")</a></span>";
						}
						$editName = $val->table . $val->field;
						$link .= '<input type="hidden" id="' . $editName . $size . 'L" name="' . $editName . $size .'L" />';
						$link .= '<input type="hidden" id="' . $editName . $size . 'R" name="' . $editName . $size .'R" />';
						$link .= '<input type="hidden" id="' . $editName . $size . 'T" name="' . $editName . $size .'T" />';
						$link .= '<input type="hidden" id="' . $editName . $size . 'B" name="' . $editName . $size .'B" />';
						$link .= "&nbsp;<a href=\"Resize" . $val->field . $size . ".php?" . $val->table . 
							"Id=" . $val->id . "\" id=\"" . $editName . $size . "Resize\" class=\"button btnresize\">$size</a>";

					} else {
						if(file_exists(_var::$home . $val->getPath($size))) {
							$url = $val->getUrl($prefix, $size);
							$list .= "<br/><span><a target=\"_blank\" class=\"imgpopup\" href=\"$url\">$size (" . $val->s_bytes[$size] . 
								" bytes, " . $val->s_width[$size] . " X " . $val->s_height[$size] . ")</a></span>";
						}
					}
				}
				return $link . $list;
			} else {				//_file
				if($val->isnull()) return "<span>(no file)</span>";
				$path = $val->getPath();
				if(!$path) return "<span>(no file)</span>";
				$url = $val->getUrl($prefix);
				$link = "<span><a target=\"_blank\" href=\"$url\">" . $val->name . '.' . $val->ext . 
					' (' . $val->bytes . ' bytes)</a></span>';
				$link .= '<a class="button btnremove file">Remove</a>';
				return $link;
			}
		}
	}
?>
