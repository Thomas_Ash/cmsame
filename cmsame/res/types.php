<?php namespace cmsame;
	require_once('markdown.php');
	require_once('mdlite.php');
	class getsql {
		private static 	function sqlstr($str, $qd) {
			if(is_null($str)) return "NULL";
			if($qd==1) {
				return "'" . str_replace("'", "''", str_replace('\\', '\\\\', $str)) . "'";
			} else if ($qd==2) {
				return "'" . str_replace("'", "''''", str_replace('\\', '\\\\\\\\', $str)) . "'";
			} else return "NULL";
		}

		public static function sql($val, $qd=1, $field="") {
			if(!is_a($val, 'cmsame\_var') || $val->isnull()) return "NULL";
			if(is_a($val, 'cmsame\_pointer')) return strval($val->getId());
			if(is_a($val, 'cmsame\_bool')) return $val->val ? "1" : "0";
			if(is_a($val, 'cmsame\_int')) return strval($val->val);
			if(is_a($val, 'cmsame\_real')) return strval($val->val);
			if(is_a($val, 'cmsame\_password')) {
				if($field=="") return self::sqlstr($val->val, $qd);
				if($field=="Salt") return self::sqlstr($val->salt, $qd);
				return "NULL";
			}
			if(is_a($val, 'cmsame\_datetime')) return "'" . $val->val->format('Y-m-d H:i:s') . "'";
			if(is_a($val, 'cmsame\_text')) {
				if($field=="") return self::sqlstr($val->val, $qd);
				if($field=="MarkdownHtml") {
					$md = new markdown();
					return self::sqlstr($md->get_html($val->val), $qd);
				}
				if($field=="MarkdownText") {
					$md = new markdown();
					return self::sqlstr(strip_tags($md->get_html($val->val)), $qd);
				}
				if($field=="MarkdownSLHtml") {
					$md = new markdown();
					return self::sqlstr($md->get_html_singleline($val->val), $qd);
				}
				if($field=="MarkdownSLText") {
					$md = new markdown();
					return self::sqlstr(strip_tags($md->get_html_singleline($val->val)), $qd);
				}
				if($field=="MdHtml") {
					$md = new mdlite();
					return self::sqlstr($md->get_html($val->val), $qd);
				}
				if($field=="MdText") {
					$md = new mdlite();
					return self::sqlstr(strip_tags($md->get_html($val->val)), $qd);
				}
				if($field=="MdSLHtml") {
					$md = new mdlite();
					return self::sqlstr($md->get_html_singleline($val->val), $qd);
				}
				if($field=="MdSLText") {
					$md = new mdlite();
					return self::sqlstr(strip_tags($md->get_html_singleline($val->val)), $qd);
				}
			}
			if(is_a($val, 'cmsame\_image')) {
				if($field=="") return self::sqlstr($val->val, $qd);
				if($field=="Bytes") return self::sqlstr($val->bytes, $qd);
				if($field=="Width") return self::sqlstr($val->width, $qd);
				if($field=="Height") return self::sqlstr($val->height, $qd);
				if(preg_match('~^([A-Za-z0-9_]+)(Bytes|Width|Height)$~', $field, $match)) {
//					error_log('REGEX PASS' . print_r($match, true));
					if($match[2]=='Bytes'&&isset($val->s_bytes[$match[1]]))
						return self::sqlstr($val->s_bytes[$match[1]], $qd);
					if($match[2]=='Width'&&isset($val->s_width[$match[1]]))
						return self::sqlstr($val->s_width[$match[1]], $qd);
					if($match[2]=='Height'&&isset($val->s_height[$match[1]]))
						return self::sqlstr($val->s_height[$match[1]], $qd);
				}
				return "NULL";
			}
			if(is_a($val, 'cmsame\_file')) {
				if($field=="") return self::sqlstr($val->val, $qd);
				if($field=="Bytes") return self::sqlstr($val->bytes, $qd);
				return "NULL";
			}
		}
	}

	abstract class _object {
		public abstract function getId();
		public abstract function setId($id);
		public abstract function load();
		public function getName() {
			return "";
		}
	}

	abstract class _search {
		public abstract function mirror($id, $key);
		public abstract function get();
	}

	abstract class _var {
		static $home = NULL;
		public static function init() {
			_var::$home = realpath(__DIR__ . '/../../www');
		}
		public function __construct($val = NULL, $cfg=[]) {
			$this->set($val);
		}
		public function isnull() {
			return is_null($this->val);
		}
		public abstract function set($val);
	} _var::init();
	class _bool extends _var {
		public function set($val) {
			$this->errors = NULL;
			if($val==1||$val==TRUE||strtolower(strval($val))=='true') $this->val = TRUE;
			else if($val==0||$val==FALSE||strtolower(strval($val))=='false') $this->val = FALSE;
			else if(is_null($val) || $val=="") $this->val = NULL;
			else if(is_bool($val)) $this->val = $val;
			else $this->val = boolval($val);
		}
	}
	class _int extends _var {
		public function set($val) {
			$this->errors = NULL;
			if(is_null($val) || $val=="") $this->val = NULL;
			else if(is_int($val)) $this->val = $val;
			else if(is_numeric($val)) $this->val = intval($val);
			else {
				$this->errors = ['@ is not an integer.'];
				$this->val = NULL;
			}
		}
	}
	class _enum extends _int {
		public function __construct($val = NULL, $cfg=[]) {
			$this->fwd=[]; $this->rev=[];
			if(isset($cfg['values'])) {
				$values=$cfg['values'];
				for($i=1; $i<=count($values); $i++) { //zero used to post null
					$this->fwd[$i] = $values[$i-1];
					$this->rev[$values[$i-1]] = $i;
				}
			}
			$this->set($val);
		}
		public function set($val) {
			$this->errors = NULL;
			if(is_null($val) || $val=="") $this->val = NULL;
			else if(is_int($val) || is_numeric($val)) {
				$val=intval($val);
				if(array_key_exists($val, $this->fwd)) $this->val=$val;
				else if($val==0)  $this->val = NULL;
				else {
					$this->errors = ['@ is out of range.'];
					$this->val = NULL;
				}
			} else if(is_string($val)) {
				if(array_key_exists($val, $this->rev)) $this->val=$this->rev[$val];
				else {
					$this->errors = ['@ is not one of the preset values.'];
					$this->val = NULL;
				}
			} else {
				$this->errors = ['@ is not a number or one of the preset values.'];
				$this->val = NULL;
			}
		}
	}

	class _real extends _var {
		public function set($val) {
			$this->errors = NULL;
			if(is_null($val) || $val=="") $this->val = NULL;
			else if(is_float($val)) $this->val = $val;
			else if(is_numeric($val)) $this->val = floatval($val);
			else {
				$this->errors = ['@ is not a number.'];
				$this->val = NULL;
			}
		}
	}
	class _text extends _var {
		//accepts raw string or array [raw, html, text]
		public function set($val) {
			$this->errors = NULL;
			$this->html = NULL;
			$this->text = NULL;
			if(is_null($val)||(is_string($val)&&strlen($val)==0)) $this->val = NULL;
			else if(is_string($val)) $this->val = $val;
			else if(is_array($val) && count($val)==3) {
				$this->val = strval($val[0]);
				$this->html = strval($val[1]);
				$this->text = strval($val[2]);
			} else if(is_array($val) || is_object($val)) {
				$this->errors = ['@ is not a text string.'];
				$this->val = NULL;
			} else $this->val = strval($val);
		}
	}

	//opening re. reference types
	//calling set will detect open or closed values and set flag
	//also, closed state knows how to open itself, although not as fast as doing a join

	class _pointer extends _var {
		public function __construct($val=NULL, $cfg=[]) {
			$this->type=NULL;
			$this->val=NULL;
			$this->isOpen=false;
			if(isset($cfg['type'])) {
				if(is_a($cfg['type'], 'cmsame\_object', true)) {
					$this->type = is_string($cfg['type']) ?
						$cfg['type'] : get_class($cfg['type']);
				}
			}
			$this->set($val);
		}
		public function set($val) {
			$this->errors = NULL;
			if(!$this->type) return;
			if (is_numeric($val)) {
				$this->val=(intval($val)==0?NULL:intval($val));
				$this->isOpen=false;
			} else if(is_a($val, $this->type, true)) {
				$this->val=$val;
				$this->isOpen=true;
			} else if (is_null($val)) {
				$this->val=NULL;
			} else {
				$this->errors = ['@ is not an id or object.'];
				$this->val = NULL;
			}
		}

		public function open() {
			if(is_numeric($this->val)) {
				$id=$this->val;
				$this->val = new $this->type();
				$this->val->setId($id);
				$this->val->load();
				$this->isOpen=true;
			}
		}

		public function getId() {
			if(is_null($this->val)) return NULL;
			if($this->isOpen) return $this->val->getId();
			return $this->val;
		}
	}
	class _list extends _var {
		public function __construct($val=NULL, $cfg=[]) {
			$this->type=NULL;
			$this->searchtype=NULL;
			$this->key=NULL;
			$this->val=NULL;
			$this->count=NULL;
			$this->isOpen=false;
			if(isset($cfg['type'])) {
				if(is_a($cfg['type'], 'cmsame\_object', true)) {
					$this->type = is_string($cfg['type']) ?
						$cfg['type'] : get_class($cfg['type']);
				}
			}
			if(isset($cfg['searchtype'])) {
				if(is_a($cfg['searchtype'], 'cmsame\_search', true)) {
					$this->searchtype = is_string($cfg['searchtype']) ?
						$cfg['searchtype'] : get_class($cfg['searchtype']);
				}
			}
			if(isset($cfg['key'])) {
				$this->key = strval($cfg['key']);
			}
			$this->set($val);
		}

		public function set($val) {
			$this->errors = NULL;
			if(!$this->type||!$this->searchtype||!$this->key) return;
			if(is_null($val)) {
				$val=NULL;
			} else if(is_array($val)) {
				if(count($val)==2&&is_numeric($val[0])&&is_numeric($val[1])) {
//					id, count
					$this->val = intval($val[0]);
					$this->count = intval($val[1]);
					$this->isOpen=false;
				} else {
					$this->val=[];
					foreach($val as $item) {
						if(is_a($item, $this->type)) {
							$this->val[$item->getId()] = $item;
						}
					}
					$this->count=count($this->val);
					$this->isOpen=true;
				}
			}
		}

		public function open($id) {
			$pt=new $this->searchtype();
			$pt->mirror(intval($id), $this->key);
			$this->val=[];
			$res=$pt->get();
			foreach($res as $item) {
				$this->val[$item->getId()] = $item;
			}
			$this->count=count($this->val);

			$this->isOpen=true;
		}
	}


	class _password extends _var {
		public function set($val) {
			$this->errors = NULL;
			if(is_null($val) || (is_string($val) && $val=="")) {
				$this->val = NULL;
				$this->salt = NULL;
			} else if(is_string($val)) {
				if(strlen($val) < 8) {
					$this->errors = ['@ must be at least 8 characters.'];
					$this->val = NULL;
					$this->salt = NULL;
				} else {
					$hasUC = preg_match('~[A-Z]~', $val);
					$hasLC = preg_match('~[a-z]~', $val);
					$hasDT = preg_match('~[0-9]~', $val);
					if(($hasUC&&$hasLC)||($hasUC&&$hasDT)||($hasDT&&$hasLC)) {
						$this->salt = crypto::salt();
						$this->val = crypto::blake2s128($this->salt . $val);
					} else {
						$this->errors = ['@ must contain at least two out of uppercase, lowercase and numbers.'];
						$this->val = NULL;
						$this->salt = NULL;
					}
				}
			} else if(!is_array($val) || !is_string($val[0]) || !is_string($val[1])) {
				$this->errors = ['@ is not an encrypted or plaintext password.'];
				$this->val = NULL;
				$this->salt = NULL;
			} else {
				$this->val = strval($val[0]);
				$this->salt = strval($val[1]);

				if($this->val!='=============HIDDEN============='&&!preg_match('~^[0-9a-f]{32}$~', $this->val)) {
					$this->errors = ['@ is not a valid hash.'];
					$this->val = NULL;
					$this->salt = NULL;
				}
			}
		}
	}
	class _datetime extends _var {
		public function set($val) {
			$this->errors = NULL;
			if(is_null($val) || $val=="") $this->val = NULL;
			else if(is_object($val) && get_class($val) == "DateTime") $this->val = $val;
			else if(is_string($val)) {
				$this->val = \DateTime::createFromFormat('Y-m-d H:i:s', $val);
				if(!$this->val) {
					$this->errors = ['@ is not a valid date/time.'];
					$this->val = NULL;
				}
			} else {
				$this->errors = ['@ is not a valid date/time.'];
				$this->val = NULL;
			}
		}
	}

	class _file extends _var {
		//from var
		public function __construct($val = NULL, $cfg=[]) {
			if(isset($cfg['mime'])) {
				if(is_array($cfg['mime'])) $this->mime = $cfg['mime'];
				else if (is_string($cfg['mime'])) $this->mime = [$cfg['mime']];
			}
			if(isset($cfg['maxbytes']) && is_numeric($cfg['maxbytes'])) {
				$this->maxbytes=intval($cfg['maxbytes']);
			}
			$this->set($val);
		}

		public function set($val) {
			$this->temp=NULL;
			if(is_null($val) || $val=="") {
				$this->val = NULL;
				$this->id = NULL;
				$this->errors = NULL;
				$this->bytes = NULL;
			} else if(is_string($val)) {
				//if $val is a string, fromPath
				$this->fromPath($val);
			} else if(is_array($val) && count($val) == 2) {
				//[string, array] => fromRow
				if(is_array($val[1])) $this->fromRow($val[0], $val[1]);
				//[string, string] => fromPost
				else if (is_string($val[1])) $this->fromPost($val[0], $val[1]);
			}

			if($this->errors) $this->val = NULL;
				else $this->val = $this->getPath();
		}

		//helpers
		public function getPath() {
			if(!$this->id) return NULL;
			return '/upload/' . $this->table . '/' .
			$this->id . '/' . $this->field . '/' .
			$this->name . '.' . $this->ext;
		}

		public function getUrl($prefix) {
			return $prefix . $this->getPath();
		}

		public function needsSync() {
			return (!is_null($this->temp));
		}

		//initializers
		public function fromPath($path) {
			//error_log("fromPath: " . $path);
			//error_log('file::frompath');
			if(is_null($path) || $path == "") {
				$this->val = NULL;
			} else if(preg_match('~^/upload/([^\\s/]+)/(\\d+)/([^\\s/]+)/([^/]+)\\.([^/]+)$~', $path, $m)) {
				$this->val = $path;
				$this->table = $m[1];
				$this->id = $m[2];
				$this->field = $m[3];
				$this->name = $m[4];
				$this->ext = $m[5];
				if(!file_exists(_var::$home . $this->getPath())) $this->errors = ['Data for @ missing.'];
			} else $this->errors = ['@ is not a valid upload path.'];
			//error_log(print_r($this->errors, true));
			//error_log('after file::frompath:' . print_r($this, true));
		}

		public function fromRow($path, $row) {
			$this->fromPath($path);
//			error_log(print_r($row, true));
			if(!$this->isnull() && is_numeric($row[$this->table . $this->field . "Bytes"]))
				$this->bytes = intval($row[$this->table . $this->field . "Bytes"]);
			else $this->bytes = NULL;
		}

		public function fromPost($table, $field) { //, $mime, $maxbytes) {
			//error_log(print_r($mime,true));
			$pkey = $table . $field;
			$fkey = $pkey . 'Upload';
			$dkey = $pkey . 'Remove';
			//if $_FILES[$table . $field . 'Upload'] is file:
			if(isset($_FILES[$fkey])&&is_uploaded_file($_FILES[$fkey]['tmp_name'])) {
				//check mimetype
				if(is_null($this->mime)) {
					$this->errors=['@ has no file types enabled.'];
					return;
				}
				if(is_array($this->mime)) {
					$f = false;
					foreach($this->mime as $m) {
						if($_FILES[$fkey]['type'] == $m) {
							$f = true;
							break;
						}
					}
					if(!$f) {
						$this->errors=['"' . $_FILES[$fkey]['type'] . '" type files are not enabled on @.'];
						return;
					}
				} else if ($_FILES[$fkey]['type'] != $this->mime) {
						$this->errors=['"' . $_FILES[$fkey]['type'] . '" type files are not enabled on @.'];
						return;
				}

				//and file size
				if(is_null($this->maxbytes) || !is_numeric($this->maxbytes) || ($_FILES[$fkey]['size'] > $this->maxbytes)) {
					$this->errors=[$_FILES[$fkey]['size'] . ' bytes is too large for @.'];
					return;
				}
				$this->bytes = $_FILES[$fkey]['size'];
				//populate name & ext from $_FILES
				if(preg_match('~(.*/)?([^/]+)\\.([^/]+)~', $_FILES[$fkey]['name'], $m)) {
					$this->name = $m[2];
					$this->ext = $m[3];
				} else {
					$this->errors=['Couldn\'t parse file name "' . $_FILES[$fkey]['name'] . '" for @.'];
					return;
				}
				//get data
				$this->table = $table;
				$this->field = $field;
				$path = '/upload';

				//error_log("Upload root: " . _var::$home . $path);
				if(!file_exists(_var::$home . $path)) mkdir(_var::$home . $path);

				$path = $path . '/_inbox';
				if(!file_exists(_var::$home . $path)) mkdir(_var::$home . $path);

				$path = $path . '/' . $this->name . '.' . $this->ext;
				if(move_uploaded_file($_FILES[$fkey]['tmp_name'], _var::$home . $path)) {
					chmod(_var::$home . $path, 0666);
					$this->temp = $path;
				} else {
					$this->errors=['Upload failed for @.'];
					return;
				}

				//ready for sync!
			} else if (isset($_POST[$dkey])&&$_POST[$dkey]=='Remove'){
				$this->val = NULL;
				$this->id = NULL;
				$this->errors = NULL;
			} else if (isset($_POST[$pkey])) {
				//if no file, $_POST[$table . $field] should be a path string..
				//pass into fromPath
				$this->fromPath($_POST[$pkey]);
			}
		}

		//set id before calling sync
		public function sync() {
			error_log('_file::sync');
			//error_log(print_r($this, true));
			if(is_null($this->temp)||is_null($this->id)) return;

			//make sure path exists
			$path = '/upload';
			if(!file_exists(_var::$home . $path)) mkdir(_var::$home . $path);
			$path = $path . '/' . $this->table;
			if(!file_exists(_var::$home . $path)) mkdir(_var::$home . $path);
			$path = $path . '/' . $this->id;
			if(!file_exists(_var::$home . $path)) mkdir(_var::$home . $path);
			$path = $path . '/' . $this->field;
			if(!file_exists(_var::$home . $path)) mkdir(_var::$home . $path);
			$path = $path . '/' . $this->name . '.' . $this->ext;

			if(!rename(_var::$home . $this->temp, _var::$home . $path)) {
				$this->errors=['Sync failed for @.'];
				return;
			}
			//then clear $this->temp so needsSync returns false
			$this->temp = NULL;
			$this->val = $this->getPath();

		}
		public function cleanup($all=false) {
			if(!$this->id) return;
			$path = '/upload/' . $this->table . '/' .
			$this->id . '/' . $this->field . '/';
			$curr= $this->name . '.' . $this->ext;

			foreach(glob(_var::$home . $path . '*.*') as $file) {
				if(!$all && substr($file, strrpos($file, '/')+1)==$curr) continue;
				unlink($file);
			}
		}
	}

//paths like:
// /upload/MyTable/Id/MyImage/hello.jpg
// /upload/MyTable/Id/MyImageLarge/hello.jpg
// /upload/MyTable/Id/MyImageSmall/hello.jpg

	class _image extends _file {
		public function __construct($val = NULL, $cfg=[]) {
			if(isset($cfg['mime'])) {
				if(is_array($cfg['mime'])) $this->mime = $cfg['mime'];
				else if (is_string($cfg['mime'])) $this->mime = [$cfg['mime']];
			}
			if(isset($cfg['maxbytes']) && is_numeric($cfg['maxbytes'])) {
				$this->maxbytes=intval($cfg['maxbytes']);
			}
			if(isset($cfg['sizes']) && is_array($cfg['sizes'])) {
				$this->setSizes($cfg['sizes']);
			}
			$this->set($val);
		}

		public function set($val) {
//			error_log('image set: ' . print_r($val, true));
			$this->temp=NULL;
			if(is_null($val) || $val=="") {
				$this->val = NULL;
				$this->id = NULL;
				$this->errors = NULL;
				$this->bytes = NULL;
			} else if(is_string($val)) {
				//if $val is a string, fromPath
				$this->fromPath($val);
			} else if(is_array($val) && count($val) == 2) {
				//[string, array] => fromRow
				if(is_array($val[1])) $this->fromRow($val[0], $val[1]);
				//[string, string] => fromPost
				else if (is_string($val[1])) $this->fromPost($val[0], $val[1]);
			}

			if($this->errors) $this->val = NULL;
			else $this->val = $this->getPath();
		}

		public function fromRow($path, $row) {
			//error_log('image::fromrow');
			$this->fromPath($path);

			if(!$this->isnull()) {
//				error_log('image::fromrow');
//				error_log(print_r($this, true));
//				error_log(print_r($row, true));
				if(is_numeric($row[$this->table . $this->field . "Bytes"]))
					$this->bytes = intval($row[$this->table . $this->field . "Bytes"]);
				else $this->bytes=NULL;
				if(is_numeric($row[$this->table . $this->field . "Width"]))
					$this->width = intval($row[$this->table . $this->field . "Width"]);
				else $this->width=NULL;
				if(is_numeric($row[$this->table . $this->field . "Height"]))
					$this->height = intval($row[$this->table . $this->field . "Height"]);
				else $this->height=NULL;
				foreach(array_keys($this->sizes) as $size) {
					if(is_numeric($row[$this->table . $this->field . $size . "Bytes"]))
						$this->s_bytes[$size] = intval($row[$this->table . $this->field . $size . "Bytes"]);
					if(is_numeric($row[$this->table . $this->field . $size . "Width"]))
						$this->s_width[$size] = intval($row[$this->table . $this->field . $size . "Width"]);
					if(is_numeric($row[$this->table . $this->field . $size ."Height"]))
						$this->s_height[$size] = intval($row[$this->table . $this->field . $size . "Height"]);

					foreach($this->sizes[$size] as $step) {
						if(preg_match('~^dynamic_[a-z]{0,4}$~', $step[0])) {

							if(is_numeric($row[$this->table . $this->field . $size . "PreBytes"]))
								$this->s_bytes[$size . "Pre"] = intval($row[$this->table .
									$this->field . $size . "PreBytes"]);
							if(is_numeric($row[$this->table . $this->field . $size . "PreWidth"]))
								$this->s_width[$size . "Pre"] = intval($row[$this->table .
									$this->field . $size . "PreWidth"]);
							if(is_numeric($row[$this->table . $this->field . $size ."PreHeight"]))
								$this->s_height[$size . "Pre"] = intval($row[$this->table .
									$this->field . $size . "PreHeight"]);
						}
					}
				}
			}
			else {
				$this->bytes = NULL;
				$this->width = NULL;
				$this->height = NULL;
			}
		}

		public function setSizes($sizes) {
			if(!is_array($sizes)) return;

			$this->sizes=$sizes;

			foreach(array_keys($this->sizes) as $size) {
				$this->hasDyn[$size]=false;
				foreach($this->sizes[$size] as $step) {
					if(!is_array($step)) continue;
					if(preg_match('~^dynamic_[a-z]{0,4}$~', $step[0])) { //
						$this->hasDyn[$size]=true;
						break;
					}
				}
			}
		}

		public function getPath($size = "") {
			if(!$this->id) return NULL;
			return '/upload/' . $this->table . '/' .
			$this->id . '/' . $this->field . $size . '/' .
			$this->name . '.' . $this->ext;
		}

		public function getUrl($prefix, $size = "") {
			return $prefix . $this->getPath($size);
		}

		public function needsSync() {
			//check for resize & return true, otherwise same as file

			foreach(array_keys($this->sizes) as $size) {
				if(!$this->hasDyn[$size]) continue;
				foreach($this->sizes[$size] as $step) {
					if(preg_match('~^dynamic_[a-z]{0,4}$~', $step[0])) {
						$pkey = $this->table . $this->field . $size;
						if(isset($_POST[$pkey . 'L']) && is_numeric($_POST[$pkey . 'L'])&&
							isset($_POST[$pkey . 'R']) && is_numeric($_POST[$pkey . 'R'])&&
							isset($_POST[$pkey . 'T']) && is_numeric($_POST[$pkey . 'T'])&&
							isset($_POST[$pkey . 'B']) && is_numeric($_POST[$pkey . 'B'])) return true;
					}
				}
			}

			return(!is_null($this->temp));
		}

		public function sync() {
			error_log('_image::sync');
			parent::sync();
			if($this->errors) return;
			$path = '/upload/' . $this->table . '/' . $this->id . '/';
			$srcpath = $path . $this->field . '/' . $this->name . '.' . $this->ext;

			$mime = mime_content_type(_var::$home . $srcpath);
			$fmt = NULL;
			if($mime=="image/jpeg") $fmt = IMAGETYPE_JPEG;
			if($mime=="image/png") $fmt = IMAGETYPE_PNG;
			if($mime=="image/gif") $fmt = IMAGETYPE_GIF;
			if(!$fmt) {
				$this->errors=['"' . $mime . '" type images are not supported.'];
				return;
			}
			$srcimg = NULL;
			switch($fmt) {
				case IMAGETYPE_JPEG:
					$srcimg = imagecreatefromjpeg(_var::$home . $srcpath);
					break;
				case IMAGETYPE_PNG:
					$srcimg = imagecreatefrompng(_var::$home . $srcpath);
					break;
				case IMAGETYPE_GIF:
					$srcimg = imagecreatefromgif(_var::$home . $srcpath);
					break;
			}
			if(!$srcimg) {
				$this->errors = ['Source image "' . $srcpath . '" is missing or corrupted.'];
				return;
			}

			//TODO: overwrite $path with $srcimg so everything is run thru GD for safety

			$this->width=imagesx($srcimg);
			$this->height=imagesy($srcimg);

			$this->s_bytes=[];
			$this->s_width=[];
			$this->s_height=[];

			$p_sizes=[];

			foreach(array_keys($this->sizes) as $size) {
				if($this->hasDyn[$size]) {
					$steps=[$this->sizes[$size][0]];
					$steps_pre=[$this->sizes[$size][0]];
					$ready=true;
					foreach($this->sizes[$size] as $step) {
						if(!is_array($step)) continue;
						if(preg_match('~^dynamic_[a-z]{0,4}$~', $step[0])) {
							$p_sizes[$size . 'Pre'] = $steps_pre;
							$pkey = $this->table . $this->field . $size;

							if(isset($_POST[$pkey . 'L']) && is_numeric($_POST[$pkey . 'L'])) $l = intval($_POST[$pkey . 'L']);
							else $ready = false;
							if(isset($_POST[$pkey . 'R']) && is_numeric($_POST[$pkey . 'R'])) $r = intval($_POST[$pkey . 'R']);
							else $ready = false;
							if(isset($_POST[$pkey . 'T']) && is_numeric($_POST[$pkey . 'T'])) $t = intval($_POST[$pkey . 'T']);
							else $ready = false;
							if(isset($_POST[$pkey . 'B']) && is_numeric($_POST[$pkey . 'B'])) $b = intval($_POST[$pkey . 'B']);
							else $ready = false;

							if($ready) {
								array_push($steps, ['croppre', $l, $t]);
								array_push($steps, ['croppost', $r, $b]);
							}
						} else {
							array_push($steps, $step);
							array_push($steps_pre, $step);
						}
					}
					if($ready )$p_sizes[$size] = $steps;
				} else $p_sizes[$size] = $this->sizes[$size];
			}


			foreach(array_keys($p_sizes) as $size) {

				//do resize
				//$this->sizes[$size] is nx3 array of steps

				//process step array, replace dynamic with two crops if dimensions were POSTed
				//otherwise if has dynamic & no dimensions, generate the "pre" image: output of step before dynamic one

				//error_log('size named ' . $size . '=' . print_r($p_sizes[$size], true));

				$destpath = $path . $this->field . $size . '/';
				if(!file_exists(_var::$home . $destpath)) mkdir(_var::$home . $destpath);
				$destpath = $destpath . $this->name . '.' . $this->ext;

				if(is_null($p_sizes[$size][0])) {
					$stepimg = $srcimg;
				} else {
					$startpath = $path . $this->field . $p_sizes[$size][0] . '/' . $this->name . '.' . $this->ext;
					$stepimg = NULL;
					switch($fmt) {
						case IMAGETYPE_JPEG:
							$stepimg = imagecreatefromjpeg(_var::$home . $startpath);
							break;
						case IMAGETYPE_PNG:
							$stepimg = imagecreatefrompng(_var::$home . $startpath);
							break;
						case IMAGETYPE_GIF:
							$stepimg = imagecreatefromgif(_var::$home . $startpath);
							break;
					}

					if(!$stepimg) continue; //might not exist yet, carry on
				}

				foreach($p_sizes[$size] as $step) {
					if(!is_array($step)) continue;

//					error_log(print_r($step, true));
					$sx = imagesx($stepimg);
					$sy = imagesy($stepimg);
					$dx = $step[1];
					$dy = $step[2];
					if(preg_match('~^scale[a-z]{0,4}$~', $step[0])) { // scale or subtype
						//generate $destimg
						if($dx==0&&$dy==0) { //do nothing
							$dx = $sx; $dy = $sy;
						}
						if($dx==0) { //height setup
							if(($step[0]=="scaleup"&&$sy>$dy)||($step[0]=="scaledown"&&$sy<$dy)) $dy = $sy;
							$ar = (float)$sx / $sy;
							$dx = (int)floor($dy * $ar);
						} else if($dy==0) { //width setup
							if(($step[0]=="scaleup"&&$sx>$dx)||($step[0]=="scaledown"&&$sx<$dx)) $dx = $sx;
							$ar = (float)$sx / $sy;
							$dy = (int)floor($dx / $ar);
						} else { //distort aspect - just need to check for subtype rules
							if(($step[0]=="scaleup"&&$sy>$dy)||($step[0]=="scaledown"&&$sy<$dy)) $dy = $sy;
							if(($step[0]=="scaleup"&&$sx>$dx)||($step[0]=="scaledown"&&$sx<$dx)) $dx = $sx;
						}
						$cx = 0; $cy = 0;
						$cw = $sx; $ch = $sy;

					} else if(preg_match('~^crop[a-z]{0,4}$~', $step[0])) { //crop or subtype
						//generate $destimg
						if($dx==0 || $dx >= $sx) {
							$cx = 0; $cw = $sx; $dx = $sx;
						} else {
							if($step[0]=="croppre") $cx = $sx - $dx;
							else if($step[0]=="croppost") $cx = 0;
							else $cx = (float)($sx - $dx) / 2;
							$cw = $dx;
						}
						if($dy==0 || $dy >= $sy) {
							$cy = 0; $ch = $sy; $dy = $sy;
						} else {
							if($step[0]=="croppre") $cy = $sy - $dy;
							else if($step[0]=="croppost") $cy = 0;
							else $cy = (float)($sy - $dy) / 2;
							$ch = $dy;
						}
					}
							//resample
					$destimg = imagecreatetruecolor($dx, $dy);
	//				error_log("imagecopyresampled(destimg, stepimg, 0, 0, $cx, $cy, $dx, $dy, $cw, $ch)");
					imagecopyresampled($destimg, $stepimg, 0, 0, $cx, $cy, $dx, $dy, $cw, $ch);

					if($stepimg!=$srcimg) imagedestroy($stepimg);
					$stepimg = $destimg;
				}

				//save

				$this->s_width[$size]=imagesx($stepimg);
				$this->s_height[$size]=imagesy($stepimg);

				switch($fmt) {
					case IMAGETYPE_JPEG:
						imagejpeg($stepimg, _var::$home . $destpath);
						break;
					case IMAGETYPE_PNG:
						imagepng($stepimg, _var::$home . $destpath);
						break;
					case IMAGETYPE_GIF:
						imagegif($stepimg, _var::$home . $destpath);
						break;
				}

				$this->s_bytes[$size]=filesize(_var::$home . $destpath);

				if($stepimg!=$srcimg) imagedestroy($stepimg);
			}

			imagedestroy($srcimg);
		}

		public function cleanup($all=false) {
			if(!$this->id) return;
			parent::cleanup($all);
			$curr= $this->name . '.' . $this->ext;

			foreach(array_keys($this->sizes) as $size) {
				$path = '/upload/' . $this->table . '/' .
				$this->id . '/' . $this->field . $size . '/';
				foreach(glob(_var::$home . $path . '*.*') as $file) {
					if(!$all && substr($file, strrpos($file, '/')+1)==$curr) continue;
					unlink($file);
				}
			}
		}
	}
?>
