//c++
#include <string>
#include <sstream>

//c
#include <cstring>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "common.h"
#include "element.h"
#include "schema.h"
#include "site.h"
#include "table.h"
#include "target.h"
#include "column.h"
#include "record.h"
#include "permission.h"
#include "crypto.h"

using namespace std;
using namespace rapidxml;

Table::Table(xml_node<>* tableNode, Schema* schema) : Element()
{
	_schema = schema;
	_permission = new Permission(_schema->getPermission());
	_description = "";

	string nameColumnName = "";

	//get attributes
	for (xml_attribute<>* attr = tableNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "singular") == 0) _singular = attr->value();
		else if(strcmp(attr->name(), "plural") == 0) _plural = attr->value();
		else if(strcmp(attr->name(), "description") == 0) _description = attr->value();
		else if(strcmp(attr->name(), "namecolumn") == 0) nameColumnName = attr->value();
	}

	if(_singular.length() < 1 || _plural.length() < 1)
	{
		_isOk = false;
		_statusMessage = "Table load error: unnamed table.";
		return;
	}

	if(_schema->getTableByPlural(_plural) != NULL)
	{
		_isOk = false;
		_statusMessage = "Table load error: duplicate table name.";
		return;
	}

	//first pass thru child nodes: get permissions so columns can inherit
	for (xml_node<>* node = tableNode->first_node(); node; node = node->next_sibling()) {
		if(strcmp(node->name(), "permission") == 0)
		{
			_permission->apply(node);
		}
	}


	Column* column;
	Record* record;

	_idColumn = Column::createIdColumn(this);
	_columns.push_back(_idColumn);

	//second pass: column, wrecord
	for (xml_node<>* node = tableNode->first_node(); node; node = node->next_sibling())
	{
		if(strcmp(node->name(), "column") == 0 || strcmp(node->name(), "refcolumn") == 0)
		{
			column = new Column(this, node);

			if(!column->isOk())
			{
				_statusMessage = column->statusMessage();
				_isOk = false;
				break;
			}
			_columns.push_back(column);


		}
		else if(strcmp(node->name(), "record") == 0)
		{
			record = new Record(node, this);
			if(!record->isOk())
			{
				_statusMessage = record->statusMessage();
				_isOk = false;
				break;
			}
			_records.push_back(record);
		}
		else if(strcmp(node->name(), "section") == 0)
		{
			column = new Section(this, node);

			if(!column->isOk())
			{
				_statusMessage = column->statusMessage();
				_isOk = false;
				break;
			}
			_columns.push_back(column);
		}
	}

	if(nameColumnName!="") {
		_nameColumn = getColumn(nameColumnName);
		if(!_nameColumn) {
			_isOk = false;
			_statusMessage = "Table load error: specified name column not found.";
			return;
		} else if(!_nameColumn->hasType(cmsame::text)) {
			_isOk = false;
			_statusMessage = "Table load error: name column must be text.";
			return;
		}
	} else _nameColumn=NULL;


}

Table::Table(Schema* schema) : Element () { //unused?
	_schema = schema;
	_permission = new Permission(_schema->getPermission());
	_idColumn = Column::createIdColumn(this);
	_columns.push_back(_idColumn);
}

//used to create an implicit link table
Table::Table(string singular, string plural, string description, Schema* schema, Permission* permission) : Element () {
	_singular = singular;
	_plural = plural;
	_description = description;
	_schema = schema;
	_permission = new Permission(permission);
	_idColumn = Column::createIdColumn(this);
	_columns.push_back(_idColumn);
	_nameColumn=NULL;
}

Table::~Table()
{
	if(_schema->getSite()->isVerbose()) cout << "~" << _plural << ", pointer=" << this << "\n";
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) { delete *column; }
	for(vector<Record*>::iterator record = _records.begin(); record != _records.end(); ++record) { delete *record; }
	delete _permission;
}

void Table::linkupRefTarget() {
	if(_schema->getSite()->isVerbose()) cout << "Table[" << _plural << "]::linkupRefTarget" << endl;

	_permission->sortGroups();
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->linkupRefTarget();
		if(!(*column)->isOk())	{
			_statusMessage = (*column)->statusMessage();
			_isOk = false;
			return;
		}
	}
}
void Table::linkupMirror() {
	if(_schema->getSite()->isVerbose()) cout << "Table[" << _plural << "]::linkupMirror" << endl;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->linkupMirror();
		if(!(*column)->isOk())	{
			_statusMessage = (*column)->statusMessage();
			_isOk = false;
			return;
		}
	}
}
vector<Table*>* Table::linkupLinkTable() {
	if(_schema->getSite()->isVerbose()) cout << "Table[" << _plural << "]::linkupLinkTable" << endl;

	vector<Table*>* tres = new vector<Table*>();
	vector<Table*>* res = NULL;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		res = (*column)->linkupLinkTable();
		if(!(*column)->isOk())	{
			_statusMessage = (*column)->statusMessage();
			_isOk = false;
			if(res) delete res;
			delete tres;
			return NULL;
		}

		tres->insert(tres->end(), res->begin(), res->end());
		delete res;
	}
	return tres;
}
void Table::linkupRecords() {
	if(_schema->getSite()->isVerbose()) cout << "Table[" << _plural << "]::linkupRecords" << endl;
	for(vector<Record*>::iterator record = _records.begin(); record != _records.end(); ++record) {
		(*record)->linkup();
		if(!(*record)->isOk())	{
			_statusMessage = (*record)->statusMessage();
			_isOk = false;
			return;
		}
	}
}

Column* Table::getColumn(const string& name) {
	Column* findColumn = NULL;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		findColumn = (*column)->findByName(name);
		if (findColumn) return findColumn;
	}
	return NULL;
}

Column* Table::getIdColumn() {
	return _idColumn;
}

Column* Table::getNameColumn() {
	return _nameColumn;
}

bool Table::isSingular(string& singular) {
	return (singular == _singular);
}

bool Table::isPlural(string& plural) {
	return (plural == _plural);
}

const string& Table::getSingular() {
	return _singular;
}

const string& Table::getPlural() {
	return _plural;
}

bool Table::hasColumnType(cmsame::ColumnType type) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if((*column)->hasType(type)) return true;
	}
	return false;
}

Schema* Table::getSchema() {
	return _schema;
}

Permission* Table::getPermission() {
	return _permission;
}

size_t Table::getRecordId(string& name) {
	if(!_nameColumn) return 0;
	for(vector<Record*>::iterator record = _records.begin(); record!=_records.end(); ++record) {
		if((*record)->getValue(_nameColumn->getName())==name) return (record-_records.begin()) + 1;
	}
	return 0;
}

void Table::addColumn(Column* column) {
	_columns.push_back(column);
}

void Table::addRecord(Record* record) {
	_records.push_back(record);
}

//returns true if table is just singular and plural
//parser interprets this as a link to a table defined elsewhere
//avoids duplicate error for linktables
bool Table::isSkeleton() {
//cout << endl << _plural;
	if(_columns.size()>1) return false;
	if(_records.size()>0) return false;
	if(*_permission	!= *(_schema->getPermission())) return false;
	if(_description!="") return false;
	return true;
}

///////////
/// SQL ///
///////////

void Table::writeCreate(ofstream& sqlFile)
{
	if(_schema->getSite()->isVerbose()) cout << "writecreate " << _plural << "\n";

	sqlFile << "CREATE TABLE " << _plural << " (" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
//		if(column != _columns.begin()) sqlFile << "," << endl_ind;
		(*column)->writeSqlDefinition(sqlFile);
	}

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlIndex(sqlFile);
	}

	sqlFile << dec_ind << endl_ind << ");" << endl_ind << endl_ind;

	for(vector<Record*>::iterator record = _records.begin(); record != _records.end(); ++record)
	{
		(*record)->writeInsert(sqlFile);
		if(!(*record)->isOk()) {

			_statusMessage = (*record)->statusMessage();
			_isOk = false;
			return;
		}
	}

	//cout << "done\n";
}

void Table::writeDrop(ofstream& sqlFile) {
	sqlFile << "DROP TABLE " << _plural << ";" << endl_ind;

}

void Table::writeAddForeignKey(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlAddForeignKey(sqlFile);
	}
}

void Table::writeDropForeignKey(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlDropForeignKey(sqlFile);
	}
}

void Table::writeCreateProc(ofstream& sqlFile, bool debug) {

//	cout << "writecreateproc " << _plural << "\n";

	for(vector<string*>::iterator group = Permission::getAllGroups().begin(); group!=Permission::getAllGroups().end(); ++group) {

		writeProcLocate(sqlFile, **group); 	//locate (select single row by ID)
		writeProcSearch(sqlFile, **group);	//search (select mutiple rows by any/all fields)
		writeProcUpdate(sqlFile, **group);	//update (update single row by ID)
		writeProcInsert(sqlFile, **group);	//insert
		writeProcDelete(sqlFile, **group);	//delete
		writeProcCount(sqlFile, **group);	//count

		if(debug){
			writeProcSearch(sqlFile, **group, true);
			writeProcUpdate(sqlFile, **group, true);
		}
	}
	//cout << "done\n";
}

void Table::writeProcLocate(ofstream& sqlFile, string& group) {

	bool first;

	if(!_permission->hasGroup(group, cmsame::VIEW)) return;
	sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." << group << _singular <<
			"Locate(Locate" << _idColumn->getName() <<  " INT";

	writeSqlRefParam(sqlFile, group);

	sqlFile << ")" << endl_ind <<
		"BEGIN" << inc_ind << endl_ind <<
		"DECLARE SqlSelect TEXT;" << endl_ind <<
		"SET SqlSelect = 'SELECT ";

	first = true;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		first = !(*column)->writeSqlSelect(sqlFile, group, first) && first;
	}

	sqlFile << "';" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlSelectRef(sqlFile, group);
	}

	sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' FROM " << _schema->getSite()->getSiteName() << "." << _plural << "');" << endl_ind;

	//JOIN
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlJoin(sqlFile, group);
	}

	sqlFile  << "SET SqlSelect = CONCAT(SqlSelect, ' WHERE " <<
		_plural << "." << _singular << _idColumn->getName() << " = ', Locate" << _idColumn->getName() <<
		", ' GROUP BY " << _plural << "." << _singular << _idColumn->getName() <<  " LIMIT 0, 1');" << endl_ind <<
		"SET @Statement = SqlSelect;" << endl_ind <<
		"PREPARE Query FROM @Statement;" << endl_ind <<
		"EXECUTE Query;" << dec_ind << endl_ind <<
		"END |" << endl_ind;
}


void Table::writeProcSearch(ofstream& sqlFile, string& group, bool debug) {

	if(!_permission->hasGroup(group, cmsame::VIEW)) return;

	if(debug) {
		sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
			group << _singular << "SearchDebug(";
	} else {
		sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
			group << _singular << "Search(";
	}
		//limits

	sqlFile << "LimitX INT, LimitY INT, ";


	//order
	//validate fieldnames in php
	//0 = ASC, 1 = DESC

	sqlFile << "OrderField1 VARCHAR(32), OrderDir1 BIT, " <<
		"OrderField2 VARCHAR(32), OrderDir2 BIT, " <<
		"OrderField3 VARCHAR(32), OrderDir3 BIT";

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeSqlSearchParam(sqlFile);
	}

	writeSqlRefParam(sqlFile, group);

	sqlFile << ")" << endl_ind <<
		"BEGIN" << inc_ind << endl_ind <<
		"DECLARE SqlSelect TEXT;" << endl_ind <<
		"DECLARE ValueParam VARCHAR(4096);" << endl_ind <<
		"SET SqlSelect = 'SELECT ";

	bool first = true;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		first = !(*column)->writeSqlSelect(sqlFile, group, first) && first;
	}

	sqlFile << "';" << endl_ind;

//		if(hasRef) {
//			sqlFile << "SET SqlSelect = IF(OpenRefs, CONCAT(SqlSelect, '";

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlSelectRef(sqlFile, group);
	}

//			sqlFile << "'), SqlSelect);" << endl_ind;
//		}


	sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' FROM " << _schema->getSite()->getSiteName() <<
				"." << _plural << "');" << endl_ind;

		//JOIN
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlJoin(sqlFile, group);
	}

	sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' WHERE 1 = 1');" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeSqlSearchWhere(sqlFile);
	}

	sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' GROUP BY " << _plural << "." << _singular << _idColumn->getName() << "');" << endl_ind;

	sqlFile << "SET SqlSelect = IF(ISNULL(OrderField1)," << inc_ind << endl_ind <<
		"SqlSelect, IF(ISNULL(OrderField2)," << endl_ind <<
		"CONCAT(SqlSelect, CONCAT(' ORDER BY ', OrderField1, ' ', IF(COALESCE(OrderDir1, 0), 'DESC', 'ASC'))), IF(ISNULL(OrderField3)," << endl_ind <<
		"CONCAT(CONCAT(SqlSelect, CONCAT(' ORDER BY ', OrderField1, ' ', IF(COALESCE(OrderDir1, 0), 'DESC', 'ASC'))), " << inc_ind << endl_ind <<
		"CONCAT(', ', OrderField2, ' ', IF(COALESCE(OrderDir2, 0), 'DESC', 'ASC')))," << dec_ind << endl_ind <<
		"CONCAT(CONCAT(CONCAT(SqlSelect, CONCAT(' ORDER BY ', OrderField1, ' ', IF(COALESCE(OrderDir1, 0), 'DESC', 'ASC')))," << inc_ind << endl_ind <<
		"CONCAT(' ORDER BY ', OrderField2, ' ', IF(COALESCE(OrderDir2, 0), 'DESC', 'ASC')))," << endl_ind <<
		"CONCAT(', ', OrderField3, ' ', IF(COALESCE(OrderDir3, 0), 'DESC', 'ASC'))))));" << dec_ind << dec_ind << endl_ind;

	sqlFile << "SET SqlSelect = IF(ISNULL(LimitX), SqlSelect, IF(ISNULL(LimitY), CONCAT(SqlSelect, CONCAT(' LIMIT ', LimitX))," << inc_ind << endl_ind <<
		"CONCAT(SqlSelect, CONCAT(' LIMIT ', CONCAT(LimitX, CONCAT(', ', LimitY))))));" << dec_ind << endl_ind;

	if(debug) {
		sqlFile << "SELECT SqlSelect;" << dec_ind << endl_ind <<
			"END |" << endl_ind;
	} else {
		sqlFile << "SET @Statement = SqlSelect;" << endl_ind <<
			"PREPARE Query FROM @Statement;" << endl_ind <<
			"EXECUTE Query;" << dec_ind << endl_ind <<
			"END |" << endl_ind;
	}
}

void Table::writeProcUpdate(ofstream& sqlFile, string& group, bool debug) {

	//NEW VERSION permission based

	string idName = "Locate" + _idColumn->getName();

	if(!_permission->hasGroup(group, cmsame::EDIT)) return;

	if(debug) {
		sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
			group << _singular << "UpdateDebug(Locate" << _idColumn->getName() << " INT";
	} else {
		sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
			group << _singular << "Update(Locate" << _idColumn->getName() << " INT";
	}

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column != _idColumn) {
			(*column)->writeSqlUpdateParam(sqlFile, group);
		}
	}

	sqlFile << ")" << endl_ind << "BEGIN" << inc_ind << endl_ind <<
		"DECLARE SqlUpdate TEXT;" << endl_ind <<
		"DECLARE ValueParam VARCHAR(4096);" << endl_ind <<
		"DECLARE RowCount INT;" << endl_ind <<
		"SET SqlUpdate = '';" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column != _idColumn) {
			(*column)->writeSqlUpdateSet(sqlFile, group);
		}
	}

	sqlFile << "SET SqlUpdate = CONCAT(CONCAT('UPDATE " << _schema->getSite()->getSiteName() <<
			"." << _plural << " SET ', SqlUpdate), CONCAT(' WHERE " << _singular <<
			_idColumn->getName() << " = ', Locate" << _idColumn->getName() << "));" << endl_ind;

	if(debug) {
		sqlFile << "SELECT SqlUpdate;" << dec_ind << endl_ind <<
			"END |" << endl_ind;
	} else {
		sqlFile << "SET @Statement = SqlUpdate;" << endl_ind <<
			"PREPARE Query FROM @Statement;" << endl_ind <<
			"EXECUTE Query;" << endl_ind <<
			"SET RowCount = ROW_COUNT();" << endl_ind;
			//if a column is oneof mirroring oneof, need to update.

		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			(*column)->writeSqlUpdateMirror(sqlFile, group);
		}

		sqlFile << "SELECT RowCount;" << dec_ind << endl_ind <<
			"END |" << endl_ind;
	}

}

void Table::writeProcInsert(ofstream& sqlFile, string& group) {
	//NEW VERSION permission based

	string idName = "New" + _singular + _idColumn->getName();

	if(!_permission->hasGroup(group, cmsame::ADD)) return;

	sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." << group << _singular <<
		"Insert(";

	bool first = true;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column != _idColumn) {
			first = !(*column)->writeSqlInsertParam(sqlFile, group, first) && first;
		}
	}


	sqlFile << ")" << endl_ind << "BEGIN" << inc_ind << endl_ind <<
		"DECLARE " << idName << " INT;" << endl_ind <<
		"INSERT INTO " << _schema->getSite()->getSiteName() << "." << _plural << " (";

	first=true;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column != _idColumn) {
			first = !(*column)->writeSqlName(sqlFile, group, first) && first;
		}
	}

	sqlFile << ") VALUES (";

	first=true;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column != _idColumn) {
			first = !(*column)->writeSqlParam(sqlFile, group, first) && first;
		}
	}

	sqlFile << ");" << endl_ind <<
	"SET " << idName << " = LAST_INSERT_ID();" << endl_ind;

		//if a column is oneof mirroring oneof, need to update.

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlInsertMirror(sqlFile, group);
	}

	sqlFile << "SELECT " << idName << " AS InsertId;" << dec_ind << endl_ind <<
		"END |" << endl_ind;

}

void Table::writeProcDelete(ofstream& sqlFile, string& group) {
	//NEW VERSION permission based

	if(!_permission->hasGroup(group, cmsame::DELETE)) return;

	sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." << group << _singular <<
		"Delete(Locate" << _idColumn->getName() << " INT)" << endl_ind <<
		"BEGIN" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlDeleteMirror(sqlFile, group);
	}

	sqlFile << "DELETE FROM " << _schema->getSite()->getSiteName() << "." << _plural << " WHERE " <<
		_singular << _idColumn->getName() << " = Locate" <<
		_idColumn->getName() << ";" << endl_ind;
	sqlFile << "SELECT ROW_COUNT() AS RowCount;" << dec_ind << endl_ind <<
		"END |" << endl_ind;

}

void Table::writeProcCount(ofstream& sqlFile, string& group) {
	//NEW VERSION permission based

	if(group=="_self"||group=="_cmsame") return;
	if(!_permission->hasGroup(group, cmsame::VIEW)) return;

	sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." << group << _singular <<
		"Count()" << endl_ind <<
		"BEGIN" << inc_ind << endl_ind <<
		"SELECT COUNT(*) AS Count FROM " << _schema->getSite()->getSiteName() << "." << _plural <<
			";" << dec_ind << endl_ind <<
		"END |" << endl_ind;


	//oneof type generate another proc
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlCreateCount(sqlFile, group);
	}
}

void Table::writeSqlSelect(ofstream& sqlFile, string group, string alias) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlSelect(sqlFile, group, false, alias);
	}
}

void Table::writeSqlTailJoin(ofstream& sqlFile, string group, string alias) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlTailJoin(sqlFile, group, alias);
	}
}

void Table::writeDropProc(ofstream& sqlFile, bool debug) {

	//NEW VERSION permission based

	for(vector<string*>::iterator group = Permission::getAllGroups().begin(); group!=Permission::getAllGroups().end(); ++group) {
		if(_permission->hasGroup(**group, cmsame::VIEW)) {
			sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
					**group << _singular << "Locate;" << endl_ind;

			if((**group)!="_self"&&(**group)!="_cmsame") {
				sqlFile <<"DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
					**group << _singular << "Search;" << endl_ind;

				if (debug)
					sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." << **group << _singular << "SearchDebug;" << endl_ind;

				sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
					**group << _singular << "Count;" << endl_ind;
			}
		}
		if(_permission->hasGroup(**group, cmsame::EDIT)) {
			sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				**group << _singular << "Update;" << endl_ind;
			if (debug)
				sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
					**group << _singular << "UpdateDebug;" << endl_ind;

		}
		if(_permission->hasGroup(**group, cmsame::ADD))
			sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				**group << _singular << "Insert;" << endl_ind;
		if(_permission->hasGroup(**group, cmsame::DELETE))
			sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				**group << _singular << "Delete;" << endl_ind;
	}

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlDropCount(sqlFile);
	}
}


void Table::writeGrantExec(ofstream& sqlFile, string& user, bool canEdit, string group, bool debug) {

	if(_permission->hasGroup(group, cmsame::VIEW)) {

		sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
			group << _singular << "Locate TO " << user << ";" << endl_ind;
		if(group!="_cmsame"&&group!="_self") {
			sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				group << _singular << "Search TO " << user << ";" << endl_ind;
			if (debug)
				sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
					group << _singular << "SearchDebug TO " << user << ";" << endl_ind;
			sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				group << _singular << "Count TO " << user << ";" << endl_ind;
		}
		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			(*column)->writeGrantExec(sqlFile, user, group);
		}
	}

	if(canEdit && _permission->hasGroup(group, cmsame::EDIT)) {
		sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
			group << _singular << "Update TO " << user << ";" << endl_ind;
		if (debug)
			sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				group << _singular << "UpdateDebug TO " << user << ";" << endl_ind;
	}

	if(canEdit && _permission->hasGroup(group, cmsame::ADD)) {
		sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." << group <<
			_singular << "Insert TO " << user << ";" << endl_ind;
	}

	if(canEdit && _permission->hasGroup(group, cmsame::DELETE)) {
		sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << "." << group <<
			_singular << "Delete TO " << user << ";" << endl_ind;
	}
}

void Table::writeSqlRefParam(ofstream& sqlFile, string group) {

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlRefParam(sqlFile, group);
	}

}

/////////////
/// MODEL ///
/////////////

void Table::writeModel(ofstream& phpFile, UserTable* userTable, double phpVersion, bool close)
{
//	cout << "writemodel " << _plural << "\n";

	phpFile << "class " << _singular << " extends \\cmsame\\_object {" << inc_ind << endl_ind;
	writeModelConstruct(phpFile);
	writeModelFromRow(phpFile, userTable);
	writeModelFromPost(phpFile, userTable);
	writeModelObject(phpFile);
	writeModelLoad(phpFile, userTable);
	writeModelSave(phpFile);
	writeModelInsert(phpFile, userTable);
	writeModelUpdate(phpFile, userTable);
	writeModelDelete(phpFile, userTable);
	writeModelSearch(phpFile, userTable);
	//writeModelValues(phpFile);
	writeModelCount(phpFile, userTable);
	writeModelCleanText(phpFile);
	writeModelRenderText(phpFile);
	writeModelValidate(phpFile);
	writeModelDefault(phpFile);
	if(close) phpFile << dec_ind << endl_ind << "}" << endl_ind << endl_ind;

//	cout << "done\n";
}

void Table::writeSearchModel(ofstream& phpFile) {
	phpFile << "class " << _singular << "Search extends \\cmsame\\_search {" << inc_ind << endl_ind;

	writeModelConstruct(phpFile, true);

	phpFile << "public function mirror($id, $key) {" << inc_ind << endl_ind <<
		"if(!is_numeric($id)||!is_string($key)) return;" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelMirror(phpFile);
	}

	phpFile << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"public function get() {" << inc_ind << endl_ind <<
		"return " << _singular << "::search($this);" << dec_ind << endl_ind <<
		"}" <<  dec_ind << endl_ind <<
		"}" << endl_ind << endl_ind;
}

void Table::writeModelConstruct(ofstream& phpFile, bool search)
{
	phpFile << "public function __construct() {" << inc_ind << endl_ind;

	if(search) {
		phpFile << 
//"$this->_Order = [];" << endl_ind <<
			"$this->_Take = NULL;" << endl_ind <<
			"$this->_Skip = NULL;" << endl_ind <<
			"$this->_Compare = [];" << endl_ind <<
			"$this->_Order = [];" << endl_ind <<
			"$this->_Priority = [];" << endl_ind;
	}

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelConstruct(phpFile, search);
	}

	phpFile << dec_ind << endl_ind << "}" << endl_ind << endl_ind;

}

void Table::writeModelFromRow(ofstream& phpFile, UserTable* userTable)
{
	phpFile << "public function fromRow($row, $recur=true, $alias='') {" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelFromRow(phpFile, userTable);
	}

	phpFile << dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}

void Table::writeModelFromPost(ofstream& phpFile, UserTable* userTable)
{
	//merge arg removed - *id col is always first now*
	//if it is posted - attempt a load
	//then overwrite other fields according to permissions, either way
	phpFile << "public function fromPost() {" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelFromPost(phpFile, userTable);
	}

	phpFile << dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}


void Table::writeModelObject(ofstream& phpFile) {
	//as per _object abstract parent
	phpFile << "public function setId($id) {" << inc_ind << endl_ind <<
		"$this->" << _idColumn->getName() << "->set($id);" << dec_ind << endl_ind <<
		"}" << endl_ind;

	phpFile << "public function getId() {" << inc_ind << endl_ind <<
		"return $this->" << _idColumn->getName() << "->val;" << dec_ind << endl_ind <<
		"}" << endl_ind;

	if(_nameColumn) {
		phpFile << "public function getName() {" << inc_ind << endl_ind <<
			"return $this->" << _nameColumn->getName() << "->val;" << dec_ind << endl_ind <<
			"}" << endl_ind;
	}
}


void Table::writeModelLoad(ofstream& phpFile, UserTable* userTable)
{
	phpFile << "public function load(";

	bool first=true;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		first= !(*column)->writeModelOpenParam(phpFile, first) && first;
	}

	phpFile << ") {" << inc_ind << endl_ind;
	phpFile << "if($this->" << _idColumn->getName() << "->isnull()) return;" << endl_ind;

	phpFile << "if(!" << userTable->getSingular() << "::hasGroups('";
	_permission->writeViewGroups(phpFile);
	phpFile << "')) return false;" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) $pn = '" << **group << _singular << "Locate';" << endl_ind;
	}

	//////
	writeModelLoadQuery(phpFile, userTable);

	phpFile << "$rows = " << _schema->getSite()->getSiteName() << "::read_assoc($query);" << endl_ind <<
		"if(count($rows) != 1) return false;" << endl_ind <<
		"$this->fromRow($rows[0]);" << endl_ind <<
		"return true;" << dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}

void Table::writeModelLoadQuery(ofstream& phpFile, UserTable* userTable) {
	phpFile << "$query = 'CALL ' . $pn . '(' . $this->" << _idColumn->getName() << "->val;" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelOpenValue(phpFile, userTable);
	}

	phpFile << "$query .= ');';" << endl_ind;
}


void Table::writeModelSave(ofstream& phpFile)
{
	phpFile << "public function save() {" << inc_ind << endl_ind <<
		"if($this->" << _idColumn->getName() << "->isnull()) $this->insert(); else $this->update();" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}


void Table::writeModelUpdate(ofstream& phpFile, UserTable* userTable)
{
	phpFile << "public function update(";

	bool first = true;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column!=_idColumn) {
			first = !(*column)->writeModelUpdateParam(phpFile, first) && first;
		}
	}

	phpFile << ") {" << inc_ind << endl_ind;

	phpFile << "if($this->" << _idColumn->getName() << "->isnull()) return false;" << endl_ind;

	phpFile << "if(!" << userTable->getSingular() << "::hasGroups('";
	_permission->writeEditGroups(phpFile);
	phpFile << "')) return false;" << endl_ind;

	phpFile << "$this->clean_text();" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) $pn = '" << **group << _singular << "Update';" << endl_ind;
	}

	writeModelUpdateQuery(phpFile, userTable);

	phpFile << "$rows = " << _schema->getSite()->getSiteName() << "::edit_assoc($query);" << endl_ind <<
		"return(count($rows)==1&&isset($rows[0]['RowCount'])&&$rows[0]['RowCount']==1);" << dec_ind << endl_ind << 
		"}" << endl_ind << endl_ind;
}

void Table::writeModelUpdateQuery(ofstream& phpFile, UserTable* userTable) {
	phpFile << "$query = 'CALL ' . $pn . '(' . $this->" << _idColumn->getName() << "->val;" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column!=_idColumn)
			(*column)->writeModelUpdateValue(phpFile, userTable); //this can now safely call $this->File->getPath to write to db
	}

	phpFile << "$query .= ');';" << endl_ind;
}

void Table::writeModelInsert(ofstream& phpFile, UserTable* userTable)
{
	phpFile << "public function insert() {" << inc_ind << endl_ind <<
		"if(!" << userTable->getSingular() << "::hasGroups('";
	_permission->writeAddGroups(phpFile);
	phpFile << "')) return;" << endl_ind;

	phpFile << "$this->clean_text();" << endl_ind;

	phpFile << "$query = 'CALL " << _schema->getSite()->getSiteName() << ".';" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) $query .= '" << **group << "';" << endl_ind;
	}

	phpFile << "$query .= '" << _singular << "Insert(';" << endl_ind;

	bool first = true;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		if(*column!=_idColumn) {
			first = !(*column)->writeModelInsertValue(phpFile, first) && first;
		}
	}

	phpFile << "$query .= ');';" << endl_ind <<
		"$rows = " << _schema->getSite()->getSiteName() << "::edit_assoc($query);" << endl_ind <<
		"$this->" << _idColumn->getName() << "->set($rows[0][\"InsertId\"]);" << endl_ind;

		//if any not null file columns, call update
	phpFile << "$files = false;" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelInsertFile(phpFile);
	}

	phpFile << "if($files) $this->update();" << dec_ind << endl_ind <<
		"}" << endl_ind << endl_ind;
}

void Table::writeModelDelete(ofstream& phpFile, UserTable* userTable)
{
	phpFile << "public function delete() {" << inc_ind << endl_ind;
	phpFile << "if(!" << userTable->getSingular() << "::hasGroups('";
	_permission->writeDeleteGroups(phpFile);
	phpFile << "')) return false;" << endl_ind;

	//clean up any files
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelDeleteFile(phpFile);
	}

	phpFile << "$query = 'CALL " << _schema->getSite()->getSiteName() << ".';" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) $query .= '" << **group << "';" << endl_ind;
	}

	phpFile << "$query .= '" << _singular << "Delete(' . $this->" << _idColumn->getName() <<
		"->val . ');';" << endl_ind;
	phpFile << "$rows = " << _schema->getSite()->getSiteName() << "::edit_assoc($query);" << endl_ind <<
		"return(count($rows)==1&&isset($rows[0]['RowCount'])&&$rows[0]['RowCount']==1);" << dec_ind << endl_ind << 
		"}" << endl_ind << endl_ind;



//_schema->getSite()->getSiteName() <<		"::mysqli_edit()->query($query);" << endl_ind << dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}

void Table::writeModelSearch(ofstream& phpFile, UserTable* userTable)
{
//new: create prototype object so we can use sql() func

	phpFile << "public static function search($pt";

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelOpenParam(phpFile, false);
	}

	phpFile << ") {" << inc_ind << endl_ind;
	phpFile << "if(!" << userTable->getSingular() << "::hasGroups('";
	_permission->writeViewGroups(phpFile);
	phpFile << "')) return;" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) ";
		if((**group)=="_cmsame"||(**group)=="_self")
			phpFile << " return [];" << endl_ind;
		else
			phpFile << "$pn = '" << **group << _singular << "Search';" << endl_ind;
	}

	writeModelSearchQuery(phpFile, userTable);

	phpFile << "$rows = " << _schema->getSite()->getSiteName() << "::read_assoc($query);" << endl_ind <<
		"$arr = array();" << endl_ind <<
		"foreach($rows as $row) {" << inc_ind << endl_ind <<
		"$obj = new " << _singular << ";" << endl_ind <<
		"$obj->fromRow($row);" << endl_ind <<
		"array_push($arr, $obj);" <<  endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind <<
		"return $arr;" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}


void Table::writeModelSearchQuery(ofstream& phpFile, UserTable* userTable) {
	phpFile << "$query = 'CALL ' . $pn . '(';" << endl_ind <<
		"if(is_null($pt->_Take)) {" << inc_ind << endl_ind <<
		"$query .= \"NULL, NULL\";" << dec_ind << endl_ind <<
		"} else {" << inc_ind << endl_ind <<
		"if(is_null($pt->_Skip)) {" << inc_ind << endl_ind <<
		"$query .= $pt->_Take . \", NULL\";" << dec_ind << endl_ind <<
		"} else {" << inc_ind << endl_ind <<
		"$query .= $pt->_Skip . \", \" . $pt->_Take;" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"}" << endl_ind;


	phpFile << "$l=-1;" << endl_ind <<
		"for($i=0;$i<3;$i++) {" << inc_ind << endl_ind <<
		"$o=NULL;$p=255;" << endl_ind;
//unroll fields 
//if($pt->_Priority->_name < $p && $pt.._name > $l) {
//	$p=$pt->.._name;$o='_name';
//	}
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelSearchOrder(phpFile);
	}

	phpFile << "if(is_null($o)) $query .= ', NULL, NULL';" << endl_ind <<
		"else $query .= \", '" << _singular << "$o', \" . (($pt->_Order[$o] == Order::Descending) ? '1' : '0');" << endl_ind <<
		"$l=$p;" << dec_ind << endl_ind <<
		"}" << endl_ind;
/*
	phpFile << "$orderLayers = 0;" << endl_ind <<
		"if(is_array($pt->Order)) {" << inc_ind << endl_ind <<
		"foreach ($pt->Order as $orderLayer) {" << inc_ind << endl_ind <<
		"if(!is_array($orderLayer)) break;" << endl_ind <<
		"if(!property_exists(\"" << _singular << "\", $orderLayer[0])) break;" << endl_ind <<
		"if(!is_numeric($orderLayer[1])) break;" << endl_ind <<
		"if($orderLayer[1] < 0 || $orderLayer[1] > 7) break;" << endl_ind <<
		"$orderLayers++;" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"}" << endl_ind;

	phpFile << "for($i=0;$i<3;$i++) {" << inc_ind << endl_ind <<
		"if($i < $orderLayers) $query .= \", '\" . $pt->Order[i][0] . \"', \" . $pt->Order[i][1];" << endl_ind <<
		"else $query .= \", NULL, NULL\";" << dec_ind << endl_ind <<
		"}" << endl_ind;
*/

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelSearchValue(phpFile);
	}

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelOpenValue(phpFile, userTable);
	}

	phpFile << "$query .= ');';" << endl_ind;

}



void Table::writeModelCount(ofstream& phpFile, UserTable* userTable) {
	phpFile << "public static function count() {" << inc_ind << endl_ind;
	phpFile << "if(!" << userTable->getSingular() << "::hasGroups('";
	_permission->writeViewGroups(phpFile);
	phpFile << "')) return;" << endl_ind;

	phpFile << "$query = \"CALL " << _schema->getSite()->getSiteName() << ".\";" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) $query .= '" << **group << "';" << endl_ind;
	}

	phpFile << "$query .= \"" << _singular << "Count();\";" << endl_ind <<
		"$rows = " << _schema->getSite()->getSiteName() << "::read_assoc($query);" << endl_ind <<
		"return $rows[0][\"Count\"];" << dec_ind << endl_ind <<
		"}" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelCount(phpFile, userTable);
	}

}

void Table::writeModelCleanText(ofstream& phpFile) {
	phpFile << "private function clean_text() {" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelCleanText(phpFile);
	}

	phpFile << dec_ind << endl_ind << "}" << endl_ind << endl_ind;

}

void Table::writeModelRenderText(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelRenderText(phpFile);
	}
}


void Table::writeModelValidate(ofstream& phpFile) {

	phpFile << "public function validate() {" << inc_ind << endl_ind <<
		"$errors = [];" << endl_ind <<
		"$updates = [];" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column != _idColumn) (*column)->writeModelValidate(phpFile);
	}

//	phpFile << "error_log('===OBJECT===');error_log(preg_replace('/\\s+/', ' ', print_r($this, true)));" <<
//		"error_log('===ERRORS===');error_log(preg_replace('/\\s+/', ' ', print_r($errors, true)));" << endl_ind;

	phpFile << "return ['errors'=>$errors,'updates'=>$updates];" << dec_ind << endl_ind <<
		"}" << endl_ind;

}

void Table::writeModelDefault(ofstream& phpFile) {
	phpFile << "public function fromDefault() {" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelDefault(phpFile);
	}

	phpFile << dec_ind << endl_ind << "}" << endl_ind;
}


///////////
/// CMS ///
///////////

void Table::writeCmsMenu(ofstream& phpFile, UserTable* userTable)
{
	phpFile << "if(" << userTable->getSingular() << "::isLoggedIn() && " <<
		userTable->getSingular() << "::hasGroups(\"";

	_permission->writeViewGroups(phpFile);

	phpFile <<	"\")) {" << inc_ind << endl_ind <<
				"echo '<div class=\"section\">';" << endl_ind <<
				"echo '<h3>" << _plural << "</h3>';" << endl_ind;

	if(_description.length()>0)	phpFile << "echo '" << _description << "<br /><br />';" << endl_ind;

	phpFile << "echo '<a href=\"" << _plural << "/\" class=\"button\">Browse</a>';" << endl_ind <<
				"if("  << userTable->getSingular() << "::hasGroups(\"";

	_permission->writeAddGroups(phpFile);

	phpFile << 	"\"))" << inc_ind << endl_ind <<
				"echo '<a href=\"" << _plural << "/detail.php\" class=\"button\">New</a>';" << dec_ind << endl_ind <<
				"echo '</div>';" << dec_ind << endl_ind <<
				"}" << endl_ind << endl_ind;


}

void Table::writeCmsIndex(bool requireSSL, UserTable* userTable)
{
	if(!Target::assertPath((SITESPATH + _schema->getSite()->getName() + "/www/cms/" + _plural + "/").c_str())) {
		_statusMessage = "Target generate error: couldn't create directory.";
		_isOk = false;
		return;
	}

	string phpFileName = Target::nativePath(SITESPATH + _schema->getSite()->getName() +
		"/www/cms/" + _plural + "/index.php");
	ofstream phpFile;
	phpFile.open(phpFileName.c_str(), ios_base::trunc);

	if(!phpFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
		_isOk = false;
		if (phpFile.is_open()) phpFile.close();
		return;
	}

	if(_schema->getSite()->isVerbose()) cout << "Writing to " << phpFileName << "...\n";


	if(requireSSL) phpFile << "<?php if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on'){" <<
		"header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);" <<
		"die();} ?>" << endl_ind;

	writeAccessControl(phpFile, userTable);

	phpFile << 	"<!DOCTYPE html>" << endl_ind <<
				"<html>" << inc_ind << endl_ind <<
				"<head>" << inc_ind << endl_ind;

	TargetCms::writeUIIncludes(phpFile, 1);

	phpFile << "<title>" << _plural << " | " << _schema->getSite()->getSiteDescription() <<
			" | CMSame</title>" << dec_ind << endl_ind <<
		"</head>" << endl_ind <<
		"<body>" << inc_ind << endl_ind <<
		"<div class=\"wrapper\">" << inc_ind << endl_ind <<
		"<div class=\"header\">" << inc_ind << endl_ind;

	userTable->writeCmsAccountPanel(phpFile, 1);

	phpFile <<  "<?php" << inc_ind << endl_ind;

	bool first=true;

	//check querystring for FK value

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		first = !(*column)->writeCmsIndexCount(phpFile, userTable, first) && first;
	}

	if (!first) phpFile << "else ";

	phpFile << "$count = " << _singular << "::count();" << endl_ind <<
		"$page = (isset($_GET[\"page\"]) ? intval($_GET[\"page\"]) : 0);" << endl_ind <<
		"$skip = $page * 10;" << endl_ind <<
		"if($count > 0 && $skip >= $count) {" << inc_ind << endl_ind <<
		"header(\"Location: index.php\");" << endl_ind <<
		"die();" << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"$pt = new " << _singular << "Search();" << endl_ind <<
		"$pt->Take = 10; $pt->Skip = $skip;" << endl_ind;

	first = true;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		first = !(*column)->writeCmsIndexFilter(phpFile, userTable, first) && first;
	}

	//search ready to go

	phpFile << dec_ind << endl_ind << "?>" << endl_ind;


	phpFile << "<h1>" << _schema->getSite()->getSiteDescription() << "</h1>" << endl_ind <<
		"<h2>" << _plural << "<?php if(isset($fttl)) echo \" $fttl\"; ?></h2>" << dec_ind << endl_ind <<
		"</div>" << endl_ind <<
		"<div class=\"main\">" << inc_ind << endl_ind <<
		"<?php" << inc_ind << endl_ind;



	phpFile << "$items = " << _singular << "::search($pt);" << endl_ind <<
		"if(count($items)==0) echo '<div class=\"section\">No " << _plural << ".</div>';" << endl_ind <<
		"else {" << inc_ind << endl_ind <<
		"echo '<table><thead><tr>';" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeCmsIndexHeader(phpFile, userTable);
	}

	phpFile << endl_ind << "echo '<th class=\"buttons\"></th></tr></thead><tbody>';" << endl_ind <<
		"foreach($items as $item) {" << inc_ind << endl_ind <<
		"echo '<tr>';" << endl_ind;

	if(_schema->getSite()->isVerbose()) {
		cout << _columns.size() << " columns in table.\n";
		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			cout << "pointer=" << (*column) << ", ";
			cout << "name=" << (*column)->getName() << endl;
		}
	}


	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeCmsIndexContent(phpFile, userTable);
	}


	phpFile << "echo '<td class=\"buttons\">';" << endl_ind;

	//edit / view button
	phpFile << "if(" << userTable->getSingular() << "::hasGroups(\"";
	_permission->writeViewGroups(phpFile);
	phpFile << "\")) {" << inc_ind << endl_ind <<
				"echo '<a href=\"detail.php?" << _singular << _idColumn->getName() <<
					"=' . $item->" << _idColumn->getName() << "->val . '\" class=\"button\">';" << endl_ind <<
				"if(" << userTable->getSingular() << "::hasGroups(\"";

	_permission->writeEditGroups(phpFile);
	phpFile << "\")) echo 'Edit'; else echo 'View';" << endl_ind <<
				"echo '</a>';" << dec_ind << endl_ind;

	phpFile << "}" << endl_ind << endl_ind;

	//delete button
	phpFile << "if(" << userTable->getSingular() << "::hasGroups(\"";
	_permission->writeDeleteGroups(phpFile);
	phpFile << "\")) echo '<form class=\"inline\" action=\"./detail.php\" method=\"post\"><input type=\"hidden\" name=\"" <<
		_singular << _idColumn->getName() << "\" value=\"' . $item->" << _idColumn->getName() <<
		"->val . '\" /><input type=\"submit\" value=\"Delete\" class=\"button btndelete\" /></form>';" << endl_ind;

	phpFile << "echo '</td></tr>';" << dec_ind << endl_ind <<
				"}" << dec_ind << endl_ind <<
				"}" << endl_ind <<
				"echo '</tbody></table>';" << endl_ind;

	//pagination
	phpFile << "if($count>10) {" << inc_ind << endl_ind <<
		"echo '<div class=\"section pager\">';" << endl_ind <<
		"$pages = ceil($count/10);" << endl_ind <<
		"for($p=0;$p<$pages;$p++) {" << inc_ind << endl_ind <<
		"$op = $p+1;" << endl_ind <<
		"if($p==$page) echo \"<span>{$op}</span>\";" << endl_ind <<
		"else echo \"<a href=\\\"index.php?page={$p}\\\">{$op}</a>\";" << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"echo '</div>';" << dec_ind << endl_ind <<
		"}" << endl_ind;


	//new button
	phpFile << "echo '<div class=\"section\">';" << endl_ind;
	phpFile << "if(" << userTable->getSingular() << "::hasGroups(\"";
	_permission->writeAddGroups(phpFile);
	phpFile << "\")) echo '<a href=\"./detail.php\" class=\"button\">New</a>'; ?>" << endl_ind;

	//main menu button
	phpFile << "<a href=\"../\" class=\"button\">Main Menu</a>";

	//back to parent item button if filter is active

	phpFile << "<?php if(isset($furl)) echo \"<a href=\\\"$furl\\\" class=\\\"button\\\">$fbtn</a>\"; ?>" << endl_ind;

	phpFile << dec_ind << endl_ind << "</div>" << dec_ind << endl_ind;

	phpFile << "</div>" << endl_ind << "<div class=\"footer\">" << inc_ind << endl_ind;

	_schema->writeSign(phpFile);
	phpFile << "<br/>" << endl_ind;
	Target::writeSign(phpFile);

	phpFile << dec_ind << endl_ind << "</div>" << dec_ind << endl_ind <<
				"</div>" << dec_ind << endl_ind <<
				"</body>" << dec_ind << endl_ind <<
				"</html>" << endl_ind;


	phpFile.close();
}

void Table::writeCmsDetail(bool requireSSL, UserTable* userTable)
{
	if(!Target::assertPath((SITESPATH + _schema->getSite()->getName() + "/www/cms/" + _plural + "/").c_str())) {
		_statusMessage = "Target generate error: couldn't create directory.";
		_isOk = false;
		return;
	}
	string phpFileName = Target::nativePath(SITESPATH + _schema->getSite()->getName() +
		"/www/cms/" + _plural + "/detail.php");
	ofstream phpFile;
	phpFile.open(phpFileName.c_str(), ios_base::trunc);

	if(!phpFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
		_isOk = false;
		if (phpFile.is_open()) phpFile.close();
		return;
	}

	if(_schema->getSite()->isVerbose()) cout << "Writing to " << phpFileName << "...\n";

	if(requireSSL) phpFile << "<?php if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on'){" <<
		"header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);" <<
		"die();} ?>" << endl_ind;

	writeAccessControl(phpFile, userTable);

	//get/post handler goes here now, not inside the form
	phpFile << "<?php" << inc_ind << endl_ind <<
		"$item = new " << _singular << ";" << endl_ind <<
 		"if (isset($_GET['" << _singular << _idColumn->getName() << "'])) {" << inc_ind << endl_ind <<
		"$item->" << _idColumn->getName() << "->set($_GET['" << _singular << //load by id
			_idColumn->getName() << "']);" << endl_ind <<
		"if(!$item->load()) {" << inc_ind << endl_ind <<
		"header('location: ./');die();" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"} else if (isset($_POST['" << _singular << _idColumn->getName() << "'])) {" << inc_ind << endl_ind <<
		"$item->fromPost();" << endl_ind <<	//build from postdata and..

		"if (isset($_POST['_DELETE']) && " << userTable->getSingular() << "::hasGroups('";

	_permission->writeDeleteGroups(phpFile);

	phpFile << "')) {" << inc_ind << endl_ind <<
		"$item->delete();" << endl_ind <<	//del & redir
		"header('location: ./');die();" << dec_ind << endl_ind <<
		"} else if(" << userTable->getSingular() << "::hasGroups('";

	_permission->writeEditGroups(phpFile);

	phpFile << "') || " << userTable->getSingular() << "::hasGroups('";

	_permission->writeAddGroups(phpFile);

	phpFile << "')) {" << inc_ind << endl_ind <<
		"$item->save();" << endl_ind <<		//save & redir
		"header(\"location: ./detail.php?" << _singular <<
			_idColumn->getName() << "=\" . $item->" << _idColumn->getName() <<
			"->val);die();" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"} else if (" << userTable->getSingular() << "::hasGroups(\"";

	_permission->writeAddGroups(phpFile);

	phpFile << "\")) {" << inc_ind << endl_ind <<
		"$item->fromDefault();" << dec_ind << endl_ind << //blank slate, defaults go here
		"} else { header(\"location: ./\");die(); }" << dec_ind << endl_ind << //security fail, redirect
		"?>" << endl_ind;


	phpFile << "<!DOCTYPE html>" << endl_ind <<
		"<html>" << inc_ind << endl_ind <<
		"<head>" << inc_ind << endl_ind;

	TargetCms::writeUIIncludes(phpFile, 1);

	if(!_nameColumn) phpFile << "<title>" << _singular << " | " << _schema->getSite()->getSiteDescription() << " | CMSame</title>";
	else phpFile << "<title><?php echo ($item->" << _idColumn->getName() << "->isnull() ? '" << _singular << 
		"' :  $item->" << _nameColumn->getName() << "->val); ?> | " << _schema->getSite()->getSiteDescription() << " | CMSame</title>";

	phpFile << dec_ind << endl_ind <<
		"</head>" << endl_ind <<
		"<body>" << inc_ind << endl_ind <<
		"<div class=\"wrapper\">" << inc_ind << endl_ind <<
		"<div class=\"header\">" << inc_ind << endl_ind;
	userTable->writeCmsAccountPanel(phpFile, 1);
	phpFile << "<h1>" << _schema->getSite()->getSiteDescription() << "</h1>" << endl_ind;


	phpFile << "<?php" << inc_ind << endl_ind <<
		"if($item->" << _idColumn->getName() << "->isnull()) echo '<h2>New " << _singular << "</h2>';" << endl_ind <<
		"else ";

	if(!_nameColumn) phpFile << "echo '<h2>Edit " << _singular << "</h2>';";
	else {
		if(_schema->getSite()->isVerbose()) cout << _plural << ": dynamic H2 using namecolumn\n";
		phpFile << "echo '<h2>Edit " << _singular << ": ' . $item->" << _nameColumn->getName() << "->val . '</h2>';";
	}

	phpFile << dec_ind << endl_ind << "?>" << dec_ind << endl_ind;

	phpFile << "</div>" << endl_ind <<
		"<div class=\"main\">" << inc_ind << endl_ind <<
		"<form enctype=\"multipart/form-data\" class=\"detail\" id=\"" << _singular <<
		"\" action=\"./detail.php\" method=\"post\">" << inc_ind << endl_ind <<
		"<div class=\"section\">" << inc_ind << endl_ind <<
		"<?php" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeCmsDetailField(phpFile, userTable);
	}

	//save button

	phpFile << "if($item->" << _idColumn->getName() << "->isnull()) {" << inc_ind << endl_ind; //add

	phpFile << "if(" << userTable->getSingular() << "::hasGroups(\"";
	_permission->writeAddGroups(phpFile);
	phpFile << "\")) echo '<input type=\"submit\" id=\"Save\" value=\"Save\" class=\"button\" />';" << dec_ind << endl_ind;

	phpFile << "} else {" << inc_ind << endl_ind;											//edit

	phpFile << "if(" << userTable->getSingular() << "::hasGroups(\"";
	_permission->writeEditGroups(phpFile);
	phpFile << "\")) echo '<input type=\"submit\" id=\"Save\" value=\"Save\" class=\"button\" />';" << dec_ind << endl_ind;

	phpFile << "}" << endl_ind;

	//delete button

	phpFile << "if(!$item->" << _idColumn->getName() << "->isnull() && " <<
		userTable->getSingular() << "::hasGroups(\"";

	_permission->writeDeleteGroups(phpFile);

	phpFile << "\")) echo '<input type=\"submit\" id=\"Delete\" value=\"Delete\" class=\"button btndelete\"/>';" << dec_ind << endl_ind;




	phpFile << "?>" << dec_ind << endl_ind <<
		"</div>" << dec_ind << endl_ind <<
		"</form>" << endl_ind <<
		"<div class=\"section\"><a href=\"./\" class=\"button\">Browse</a><a href=\"../\" class=\"button\">Main Menu</a></div>" << dec_ind << endl_ind <<
		"</div>" << endl_ind <<
		"<div class=\"footer\">" << inc_ind << endl_ind;

	Target::writeSign(phpFile);

	phpFile << dec_ind << endl_ind << "</div>" << dec_ind << endl_ind <<
		"</div>" << dec_ind << endl_ind <<
		"</body>" << dec_ind << endl_ind <<
		"</html>" << endl_ind;

	phpFile.close();
}

void Table::writeAccessControl(ofstream& phpFile, UserTable* userTable)
{

	phpFile << "<\?php " << inc_ind << endl_ind <<
		"require_once('cmsame/" <<  _schema->getSite()->getName() << "/" << _schema->getSite()->getSiteName() << ".php');" << endl_ind <<
		"require_once('cmsame/" <<  _schema->getSite()->getName() << "/cms.php');" << endl_ind;
	phpFile << "if(!" << userTable->getSingular() << "::isLoggedIn() || !" <<
		userTable->getSingular() << "::hasGroups(\"";

	_permission->writeViewGroups(phpFile);

	phpFile <<	"\")) { header(\"location: ../\");die(); }" << dec_ind << endl_ind <<
				"?>" << endl_ind;
}

void Table::writeCmsAJAXPreview(bool requireSSL) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) 	{
		(*column)->writeCmsAJAXPreview(requireSSL);
	}
}

void Table::writeCmsAJAXResize(bool requireSSL, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) 	{
		(*column)->writeCmsAJAXResize(requireSSL, userTable);
	}
}

void Table::writeCmsAJAXValidate(bool requireSSL, UserTable* userTable) {
	if(!Target::assertPath((SITESPATH + _schema->getSite()->getName() + "/www/cms/" + _plural + "/").c_str())) {
		_statusMessage = "Target generate error: couldn't create directory.";
		_isOk = false;
		return;
	}

	string phpFileName = Target::nativePath(SITESPATH + _schema->getSite()->getName() +
		"/www/cms/" + _plural + "/validate.php");
	ofstream phpFile;
	phpFile.open(phpFileName.c_str(), ios_base::trunc);

	if(!phpFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
		_isOk = false;
		if (phpFile.is_open()) phpFile.close();
		return;
	}

	if(requireSSL) phpFile << "<?php if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on'){" <<
		"header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);" <<
		"die();} ?>" << endl_ind;

	writeAccessControl(phpFile, userTable);

	phpFile << "<?php" << inc_ind << endl_ind <<
		"$item = new " << _singular << ";" << endl_ind <<
		"$item->fromPost();" << endl_ind <<
		"echo json_encode($item->validate());" << dec_ind << endl_ind <<
		"?>" << endl_ind;


}


/////////////////
/// USERTABLE ///
/////////////////


UserTable::UserTable(xml_node<>* tableNode, Schema* schema) : Table(tableNode, schema)
{
	_idColumn->applyPermission("_cmsame", cmsame::VIEW, cmsame::ALLOW);
	_idColumn->applyPermission("_self", cmsame::VIEW, cmsame::ALLOW);
	_permission->apply("_cmsame", cmsame::VIEW, cmsame::ALLOW);
	_permission->apply("_self", cmsame::VIEW, cmsame::ALLOW);
	_permission->apply("_self", cmsame::EDIT, cmsame::ALLOW);

	_usernameColumn = new Column(this, "Username", "Username", cmsame::text, 256, 0, false, true);
	_passwordColumn = new Column(this, "Password", "Password hash and salt", cmsame::password, 32, 0, false, false);
	_groupColumn = new Column(this, "SecurityGroup", "Security group", cmsame::options, 0, 0, true, true);

	_usernameColumn->applyPermission("_self", cmsame::EDIT, cmsame::DENY);
	_groupColumn->applyPermission("_self", cmsame::EDIT, cmsame::DENY);

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		_groupColumn->addValue(**group);
	}

	_columns.push_back(_usernameColumn);
	_columns.push_back(_passwordColumn);
	_columns.push_back(_groupColumn);

//	_columns.push_back(new Column(this, "Groups", "Security group membership", cmsame::tokens, 256, 0, false, true));

	//check XML again for usertable-specific attributes
	for (xml_attribute<>* attr = tableNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "hashfunc") == 0) {
			if(strcmp(attr->value(), "md5") == 0) _pwHashFunc = cmsame::md5;
			if(strcmp(attr->value(), "blake2s") == 0) _pwHashFunc = cmsame::blake2s;
		}
		if(strcmp(attr->name(), "clienthash") == 0 && strcmp(attr->value(), "false") == 0) _pwHashClientSide = false;
	}


}

cmsame::HashFunc UserTable::getHashFunc() { return _pwHashFunc; }
bool UserTable::isHashClientSide() { return _pwHashClientSide; }

void UserTable::writeModel(ofstream& phpFile, UserTable* userTable, double phpVersion, bool close)
{
	if(!Target::assertPath((SITESPATH + _schema->getSite()->getName() + "/model/keys/").c_str())) {
		_statusMessage = "Target generate error: couldn't create directory.";
		_isOk = false;
		return;
	}

	writeModelAJAXLogin();

	phpFile << "class " << _singular << " {" << inc_ind << endl_ind;
	writeModelConstruct(phpFile);
	writeModelFromRow(phpFile, userTable);
	writeModelFromPost(phpFile, userTable);
	writeModelObject(phpFile);
	writeModelLoad(phpFile, userTable);
	writeModelSave(phpFile);
	writeModelInsert(phpFile, userTable);
	writeModelUpdate(phpFile, userTable);
	writeModelDelete(phpFile, userTable);
	writeModelSearch(phpFile, userTable);
	//writeModelValues(phpFile);
	writeModelCount(phpFile, userTable);
	writeModelCleanText(phpFile);
	writeModelRenderText(phpFile);
	writeModelValidate(phpFile);
	writeModelDefault(phpFile);

	phpFile << "public static function isLoggedIn() {" << inc_ind << endl_ind <<
		"return (isset($_SESSION['" << _singular << "']) && is_a($_SESSION['" << _singular <<
			"'], '" << _singular << "'));" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind << endl_ind <<


		"public static function login($username, $creds) {" << inc_ind << endl_ind <<
		"unset($_SESSION['" << _singular << "']);" << endl_ind <<
		"unset($_SESSION['" << _singular << "SecurityContextCache']);" << endl_ind;

		//DEBUG
		//"self::gnupg_refresh();" << endl_ind <<

	//check session token
	phpFile << "while(true) { " << inc_ind << endl_ind <<
		"if(!isset($_SESSION['" << _singular << "LoginToken'])) { error_log('no token'); break; }" << endl_ind <<
		"if(!is_array($_SESSION['" << _singular << "LoginToken'])) { error_log('token not array'); break; }" << endl_ind <<
		"if(count($_SESSION['" << _singular << "LoginToken']) != 3) { error_log('token wrong length'); break; }" << endl_ind <<
		"if(!is_string($_SESSION['" << _singular << "LoginToken'][0])) { error_log('username not string'); break; }" << endl_ind <<
		"if($_SESSION['" << _singular << "LoginToken'][0] != $username) { error_log('username doesn`t match token'); break; }" << endl_ind <<
		"if(!is_object($_SESSION['" << _singular << "LoginToken'][1])) { error_log('timestamp not object'); break; }" << endl_ind <<
		"if(!is_a($_SESSION['" << _singular << "LoginToken'][1], 'DateTime')) { error_log('timestamp not datetime'); break; }" << endl_ind <<
		"$age = $_SESSION['" << _singular << "LoginToken'][1]->diff(new DateTime());" << endl_ind <<
		"if($age->y!=0||$age->m!=0||$age->d!=0||$age->h!=0||$age->m!=0||$age->s>5) { error_log('token expired'); break; }" << endl_ind <<
		"if(!is_string($_SESSION['" << _singular << "LoginToken'][2])) { error_log('token id not string'); break; }" << endl_ind <<
		"$token = $_SESSION['" << _singular << "LoginToken'][2];"
		"$unl = strlen($username);" << endl_ind;

	//start checking posted credentials
	phpFile << "if(strlen($creds) <= $unl) { error_log('creds too short'); break; }" << endl_ind <<
		"if(substr($creds, 0, $unl + 1) != $username . ':') { error_log('username doesn`t match creds'); break; }" << endl_ind <<
		"if(strlen($creds) <= 17) { error_log('creds too short'); break; }" << endl_ind <<
		"if(substr($creds, -17) != ':' . $token) { error_log('token id doesn`t match creds'); break; }" << endl_ind <<
	//$cred_rem ainder with username and token snipped
		"$cred_rem = substr($creds, $unl + 1, strlen($creds) - ($unl + 18));" << endl_ind <<
		"unset($_SESSION['" << _singular << "LoginToken']);" << endl_ind;

	//get salt from DB
	phpFile << "$usr = new " << _singular << "();" << endl_ind <<
		"$usr->Username->set($username);" << endl_ind <<
		"$_SESSION['" << _singular << "'] = new " << _singular << "();" << endl_ind <<
		"$_SESSION['" << _singular << "']->SecurityGroup->set('_cmsame');" << endl_ind <<
		"$usr->load();" << endl_ind <<
		"unset($_SESSION[\"" << _singular << "\"]);" << endl_ind;// <<
//		"if(count($users) != 1) { error_log('couldn`t find user record'); break; }" << endl_ind;


	if(_pwHashClientSide) {
		//expected credentials:
		//   username:tkhash:password:token
		phpFile << "$cred_pw = substr($cred_rem, 33);" << endl_ind <<
			"$cred_tkhash = substr($cred_rem, 0, 32);" << endl_ind;

		switch(_pwHashFunc) {
			case cmsame::md5:
				phpFile << "$hash = md5($usr->Password->salt . $cred_pw);" << endl_ind <<
					"$tkhash = md5($token . $cred_pw);" << endl_ind;
				break;

			case cmsame::blake2s:
				phpFile << "$hash = \\cmsame\\crypto::blake2s128($usr->Password->salt . $cred_pw);" << endl_ind <<
					"$tkhash = \\cmsame\\crypto::blake2s128($token . $cred_pw);" << endl_ind;
				break;
		}

		phpFile << "if($cred_tkhash != $tkhash) { error_log('client hash of pw+token wrong'); break; }" << endl_ind;

	} else {
		//expected credentials:
		//   username:password:token
		switch(_pwHashFunc) {
			case cmsame::md5:
				phpFile << "$hash = md5($usr->Password->salt . $cred_rem);" << endl_ind;
				break;

			case cmsame::blake2s:
				phpFile << "$hash = \\cmsame\\crypto::blake2s128($usr->Password->salt . $cred_rem);" << endl_ind;
				break;
		}
	}
	//2nd load
	phpFile << "$usr->" << _passwordColumn->getName() << "->set([$hash, $usr->" << _passwordColumn->getName() << "->salt]);" << endl_ind <<	
		"$_SESSION['" << _singular << "'] = new " << _singular << "();" << endl_ind <<
		"$_SESSION['" << _singular << "']->SecurityGroup->set('_cmsame');" << endl_ind <<
		"$usr->load();" << endl_ind <<
		"unset($_SESSION[\"" << _singular << "\"]);" << endl_ind <<
		"if($usr->" << _idColumn->getName() << "->isnull()) { error_log('couldn`t find user record'); break; }" << endl_ind;

		//if we didn't hit a break, login is successful
	phpFile << "$_SESSION['" << _singular << "'] = $usr;" << endl_ind <<
		"return true;" << dec_ind << endl_ind <<
		"}" << endl_ind;
		//otherwise, something went wrong
		//waste some time to obfuscate
	if(phpVersion<7.0) {
		phpFile << "$b=chr(rand(0,255)).chr(rand(0,255)).chr(rand(0,255));" << endl_ind;
	} else {
		phpFile << "$b=random_bytes(3);" << endl_ind;
	}

	phpFile << "$w=100000+$b[0]+$b[1]*256+$b[2]*2048;" << endl_ind <<
		"usleep($w);" << dec_ind << endl_ind <<
		"return false;" << dec_ind << endl_ind <<
		"}" << endl_ind << endl_ind <<

		"public static function logout() {" << inc_ind << endl_ind <<
		"unset($_SESSION['" << _singular << "']);" << endl_ind <<
		"unset($_SESSION['" << _singular << "LoginToken']);" << endl_ind <<
		"unset($_SESSION['" << _singular << "SecurityContextGroups']);" << endl_ind <<
		"unset($_SESSION['" << _singular << "SecurityContextCache']);" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind << endl_ind <<

		"public static function current() {" << inc_ind << endl_ind <<
		"if(self::isLoggedIn()) return $_SESSION['" << _singular << "'];" << endl_ind <<
		"return NULL;" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind << endl_ind <<

		//now groups have canonical priority order, we can make this work
		"public static function currentGroup() {" << inc_ind << endl_ind;

		vector<string*> allGroups = Permission::getAllGroups();

		for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
			phpFile << "if(self::hasGroup('" << **group << "')) return '" << **group << "';" << endl_ind;
		}

		phpFile << dec_ind << endl_ind << "}" << endl_ind << endl_ind <<

		"public static function setSecurityContext($groupList) {" << inc_ind << endl_ind <<
		"if($_SESSION[\"" << _singular << "SecurityContextGroups\"] == $groupList) return;" << endl_ind <<
		"$_SESSION[\"" << _singular << "SecurityContextGroups\"] = $groupList;" << endl_ind <<
		"unset($_SESSION[\"" << _singular << "SecurityContextCache\"]);" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind << endl_ind <<

		"public static function echoSecure($text) {" << inc_ind << endl_ind <<
		"if(!isset($_SESSION[\"" << _singular << "SecurityContextCache\"])) {" << inc_ind << endl_ind <<
		"$_SESSION[\"" << _singular << "SecurityContextCache\"] = self::hasGroups($_SESSION[\"" <<
			_singular << "SecurityContextGroups\"]);" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind <<
		"if($_SESSION[\"" << _singular << "SecurityContextCache\"]) echo $text;" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind << endl_ind <<


		"public static function hasGroup($group) {" << inc_ind << endl_ind <<
		"if(self::isLoggedIn() && !self::current()->SecurityGroup->isnull()) {" << inc_ind << endl_ind <<
		"if(array_key_exists($group, self::current()->SecurityGroup->rev) && " <<
		"self::current()->SecurityGroup->val == self::current()->SecurityGroup->rev[$group]) return true;" << endl_ind <<
		"else if($group=='_self'&&preg_match('~^(.+)?/cms/" << _plural << "/(detail|validate)\\.php(\\?" <<
			_singular << "Id=\\d+)?$~', $_SERVER['REQUEST_URI'])&&((isset($_GET['" << _singular <<
			"Id'])&&$_GET['" << _singular << "Id']==self::current()->Id->val)||(isset($_POST['" <<
			_singular << "Id'])&&$_POST['" << _singular << "Id']==self::current()->Id->val))) return true;" << dec_ind << endl_ind <<
		"} else if($group=='_anon') return true;" << endl_ind <<
		"return false;" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind <<


		"public static function hasGroups($groupList) {" << inc_ind << endl_ind <<
		"$group = strtok($groupList, ' ');" << endl_ind <<
		"while($group) {" << inc_ind << endl_ind <<
		"if(self::hasGroup($group)) return true;"
		"$group = strtok(' ');" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind <<
		"return false;" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind <<
		dec_ind << endl_ind << "}" << endl_ind;


}

void UserTable::writeModelFromPost(ofstream& phpFile, UserTable* userTable) {
	phpFile << "public function fromPost() {" << inc_ind << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelFromPost(phpFile, userTable);
	}

	phpFile << dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}

void UserTable::writeModelAJAXLogin() {
	if(!Target::assertPath((SITESPATH + _schema->getSite()->getName() + "/www/cms/" + _plural + "/").c_str())) {
		_statusMessage = "Target generate error: couldn't create directory.";
		_isOk = false;
		return;
	}

	string phpFileName = Target::nativePath(SITESPATH + _schema->getSite()->getName() +
		"/www/cms/" + _plural + "/startlogin.php");
	ofstream phpFile;
	phpFile.open(phpFileName.c_str(), ios_base::trunc);

	if(!phpFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
		_isOk = false;
		if (phpFile.is_open()) phpFile.close();
		return;
	}

	phpFile << "<?php" << inc_ind << endl_ind <<
		"require_once('cmsame/" <<  _schema->getSite()->getName() << "/" << _schema->getSite()->getSiteName() << ".php');" << endl_ind <<
		"if(isset($_GET['" << _singular << "Username'])) {" << inc_ind << endl_ind <<
		"$token = \\cmsame\\crypto::salt();" << endl_ind <<
		"$_SESSION['" << _singular << "LoginToken'] = [strval($_GET['" << _singular << "Username']), new DateTime(), $token];" << endl_ind;
/*
	if(_pwHashClientSide) {
		//send salt:token
		phpFile << "$fake = \\cmsame\\crypto::salt($_GET['" << _singular << "Username'] . '" << Crypto::getRandomPassword() << "');" << endl_ind <<
			"$_SESSION[\"" << _singular << "\"] = new " << _singular << "();" << endl_ind <<
			"$_SESSION[\"" << _singular << "\"]->Groups->set('_cmsame');" << endl_ind <<
			"$pt = new " << _singular << "Search();" << endl_ind <<
			"$pt->Username->set($_GET['" << _singular << "Username']);" << endl_ind <<
			"$pt->UsernameCom = Compare::Equal;" << endl_ind <<
			"$user = " << _singular << "::search($pt);" << endl_ind <<
			"unset($_SESSION[\"" << _singular << "\"]);" << endl_ind <<
			"if(count($user)==1) echo $user[0]->Password->salt;" << endl_ind <<
			"else echo $fake;" << endl_ind <<
			"echo ':' . $token;" << dec_ind << endl_ind;

	} else {
		//just send token*/
		phpFile << "echo $token;" << dec_ind << endl_ind;

//	}

	phpFile << "}" << dec_ind << endl_ind <<
		"?>" << endl_ind;

/*
	phpFile << "<?php" << inc_ind << endl_ind <<
		"require_once('cmsame/" <<  _schema->getSite()->getName() << "/" << _schema->getSite()->getSiteName() << ".php');" << endl_ind <<
		"if(isset($_GET['" << _singular << "Username'])) {" << inc_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"?>" << endl_ind;
*/
	phpFile.close();
}

void UserTable::writeCmsAccessHandler(ofstream& phpFile) {
	phpFile << "<?php" << inc_ind << endl_ind <<
		"require_once('cmsame/" <<  _schema->getSite()->getName() << "/" << _schema->getSite()->getSiteName() << ".php');" << endl_ind <<
		"if(isset($_POST[\"" << _singular << "Logout\"])) {" << inc_ind << endl_ind <<
		_singular << "::logout();" << endl_ind <<
		"header(\"Location: \" . $_SERVER['REQUEST_URI']);" <<  endl_ind <<
		"exit();" << dec_ind << endl_ind <<
		"} else if(isset($_POST[\"" << _singular << "Username\"]) && isset($_POST[\"" <<
		_singular << "Credentials\"])) {" << inc_ind << endl_ind <<
		_singular << "::login($_POST[\"" << _singular << "Username\"], $_POST[\"" <<
		_singular << "Credentials\"]);" << endl_ind <<
		"header(\"Location: \" . $_SERVER['REQUEST_URI']);" <<  endl_ind <<
		"exit();" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind << "?>" << endl_ind;
}

void UserTable::writeCmsAccountPanel(ofstream& phpFile, int dir) {

	string prefix = "";
	while(dir--) prefix += "../";

	phpFile << "<?php" << inc_ind << endl_ind <<
		"if(" << _singular << "::isLoggedIn()) {" << inc_ind << endl_ind <<
		"echo '<form id=\"logout\" action=\"" << prefix << "index.php\" method=\"post\">';" << endl_ind <<
		"echo '<div class=\"section\">';" << endl_ind <<

		"echo 'You are logged in as <strong>' . " << _singular <<
			"::current()->Username->val . '</strong>.<br /><br />';" << endl_ind <<
		"echo '<input type=\"hidden\" name=\"" << _singular << "Logout\"></input>';" << endl_ind <<
		"echo '<a href=\"" << prefix << _plural << "/detail.php?" << _singular << _idColumn->getName() << "=' . " <<
			_singular << "::current()->" << _idColumn->getName() << "->val . '\" class=\"button\">Edit Profile</a>';" <<
		"echo '<input type=\"submit\" value=\"Logout\" class=\"button\"></input>';" << endl_ind <<
		"echo '</div></form>';" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind << "?>" << endl_ind;
}

void UserTable::writeCmsAccessScript(ofstream& phpFile) {
	phpFile << "<script type=\"text/javascript\">" << inc_ind << endl_ind <<
		"$(document).ready(function(){" << inc_ind << endl_ind <<
		"$('form#login').on('submit', function(e) {" << inc_ind << endl_ind <<
		"var form = this;" << endl_ind <<
		"e.preventDefault();" << endl_ind <<
		"var un = $('input#" << _singular << "Username').val();" << endl_ind <<
		"var pw = $('input#" << _singular << "Password').val();" << endl_ind <<
		"$.ajax({method:'GET'," << inc_ind << endl_ind <<
		"url:'" << _plural << "/startlogin.php?" <<
			_singular << "Username='+un," << endl_ind <<
		"success: function(d, s, j){ " << inc_ind << endl_ind;

	if(_pwHashClientSide) {
		phpFile << "var hash = blake2s128(d + pw);" << endl_ind <<
			"var creds = un + ':' + hash + ':' + pw + ':' + d;" << endl_ind;
	} else {
		phpFile << "var creds = un + ':' + pw + ':' + d;" << endl_ind;
	}



		//openpgp.js
//		"openpgp.initWorker({path:'js/openpgp.worker.min.js'});" << endl_ind <<
//		"openpgp.config.aead_protect = true;" << endl_ind <<
//		"var key = openpgp.key.readArmored($('input#" << _singular << "PublicKey').val()).keys;" << endl_ind <<
//		"openpgp.encrypt({data: username + ':' + $('input#" << _singular <<
//			"Password').val() + ':' + d, publicKeys: key}).then(function(encrypted) {" << inc_ind << endl_ind <<
//		"$('input#" << _singular << "Credentials').val(encrypted.data);" << endl_ind <<
//		"$('input#" << _singular << "Password').val('');" << endl_ind <<
//		"});" << dec_ind << endl_ind <<
	phpFile << "$('input#" << _singular << "Credentials').val(creds);" << endl_ind <<
		"form.submit();" << dec_ind << endl_ind <<
		"}});" << dec_ind << endl_ind <<
		"});" << dec_ind << endl_ind <<
		"});" << dec_ind << endl_ind <<
		"</script>" << endl_ind;
}

void UserTable::writeCmsAccessForm(ofstream& phpFile) {
	phpFile << "<?php" << inc_ind << endl_ind <<
		"if(!" << _singular << "::isLoggedIn()) {" << inc_ind << endl_ind <<
		"echo '<form id=\"login\" action=\"index.php\" method=\"post\">';" << endl_ind <<
		"echo '<div class=\"section\">';" << endl_ind <<
		"echo '<h3>Access Control</h3>';" << endl_ind <<
		"echo 'You are not logged in.<br /><br />';" << endl_ind <<
		"echo '<div class=\"field\"><div class=\"label\"><label>Username:</label></div>" <<
			"<div class=\"input\"><input type=\"text\" id=\"" << _singular <<
		"Username\" name=\"" << _singular << "Username\" class=\"text\"></input></div></div>';" << endl_ind <<
		"echo '<div class=\"field\"><div class=\"label\"><label>Password:</label></div>" <<
			"<div class=\"input\"><input type=\"password\" id=\"" << _singular <<
		"Password\" name=\"" << _singular << "Password\" class=\"password text\"></input></div></div>';" << endl_ind <<
		"echo '<input type=\"hidden\" id=\"" << _singular << "Credentials\" name=\"" <<
			_singular << "Credentials\"></input>';" << endl_ind <<
		"echo '<input type=\"submit\" value=\"Login\" class=\"button\"></input>';" << endl_ind <<
		"echo '</div></form>';" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind << "?>" << endl_ind;

}
/// SQL

void UserTable::writeProcLocate(ofstream& sqlFile, string& group) {
	if(group=="_cmsame") {

	//_cmsame: two modes
	//	username in, pw salt or fake out
	//	username, pw hash and salt in, all other fields or nothing out
//

		bool first;

		if(!_permission->hasGroup(group, cmsame::VIEW)) return;
		sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." << group << _singular <<
			"Locate(LocateUsername VARCHAR(256), LocatePassword VARCHAR(32), LocatePasswordSalt VARCHAR(32)";

		writeSqlRefParam(sqlFile, group);

		sqlFile << ")" << endl_ind <<
			"BEGIN" << inc_ind << endl_ind <<
			"DECLARE SqlSelect TEXT;" << endl_ind <<
			"DECLARE SqlSwitch TEXT;" << endl_ind <<
			"SET SqlSelect = 'SELECT ";

		first = true;
		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			if((*column)==_usernameColumn) continue;
			if((*column)==_passwordColumn) continue;
			first = !(*column)->writeSqlSelect(sqlFile, group, first) && first;
		}

		sqlFile << "';" << endl_ind;

		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			(*column)->writeSqlSelectRef(sqlFile, group);
		}

		sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' FROM " << _schema->getSite()->getSiteName() << "." << _plural << " WHERE " <<
			_singular << "Username = ', QUOTE(LocateUsername), ' AND " << 
			_singular << "Password = ', QUOTE(LocatePassword), ' AND " <<
			_singular << "PasswordSalt = ', QUOTE(LocatePasswordSalt));";

		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			(*column)->writeSqlJoin(sqlFile, group);
		}

		sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' LIMIT 1');" << endl_ind;


		sqlFile << "SET SqlSwitch = IF(ISNULL(LocatePassword) AND ISNULL(LocatePasswordSalt), " << endl_ind <<
			"CONCAT('(SELECT ''=============HIDDEN============='' AS " << _singular << "Password, " << 
			_singular << "PasswordSalt FROM " << _schema->getSite()->getSiteName() <<
			"." << _plural << " WHERE " << _singular << "Username = ', QUOTE(LocateUsername), ' LIMIT 1) " <<
			"UNION (SELECT ''=============HIDDEN============='' AS " << _singular << "Password, " <<
			"LEFT(TO_BASE64(UNHEX(MD5(CONCAT(', QUOTE(LocateUsername), ',''" << Crypto::getRandomPassword() << 
			"'')))), 16) AS " << _singular << "PasswordSalt) LIMIT 1'), SqlSelect);" << endl_ind;

		sqlFile << "SET @Statement = SqlSwitch;" << endl_ind <<
			"PREPARE Query FROM @Statement;" << endl_ind <<
			"EXECUTE Query;" << dec_ind << endl_ind <<
			"END |" << endl_ind;

	} else if(group=="_self") { //TODO: redundant, remove

	//_self: one mode
	//	don't return real pw hash

		bool first;

		if(!_permission->hasGroup(group, cmsame::VIEW)) return;
		sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." << group << _singular <<
			"Locate(Locate" << _idColumn->getName() <<  " INT";

		writeSqlRefParam(sqlFile, group);

		sqlFile << ")" << endl_ind <<
			"BEGIN" << inc_ind << endl_ind <<
			"DECLARE SqlSelect TEXT;" << endl_ind <<
			"SET SqlSelect = 'SELECT ";

		first = true;
		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			if((*column)==_passwordColumn) {
				//select dummy PW but real salt
				if(first) first=false;
				else sqlFile << ", ";
				sqlFile << "''=============HIDDEN============='' AS " << _singular << (*column)->getName() <<
					", " << _singular << (*column)->getName() << "Salt";

			} else {
				first = !(*column)->writeSqlSelect(sqlFile, group, first) && first;
			}
		}

		sqlFile << "';" << endl_ind;

		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			(*column)->writeSqlSelectRef(sqlFile, group);
		}

		sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' FROM " << _schema->getSite()->getSiteName() << "." << _plural << "');" << endl_ind;

		//JOIN
		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			(*column)->writeSqlJoin(sqlFile, group);
		}

		sqlFile  << "SET SqlSelect = CONCAT(SqlSelect, ' WHERE " <<
			_plural << "." << _singular << _idColumn->getName() << " = ', Locate" << _idColumn->getName() <<
			", ' GROUP BY " << _plural << "." << _singular << _idColumn->getName() <<  " LIMIT 0, 1');" << endl_ind <<
			"SET @Statement = SqlSelect;" << endl_ind <<
			"PREPARE Query FROM @Statement;" << endl_ind <<
			"EXECUTE Query;" << dec_ind << endl_ind <<
			"END |" << endl_ind;

	} else Table::writeProcLocate(sqlFile, group);
}
void UserTable::writeProcSearch(ofstream& sqlFile, string& group, bool debug) {
	if(group=="_cmsame"||group=="_self") return;
	else Table::writeProcSearch(sqlFile, group, debug);
}
void UserTable::writeProcUpdate(ofstream& sqlFile, string& group, bool debug) {
	if(group=="_self") {
		string idName = "Locate" + _idColumn->getName();

		if(!_permission->hasGroup(group, cmsame::EDIT)) return;

		if(debug) {
			sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				group << _singular << "UpdateDebug(Locate" << _idColumn->getName() << " INT";
		} else {
			sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << "." <<
				group << _singular << "Update(Locate" << _idColumn->getName() << " INT";
		}

		//extra params
		sqlFile << ", Locate" << _usernameColumn->getName() << " VARCHAR(256), Locate" << 
		_passwordColumn->getName() << " VARCHAR(32), Locate" << 
			_passwordColumn->getName() << "Salt VARCHAR(32), Locate" <<
			_groupColumn->getName() << " TINYINT";


		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			if(*column != _idColumn) {
				(*column)->writeSqlUpdateParam(sqlFile, group);
			}
		}

		sqlFile << ")" << endl_ind << "BEGIN" << inc_ind << endl_ind <<
			"DECLARE SqlUpdate TEXT;" << endl_ind <<
			"DECLARE ValueParam VARCHAR(4096);" << endl_ind <<
			"SET SqlUpdate = '';" << endl_ind;

		for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
			if(*column != _idColumn) {
				(*column)->writeSqlUpdateSet(sqlFile, group);
			}
		}

		sqlFile << "SET SqlUpdate = CONCAT('UPDATE " << _schema->getSite()->getSiteName() << "." << _plural << 
			" SET ', SqlUpdate, ' WHERE " << _singular << _idColumn->getName() << " = ', Locate" << _idColumn->getName() << 
			", ' AND " << _singular << _usernameColumn->getName() << " = ', QUOTE(Locate" << _usernameColumn->getName() << 
			"), ' AND " << _singular << _passwordColumn->getName() << " = ', QUOTE(Locate" << _passwordColumn->getName() << 
			"), ' AND " << _singular << _passwordColumn->getName() << "Salt = ', QUOTE(Locate" << _passwordColumn->getName() << 
			"Salt), ' AND " << _singular << _groupColumn->getName() << " = ', Locate" << _groupColumn->getName() << ", ';');" << endl_ind;

		if(debug) {
			sqlFile << "SELECT SqlUpdate;" << dec_ind << endl_ind <<
				"END |" << endl_ind;
		} else {
			sqlFile << "SET @Statement = SqlUpdate;" << endl_ind <<
				"PREPARE Query FROM @Statement;" << endl_ind <<
				"EXECUTE Query;" << endl_ind;
			//if a column is oneof mirroring oneof, need to update.

			for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
				(*column)->writeSqlUpdateMirror(sqlFile, group);
			}

			sqlFile << dec_ind << endl_ind <<
				"END |" << endl_ind;
		}

	} else Table::writeProcUpdate(sqlFile, group, debug);
}


/// MODEL

void UserTable::writeModelLoad(ofstream& phpFile, UserTable* userTable)
{
	phpFile << "public function load(";

	bool first=true;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		first= !(*column)->writeModelOpenParam(phpFile, first) && first;
	}

	phpFile << ") {" << inc_ind << endl_ind;

	phpFile << "if(!" << userTable->getSingular() << "::hasGroups('";
	_permission->writeViewGroups(phpFile);
	phpFile << "')) return false;" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) $pn = '" << **group << _singular << "Locate';" << endl_ind;
	}

	//////
	writeModelLoadQuery(phpFile, userTable);

	phpFile << "$rows = " << _schema->getSite()->getSiteName() << "::read_assoc($query);" << endl_ind <<
		"if(count($rows) != 1) return false;" << endl_ind;

	phpFile << "if($pn=='_cmsame" << _singular << "Locate') { " << inc_ind << endl_ind <<
		"if(!$this->" << _idColumn->getName() << "->isnull()||$this->" << 
		_usernameColumn->getName() << "->isnull()) return;" << endl_ind <<
		"if($this->" << _passwordColumn->getName() << "->isnull()) {" << inc_ind << endl_ind <<
	//	username in, pw salt or fake out
		"$this->" << _passwordColumn->getName() << "->set([$rows[0]['" << _singular << _passwordColumn->getName() << 
		"'], $rows[0]['" << _singular << _passwordColumn->getName() << "Salt']]);" << dec_ind << endl_ind <<
		"} else {" << inc_ind << endl_ind;
	//	username, pw hash and salt in, all other fields or nothing out
	//unroll fromRow
	phpFile << "$row=$rows[0];$alias='';" << endl_ind;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		if((*column)==_passwordColumn||(*column)==_usernameColumn) continue;
		(*column)->writeModelFromRow(phpFile, userTable);
	}

	phpFile << "}" << dec_ind << endl_ind << 
		"} else $this->fromRow($rows[0]);" << endl_ind;


	phpFile << "return true;" << dec_ind << endl_ind << "}" << endl_ind << endl_ind;
}


void UserTable::writeModelLoadQuery(ofstream& phpFile, UserTable* userTable) {
	//_cmsame: two modes
	phpFile << "if($pn=='_cmsame" << _singular << "Locate') { " << inc_ind << endl_ind <<
		"if(!$this->" << _idColumn->getName() << "->isnull()||$this->" << 
		_usernameColumn->getName() << "->isnull()) return;" << endl_ind <<
		"if($this->" << _passwordColumn->getName() << "->isnull()) {" << inc_ind << endl_ind <<
	//	username in, pw salt or fake out
		"$query = 'CALL ' . $pn . '(' . \\cmsame\\getsql::sql($this->" << _usernameColumn->getName() << 
		") . ', NULL, NULL)';" << dec_ind << endl_ind <<
		"} else {" << inc_ind << endl_ind <<
	//	username, pw hash and salt in, all other fields or nothing out
		"$query = 'CALL ' . $pn . '('  . \\cmsame\\getsql::sql($this->" << _usernameColumn->getName() << 
		") . ', ' . \\cmsame\\getsql::sql($this->" << _passwordColumn->getName() << 
		") . ', ' . \\cmsame\\getsql::sql($this->" << _passwordColumn->getName() << 
		", 1, 'Salt');" << endl_ind;

	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{
		(*column)->writeModelOpenValue(phpFile, userTable);
	}

	phpFile << "$query .= ');';" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind << 
		"} else {" << inc_ind << endl_ind; //_self has different behaviour but same footprint so ok
	phpFile << "if($this->" << _idColumn->getName() << "->isnull()) return;" << endl_ind;
	Table::writeModelLoadQuery(phpFile, userTable);
	phpFile << dec_ind << endl_ind << "}" << endl_ind;

}

void UserTable::writeModelUpdateQuery(ofstream& phpFile, UserTable* userTable) {
	//_cmsame doesn't have permission anyway
	//_self: where && username, pw, group match
	phpFile << "if($pn=='_self" << _singular << "Update') { " << inc_ind << endl_ind;

	//extra WHERE args
	//password ones need to be $_POST['OldUserPassword'] and old salt, which has been
	//overwritten by frompost
	phpFile << "$query = 'CALL ' . $pn . '(' . \\cmsame\\getsql::sql($this->" << _idColumn->getName() <<
		") . ', ' . \\cmsame\\getsql::sql($this->" << _usernameColumn->getName() << 
		") . ', ' . \\cmsame\\getsql::sql($this->old" << _passwordColumn->getName() << 
		") . ', ' . \\cmsame\\getsql::sql($this->old" << _passwordColumn->getName() << 
		", 1, 'Salt') . ', ' . \\cmsame\\getsql::sql($this->" << _groupColumn->getName() <<
                ");" << endl_ind;

	//same as normal now
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(*column!=_idColumn)
			(*column)->writeModelUpdateValue(phpFile, userTable); 
	}

	phpFile << "$query .= ');';error_log($query);" << dec_ind << endl_ind;


	phpFile << "} else {" << inc_ind << endl_ind;
	Table::writeModelUpdateQuery(phpFile, userTable);
	phpFile << dec_ind << endl_ind << "}" << endl_ind;
}


/// CMS

void UserTable::writeAccessControl(ofstream& phpFile, UserTable* userTable)
{

	phpFile << "<\?php " << inc_ind << endl_ind <<
		"require_once('cmsame/" <<  _schema->getSite()->getName() << "/" << _schema->getSite()->getSiteName() << ".php');" << endl_ind <<
		"require_once('cmsame/" <<  _schema->getSite()->getName() << "/cms.php');" << endl_ind <<
		"if(!" << userTable->getSingular() << "::isLoggedIn()) { header('location: ../');die(); }" << endl_ind;

	phpFile << "if(!" << userTable->getSingular() << "::hasGroups('";
		_permission->writeViewGroups(phpFile);
		phpFile << "')) { header('location: ../');die(); }" << endl_ind;


 	phpFile << dec_ind << endl_ind << "?>" << endl_ind;
}

///////////////////
/// CMSAMETABLE ///
///////////////////

CmsameTable::CmsameTable(Schema* schema) : Table(schema) {
	_singular = "CmsameDatum";
	_plural = "CmsameData";
	_description = "Cmsame system data. You should never see this text.";

	_columns.push_back(new Column(this, "Key", "Key", cmsame::text, 256, 0, false, true));
	_columns.push_back(new Column(this, "Value", "Value", cmsame::text, 256, 0, false, false));

	Record* record;
	record = new Record(this);
	record->addField("Key", "CmsameVersion");
	record->addField("Value", VERSION);
	_records.push_back(record);

	record = new Record(this);
	record->addField("Key", "SchemaVersion");
	record->addField("Value", _schema->getVersion());
	_records.push_back(record);

}

void CmsameTable::writeCreateProc(ofstream& sqlFile, bool debug) {
	sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << ".CmsameVersion()" << endl_ind <<
		"BEGIN" << inc_ind << endl_ind <<
		"SELECT " << _singular << "Value FROM " << _schema->getSite()->getSiteName() << "." << _plural <<
		" WHERE " << _singular << "Key = 'CmsameVersion';" << dec_ind << endl_ind <<
		"END |" << endl_ind;
	sqlFile << "CREATE PROCEDURE " << _schema->getSite()->getSiteName() << ".SchemaVersion()" << endl_ind <<
		"BEGIN" << inc_ind << endl_ind <<
		"SELECT " << _singular << "Value FROM " << _schema->getSite()->getSiteName() << "." << _plural <<
		" WHERE " << _singular << "Key = 'SchemaVersion';" << dec_ind << endl_ind <<
		"END |" << endl_ind;

}

void CmsameTable::writeDropProc(ofstream& sqlFile, bool debug) {
	sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << ".CmsameVersion;" << endl_ind;
	sqlFile << "DROP PROCEDURE " << _schema->getSite()->getSiteName() << ".SchemaVersion;" << endl_ind;
}

void CmsameTable::writeGrantExec(ofstream& sqlFile, string& user, bool canEdit, bool debug) {
	sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << ".CmsameVersion TO " << user << ";" << endl_ind;
	sqlFile << "GRANT EXECUTE ON PROCEDURE " << _schema->getSite()->getSiteName() << ".SchemaVersion TO " << user << ";" << endl_ind;
}

void CmsameTable::writeModel(ofstream& phpFile, UserTable* userTable, bool close) { }
void CmsameTable::writeSearchModel(ofstream& phpFile) { }
void CmsameTable::writeCmsMenu(ofstream& phpFile, UserTable* userTable) { }
void CmsameTable::writeCmsIndex(bool requireSSL, UserTable* userTable) { }
void CmsameTable::writeCmsDetail(bool requireSSL, UserTable* userTable) { }
