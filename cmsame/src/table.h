#ifndef __table__
#define __table__

//c++
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "column.h"
#include "record.h"
#include "permission.h"

namespace cmsame {
	enum HashFunc {
		md5, blake2s
	};
}

using namespace std;
using namespace rapidxml;

class Schema;
class UserTable;

class Table : public Element
{
	public:
		Table(xml_node<>* node, Schema* schema);
		Table(string singular, string plural, string description, Schema* schema, Permission* permission);
		virtual ~Table();
		virtual void linkupRefTarget();
		virtual void linkupMirror();
		virtual vector<Table*>* linkupLinkTable();
		virtual void linkupRecords();
		Column* getColumn(const string& name);
		Column* getIdColumn();
		Column* getNameColumn();
		Schema* getSchema();
		Permission* getPermission();
		bool isSingular(string& singular);
		bool isPlural(string& plural);
		const string& getSingular();
		const string& getPlural();
		bool hasColumnType(cmsame::ColumnType type);
		size_t getRecordId(string& name);
		void addColumn(Column* column);
		void addRecord(Record* record);
		bool isSkeleton();

		//sql
		virtual void writeCreate(ofstream& sqlFile);
		virtual void writeDrop(ofstream& sqlFile);
		virtual void writeCreateProc(ofstream& sqlFile, bool debug = false);
		virtual void writeDropProc(ofstream& sqlFile, bool debug = false);

		virtual void writeGrantExec(ofstream& sqlFile, string& user, bool canEdit, string group, bool debug = false);
		virtual void writeAddForeignKey(ofstream& sqlFile);
		virtual void writeDropForeignKey(ofstream& sqlFile);
		void writeSqlSelect(ofstream& sqlFile, string group, string alias="");	//not a full procedure. column list for other tables which refer to this
		void writeSqlTailJoin(ofstream& sqlFile, string group, string alias);

		//model
		virtual void writeModel(ofstream& phpFile, UserTable* userTable, double phpVersion, bool close = true);
		virtual void writeSearchModel(ofstream& phpFile);

		//cms
		virtual void writeCmsMenu(ofstream& phpFile, UserTable* userTable);
		virtual void writeCmsIndex(bool requireSSL, UserTable* userTable);
		virtual void writeCmsDetail(bool requireSSL, UserTable* userTable);
		virtual void writeCmsAJAXPreview(bool requireSSL);
		virtual void writeCmsAJAXValidate(bool requireSSL, UserTable* userTable);
		virtual void writeCmsAJAXResize(bool requireSSL, UserTable* userTable);
	protected:
		Table(Schema* schema);

		//sql subsections - put these inside group loop
		virtual void writeProcLocate(ofstream& sqlFile, string& group);
		virtual void writeProcSearch(ofstream& sqlFile, string& group, bool debug = false);
		virtual void writeProcUpdate(ofstream& sqlFile, string& group, bool debug = false);

		void writeProcInsert(ofstream& sqlFile, string& group);
		void writeProcDelete(ofstream& sqlFile, string& group);
		void writeProcCount(ofstream& sqlFile, string& group);
		void writeSqlRefParam(ofstream& sqlFile, string group);	//not a full procedure. end of argument list for locate & search


		//model subsections

		virtual void writeModelFromPost(ofstream& phpFile, UserTable* userTable);
		virtual void writeModelLoadQuery(ofstream& phpFile, UserTable* userTable);
		virtual void writeModelUpdateQuery(ofstream& phpFile, UserTable* userTable);
		virtual void writeModelSearch(ofstream& phpFile, UserTable* userTable);
		virtual void writeModelLoad(ofstream& phpFile, UserTable* userTable);

		void writeModelSearchQuery(ofstream& phpFile, UserTable* userTable);
		void writeModelUpdate(ofstream& phpFile, UserTable* userTable);
		void writeModelObject(ofstream& phpFile);
		void writeModelConstruct(ofstream& phpFile, bool search = false);
		void writeModelFromRow(ofstream& phpFile, UserTable* userTable);
		void writeModelSave(ofstream& phpFile);
		void writeModelInsert(ofstream& phpFile, UserTable* userTable);
		void writeModelDelete(ofstream& phpFile, UserTable* userTable);
//		void writeModelValues(ofstream& phpFile);
		void writeModelCount(ofstream& phpFile, UserTable* userTable);
		void writeModelCleanText(ofstream& phpFile);
		void writeModelRenderText(ofstream& phpFile);
		void writeModelValidate(ofstream& phpFile);
		void writeModelDefault(ofstream& phpFile);

		//cms subsections
//		void writeUIIncludes(ofstream& phpFile);	//css & js - MOVED TO TargetCms
		virtual void writeAccessControl(ofstream& phpFile, UserTable* userTable);

		Schema* _schema;
		vector<Column*> _columns;
		Column* _idColumn;
		vector<Record*> _records;
		Permission* _permission;
		Column* _nameColumn;

		string _singular;
		string _plural;
		string _description;

};

class UserTable : public Table
{
	public:
		UserTable(xml_node<>* node, Schema* schema);
		virtual void writeModel(ofstream& phpFile, UserTable* userTable, double phpVersion, bool close = true);
		virtual void writeModelAJAXLogin();
		virtual void writeCmsAccessHandler(ofstream& phpFile); //same output whether logged in or not
		virtual void writeCmsAccountPanel(ofstream& phpFile, int dir=0); //does nothing if NOT logged in
		virtual void writeCmsAccessScript(ofstream& phpFile);	//always renders script but never run if logged in
		virtual void writeCmsAccessForm(ofstream& phpFile);	//does nothing if logged in
		cmsame::HashFunc getHashFunc();
		bool isHashClientSide();

	protected:

		virtual void writeProcLocate(ofstream& sqlFile, string& group);
		virtual void writeProcSearch(ofstream& sqlFile, string& group, bool debug = false);
		virtual void writeProcUpdate(ofstream& sqlFile, string& group, bool debug = false);

		virtual void writeModelFromPost(ofstream& phpFile, UserTable* userTable);
		virtual void writeModelLoadQuery(ofstream& phpFile, UserTable* userTable);
		virtual void writeModelUpdateQuery(ofstream& phpFile, UserTable* userTable);
//		virtual void writeModelSearch(ofstream& phpFile, UserTable* userTable);
		virtual void writeModelLoad(ofstream& phpFile, UserTable* userTable);

		virtual void writeAccessControl(ofstream& phpFile, UserTable* userTable);

	private:
		//string _gpgPassphrase;
		cmsame::HashFunc _pwHashFunc = cmsame::blake2s;
		bool _pwHashClientSide = true;
		Column* _usernameColumn;
		Column* _passwordColumn;
		Column* _groupColumn;

};

class CmsameTable : public Table
{
	public:
		CmsameTable(Schema* schema);
		virtual void writeModel(ofstream& phpFile, UserTable* userTable, bool close = true);
		virtual void writeSearchModel(ofstream& phpFile);

		//sql
		virtual void writeCreateProc(ofstream& sqlFile, bool debug = false);
		virtual void writeDropProc(ofstream& sqlFile, bool debug = false);

		virtual void writeGrantExec(ofstream& sqlFile, string& user, bool canEdit, bool debug = false);


		//cms
		virtual void writeCmsMenu(ofstream& phpFile, UserTable* userTable);
		virtual void writeCmsIndex(bool requireSSL, UserTable* userTable);
		virtual void writeCmsDetail(bool requireSSL, UserTable* userTable);
};

#endif
