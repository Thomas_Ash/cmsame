//c++
#include <string>
#include <sstream>
#include <random>
#include <iomanip>
//#include <iostream> //for testing

//me
#include "crypto.h"

using namespace std;


//PASSWORDS

const char* Crypto::names[] = {"Alice",	"Adam", "Beth",	"Bernard", "Claire", "Chris", "Diana", "Daniel",	//8
	"Emily", "Eric", "Felicity", "Frank", "Gemma", "Gary", "Hannah", "Harry", "Imogen", "Igor",		//18
	"Josie", "Jonathan", "Katie", "Keith", "Lucy", "Liam", "Michelle", "Melvin", "Naomi", "Nigel",		//28
	"Olga", "Oscar", "Pamela", "Peter", "Rachael", "Robert", "Sarah", "Simon", "Tamara", "Terry",		//38
	"Ursula", "Umberto", "Virginia", "Victor", "Wendy", "Walter"};						//44

const char* Crypto::attitudes[] = {"Aggressive",	"Altruistic",	"Abnormal",	"Ambitious",		//4
	"Boastful",	"Benevolent",	"Bashful",	"Brainy",	"Competent",	"Clever",		//10
	"Caring",	"Creepy",	"Diplomatic",	"Dramatic",	"Distressed",	"Devoted",
	"Effective",	"Educational",	"Earnest",	"Eccentric",	"Fearless",	"Fanciful",
	"Flaccid",	"Flexible",	"Glorious",	"Gossipy",	"Gentle",	"Gothic",
	"Horrible",	"Handsome",	"Helpful",	"Hesitant",	"Important",	"Immature",
	"Innocent",	"Impolite",	"Logical",	"Lively",	"Lonely",	"Lucky",
	"Modern",	"Musical",	"Modest",	"Magical", 	"Noble",	"Naughty",
	"Nervous",	"Nonchalant",	"Optimistic",	"Opinionated",	"Obedient",	"Obsessive",
	"Protective",	"Popular",	"Peaceful",	"Pompous",	"Reliable",	"Reckless",
	"Realistic",	"Reflective",	"Serious",	"Stunning",	"Sensitive",	"Sarcastic",
	"Terrible",	"Thirsty",	"Tactful",	"Talented",	"Unique",	"Unhelpful",
	"Unlucky",	"Untidy",	"Vindictive",	"Versatile",	"Vulnerable",	"Vulgar",		//76
	"Wasteful",	"Wacky",	"Wholesome",	"Wonderful"};						//80

const char* Crypto::animals[] = {"Aardvark",	"Albatross",	"Alligator",	"Amoeba",			//4
	"Anteater",	"Starfish",	"Anaconda",	"Pixie",	"Antelope",	"Skeleton",		//10
	"Stingray",	"Earthworm",	"Badger",	"Buzzard",	"Bullfrog",	"Butterfly",
	"Baboon",	"Computer",	"Caveman",	"Earthworm",	"Beaver",	"Goblin",
	"Calculator",	"Barnacle",	"Cheetah",	"Cuckoo",	"Crocodile",	"Caterpillar",
	"Chinchilla",	"Chicken",	"Catfish",	"Cuttlefish",	"Capybara",	"Cassowary",
	"Chameleon",	"Centipede",	"Dolphin",	"Duckling",	"Dinosaur",	"Dragonfly",
	"Donkey",	"Motorbike",	"Helicopter",	"Robot",	"Dingo",	"Vampire",
	"Sasquatch",	"Zombie",	"Elephant",	"Eagle",	"Skateboard",	"Peanut",
	"Ferret",	"Flamingo",	"Armchair",	"Banana",	"Giraffe",	"Goose",
	"Gecko",	"Grasshopper",	"Gopher",	"Carpet",	"Toaster",	"Microscope",
	"Gerbil",	"Insect",	"Paperclip",	"Raisin",	"Hedgehog",	"Heron",
	"Apple",	"Watermelon",	"Hamster",	"Hummingbird",	"Saxophone",	"Laptop",
	"Kangaroo",	"Kingfisher",	"Lampshade",	"Toothbrush",	"Llama",	"Lapwing",
	"Lizard",	"Lobster",	"Mongoose",	"Magpie",	"Statue",	"Sandwich",
	"Ocelot",	"Ostrich",	"Otter",	"Octopus",	"Porcupine",	"Penguin",
	"Piranha",	"Pencil",	"Panther",	"Pelican",	"Mollusc",	"Squid",
	"Possum",	"Parrot",	"Crustacean",	"Whale",	"Platypus",	"Pigeon",
	"Goldfish",	"Radiator",	"Squirrel",	"Sparrow",	"Rattlesnake",	"Spider",
	"Sloth",	"Seagull",	"Tarantula",	"Scorpion",	"Tiger",	"Toucan",
	"Tortoise",	"Lunchbox",	"Tapir",	"Turkey",	"Turtle",	"Pizza",
	"Weasel",	"Woodpecker",	"Scarecrow",	"Mushroom",	"Wombat",	"Tomato",
	"Orange",	"Potato",	"Wolverine",	"Walrus",	"Wallaby",	"Zebra"};		//136

#ifdef __linux__
	random_device Crypto::rd;
	mt19937 Crypto::gen = mt19937(Crypto::rd());
#endif
#ifdef _WIN32
	mt19937 Crypto::gen = mt19937(time(NULL));
#endif

string Crypto::getRandomPassword() {
	string pw = "";
	const char* base64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

	uniform_int_distribution<int> dist(0,63);

	for(int i=0;i<16;i++) {
		pw += base64[dist(gen)];
	}
	return pw;
}

string Crypto::getMnemonicPassword() {
	string pw = "";
	uniform_int_distribution<int> distname(0,43);
	uniform_int_distribution<int> distatti(0,79);
	uniform_int_distribution<int> distanim(0,135);
	uniform_int_distribution<int> distl33t(0,5);
	size_t i;
	bool hasO, hasI, hasE, hasS, hasT, hasA, hasl33t = false;
	char rep;
	do {
		pw = "";
		hasO = hasI = hasE = hasS = hasT = hasA = hasl33t = false;
		pw+=Crypto::names[distname(gen)];
		pw+="The";
		pw+=Crypto::attitudes[distatti(gen)];
		pw+=Crypto::animals[distanim(gen)];
		for(i=0;i<pw.length();i++) {
			if(pw[i]=='o'||pw[i]=='O') hasO = true;
			if(pw[i]=='i'||pw[i]=='I') hasI = true;
			if(pw[i]=='e'||pw[i]=='E') hasE = true;
			if(pw[i]=='s'||pw[i]=='S') hasS = true;
			if(pw[i]=='t'||pw[i]=='T') hasT = true;
			if(pw[i]=='a'||pw[i]=='A') hasA = true;
		}
	} while(!hasO&&!hasI&&!hasE&&!hasS&&!hasT&&!hasA);

	while(!hasl33t) {
		switch(distl33t(gen)) {
			case 0:
				if(!hasO) break;
				for(i=0;i<pw.length();i++) {
					if(pw[i]=='o'||pw[i]=='O') pw[i]='0';
				}
				hasl33t=true;
				break;
			case 1:
				if(!hasI) break;
				rep = (distl33t(gen)<3?'1':'!');
				for(i=0;i<pw.length();i++) {
					if(pw[i]=='i'||pw[i]=='I') pw[i]=rep;
				}
				hasl33t=true;
				break;
			case 2:
				if(!hasE) break;
				for(i=0;i<pw.length();i++) {
					if(pw[i]=='e'||pw[i]=='E') pw[i]='3';
				}
				hasl33t=true;
				break;
			case 3:
				if(!hasS) break;
				rep = (distl33t(gen)<3?'5':'$');
				for(i=0;i<pw.length();i++) {
					if(pw[i]=='s'||pw[i]=='S') pw[i]=rep;
				}
				hasl33t=true;
				break;
			case 4:
				if(!hasT) break;
				for(i=0;i<pw.length();i++) {
					if(pw[i]=='t'||pw[i]=='T') pw[i]='+';
				}
				hasl33t=true;
				break;
			case 5:
				if(!hasA) break;
				for(i=0;i<pw.length();i++) {
					if(pw[i]=='a'||pw[i]=='A') pw[i]='@';
				}
				hasl33t=true;
				break;
		}
	}

	return pw;
}

bool Crypto::isStrongPassword(string pw) {
	if(pw.length() < 8) return false;
	bool hasUC=false;
	bool hasLC=false;
	bool hasDT=false;
	for(string::iterator i=pw.begin(); i<pw.end(); ++i) {
		if(*i>='A'&&*i<='Z') hasUC=true;
		else if(*i>='a'&&*i<='z') hasLC=true;
		else if(*i>='0'&&*i<='9') hasDT=true;
	}
	if(hasUC&&hasLC) return true;
	if(hasUC&&hasDT) return true;
	if(hasDT&&hasLC) return true;
	return false;
}

//BLAKE2S128


const uint32_t Crypto::blake_init[8] = {
	0x6A09E667, 0xBB67AE85, 0x3C6EF372, 0xA54FF53A,
	0x510E527F, 0x9B05688C, 0x1F83D9AB, 0x5BE0CD19};

const uint8_t Crypto::blake_sigma[10][16] = {
	{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15},
	{14, 10, 4, 8, 9, 15, 13, 6, 1, 12, 0, 2, 11, 7, 5, 3},
	{11, 8, 12, 0, 5, 2, 15, 13, 10, 14, 3, 6, 7, 1, 9, 4},
	{7, 9, 3, 1, 13, 12, 11, 14, 2, 6, 5, 10, 4, 0, 15, 8},
	{9, 0, 5, 7, 2, 4, 10, 15, 14, 1, 11, 12, 6, 8, 3, 13},
	{2, 12, 6, 10, 0, 11, 8, 3, 4, 13, 7, 5, 15, 14, 1, 9},
	{12, 5, 1, 15, 14, 13, 4, 10, 0, 7, 6, 3, 9, 2, 8, 11},
	{13, 11, 7, 14, 12, 1, 3, 9, 5, 0, 15, 4, 8, 6, 2, 10},
	{6, 15, 14, 9, 11, 3, 0, 8, 12, 2, 13, 7, 1, 4, 10, 5},
	{10, 2, 8, 4, 7, 6, 1, 5, 15, 11, 9, 14, 3, 12, 13, 0}};

const uint8_t Crypto::blake_seq[8][4] = {
	{0, 4, 8, 12}, {1, 5, 9, 13}, {2, 6, 10, 14}, {3, 7 ,11, 15},
	{0, 5, 10, 15}, {1, 6, 11, 12}, {2, 7, 8, 13}, {3, 4, 9, 14}};

string Crypto::blake2s128(string data) {

//	cout << "Hash of '" << data << "':\n";

	blake_ctx ctx;
	size_t i;	//loop var

	//init
	for(i=0;i<8;i++) ctx.state[i] = blake_init[i];

	//no key
	ctx.state[0] ^= 0x01010010;
	ctx.tl=0;
	ctx.th=0;
	ctx.ptr=0;

	for(i=0; i<64; i++) ctx.buf[i]=0;
	//init done

	//update
	for(i=0; i<data.length(); i++) {
		if(ctx.ptr==64) {
			ctx.tl += 64;
			if(ctx.tl < 64) ctx.th++;
			//compress
			blake_comp(ctx, false);
			ctx.ptr=0;
		}
		ctx.buf[ctx.ptr++]=(uint8_t)(data[i]);
	}
	//update done

	//final
	ctx.tl += ctx.ptr;
	if(ctx.tl < ctx.ptr) ctx.th++;

	while(ctx.ptr<64) ctx.buf[ctx.ptr++] = 0;
	blake_comp(ctx, true);

	stringstream hash;
	for(i=0;i<16;i++) {
//		cout << "Hash index: " << dec << i << "\n";
//		cout << "State index: " << dec << (i >> 2) << "\n";
//		cout << "Shift bits: " << dec << (8 * (i & 3)) << "\n";
		hash << hex << setfill('0') << setw(2) << (int)((ctx.state[i >> 2] >> (8 * (i & 3))) & 0xFF);
	}

//	cout << hash.str() << "\n";

	return hash.str();
}

void Crypto::blake_comp(blake_ctx& ctx, bool last) {
//	cout << "Compressing block.\nState: ";
	size_t i, j;
/*	for(i=0;i<8;i++) cout << hex << setfill('0') << setw(8) << ctx.state[i] << " ";
	cout << "\nBuffer: ";
	for(i=0;i<64;i++) {
		cout << hex << setfill('0') << setw(2) << (int)(ctx.buf[i]) << " ";
		if(i%16==15) cout << "\n        ";
	}
	cout << "\nTotal lo:" << hex << setfill('0') << setw(8) << ctx.tl;
	cout << "\nTotal hi:" << hex << setfill('0') << setw(8) << ctx.th;
	cout << "\nPointer:" << dec << ctx.ptr << "\n";  */
	uint32_t v[16];
	uint32_t m[16];

	for(i=0;i<8;i++) {
		v[i] = ctx.state[i];
		v[i+8] = blake_init[i];
	}

	v[12] ^= ctx.tl;
	v[13] ^= ctx.th;
	if(last) v[14] = ~v[14];
/*
	cout << "v: ";
	for(i=0;i<16;i++) {
		cout << hex << setfill('0') << setw(8) << v[i] << " ";
		if(i==7) cout<<"\n   ";
	}
	cout<<"\n";
*/

	for(i=0;i<16;i++) {
		//m[i] = ctx.buf[4*i ... 4*i+3], but reverse byte order
		m[i] = 0;
		for(j=0;j<4;j++) {
			m[i] = m[i] << 8;
			m[i] = m[i] ^ ctx.buf[4*i+3-j];
		}
	}
/*
	cout << "m: ";
	for(i=0;i<16;i++) {
		cout << hex << setfill('0') << setw(8) << m[i] << " ";
		if(i==7) cout<<"\n   ";
	}
	cout<<"\n";
*/

	for(i=0;i<10;i++) {
		for(j=0;j<8;j++) {
			//mixing function
			//params: blake_seq[j][0...3], m[blake_sigma[i][j*2]], m[blake_sigma[i][j*2+1]]
			v[blake_seq[j][0]] = v[blake_seq[j][0]] + v[blake_seq[j][1]] + m[blake_sigma[i][j*2]];
			v[blake_seq[j][3]] = blake_rrot(v[blake_seq[j][3]] ^ v[blake_seq[j][0]], 16);
			v[blake_seq[j][2]] = v[blake_seq[j][2]] + v[blake_seq[j][3]];
			v[blake_seq[j][1]] = blake_rrot(v[blake_seq[j][1]] ^ v[blake_seq[j][2]], 12);
			v[blake_seq[j][0]] = v[blake_seq[j][0]] + v[blake_seq[j][1]] + m[blake_sigma[i][j*2+1]];
			v[blake_seq[j][3]] = blake_rrot(v[blake_seq[j][3]] ^ v[blake_seq[j][0]], 8);
			v[blake_seq[j][2]] = v[blake_seq[j][2]] + v[blake_seq[j][3]];
			v[blake_seq[j][1]] = blake_rrot(v[blake_seq[j][1]] ^ v[blake_seq[j][2]], 7);
		}
	}

	for(i=0;i<8;i++) {
		ctx.state[i] ^= v[i] ^ v[i+8];
	}
}

uint32_t Crypto::blake_rrot(uint32_t in, size_t bits) {
	return (in >> bits) ^ (in << (32-bits));
}

//MD5

const uint32_t Crypto::md5_init[4] = {
	0x67452301, 0xefcdab89, 0x98badcfe, 0x10325476
};

const uint32_t Crypto::md5_sine[64] = {
	0xd76aa478, 0xe8c7b756, 0x242070db, 0xc1bdceee,
	0xf57c0faf, 0x4787c62a, 0xa8304613, 0xfd469501,
	0x698098d8, 0x8b44f7af, 0xffff5bb1, 0x895cd7be,
	0x6b901122, 0xfd987193, 0xa679438e, 0x49b40821,
	0xf61e2562, 0xc040b340, 0x265e5a51, 0xe9b6c7aa,
	0xd62f105d, 0x02441453, 0xd8a1e681, 0xe7d3fbc8,
	0x21e1cde6, 0xc33707d6, 0xf4d50d87, 0x455a14ed,
	0xa9e3e905, 0xfcefa3f8, 0x676f02d9, 0x8d2a4c8a,
	0xfffa3942, 0x8771f681, 0x6d9d6122, 0xfde5380c,
	0xa4beea44, 0x4bdecfa9, 0xf6bb4b60, 0xbebfbc70,
	0x289b7ec6, 0xeaa127fa, 0xd4ef3085, 0x04881d05,
	0xd9d4d039, 0xe6db99e5, 0x1fa27cf8, 0xc4ac5665,
	0xf4292244, 0x432aff97, 0xab9423a7, 0xfc93a039,
	0x655b59c3, 0x8f0ccc92, 0xffeff47d, 0x85845dd1,
	0x6fa87e4f, 0xfe2ce6e0, 0xa3014314, 0x4e0811a1,
	0xf7537e82, 0xbd3af235, 0x2ad7d2bb, 0xeb86d391
};

string Crypto::md5(string data) {

	md5_ctx ctx;
	size_t i;	//loop var

	//init
	for(i=0;i<4;i++) ctx.state[i] = md5_init[i];
	//init done

	//update
	for(i=0; i<data.length(); i++) {
		if(ctx.ptr==64) {
			md5_comp(ctx);
			ctx.ptr=0;
		}
		ctx.buf[ctx.ptr++]=(uint8_t)(data[i]);
	}
	//update done

	//final
	if(ctx.ptr==64) {
		md5_comp(ctx);
		ctx.ptr=0;
	}
	ctx.buf[ctx.ptr++]=0x80;

	while(ctx.ptr!=56) {
		if(ctx.ptr==64) {
			md5_comp(ctx);
			ctx.ptr=0;
		}
		ctx.buf[ctx.ptr++]=0x80;
	}

	uint64_t length = data.length();

	for(i=0;i<8;i++) {
		ctx.buf[ctx.ptr++]= length >> (i*8) & 0xff;
	}
	md5_comp(ctx);

	return string("NYI!!!");

}

void Crypto::md5_comp(md5_ctx& ctx) {
}
