//c++
#ifdef __linux__
        #include <ncurses.h>
	#define BACKSPACE 0x0107
#endif

#ifdef _WIN32
        #include <curses.h>
	#define BACKSPACE 0x0008
#endif

#include <string>
#include <cstdlib>
#include <ctime>
#include <cstring>

//me
#include "element.h"
#include "site.h"
#include "crypto.h"

using namespace std;

WINDOW *mywin;

void backspace()
{
	int y, x;
	noecho();
	nocbreak();
	getyx(mywin, y, x);
	move(y, x - 1);
	delch();
	cbreak();
	refresh();
}

void getpw(string* pw)
{
	int c;
	*pw="";
	do {
		c=getch();
		if(c>32&&c<127) {
			*pw=*pw+(char)c;
			addch('*');
			refresh();
		} else if(c==BACKSPACE&&pw->length()>0) {
			*pw=pw->substr(0, pw->length()-1);
			backspace();
		}
	} while (c!='\r'&&c!='\n');
}

string askpw(string username) {
	string pw, pwc;
	bool haspw=false;

	mywin=initscr();
	erase();
	raw();
	noecho();
	cbreak();
	keypad(stdscr, TRUE);
	while(!haspw) {
		printw("Enter password for %s: ", username.c_str());
		refresh();
	        getpw(&pw);

        	printw("\nConfirm password for %s: ", username.c_str());

	        getpw(&pwc);

        	if(pw==pwc) {
                	printw("\nSaved. Press a key to continue.\n");
			haspw=true;
	        } else {
        	        printw("\nPasswords do not match. Press a key to retry.\n");
	        }
	}
        refresh();
        getch();
        endwin();
	return pw;
}

string choosepw(string username, bool mnemonic) {
	string pw;
	int c;
	size_t i;
	bool haspw=false;

	mywin=initscr();
	erase();
	raw();
	noecho();
	cbreak();
	keypad(stdscr, TRUE);
	printw("Password for %s: ", username.c_str());
	while(!haspw) {
		pw = (mnemonic ? Crypto::getMnemonicPassword() : Crypto::getRandomPassword());
		printw("%s OK? (Y/n)", pw.c_str());
	        refresh();
		c=getch();
		if(c=='n') {
			for(i=0;i<pw.length() + 10;i++) {
				backspace();
			}
		} else {
                	printw("\nSaved. Press a key to continue.\n");
			haspw=true;
		}
	}
        refresh();
        getch();
        endwin();
	return pw;
}

int main(int argc, char *argv[]){

	if(argc < 2)
	{
		cout << "Syntax error: no site name specified." << endl;
		return 1;
	}

	#ifdef _WIN32
		cout << "Warning: fallback to pseudo-random generation in Windows." << endl;
	#endif

	int i,j;
	char c;
	string sitename, alias;
	bool anext=false;
	bool verbose=false;

	alias="";

	for(i=1; i<argc; i++) {
		if(argv[i][0]=='-') {
			j=1; 
			c=argv[i][j++];
			while (c) {
				if(c=='v'||c=='V') verbose=true;
				c=argv[i][j++];
			}
		} else if(strcmp(argv[i], "as")==0) {
			anext=true;
		} else {
			if(anext) 	alias = string(argv[i]);
			else		sitename = string(argv[i]);
			anext=false;
		}
	}

	if(alias.length()==0) alias = sitename;

	Site site (sitename, alias, verbose);
	site.process();
	cout << site.statusMessage() << endl;
	return site.isOk() ? 0 : 1;
}

/* Helper function to get a storage index in a stream */
int get_indent_index() {
	/* ios_base::xalloc allocates indices for custom-storage locations. These indices are valid for all streams */
	static int index = ios_base::xalloc();
	return index;
}

ios_base& inc_ind(ios_base& stream) {
	stream.iword(get_indent_index())++;
	return stream;
}

ios_base& dec_ind(ios_base& stream) {
	stream.iword(get_indent_index())--;
	if(stream.iword(get_indent_index()) < 0)
		stream.iword(get_indent_index()) = 0;
	return stream;
}

bool strPtrAlpha(string* a, string* b) {
	return(*a<*b);
}
