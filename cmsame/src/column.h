#ifndef __column__
#define __column__

//c++
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "permission.h"
#include "record.h"
#include "filters.h"

namespace cmsame
{
	enum ColumnType	{
		none, id, integer, decimal, datetime,
		bigtext, text, password, tokens, options,
		boolean, imageupload, fileupload,
		oneof, listof
	};
}

using namespace std;
using namespace rapidxml;

class Table;
class UserTable;

class Column : public Element
{
	public:
		Column(Table* table, xml_node<>* columnNode, string prefix="");
		Column(Table* table, const char* name, const char* description,
				cmsame::ColumnType type, long x = 0, long y = 0, bool null = true, bool browse = true);
		virtual ~Column();
		virtual Column* findByName(const string& name);
//		void appendPrefix(string& prefix);
		const string& getName();
		virtual bool hasType(cmsame::ColumnType type);
		bool refTargetIs(Table* table);
//		void addIncRef(Column* column);
		static Column* createIdColumn(Table* table);
		void addValue(string& value);	//for use w/ 2nd constructor
		void applyPermission(string appGroups, cmsame::Action action, cmsame::TriState appMode);
		bool permissionMatches(Permission* other);
		virtual void linkupRefTarget();
		virtual void linkupMirror();
		virtual vector<Table*>* linkupLinkTable(); //this may create the linktable(s)
		bool setRefTarget(Table* refTarget);  //return false if already in use
		bool setMirror(Column* mirror);  //return false if already in use
		Table* getRefTarget();
		Column* getMirror();
		Table* getLinkTable();
		ImageSize* getImageSizeByName(const string& name);

		//SQL
		virtual void writeSqlDefinition(ofstream& sqlFile);		//in initial CREATE TABLE script
		virtual void writeSqlIndex(ofstream& sqlFile);			//ditto
			//use this in locate and search instead of SELECT *: check permission
		virtual bool writeSqlSelect(ofstream& sqlFile, string group, bool first=false, string alias="");
		virtual void writeSqlSelectRef(ofstream& sqlFile, string group);
		virtual void writeSqlRefParam(ofstream& sqlFile, string group);
		virtual void writeSqlJoin(ofstream& sqlFile, string group);
		virtual void writeSqlTailJoin(ofstream& sqlFile, string group, string alias);
		virtual void writeSqlSearchParam(ofstream& sqlFile);
		virtual void writeSqlSearchWhere(ofstream& sqlFile);
		virtual void writeSqlUpdateParam(ofstream& sqlFile, string group);
		virtual void writeSqlUpdateSet(ofstream& sqlFile, string group);
		virtual bool writeSqlInsertParam(ofstream& sqlFile, string group, bool first=false);
		virtual bool writeSqlName(ofstream& sqlFile, string group, bool first=false);
		virtual bool writeSqlParam(ofstream& sqlFile, string group, bool first=false);
		virtual void writeSqlAddForeignKey(ofstream& sqlFile);	//need to do this after CREATE, as ALTER; target may not exist yet
		virtual void writeSqlDropForeignKey(ofstream& sqlFile);
		virtual void writeSqlRecordInsert(stringstream& name, stringstream& value, string data, Record* record);
		virtual void writeSqlCreateCount(ofstream& sqlFile, string& group);
		virtual void writeSqlDropCount(ofstream& sqlFile);
		virtual void writeGrantExec(ofstream& sqlFile, string& user, string group);
		virtual void writeSqlInsertMirror(ofstream& sqlFile, string group);
		virtual void writeSqlUpdateMirror(ofstream& sqlFile, string group);
		virtual void writeSqlDeleteMirror(ofstream& sqlFile, string group);

		//MODEL
		virtual void writeModelConfig(ofstream& phpFile);		//new-style - passed into php ctor
		virtual void writeModelConstruct(ofstream& phpFile, bool search = false);		//set field to null in PHP constructor
		virtual void writeModelFromPost(ofstream& phpFile, UserTable* userTable);		//cleaned value from html postback
		virtual void writeModelFromRow(ofstream& phpFile, UserTable* userTable);		//cleaned value from sql select
		virtual bool writeModelUpdateParam(ofstream& phpFile, bool first=false);
		virtual void writeModelUpdateValue(ofstream& phpFile, UserTable* userTable);
		virtual bool writeModelInsertValue(ofstream& phpFile, bool first=false);
		virtual void writeModelSearchOrder(ofstream& phpFile);
		virtual void writeModelSearchValue(ofstream& phpFile);
		virtual void writeModelCleanText(ofstream& phpFile);
		virtual void writeModelRenderText(ofstream& phpFile);
		virtual void writeModelValidate(ofstream& phpFile);
		virtual void writeModelDefault(ofstream& phpFile);
		virtual void writeModelInsertFile(ofstream& phpFile);
		virtual void writeModelUpdateFile(ofstream& phpFile);
		virtual void writeModelDeleteFile(ofstream& phpFile);
		virtual void writeModelMirror(ofstream& phpFile);
		virtual void writeModelCount(ofstream& phpFile, UserTable* userTable);
		virtual bool writeModelOpenParam(ofstream& phpFile, bool first=false);
		virtual void writeModelOpenValue(ofstream& phpFile, UserTable* userTable);

		//CMS
		virtual bool writeCmsIndexCount(ofstream& phpFile, UserTable* userTable, bool first);
		virtual bool writeCmsIndexFilter(ofstream& phpFile, UserTable* userTable, bool first);
		virtual void writeCmsIndexHeader(ofstream& phpFile, UserTable* userTable);	//html table header on 'browse'
		virtual void writeCmsIndexContent(ofstream& phpFile, UserTable* userTable);	//html table content on 'browse'
		virtual void writeCmsDetailField(ofstream& phpFile, UserTable* userTable);	//label & field on add / edit / detail
		virtual void writeCmsAJAXPreview(bool requireSSL);			//separate page
		virtual void writeCmsAJAXResize(bool requireSSL, UserTable* userTable);			//separate page

	protected:
		Column() {}
		Column(Table* table);
		void addSubColumns();						//called after either constructor

		const string getSqlBaseType();					//simple approximate type, nullable. for search params
		const string getCmsameType();
		const string getCssClass();
		const string getSqlType();					//actual type in table definition
		const string getSqlName();		//for refcolumns, need to append "TargetId" on to user-spec'd column name

		Table* _table;
		Permission* _permission;
		string _name;
		string _description;
		cmsame::ColumnType _type;
		long _x;
		long _y;
		bool _null;
		bool _browse;
		vector<string*> _values;
		vector<cmsame::CleanStep> _cleanSteps;
		cmsame::RenderMode _renderMode;		//multiple rendermodes more trouble than they're worth
		vector<string*> _mimeTypes;
		vector<ImageSize*> _imageSizes;
		vector<Column*> _subColumns;
		string _refTargetName;		//hang on to this til linkup so we can get _refTarget
		Table* _refTarget;
		string _mirrorName;		// "
		Column* _mirror;
		string _linkTableSingular;
		string _linkTablePlural;
		Table* _linkTable;
//		vector<Column*> _incRefs;
};

class Section : public Column
{
	public:
		Section(Table* table, xml_node<>* sectionNode, string prefix="");
		virtual ~Section();
		virtual Column* findByName(const string& name);
		virtual bool hasType(cmsame::ColumnType type);
		virtual void linkupRefTarget();
		virtual void linkupMirror();
		virtual vector<Table*>* linkupLinkTable();

		//SQL
		virtual void writeSqlDefinition(ofstream& sqlFile);		//in initial CREATE TABLE script
		virtual void writeSqlIndex(ofstream& sqlFile);			//ditto
		virtual bool writeSqlSelect(ofstream& sqlFile, string group, bool first=false, string alias="");
		virtual void writeSqlSelectRef(ofstream& sqlFile, string group);
		virtual void writeSqlRefParam(ofstream& sqlFile, string group);
		virtual void writeSqlJoin(ofstream& sqlFile, string group);
		virtual void writeSqlTailJoin(ofstream& sqlFile, string group, string alias);
		virtual void writeSqlSearchParam(ofstream& sqlFile);
		virtual void writeSqlSearchWhere(ofstream& sqlFile);
		virtual void writeSqlUpdateParam(ofstream& sqlFile, string group);
		virtual void writeSqlUpdateSet(ofstream& sqlFile, string group);
		virtual bool writeSqlInsertParam(ofstream& sqlFile, string group, bool first = false);
		virtual bool writeSqlName(ofstream& sqlFile, string group, bool first = false);
		virtual bool writeSqlParam(ofstream& sqlFile, string group, bool first = false);
		virtual void writeSqlAddForeignKey(ofstream& sqlFile);	//need to do this after CREATE, as ALTER; target may not exist yet
		virtual void writeSqlDropForeignKey(ofstream& sqlFile);
		virtual void writeSqlCreateCount(ofstream& sqlFile, string& group);
		virtual void writeSqlDropCount(ofstream& sqlFile);
		virtual void writeGrantExec(ofstream& sqlFile, string& user, string group);
		virtual void writeSqlInsertMirror(ofstream& sqlFile, string group);
		virtual void writeSqlUpdateMirror(ofstream& sqlFile, string group);
		virtual void writeSqlDeleteMirror(ofstream& sqlFile, string group);

		//MODEL
		virtual void writeModelConstruct(ofstream& phpFile, bool search = false);		//set field to null in PHP constructor
		virtual void writeModelFromPost(ofstream& phpFile, UserTable* userTable);		//cleaned value from html postback
		virtual void writeModelFromRow(ofstream& phpFile, UserTable* userTable);		//cleaned value from sql select
		virtual bool writeModelUpdateParam(ofstream& phpFile, bool first=false);
		virtual void writeModelUpdateValue(ofstream& phpFile, UserTable* userTable);
		virtual bool writeModelInsertValue(ofstream& phpFile, bool first=false);
		virtual void writeModelSearchOrder(ofstream& phpFile);
		virtual void writeModelSearchValue(ofstream& phpFile);
		virtual void writeModelCleanText(ofstream& phpFile);
		virtual void writeModelRenderText(ofstream& phpFile);
		virtual void writeModelValidate(ofstream& phpFile);
		virtual void writeModelDefault(ofstream& phpFile);
		virtual void writeModelInsertFile(ofstream& phpFile);
		virtual void writeModelUpdateFile(ofstream& phpFile);
		virtual void writeModelDeleteFile(ofstream& phpFile);
		virtual void writeModelMirror(ofstream& phpFile);
		virtual void writeModelCount(ofstream& phpFile, UserTable* userTable);
		virtual bool writeModelOpenParam(ofstream& phpFile, bool first=false);
		virtual void writeModelOpenValue(ofstream& phpFile, UserTable* userTable);

		//CMS
		virtual bool writeCmsIndexCount(ofstream& phpFile, UserTable* userTable, bool first);
		virtual bool writeCmsIndexFilter(ofstream& phpFile, UserTable* userTable, bool first);
		virtual void writeCmsIndexHeader(ofstream& phpFile, UserTable* userTable);	//html table header on 'browse'
		virtual void writeCmsIndexContent(ofstream& phpFile, UserTable* userTable);	//html table content on 'browse'
		virtual void writeCmsDetailField(ofstream& phpFile, UserTable* userTable);	//label & field on add / edit / detail
		virtual void writeCmsAJAXPreview(bool requireSSL);			//separate page
		virtual void writeCmsAJAXResize(bool requireSSL, UserTable* userTable);			//separate page

	protected:
		vector<Column*> _columns;




};

#endif
