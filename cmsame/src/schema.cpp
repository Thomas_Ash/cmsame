//c++
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

//c
#include <cstring>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "common.h"
#include "schema.h"
#include "table.h"
#include "site.h"
#include "target.h"
#include "crypto.h"
#include "permission.h"

using namespace std;
using namespace rapidxml;

Schema::Schema(xml_node<>* schemaNode, Site* site) : Element()
{
	_site = site;
	_permission = new Permission();
	//_userTable = NULL;

	//get attributes
	for (xml_attribute<>* attr = schemaNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "version") == 0) _version = attr->value();
	}

	if(_version.length()==0)
	{
			_statusMessage = "Schema load error: unversioned schema";
			_isOk = false;
	}

	Table* table;

	//load child nodes - 'table' and special table types
	for (xml_node<>* node = schemaNode->first_node(); node; node = node->next_sibling())
	{
		if(strcmp(node->name(), "table") == 0)
		{
			table = new Table(node, this);
		} else if(strcmp(node->name(), "usertable") == 0)
		{
			table = new UserTable(node, this);
		} else {
			continue;
		}

		if(!table->isOk())
		{
			_statusMessage = table->statusMessage();
			_isOk = false;
			return;
		}
		_tables.push_back(table);
	}

	_tables.push_back(new CmsameTable(this));


}

Schema::~Schema()
{
	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) { delete *table; }
	delete _permission;
}

bool Schema::isVersion(string& version)
{
	return (version == _version);
}

const string& Schema::getVersion() {
	return _version;
}

Table* Schema::getTableByPlural(string& plural) {
	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) 	{
		if ((*table)->isPlural(plural)) return *table;
	}
	return NULL;
}

Table* Schema::getTableBySingular(string& singular) {
	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) 	{
		if ((*table)->isSingular(singular)) return *table;
	}
	return NULL;
}

//UserTable* Schema::getUserTable()
//{
//	return _userTable;
//}

Permission* Schema::getPermission() {
	return _permission;
}

void Schema::addTable(Table* table) {
	for(vector<Table*>::iterator t = _tables.begin(); t != _tables.end(); ++t) {
		if(*t==table) {
			if(_site->isVerbose()) cout << "Warning: 'new' table pointer " << table << " is already in schema." << endl;
			return;
		}
	}
	if(_site->isVerbose()) cout << "Adding table pointer " << table << " to schema." << endl;
	_tables.push_back(table);
}

Site* Schema::getSite() {
	return _site;
}

void Schema::linkup() {

	if(_site->isVerbose()) cout << "Schema::linkup" << endl;

	vector<Table*>* linkTables = new vector<Table*>();
	vector<Table*>* res=NULL;

	_permission->sortGroups();

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) 	{
		(*table)->linkupRefTarget();
		if(!(*table)->isOk())	{
			_statusMessage = (*table)->statusMessage();
			_isOk = false;
			delete linkTables;
			return;
		}
	}
	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) 	{
		(*table)->linkupMirror();
		if(!(*table)->isOk())	{
			_statusMessage = (*table)->statusMessage();
			_isOk = false;
			delete linkTables;
			return;
		}
	}
	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) 	{
		res = (*table)->linkupLinkTable();
		if(!(*table)->isOk())	{
			_statusMessage = (*table)->statusMessage();
			_isOk = false;
			if(res) delete res;
			delete linkTables;
			return;
		}
		linkTables->insert(linkTables->end(), res->begin(), res->end());
		delete res;
	}

	if(_site->isVerbose()) cout << _tables.size() << " tables in schema." << endl;
	_tables.insert(_tables.end(), linkTables->begin(), linkTables->end());
	if(_site->isVerbose()) cout << linkTables->size() << " linktables were added to the schema." << endl;
	delete linkTables;

	if(_site->isVerbose())	{
		cout << _tables.size() << " tables in schema now:" << endl;
		for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) 	{
			cout << "pointer=" << (*table) << ", ";
			cout << "name=" << (*table)->getPlural() << endl;
		}
		cout << "Table linkup ok." << endl;
	}

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) 	{
		(*table)->linkupRecords();
		if(!(*table)->isOk())	{
			_statusMessage = (*table)->statusMessage();
			_isOk = false;
			return;
		}
	}
	if(_site->isVerbose())	cout << "Record linkup ok." << endl;
}

///////////
/// SQL ///
///////////

void Schema::writeCreate(string webHost) {
	writeCreateBase();
	writeCreateBody();
	writeCreateUser(webHost);
	writeCreateProc();
	writeCreateAll();
}

void Schema::writeDrop(string webHost) {
	writeDropBase();
	writeDropBody();
	writeDropUser(webHost);
	writeDropProc();
	writeDropAll();
}

void Schema::writeCreateBase() {

	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH +
		_site->getName() + "/scripts/sql/CreateBase.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	sqlFile << "CREATE DATABASE " << _site->getSiteName() << inc_ind << endl_ind <<
				"DEFAULT CHARACTER SET utf8" << endl_ind <<
				"DEFAULT COLLATE utf8_general_ci;" << dec_ind << endl_ind;

	sqlFile.close();

}
void Schema::writeCreateBody() {

	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH +
		_site->getName() + "/scripts/sql/CreateBody.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	stringstream ss;


	sqlFile << "USE " << _site->getSiteName() << ";" << endl_ind << endl_ind;

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
		(*table)->writeCreate(sqlFile);
		if(!(*table)->isOk()) {
			_statusMessage = (*table)->statusMessage();
			_isOk = false;
			sqlFile.close();
			return;
		}
	}

	//now all tables have been created, we can add FKs
	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
		(*table)->writeAddForeignKey(sqlFile);
		if(!(*table)->isOk()) {
			_statusMessage = (*table)->statusMessage();
			_isOk = false;
			sqlFile.close();
			return;
		}
	}

	sqlFile.close();

}

void Schema::writeCreateUser(string webHost) {


	string user, sSite, sGroup;

	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/CreateUser.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	sSite = _site->getSiteName().substr(0, 12);

	//group-specific users
	vector<string*> groups = Permission::getAllGroups();
	sSite = _site->getName().substr(0, 6);
	for(vector<string*>::iterator group = groups.begin();group!=groups.end();++group) {
		sGroup = (*group)->substr(0, 6);
		user = "'" + sSite + sGroup + "read'@'" + webHost + "'";
		sqlFile << "CREATE USER " << user << " IDENTIFIED BY '" << readPassword(**group) << "';" << endl_ind;

		for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
			(*table)->writeGrantExec(sqlFile, user, false, **group);//, true);
		}

		user = "'" + sSite + sGroup + "edit'@'" + webHost + "'";
		sqlFile << "CREATE USER " << user << " IDENTIFIED BY '" << editPassword(**group) << "';" << endl_ind;

		for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
			(*table)->writeGrantExec(sqlFile, user, true, **group);//, true);
		}
	}

}

void Schema::writeCreateProc() {

	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/CreateProc.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	sqlFile << "DELIMITER |" << endl_ind;

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
		(*table)->writeCreateProc(sqlFile, true);
	}

	sqlFile << "DELIMITER ;" << endl_ind;

}

void Schema::writeCreateAll() {
	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/CreateAll.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}



	sqlFile << "SOURCE " << SITESPATH << _site->getName() << "/scripts/sql/CreateBase.sql;" << endl_ind <<
	"SOURCE " << SITESPATH << _site->getName() << "/scripts/sql/CreateBody.sql;" << endl_ind <<
	"SOURCE " << SITESPATH << _site->getName() << "/scripts/sql/CreateProc.sql;" << endl_ind <<
		"SOURCE " << SITESPATH << _site->getName() << "/scripts/sql/CreateUser.sql;" << endl_ind;

	sqlFile.close();
}

void Schema::writeDropBase()
{
	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/DropBase.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	sqlFile << "DROP DATABASE " << _site->getSiteName() << ";" << endl_ind;

	sqlFile.close();
}
void Schema::writeDropBody()
{
	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/DropBody.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
		(*table)->writeDropForeignKey(sqlFile);
	}
	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
		(*table)->writeDrop(sqlFile);
	}

	sqlFile.close();
}

void Schema::writeDropUser(string webHost) {

	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/DropUser.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);
	string user, sSite, sGroup;

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}
	//group-specific users
	vector<string*> groups = Permission::getAllGroups();
	sSite = _site->getName().substr(0, 6);
	for(vector<string*>::iterator group = groups.begin();group!=groups.end();++group) {
		sGroup = (*group)->substr(0, 6);
		user = "'" + sSite + sGroup + "read'@'" + webHost + "'";
		sqlFile << "DROP USER " << user << ";" << endl_ind;
		user = "'" + sSite + sGroup + "edit'@'" + webHost + "'";
		sqlFile << "DROP USER " << user << ";" << endl_ind;
	}

}

void Schema::writeDropProc() {

	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/DropProc.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table) {
		(*table)->writeDropProc(sqlFile, true);
	}

}

void Schema::writeDropAll() {
	Target::assertPath((SITESPATH + _site->getName() + "/scripts/sql/").c_str());
	string sqlFileName = Target::nativePath(SITESPATH + _site->getName() + "/scripts/sql/DropAll.sql");
	ofstream sqlFile;
	sqlFile.open(sqlFileName.c_str(), ios_base::trunc);

	if(!sqlFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + sqlFileName + "\".";
		_isOk = false;
		if (sqlFile.is_open()) sqlFile.close();
		return;
	}

	sqlFile << "SOURCE " << SITESPATH << _site->getName() << "/scripts/sql/DropUser.sql;" << endl_ind <<
		"SOURCE " << SITESPATH << _site->getName() << "/scripts/sql/DropProc.sql;" << endl_ind <<
		"SOURCE " << SITESPATH << _site->getName() << "/scripts/sql/DropBase.sql;" << endl_ind;

	sqlFile.close();
}

/////////////
/// MODEL ///
/////////////

void Schema::writeModel(string dbHost, UserTable* userTable, double phpVersion)
{
	string sSite, sGroup, user;
	string path = "/model/include/";
	Target::assertPath((SITESPATH + _site->getName() + path).c_str());
	string phpFileName = Target::nativePath(SITESPATH + _site->getName() + path + _site->getSiteName() + ".php");
	ofstream phpFile;
	phpFile.open(phpFileName.c_str(), ios_base::trunc);

	if(!phpFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
		_isOk = false;
		if (phpFile.is_open()) phpFile.close();
		return;
	}

	phpFile << "<?php" << inc_ind << endl_ind <<
		"require_once('types.php');" << endl_ind <<
		"require_once('crypto.php');" << endl_ind <<
		"require_once('markdown.php');" << endl_ind <<
		"require_once('mdlite.php');" << endl_ind <<
		"session_start();" << endl_ind <<
		"if(!isset($_SESSION['REMOTE_ADDR']) || $_SESSION['REMOTE_ADDR'] != $_SERVER['REMOTE_ADDR'] || " <<
		"!isset($_SESSION['HTTP_USER_AGENT']) || $_SESSION['HTTP_USER_AGENT'] != $_SERVER['HTTP_USER_AGENT']) {" << inc_ind << endl_ind <<
		userTable->getSingular() << "::logout();" << endl_ind <<
		"$_SESSION['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];" << endl_ind <<
		"$_SESSION['HTTP_USER_AGENT'] = $_SERVER['HTTP_USER_AGENT'];" << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"class " << _site->getSiteName() << " {" << inc_ind << endl_ind <<
		"private static $_version = \"" << _version << "\";" << endl_ind <<
		"private static $_mysqli_read = [];" << endl_ind  <<
		"private static $_mysqli_edit = [];" << endl_ind  << endl_ind <<
		"public static function read_assoc($query) {" << inc_ind << endl_ind <<
		"if(!$res=self::mysqli_read()->multi_query($query)) {" << inc_ind << endl_ind <<
		"error_log(\"error on:\" . $query);" << endl_ind <<
		"error_log(self::mysqli_read()->error);" << endl_ind <<
		"return NULL;" << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"if($res = self::mysqli_read()->store_result()) {" << inc_ind << endl_ind <<
		"$data = [];" << endl_ind <<
		"while($row=$res->fetch_assoc()) array_push($data, $row);" << endl_ind <<
		"self::mysqli_read()->next_result();" << endl_ind <<
		"$res->free();" << endl_ind <<
		"return $data;" << dec_ind << endl_ind <<
		"} else {" << inc_ind << endl_ind <<
		"error_log(\"error on:\" . $query);" << endl_ind <<
		"error_log(self::mysqli_read()->error);" << endl_ind <<
		"return NULL;" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"}" << endl_ind <<

		//used for transactions which need write permission and also return data (i.e. insert)
		"public static function edit_assoc($query) {" << inc_ind << endl_ind <<
		"if(!$res=self::mysqli_edit()->multi_query($query)) {" << inc_ind << endl_ind <<
		"error_log(\"error on:\" . $query);" << endl_ind <<
		"error_log(self::mysqli_edit()->error);" << endl_ind <<
		"return NULL;" << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"if($res = self::mysqli_edit()->store_result()) {" << inc_ind << endl_ind <<
		"$data = [];" << endl_ind <<
		"while($row=$res->fetch_assoc()) array_push($data, $row);" << endl_ind <<
		"self::mysqli_edit()->next_result();" << endl_ind <<
		"$res->free();" << endl_ind <<
		"return $data;" << dec_ind << endl_ind <<
		"} else {" << inc_ind << endl_ind <<
		"error_log(\"error on:\" . $query);" << endl_ind <<
		"error_log(self::mysqli_edit()->error);" << endl_ind <<
		"return NULL;" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"}" << endl_ind <<

		//these are now security-group based
		"public static function mysqli_read() {" << inc_ind << endl_ind;

		vector<string*> groups = Permission::getAllGroups();
		sSite = _site->getName().substr(0, 6);
		for(vector<string*>::iterator group = groups.begin();group!=groups.end();++group) {
			sGroup = (*group)->substr(0, 6);
			user = sSite + sGroup + "read";

			if(group!=groups.begin()) phpFile << "} else ";
			phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) {" << inc_ind << endl_ind <<
			"if(!isset(self::$_mysqli_read['" << **group << "'])) {" << inc_ind << endl_ind <<
			"self::$_mysqli_read['" << **group << "'] = new mysqli('" << dbHost << "', '" <<
				user <<  "', '" << readPassword(**group) << "', '" <<
				_site->getSiteName() <<  "');" << endl_ind <<
			"if (self::$_mysqli_read['" << **group << "']->connect_errno) {" << inc_ind << endl_ind <<
			"error_log(\"Failed to connect to MySQL: \" . self::$_mysqli_read['" << **group << "']->connect_error);" << endl_ind <<
			"return NULL;" << dec_ind << endl_ind <<
			"}" << dec_ind << endl_ind <<
			"}" << endl_ind <<
			"return self::$_mysqli_read['" << **group << "'];" << dec_ind << endl_ind;
		}

		phpFile << "} else return NULL;" << dec_ind << endl_ind <<
			"}" << endl_ind << endl_ind <<

		"public static function mysqli_edit() {" << inc_ind << endl_ind;

		for(vector<string*>::iterator group = groups.begin();group!=groups.end();++group) {
			sGroup = (*group)->substr(0, 6);
			user = sSite + sGroup + "edit";

			if(group!=groups.begin()) phpFile << "} else ";
			phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group << "')) {" << inc_ind << endl_ind <<
			"if(!isset(self::$_mysqli_edit['" << **group << "'])) {" << inc_ind << endl_ind <<
			"self::$_mysqli_edit['" << **group << "'] = new mysqli('" << dbHost << "', '" <<
				user <<  "', '" << editPassword(**group) << "', '" <<
				_site->getSiteName() <<  "');" << endl_ind <<
			"if (self::$_mysqli_edit['" << **group << "']->connect_errno) {" << inc_ind << endl_ind <<
			"error_log(\"Failed to connect to MySQL: \" . self::$_mysqli_edit['" << **group << "']->connect_error);" << endl_ind <<
			"return NULL;" << dec_ind << endl_ind <<
			"}" << dec_ind << endl_ind <<
			"}" << endl_ind <<
			"return self::$_mysqli_edit['" << **group << "'];" << dec_ind << endl_ind;
		}

		phpFile << "} else return NULL;" << dec_ind << endl_ind <<
			"}" << dec_ind << endl_ind;



/*
		"if (is_null(self::$_mysqli_read)) {" << inc_ind << endl_ind <<
		"self::$_mysqli_read = new mysqli('" << dbHost << "', '" <<
			_site->getSiteName().substr(0, 12) <<  "Read', '" << readPassword("*old*") << "', '" <<
			_site->getSiteName() <<  "');" << endl_ind <<
		"if (self::$_mysqli_read->connect_errno) {" << inc_ind << endl_ind <<
		"echo \"Failed to connect to MySQL: \" . self::$_mysqli_read->connect_error;" << endl_ind <<
		"return NULL;" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"return self::$_mysqli_read;" << dec_ind << endl_ind <<
		phpFile << "}" << endl_ind << endl_ind <<

		"public static function mysqli_edit() {" << inc_ind << endl_ind <<
		"if (is_null(self::$_mysqli_edit)) {" << inc_ind << endl_ind <<
		"self::$_mysqli_edit = new mysqli('" << dbHost << "', '" <<
			_site->getSiteName().substr(0, 12) <<  "Edit', '" << editPassword("*old*") << "', '" <<
			_site->getSiteName() <<  "');" << endl_ind <<
		"if (self::$_mysqli_edit->connect_errno) {" << inc_ind << endl_ind <<
		"echo \"Failed to connect to MySQL: \" . self::$_mysqli_edit->connect_error;" << endl_ind <<
		"return NULL;" << dec_ind << endl_ind <<
		"}" << dec_ind << endl_ind <<
		"}" << endl_ind <<
		"return self::$_mysqli_edit;" << dec_ind << endl_ind <<
		"}" << endl_ind << dec_ind << endl_ind <<
*/

		phpFile << "}" << endl_ind;


	phpFile << "class Compare {" << inc_ind << endl_ind <<
		"const Equal = 1;" << endl_ind <<
		"const NotEqual = 2;" << endl_ind <<
		"const LessThan = 3;" << endl_ind <<
		"const LessThanOrEqual = 4;" << endl_ind <<
		"const GreaterThan = 5;" << endl_ind <<
		"const GreaterThanOrEqual = 6;" << endl_ind <<
		"const Like = 7;" << dec_ind << endl_ind <<
		"}" << endl_ind;

	phpFile << "class Order {" << inc_ind << endl_ind <<
		"const Ascending = 1;" << endl_ind <<
		"const Descending = 2;" << dec_ind << endl_ind <<
		"}" << endl_ind;

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table)
	{
		(*table)->writeModel(phpFile, userTable, phpVersion);
		(*table)->writeSearchModel(phpFile);
	}

	phpFile << dec_ind << endl_ind << "?>";

	phpFile.close();
}

///////////
/// CMS ///
///////////

void Schema::writeCms(bool requireSSL, UserTable* userTable)
{
	if (!Target::assertPath((SITESPATH + _site->getName() + "/www/upload/").c_str()))
	{
		_statusMessage = "Target generate error: couldn't create directory.";
		_isOk = false;
		return;
	}
	string htaFileName = Target::nativePath(SITESPATH + _site->getName() + "/www/upload/.htaccess");
	ofstream htaFile;
	htaFile.open(htaFileName.c_str(), ios_base::trunc);

	if(!htaFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + htaFileName + "\".";
		_isOk = false;
		if (htaFile.is_open()) htaFile.close();
		return;
	}

	htaFile << "php_flag engine off" << endl_ind;

	htaFile.close();

	if (!Target::assertPath((SITESPATH + _site->getName() + "/www/cms/").c_str()))
	{
		_statusMessage = "Target generate error: couldn't create directory.";
		_isOk = false;
		return;
	}

	string phpFileName = Target::nativePath(SITESPATH + _site->getName() + "/www/cms/index.php");
	ofstream phpFile;
	phpFile.open(phpFileName.c_str(), ios_base::trunc);

	if(!phpFile.good())
	{
		_statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
		_isOk = false;
		if (phpFile.is_open()) phpFile.close();
		return;
	}

	if(requireSSL) phpFile << "<?php if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on') {" <<
		"header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);" <<
		"die();} ?>" << endl_ind;


	phpFile << "<!DOCTYPE html>" << endl_ind <<
		"<html>" << inc_ind << endl_ind <<
		"<head>" << inc_ind << endl_ind;

	TargetCms::writeUIIncludes(phpFile);

	userTable->writeCmsAccessHandler(phpFile);
	userTable->writeCmsAccessScript(phpFile);

	phpFile << "<title>" << _site->getSiteDescription() << " | CMSame</title>" << endl_ind <<
		"<?php require_once 'cmsame/" << _site->getName() << "/" << _site->getSiteName() << ".php'; ?>" << dec_ind << endl_ind <<
		"</head>" << endl_ind <<
		"<body>" << inc_ind << endl_ind <<
		"<div class=\"wrapper\">" << inc_ind << endl_ind <<
		"<div class=\"header\">" << inc_ind << endl_ind;
	userTable->writeCmsAccountPanel(phpFile);
	phpFile << "<h1>" << _site->getSiteDescription() << "</h1>" << endl_ind <<
		"<h2>Main Menu</h2>" << dec_ind << endl_ind <<
		"</div>" << endl_ind <<
		"<div class=\"main\">" << inc_ind << endl_ind;

	//login / logout

	userTable->writeCmsAccessForm(phpFile);

	phpFile << "<?php" << inc_ind << endl_ind;

	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table)
	{
		(*table)->writeCmsMenu(phpFile, userTable);
	}

	phpFile << dec_ind << endl_ind << "?>" << dec_ind << endl_ind <<
		"</div>" << endl_ind <<
		"<div class=\"footer\">" << inc_ind << endl_ind;

	writeSign(phpFile);
	phpFile << "<br/>" << endl_ind;
	Target::writeSign(phpFile);

	phpFile << dec_ind << endl_ind << "</div>" << dec_ind << endl_ind <<
		"</div>" << dec_ind << endl_ind <<
		"</body>" << dec_ind << endl_ind <<
		"</html>" << endl_ind;

	phpFile.close();


	for(vector<Table*>::iterator table = _tables.begin(); table != _tables.end(); ++table)
	{
		(*table)->writeCmsIndex(requireSSL, userTable);
		(*table)->writeCmsDetail(requireSSL, userTable);
		(*table)->writeCmsAJAXPreview(requireSSL);
		(*table)->writeCmsAJAXValidate(requireSSL, userTable);
		(*table)->writeCmsAJAXResize(requireSSL, userTable);
	}
}


void Schema::writeSign(ofstream& file) {

	file << _site->getSiteName() << " " << _version;
}

string Schema::readPassword(string group) {
	for(map<string*, string*>::iterator readUser = _readUsers.begin(); readUser != _readUsers.end(); ++readUser) {
		if(strcmp(readUser->first->c_str(), group.c_str())==0) return *(readUser->second);
	}
	string pw = Crypto::getRandomPassword();
	_readUsers.insert(pair<string*, string*>(new string(group), new string(pw)));
	return pw;
}

string Schema::editPassword(string group) {
	for(map<string*, string*>::iterator editUser = _editUsers.begin(); editUser != _editUsers.end(); ++editUser) {
		if(strcmp(editUser->first->c_str(), group.c_str())==0) return *(editUser->second);
	}
	string pw = Crypto::getRandomPassword();
	_editUsers.insert(pair<string*, string*>(new string(group), new string(pw)));
	return pw;
}
