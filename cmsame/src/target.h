#ifndef __target__
#define __target__

//c++
#include <string>
#include <iostream>
#include <fstream>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "schema.h"

using namespace std;
using namespace rapidxml;

class Site;

class Target : public Element
{
	public:
		Target(xml_node<>* targetNode, Site* site);
		virtual ~Target();
		virtual void linkup();
		virtual void generate();

		static bool assertPath(const char* path);
		static bool copyFile(const char* pathFrom, const char* pathTo);
		static string winSlashes(string path);
		static string nativePath(string path);

		static void writeSign(ofstream& file);

	protected:
		Site* _site;

};

class TargetCreate : public Target
{
	public:
		TargetCreate(xml_node<>* targetNode, Site* site);
		virtual ~TargetCreate();
		virtual void linkup();
		virtual void generate();

	protected:
		string _version;
		Schema* _schema;
		string _webHost;
};


class TargetDrop : public Target
{
	public:
		TargetDrop(xml_node<>* targetNode, Site* site);
		virtual ~TargetDrop();
		virtual void linkup();
		virtual void generate();

	protected:
		string _version;
		Schema* _schema;
		string _webHost;
};

class TargetMigrate : public Target
{
	public:
		TargetMigrate(xml_node<>* targetNode, Site* site);
		virtual ~TargetMigrate();
		virtual void linkup();
		virtual void generate();

	protected:
		string _versionFrom;
		string _versionTo;
		Schema* _schemaFrom;
		Schema* _schemaTo;
		string _dbHost;

};

class TargetCms : public Target
{
	public:
		TargetCms(xml_node<>* targetNode, Site* site);
		virtual ~TargetCms();
		virtual void linkup();
		virtual void generate();
		static void writeUIIncludes(ofstream& phpFile, int dir = 0);

	protected:
		void copyResources();	//stylesheets etc

		string _version;
		Schema* _schema;
		bool _requireSSL;
		string _userTablePlural;	//store name pre-linkup
		UserTable* _userTable;

};

class TargetModel : public Target
{
	public:
		TargetModel(xml_node<>* targetNode, Site* site);
		virtual ~TargetModel();
		virtual void linkup();
		virtual void generate();

	protected:
		void copyResources();	//cmsame.php - common php functions

		string _version;
		double _phpVersion;
		string _dbHost;
		Schema* _schema;
		string _userTablePlural;	//store name pre-linkup
		UserTable* _userTable;

};

#endif
