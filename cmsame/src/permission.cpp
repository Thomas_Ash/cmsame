//c++
#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>

//c
#include <cstring>
#include <cstdlib>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "permission.h"
#include "common.h"
#include "crypto.h"

using namespace std;
using namespace rapidxml;

vector<string*> Permission::_allGroups;
size_t Permission::count = 0;

Permission::Permission() : Element()
{
	_hash=NULL;
	++count;
}

Permission::Permission(Permission* parent) : Element() 
{
	_hash=NULL;
	++count;

	for(vector<string*>::iterator group = parent->_viewGroups.begin(); group != parent->_viewGroups.end(); ++group) 
		_viewGroups.push_back(new string(**group));
	
	for(vector<string*>::iterator group = parent->_editGroups.begin(); group != parent->_editGroups.end(); ++group) 
		_editGroups.push_back(new string(**group));
	
	for(vector<string*>::iterator group = parent->_addGroups.begin(); group != parent->_addGroups.end(); ++group) 
		_addGroups.push_back(new string(**group));

	for(vector<string*>::iterator group = parent->_deleteGroups.begin(); group != parent->_deleteGroups.end(); ++group) 
		_deleteGroups.push_back(new string(**group));
	
}

Permission::~Permission() 
{
//cout << "Permission::~Permission()" << endl;
	if(--count == 0)
		for(vector<string*>::iterator group = _allGroups.begin(); group != _allGroups.end(); ++group) 
			delete *group;
	
	for(vector<string*>::iterator group = _viewGroups.begin(); group != _viewGroups.end(); ++group) 
		delete *group;
	
	for(vector<string*>::iterator group = _editGroups.begin(); group != _editGroups.end(); ++group) 
		delete *group;
	
	for(vector<string*>::iterator group = _addGroups.begin(); group != _addGroups.end(); ++group) 
		delete *group;

	for(vector<string*>::iterator group = _deleteGroups.begin(); group != _deleteGroups.end(); ++group) 
		delete *group;

	if(_hash) delete _hash;
}
		
void Permission::apply(xml_node<>* permissionNode) 
{
	if(_hash) delete _hash;
	_hash=NULL;
	string actions = "";
	string groups = "";
	
	for (xml_attribute<>* attr = permissionNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "actions") == 0) actions = attr->value();
		if(strcmp(attr->name(), "groups") == 0) groups = attr->value();
	}	

	string temp = "view";
	cmsame::TriState applyView = checkAction(actions, temp);
	
	temp = "edit";
	cmsame::TriState applyEdit = checkAction(actions, temp);
	
	temp = "add";
	cmsame::TriState applyAdd = checkAction(actions, temp);
	
	temp = "delete";
	cmsame::TriState applyDelete = checkAction(actions, temp);

	vector<string*> applyGroups;
	
	while(popToken(applyGroups, groups)) {}

	for(vector<string*>::iterator group = applyGroups.begin(); group != applyGroups.end(); ++group) 
	{
		if(applyView == cmsame::ALLOW)
			addGroup(_viewGroups, **group);
		else if(applyView == cmsame::DENY)
			removeToken(_viewGroups, **group);
	
		if(applyEdit == cmsame::ALLOW)
			addGroup(_editGroups, **group);
		else if(applyEdit == cmsame::DENY)
			removeToken(_editGroups, **group);
	
		if(applyAdd == cmsame::ALLOW)
			addGroup(_addGroups, **group);
		else if(applyAdd == cmsame::DENY)
			removeToken(_addGroups, **group);
	
		if(applyDelete == cmsame::ALLOW)
			addGroup(_deleteGroups, **group);
		else if(applyDelete == cmsame::DENY)
			removeToken(_deleteGroups, **group);
	
	}	
	
}

void Permission::apply(string appGroups, cmsame::Action action, cmsame::TriState appMode) {
	if(_hash) delete _hash;
	_hash=NULL;
	string groups = string(appGroups);
	vector<string*> applyGroups;
	
	while(popToken(applyGroups, groups)) {}
	
	for(vector<string*>::iterator group = applyGroups.begin(); group != applyGroups.end(); ++group) {
		switch(action) {
			case cmsame::VIEW:
				if(appMode == cmsame::ALLOW) addGroup(_viewGroups, **group);
				else if(appMode == cmsame::DENY) removeToken(_viewGroups, **group);
				break;
			case cmsame::EDIT:
				if(appMode == cmsame::ALLOW) addGroup(_editGroups, **group);
				else if(appMode == cmsame::DENY) removeToken(_editGroups, **group);
				break;
			case cmsame::ADD:
				if(appMode == cmsame::ALLOW) addGroup(_addGroups, **group);
				else if(appMode == cmsame::DENY) removeToken(_addGroups, **group);
				break;
			case cmsame::DELETE:
				if(appMode == cmsame::ALLOW) addGroup(_deleteGroups, **group);
				else if(appMode == cmsame::DENY) removeToken(_deleteGroups, **group);
				break;
		}
	}
}

bool Permission::hasGroup(string group, cmsame::Action action) {

	switch(action) {
		case cmsame::VIEW:
			return hasToken(_viewGroups, group);
		case cmsame::EDIT:
			return hasToken(_editGroups, group);
		case cmsame::ADD:
			return hasToken(_addGroups, group);
		case cmsame::DELETE:
			return hasToken(_deleteGroups, group);
	}
	return false;
}

void Permission::sortGroups() {
	sort(_viewGroups.begin(), _viewGroups.end(), strPtrAlpha);
	sort(_editGroups.begin(), _editGroups.end(), strPtrAlpha);
	sort(_addGroups.begin(), _addGroups.end(), strPtrAlpha);
	sort(_deleteGroups.begin(), _deleteGroups.end(), strPtrAlpha);
}

bool Permission::operator==(Permission& other) {
	return (*(getHash())==*(other.getHash()));
}

bool Permission::operator!=(Permission& other) {
	return (*(getHash())!=*(other.getHash()));
}

string* Permission::getHash() {
	if(!_hash) {
		stringstream s;
		s << "VIEW:";
		writeTokens(_viewGroups, s);
		s << ":EDIT:";
		writeTokens(_editGroups, s);
		s << ":ADD:";
		writeTokens(_addGroups, s);
		s << ":DELETE:";
		writeTokens(_deleteGroups, s);
		_hash = new string(Crypto::blake2s128(s.str()));
	}
	return _hash;
}

cmsame::TriState Permission::checkAction(string& actions, string& allow)
{
	string deny("!" + allow);
	
	vector<string*> actionTokens;
	string actionSource(actions);

	while(popToken(actionTokens, actionSource)) {}
	
	if(hasToken(actionTokens, allow)) return cmsame::ALLOW;
	if(hasToken(actionTokens, deny)) return cmsame::DENY;
	return cmsame::INHERIT;
}

//checks if a string vector contains a string	
bool Permission::hasToken(vector<string*>& tokens, string& token)
{
	for(vector<string*>::iterator testToken = tokens.begin(); testToken != tokens.end(); ++testToken) 
		if(strcmp((*testToken)->c_str(), token.c_str()) == 0) return true;
	
	return false;
}

//adds a string to a string vector, unless it's already in there	
bool Permission::addToken(vector<string*>& tokens, string& token)
{
	if(hasToken(tokens, token)) return false;
	tokens.push_back(new string(token));
	return true;
}

//remove all matching strings from string vector (should only ever be one if addToken is used)
bool Permission::removeToken(vector<string*>& tokens, string& token)		
{
	bool result = false;
	for(vector<string*>::iterator testToken = tokens.begin(); testToken != tokens.end();) 
	{
		if(strcmp((*testToken)->c_str(), token.c_str()) == 0)
		{
			delete (*testToken);
			testToken = tokens.erase(testToken);
			result = true;
		} else { ++testToken; }
	}
	return result;
}	

//adds a token to the specified vector, and also the master static _allGroups vector	
bool Permission::addGroup(vector<string*>& groups, string& group)
{
	addToken(_allGroups, group);
//	debugAllGroups();
	return addToken(groups, group);
}

//extracts a token from the end of a string, and adds it to a vector		
bool Permission::popToken(vector<string*>& tokens, string& source)
{
	size_t pos = source.find_last_not_of(" ");
	if(pos == string::npos) return false;
	source = source.substr(0, pos + 1);
	pos = source.find_last_of(" ");
	string* token = new string(source.substr(pos + 1));
	tokens.push_back(token);
	source = source.substr(0, pos + 1);
	return true;
}	



void Permission::writeTokens(vector<string*>& tokens, ostream& file)
{
	for(vector<string*>::iterator token = tokens.begin(); token != tokens.end(); ++token) 
		file << (token == tokens.begin() ? "" : " ") << **token;
}

void Permission::writeDebug(ofstream& phpFile)
{
	phpFile << "<!--View groups: ";
	for(vector<string*>::iterator group = _viewGroups.begin(); group != _viewGroups.end(); ++group) 
	{
		if(group != _viewGroups.begin()) phpFile << ", ";
		phpFile << **group;
	}	
	phpFile << "-->" << endl;
	
	phpFile << "<!--Edit groups: ";
	for(vector<string*>::iterator group = _editGroups.begin(); group != _editGroups.end(); ++group) 
	{
		if(group != _editGroups.begin()) phpFile << ", ";
		phpFile << **group;
	}	
	phpFile << "-->" << endl;
	
	phpFile << "<!--Add groups: ";
	for(vector<string*>::iterator group = _addGroups.begin(); group != _addGroups.end(); ++group) 
	{
		if(group != _addGroups.begin()) phpFile << ", ";
		phpFile << **group;
	}	
	phpFile << "-->" << endl;
	
	phpFile << "<!--Delete groups: ";
	for(vector<string*>::iterator group = _deleteGroups.begin(); group != _deleteGroups.end(); ++group) 
	{
		if(group != _deleteGroups.begin()) phpFile << ", ";
		phpFile << **group;
	}	
	phpFile << "-->" << endl << endl;

}


void Permission::writeViewGroups(ofstream& phpFile)
{
	writeTokens(_viewGroups, phpFile);
}

void Permission::writeEditGroups(ofstream& phpFile)
{
	writeTokens(_editGroups, phpFile);
}

void Permission::writeAddGroups(ofstream& phpFile)
{
	writeTokens(_addGroups, phpFile);
}

void Permission::writeDeleteGroups(ofstream& phpFile)
{
	writeTokens(_deleteGroups, phpFile);
}

void Permission::writeAllGroups(ofstream& phpFile)
{
	writeTokens(_allGroups, phpFile);
}

vector<string*>& Permission::getAllGroups() {
	return _allGroups;
}

void Permission::debugAllGroups() {
	cout << _allGroups.size() << " groups: ";
	for(vector<string*>::iterator group=_allGroups.begin(); group!=_allGroups.end();++group) {
		if(!*group) cout << "NULL ";
		else cout << **group << "(" << *group << ") ";
	}
	cout << endl;
}

void Permission::sortAllGroups() {
//	debugAllGroups();
	sort(_allGroups.begin(), _allGroups.end(), strPtrAlpha);
//	debugAllGroups();
}
