//c++
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

//c
#include <ctime>
#include <cstring>

//me
#include "element.h"
#include "common.h"
#include "site.h"
#include "permission.h"

using namespace std;
using namespace rapidxml;

Site::Site(string& name, string& alias, bool verbose) : Element()
{
	_name = name;
	_alias = alias;
	_xmlCStr = NULL;
	_verbose = verbose;
}

Site::~Site()
{
	if (_xmlCStr) delete _xmlCStr;
	for(vector<Target*>::iterator target = _targets.begin(); target != _targets.end(); ++target) 	{ delete *target; }
	for(vector<Schema*>::iterator schema = _schemas.begin(); schema != _schemas.end(); ++schema) { delete *schema; }

}

void Site::process()
{
	if (!open()) return;
	cout << "Open phase complete." << endl;
	if (!linkup()) return;
	cout << "Linkup phase complete." << endl;
	_name=_alias;
	if (!generate()) return;
	cout << "Generate phase complete." << endl;

	_statusMessage = "Processing succesfully completed.";
}

bool Site::open()
{
	//work out file name
	string xmlFileName = Target::nativePath(SITESPATH + _name + ".xml");

	//open file, check if ok
	ifstream xmlFile;
	xmlFile.open(xmlFileName.c_str());
	if(!xmlFile.good())
	{
		_statusMessage = "Site load error: couldn't open \"" + xmlFileName + "\".";
		_isOk = false;
		if (xmlFile.is_open()) xmlFile.close();
		return false;
	}

	if(_verbose)	cout << "Opened manifest file \"" << xmlFileName << "\".\n";

	//read it into a string
	string xmlString((istreambuf_iterator<char>(xmlFile)), istreambuf_iterator<char>());
	xmlFile.close();

	//convert to char array
	_xmlCStr = new char[xmlString.size() + 1];
	strcpy(_xmlCStr, xmlString.c_str());

	//parse xml
	xml_document<> xmlDoc;
	xmlDoc.parse<0>(_xmlCStr);

	if(_verbose)	cout << "Loaded XML tree.\n";

	//find 'site' node
	xml_node<>* siteNode = NULL;
	siteNode = xmlDoc.first_node("site");

	if(!siteNode)
	{
		_statusMessage = "Site load error: couldn't find site node.";
		_isOk = false;
		return false;
	}

	//get attributes
	for (xml_attribute<>* attr = siteNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "name") == 0) _siteName = attr->value();
		if(strcmp(attr->name(), "description") == 0) _siteDescription = attr->value();
	}

	if (_siteName.length()==0)
	{
		_statusMessage = "Site load error: unnamed site.";
		_isOk = false;
		return false;
	}

	if(_verbose)	cout << "Loading site branch \"" << _siteName << "\".\n";

	//load child nodes - 'schema' and 'target'
	Schema* schema;
	Target* target;
	string targetType;

	for (xml_node<>* node = siteNode->first_node(); node; node = node->next_sibling())
	{
		if(strcmp(node->name(), "schema") == 0)
		{
			schema = new Schema(node, this);
			if(!schema->isOk())
			{
				_statusMessage = schema->statusMessage();
				_isOk = false;
				return false;
			}
			_schemas.push_back(schema);
		} else if(strcmp(node->name(), "target") == 0) {
			//get type attribute (this determines which target subclass to instantiate)
			targetType = "";

			for (xml_attribute<>* attr = node->first_attribute(); attr; attr = attr->next_attribute())
			{
				if(strcmp(attr->name(), "type") == 0) targetType = attr->value();
			}

			if(targetType=="create") {
				target = new TargetCreate(node, this);
			} else if(targetType=="drop") {
				target = new TargetDrop(node, this);
			} else if(targetType=="migrate") {
				target = new TargetMigrate(node, this);
			} else if(targetType=="cms") {
				target = new TargetCms(node, this);
			} else if(targetType=="model") {
				target = new TargetModel(node, this);
			} else {
				_statusMessage = "Site load error: unknown type of target \"" + targetType + "\".";
				_isOk = false;
				return false;
			}

			if(!target->isOk())
			{
				_statusMessage = target->statusMessage();
				_isOk = false;
				return false;
			}

			_targets.push_back(target);

		}
	}

	if(_schemas.size()==0)
	{
		_statusMessage = "Site load error: no schemas.";
		_isOk = false;
		return false;
	}
	else if(_targets.size()==0)
	{
		_statusMessage = "Site load error: no targets.";
		_isOk = false;
		return false;
	}

	if(_verbose)	cout << "Site loaded ok.\n";

	return true;
}

bool Site::linkup()
{
	Permission::sortAllGroups();

	if(_verbose)	Permission::debugAllGroups();

	for(vector<Target*>::iterator target = _targets.begin(); target != _targets.end(); ++target) {
		(*target)->linkup();
		if(!(*target)->isOk())	{
			_isOk = false;
			_statusMessage = (*target)->statusMessage();
			return false;
		}
	}

	for(vector<Schema*>::iterator schema = _schemas.begin(); schema != _schemas.end(); ++schema) {
		(*schema)->linkup();
		if(!(*schema)->isOk())	{
			_isOk = false;
			_statusMessage = (*schema)->statusMessage();
			return false;
		}
	}

	return true;
}

bool Site::generate() {

	for(vector<Target*>::iterator target = _targets.begin(); target != _targets.end(); ++target)
	{
		(*target)->generate();
		if(!(*target)->isOk())
		{
			_isOk = false;
			_statusMessage = (*target)->statusMessage();
			return false;
		}
	}
	return true;
}

Schema* Site::getSchema(string& version)
{
	for(vector<Schema*>::iterator schema = _schemas.begin(); schema != _schemas.end(); ++schema)
	{
		if((*schema)->isVersion(version)) return *schema;
	}
	return NULL;
}

const string& Site::getName()
{
	return _name;
}

const string& Site::getSiteName()
{
	return _siteName;
}

const string& Site::getSiteDescription()
{
	return _siteDescription;
}

bool Site::isVerbose() {
	return _verbose;
}
