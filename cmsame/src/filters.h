#ifndef __filters__
#define __filters__

//c++
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"

namespace cmsame {
  enum ImageStepMode {
		scale, scaleup, scaledown,
		crop, croppre, croppost,
		dynamic_free, dynamic_lock
	};

  enum CleanStepMode {
		trim, striptags, regex
	};

	enum RenderMode {
		plain, markdown, mdlite
	};

	struct ImageStep {
		ImageStep(int x, int y, ImageStepMode mode) :
      x{x}, y{y}, mode{mode} {}
		int x;
		int y;
		ImageStepMode mode;
	};

  struct CleanStep {
    CleanStep(CleanStepMode mode, string pattern="", int index=0) :
      mode{mode}, pattern{pattern}, index{index} {}
    CleanStepMode mode;
    string pattern;
    int index;
  };
}

class Column;

class ImageSize : public Element
{
	public:
		ImageSize(Column* column, xml_node<>* resizeNode);
		void writeModelArray(ofstream& phpFile);
		bool hasStepMode(cmsame::ImageStepMode);
		int dynamicX();
		int dynamicY();
		const string& getName();
		void findStartFrom();
	protected:
		Column* _column;
		string _name;
		vector<cmsame::ImageStep> _steps;
		ImageSize* _startFrom;
		string _startFromName;
};

#endif
