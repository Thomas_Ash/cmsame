#ifndef __site__
#define __site__

//c++
#include <string>
#include <iostream>
#include <fstream>
#include <vector>

//me
#include "element.h"
#include "target.h"
#include "schema.h"

using namespace std;

class Schema;

class Site : public Element
{
	public:
		Site(string& name, string& alias, bool verbose);
		~Site();
		void process();

		Schema* getSchema(string& version);
		const string& getName();
		const string& getSiteName();
		const string& getSiteDescription();
		bool isVerbose();
	
	protected:	
		bool open();
		bool linkup();
		bool generate();

		string _name;
		string _alias;
		string _siteName;
		string _siteDescription;
		bool _verbose;

		char* _xmlCStr;
		
		vector<Target*> _targets;
		vector<Schema*> _schemas;

};

#endif
