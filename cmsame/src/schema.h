#ifndef __schema__
#define __schema__

//c++
#include <string>
#include <vector>
#include <iostream>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "table.h"
#include "permission.h"

using namespace std;
using namespace rapidxml;

class Site;

class Schema : public Element
{
	public:
		Schema(xml_node<>* schemaNode, Site* site);
		~Schema();
		bool isVersion(string& version);
		const string& getVersion();
//		UserTable* getUserTable();
		Table* getTableByPlural(string& plural);
		Table* getTableBySingular(string& singular);
		Site* getSite();
		Permission* getPermission();
		void addTable(Table* table);

		void writeCreate(string webHost);
		void writeDrop(string webHost);

		void writeModel(string dbHost, UserTable* userTable, double phpVersion);
		void writeCms(bool requireSSL, UserTable* userTable);
		void writeSign(ofstream& file);

		void linkup();

	protected:
		string _version;
		Site* _site;
//		UserTable* _userTable;
		vector<Table*> _tables;
		map<string*, string*> _readUsers;
		map<string*, string*> _editUsers;
		Permission* _permission;

		string readPassword(string group);
		string editPassword(string group);

		void writeCreateBase();
		void writeCreateBody();
		void writeCreateUser(string webHost);
		void writeCreateProc();
		void writeCreateAll();
		void writeDropBase();
		void writeDropBody();
		void writeDropUser(string webHost);
		void writeDropProc();
		void writeDropAll();

};


#endif
