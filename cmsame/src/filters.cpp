//c++
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

//c
#include <cstring>
#include <cstdlib>
#include <cmath>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "column.h"
#include "filters.h"

//ImageSize
ImageSize::ImageSize(Column* column, xml_node<>* resizeNode) : Element()
{
	_column = column;
	_name = "";
	_startFromName = "";
	//attributes
	for (xml_attribute<>* attr = resizeNode->first_attribute(); attr; attr = attr->next_attribute()) {
		if(strcmp(attr->name(), "name") == 0) _name = attr->value();
		if(strcmp(attr->name(), "startfrom") == 0) _startFromName = attr->value();
	}
	//child nodes
	for (xml_node<>* node = resizeNode->first_node(); node; node = node->next_sibling()) {
		if(strcmp(node->name(), "step") == 0 && node->first_attribute("type")) {
			int x=0;
			int y=0;
			if(node->first_attribute("width")) x=atoi(node->first_attribute("width")->value());
			if(node->first_attribute("x")) x=atoi(node->first_attribute("x")->value());
			if(node->first_attribute("height")) y=atoi(node->first_attribute("height")->value());
			if(node->first_attribute("y")) y=atoi(node->first_attribute("y")->value());
			if(strcmp(node->first_attribute("type")->value(), "scale") == 0) {
				_steps.push_back(cmsame::ImageStep(x, y, cmsame::scale));
			} else if(strcmp(node->first_attribute("type")->value(), "crop") == 0) {
				_steps.push_back(cmsame::ImageStep(x, y, cmsame::crop));
			} else if(strcmp(node->first_attribute("type")->value(), "scaleup") == 0) {
				_steps.push_back(cmsame::ImageStep(x, y, cmsame::scaleup));
			} else if(strcmp(node->first_attribute("type")->value(), "scaledown") == 0) {
				_steps.push_back(cmsame::ImageStep(x, y, cmsame::scaledown));
			} else if(strcmp(node->first_attribute("type")->value(), "croppre") == 0) {
				_steps.push_back(cmsame::ImageStep(x, y, cmsame::croppre));
			} else if(strcmp(node->first_attribute("type")->value(), "croppost") == 0) {
				_steps.push_back(cmsame::ImageStep(x, y, cmsame::croppost));
			} else if(strcmp(node->first_attribute("type")->value(), "dynamic") == 0) {

				if(hasStepMode(cmsame::dynamic_free)||hasStepMode(cmsame::dynamic_lock)) {
					_isOk = false;
					_statusMessage = "Resize load error: A single resize should not have more than one dynamic step.";
				}

//		_isOk = false;
//		_statusMessage = "Dynamic resize NYI.";
				//in context of dynamic, the standard dimension attributes are used as minimums
				if(node->first_attribute("minwidth")) x=atoi(node->first_attribute("minwidth")->value());
				if(node->first_attribute("minx")) x=atoi(node->first_attribute("minx")->value());
				if(node->first_attribute("minheight")) y=atoi(node->first_attribute("minheight")->value());
				if(node->first_attribute("miny")) y=atoi(node->first_attribute("miny")->value());

				if(node->first_attribute("aspect")) { //parse integer ratio eg. 16:9
					string val=node->first_attribute("aspect")->value();
					size_t pos=val.find_last_of(':');
					double ax=(double)atoi(val.substr(0, pos).c_str());
					double ay=(double)atoi(val.substr(pos+1).c_str());
					//scale up ax and ay  so we can still use 2 ints
					if(ax<x) {
						ay *= (double)x / ax;
						ax = (double)x;
					}
					if(ay<y) {
						ax *= (double)y / ay;
						ay = (double)y;
					}
					_steps.push_back(cmsame::ImageStep((int)ceil(ax), (int)ceil(ay), cmsame::dynamic_lock));

				} else _steps.push_back(cmsame::ImageStep(x, y, cmsame::dynamic_free));
			}
		}
	}

	//check
	if(_name.length() < 1)
	{
		_isOk = false;
		_statusMessage = "Resize load error: unnamed resize.";
	}
}

void ImageSize::writeModelArray(ofstream& phpFile) {
	phpFile << "'" << _name << "' => [";
	if(_startFrom) phpFile << "'" << _startFrom->getName() << "'";
	else phpFile << "NULL";

	for(vector<cmsame::ImageStep>::iterator step = _steps.begin(); step != _steps.end(); ++step) {
		//if(step!=_steps.begin())
		phpFile << ", ";
		switch(step->mode) {
			case cmsame::scale:
				phpFile << "['scale', " << step->x << ", " << step->y << "]";
				break;
			case cmsame::crop:
				phpFile << "['crop', " << step->x << ", " << step->y << "]";
				break;
			case cmsame::scaleup:
				phpFile << "['scaleup', " << step->x << ", " << step->y << "]";
				break;
			case cmsame::scaledown:
				phpFile << "['scaledown', " << step->x << ", " << step->y << "]";
				break;
			case cmsame::croppre:
				phpFile << "['croppre', " << step->x << ", " << step->y << "]";
				break;
			case cmsame::croppost:
				phpFile << "['croppost', " << step->x << ", " << step->y << "]";
				break;
			case cmsame::dynamic_free:
				phpFile << "['dynamic_free', " << step->x << ", " << step->y << "]";
				break;
			case cmsame::dynamic_lock:
				phpFile << "['dynamic_lock', " << step->x << ", " << step->y << "]";
				break;
//				phpFile << "['crop', " << step->x << ", " << step->y << "]";
		}
	}

	phpFile << "]";// << endl_ind;
}

bool ImageSize::hasStepMode(cmsame::ImageStepMode mode) {

	for(vector<cmsame::ImageStep>::iterator step = _steps.begin(); step != _steps.end(); ++step) {
		if(step->mode == mode) return true;
	}
	return false;
}

int ImageSize::dynamicX() {
	for(vector<cmsame::ImageStep>::iterator step = _steps.begin(); step != _steps.end(); ++step) {
		if(step->mode==cmsame::dynamic_free||step->mode==cmsame::dynamic_lock) return step->x;
	}
	return 0;
}

int ImageSize::dynamicY() {
	for(vector<cmsame::ImageStep>::iterator step = _steps.begin(); step != _steps.end(); ++step) {
		if(step->mode==cmsame::dynamic_free||step->mode==cmsame::dynamic_lock) return step->y;
	}
	return 0;
}

const string& ImageSize::getName()
{
	return _name;
}

void ImageSize::findStartFrom() {
	_startFrom = _column->getImageSizeByName(_startFromName);
	if(_startFromName.length()>0 &&!_startFrom) {
		_isOk = false;
		_statusMessage = "Resize load error: couldn't find start from resize \"" +
			_startFromName + "\" on column \"" + _column->getName() + "\".";
	}
}
