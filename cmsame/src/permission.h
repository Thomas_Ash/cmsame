#ifndef __permission__
#define __permission__

//c++
#include <string>
#include <vector>
#include <iostream>
#include <fstream>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"

namespace cmsame
{
	enum TriState {ALLOW, DENY, INHERIT};
	enum Action {VIEW, EDIT, ADD, DELETE};
}

using namespace std;
using namespace rapidxml;

class Site;

class Permission : public Element
{
	public:
		Permission();
		Permission(Permission* parent);
		~Permission();
		
		void apply(xml_node<>* permissionNode);
		void apply(string appGroups, cmsame::Action action, cmsame::TriState appMode);
		void writeDebug(ofstream& phpFile);
		
		bool hasGroup(string group, cmsame::Action action);
		void sortGroups();
		bool operator==(Permission& other);
		bool operator!=(Permission& other);

		void writeViewGroups(ofstream& phpFile);
		void writeEditGroups(ofstream& phpFile);
		void writeAddGroups(ofstream& phpFile);
		void writeDeleteGroups(ofstream& phpFile);
		
		static void writeAllGroups(ofstream& phpFile);
		static void debugAllGroups();
		static vector<string*>& getAllGroups();
		static void sortAllGroups();



	protected:
		string* getHash();
		static cmsame::TriState checkAction(string& actions, string& allow);

		static bool hasToken(vector<string*>& tokens, string& token);
		static bool addToken(vector<string*>& tokens, string& token);
		static bool removeToken(vector<string*>& tokens, string& token);
		static bool addGroup(vector<string*>& groups, string& group);
		
		static bool popToken(vector<string*>& tokens, string& source);
		
		static void writeTokens(vector<string*>& tokens, ostream& file);
		

		vector<string*> _viewGroups;
		vector<string*> _editGroups;
		vector<string*> _addGroups;
		vector<string*> _deleteGroups;

		string* _hash;
		
		static vector<string*> _allGroups;
		static size_t count;

};

#endif
