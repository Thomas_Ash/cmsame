//c++
#include <string>

//me
#include "element.h"

using namespace std;

Element::Element()
{
	_isOk=true;
	_statusMessage="";
}

Element::~Element() {}

bool Element::isOk()				{ return _isOk; }
string Element::statusMessage()		{ return _statusMessage; }

