#ifndef __element__
#define __element__

//c++
#include <string>

using namespace std;

class Element
{
	public:
		Element();
		~Element();
		bool isOk();
		string statusMessage();

	protected:
		bool _isOk;
		string _statusMessage;

};

#endif
