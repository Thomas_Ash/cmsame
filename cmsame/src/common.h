#ifndef __common__
#define __common__

//c++
#include <iostream>
#include <string>

#define SITESPATH "./sites/"
#define RESPATH "./cmsame/res/"
#define VERSION "0.79"

using namespace std;

extern void backspace();
extern void getpw(string* pw);
extern string askpw(string username);
extern string choosepw(string username, bool mnemonic);
extern int get_indent_index();
extern ios_base& inc_ind(ios_base& stream);
extern ios_base& dec_ind(ios_base& stream);

extern bool strPtrAlpha(string* a, string* b);

template<class charT, class traits>
basic_ostream<charT, traits>& endl_ind(basic_ostream<charT, traits>& stream) {
    int indent = stream.iword(get_indent_index());
    stream.put(stream.widen('\n'));
    while (indent) {
        stream.put(stream.widen('\t'));
        indent--;
    }
    stream.flush();
    return stream;
}

#endif
