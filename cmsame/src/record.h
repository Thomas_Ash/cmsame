#ifndef __record__
#define __record__

//c++
#include <string>
#include <map>
#include <iostream>
#include <fstream>

//3p
#include "../../rapidxml/rapidxml.hpp"

using namespace std;
using namespace rapidxml;

class Table;

class Record : public Element
{
	public:
		Record(xml_node<>* node, Table* table);
		Record(Table* table);
		~Record();
		
		void addField(const string& fieldName, const string& value);
		string getValue(string name);
		void linkup();
		void writeInsert(ofstream& sqlFile);		
		
	protected:
		Table* _table;
		map<string*, string*> _fields;

};

#endif
