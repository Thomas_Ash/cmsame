//c++
#include <string>
#include <map>
#include <iostream>
#include <fstream>
#include <sstream>

//c
#include <cstring>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "common.h"
#include "element.h"
#include "record.h"
#include "column.h"
#include "table.h"

using namespace std;
using namespace rapidxml;

Record::Record(xml_node<>* node, Table* table)
{
	_table = table;	
	
	//get attributes
	for (xml_attribute<>* attr = node->first_attribute(); attr; attr = attr->next_attribute())
	{
		//check for dupes
	
		for(map<string*, string*>::iterator field = _fields.begin(); field != _fields.end(); ++field)
		{
			if(strcmp(attr->name(), field->first->c_str()) == 0)
			{
				_statusMessage = "Record load error: duplicate field " + *(field->first) + ".";
				_isOk = false;					
				return;				
			}
		}
		
		_fields.insert(pair<string*, string*> (new string(attr->name()), new string(attr->value())));
	}		
}

Record::Record(Table *table) {
	_table = table;
}

Record::~Record() 
{
	for(map<string*, string*>::iterator field = _fields.begin(); field != _fields.end(); ++field)
	{
		delete field->first;
		delete field->second;
	}
}

void Record::addField(const string& fieldName, const string& value) {
	_fields.insert(pair<string*, string*> (new string(fieldName), new string(value)));
}

string Record::getValue(string name) {
	for(map<string*, string*>::iterator field = _fields.begin(); field != _fields.end(); ++field)
	{	
		if((field->first)->compare(name) == 0) {
			return(*(field->second));
		}
	}
	return "";
}

void Record::linkup() {
//	cout << "record linkup" << endl;

	string myName="";
	Column* column;
	Record* record;

	for(map<string*, string*>::iterator field = _fields.begin(); field != _fields.end(); ++field)
	{	//1st pass, check & get name
		column = _table->getColumn(*(field->first));
		if(!column)
		{
			_statusMessage = "Record linkup error: couldn't find column " + *(field->first) + " in parent table.";
			_isOk = false;
			return;
		}
		if(column==_table->getNameColumn()) {
			myName=*(field->second);
		}
	}


	for(map<string*, string*>::iterator field = _fields.begin(); field != _fields.end(); ++field)
	{
		column = _table->getColumn(*(field->first));

		if(column->hasType(cmsame::listof)) {
			if(column->getMirror()->hasType(cmsame::listof)) {
				
				string list=*(field->second);
				string item="";
				for(size_t i=0; i<list.length(); i++) {
					if(list[i]=='|') {
						record = new Record(column->getLinkTable());
						record->addField(_table->getSingular(), myName);
						record->addField(column->getRefTarget()->getSingular(), item);
						column->getLinkTable()->addRecord(record);
						item="";
					} else item += list[i];
				}
				record = new Record(column->getLinkTable());
				record->addField(_table->getSingular(), myName);
				record->addField(column->getRefTarget()->getSingular(), item);
				column->getLinkTable()->addRecord(record);

			} //else if(column->getMirror()->hasType(cmsame::oneof)) {} //could enable this but not really useful
		}
	}

//	cout << "record linkup ok" << endl;
}

//need to delegate this to Column to type-dependent parse properly
void Record::writeInsert(ofstream& sqlFile)
{
	Column* column;

	stringstream columnNames;
	stringstream valueNames;	
	
	bool first = true;

	for(map<string*, string*>::iterator field = _fields.begin(); field != _fields.end(); ++field)
	{
		column = _table->getColumn(*(field->first));
		
		if(column->hasType(cmsame::listof)) continue;
		if(!first) {
			columnNames << ", ";
			valueNames << ", ";
		}
		column->writeSqlRecordInsert(columnNames, valueNames, *(field->second), this); // << "'" << *(field->second) << "'";
		if(!column->isOk()) {
			_statusMessage = column->statusMessage();
			_isOk = false;					
			return;				
		}
		first = false;
	}

	sqlFile << "INSERT INTO " << _table->getPlural() << " (" << columnNames.str() << ") VALUES (" <<
		valueNames.str() << ");" << endl_ind;
	
}

