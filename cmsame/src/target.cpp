//c++
#include <string>
#include <iostream>
#include <fstream>

//c
#include <cstring>
#include <cstdlib>
#include <ctime>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "element.h"
#include "common.h"
#include "target.h"
#include "site.h"

using namespace std;
using namespace rapidxml;

//Target - base class

Target::Target(xml_node<>* node, Site* site) : Element() {
	_site = site;
}

Target::~Target() {}

void Target::linkup() {}

void Target::generate() {}

//static file system utility functions

bool Target::assertPath(const char* path)
{
	#ifdef __linux__
		//system("pwd");
		string call(string("mkdir -p ") + path);
		return (system(call.c_str()) == 0);
	#endif

	#ifdef _WIN32
		//system("cd");
		string call = winSlashes(string("IF NOT EXIST \"") + path + string("\" md \"") + path + string("\""));
		return (system(call.c_str()) == 0);
	#endif
}

bool Target::copyFile(const char* pathFrom, const char* pathTo)
{
	#ifdef __linux__
		//system("pwd");
		string call(string("cp ") + pathFrom + string(" ") + pathTo);
		return (system(call.c_str()) == 0);
	#endif

	#ifdef _WIN32
		//system("cd");
		string call = string("COPY /Y \"") + winSlashes(string(pathFrom) + "\" \"" + pathTo + "\" >nul 2>&1");
		return (system(call.c_str()) == 0);
	#endif
}

string Target::winSlashes(string path)
{
  for (size_t i = 0; i < path.length(); ++i)  {
    if (path[i] == '/') path[i] = '\\';
  }

  return path;
}

string Target::nativePath(string path) {
	#ifdef __linux__
		return path;
	#endif
	#ifdef _WIN32
		return winSlashes(path);
	#endif
}

void Target::writeSign(ofstream& file)
{
	time_t timestamp;
	time(&timestamp);
	file << "CMSame " << VERSION << " " << ctime(&timestamp);

}

//TargetCreate - generate SQL to create database

TargetCreate::TargetCreate(xml_node<>* targetNode, Site* site) : Target(targetNode, site)
{
	//defaults
	_webHost="localhost";
	//read the attributes specific to this type of target
	for (xml_attribute<>* attr = targetNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "version") == 0) _version = attr->value();
		else if(strcmp(attr->name(), "webhost") == 0) _webHost = attr->value();
	}

}

TargetCreate::~TargetCreate() 	{}

void TargetCreate::linkup()
{
	_schema = _site->getSchema(_version);
	if (!_schema)
	{
		_statusMessage = "Target linkup error: couldn't find schema \"" + _version + "\".";
		_isOk = false;
	}
}

void TargetCreate::generate() {

	if(_site->isVerbose()) cout << "targetcreate\n";
	_schema->writeCreate(_webHost);
	if (!_schema->isOk())
	{
		_statusMessage = _schema->statusMessage();
		_isOk = false;
	}
	if(_site->isVerbose()) cout << "generate done\n";
}


//TargetDrop - generate SQL to drop database

TargetDrop::TargetDrop(xml_node<>* targetNode, Site* site) : Target(targetNode, site)
{
	//defaults
	_webHost="localhost";
	//read the attributes specific to this type of target
	for (xml_attribute<>* attr = targetNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "version") == 0) _version = attr->value();
		else if(strcmp(attr->name(), "webhost") == 0) _webHost = attr->value();
	}
}

TargetDrop::~TargetDrop() 	{}

void TargetDrop::linkup()
{
	_schema = _site->getSchema(_version);
	if (!_schema)
	{
		_statusMessage = "Target linkup error: couldn't find schema \"" + _version + "\".";
		_isOk = false;
	}
}

void TargetDrop::generate()
{

	if(_site->isVerbose()) cout << "targetdrop\n";

	_schema->writeDrop(_webHost);

	if(_site->isVerbose()) cout << "generate done\n";
}


//TargetMigrate - generate SQL version migration script

TargetMigrate::TargetMigrate(xml_node<>* targetNode, Site* site) : Target(targetNode, site)
{
	//defaults
	_dbHost="localhost";
	//read the attributes specific to this type of target
	for (xml_attribute<>* attr = targetNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "from") == 0) _versionFrom = attr->value();
		else if(strcmp(attr->name(), "to") == 0) _versionTo = attr->value();
		else if(strcmp(attr->name(), "dbhost") == 0) _dbHost = attr->value();
	}
}

TargetMigrate::~TargetMigrate() 	{}

void TargetMigrate::linkup()
{
	_schemaFrom = _site->getSchema(_versionFrom);
	if (!_schemaFrom)
	{
		_statusMessage = "Target linkup error: couldn't find schema \"" + _versionFrom + "\".";
		_isOk = false;
		return;
	}

	_schemaTo = _site->getSchema(_versionTo);
	if (!_schemaTo)
	{
		_statusMessage = "Target linkup error: couldn't find schema \"" + _versionTo + "\".";
		_isOk = false;
	}
}

void TargetMigrate::generate() {}



//TargetCms - generate PHP admin pages

TargetCms::TargetCms(xml_node<>* targetNode, Site* site) : Target(targetNode, site)
{
	_requireSSL = true;
	//read the attributes specific to this type of target
	for (xml_attribute<>* attr = targetNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "version") == 0) _version = attr->value();
		else if(strcmp(attr->name(), "requiressl") == 0 && strcmp(attr->value(), "false") == 0) _requireSSL = false;
		else if(strcmp(attr->name(), "usertable") == 0) _userTablePlural = attr->value();
	}


}

TargetCms::~TargetCms() 	{}

void TargetCms::linkup()
{
	_schema = _site->getSchema(_version);
	if (!_schema)
	{
		_statusMessage = "Target linkup error: couldn't find schema \"" + _version + "\".";
		_isOk = false;
		return;
	}

	_userTable = (UserTable*)(_schema->getTableByPlural(_userTablePlural));
	if (!_userTable)
	{
		_statusMessage = "Target linkup error: couldn't find user table \"" + _userTablePlural + "\".";
		_isOk = false;
	}
}

void TargetCms::generate()
{
	if(_site->isVerbose()) cout << "targetcms\n";
	copyResources();
	_schema->writeCms(_requireSSL, _userTable);

	if(_site->isVerbose()) cout << "generate done\n";

}

void TargetCms::copyResources()
{
	string path = "/www/cms/css/";
	assertPath((SITESPATH + _site->getName() + path).c_str());
	copyFile((RESPATH + string("cmsame.css")).c_str(), (SITESPATH + _site->getName() + path + "cmsame.css").c_str());
	copyFile((RESPATH + string("jquery-ui.min.css")).c_str(), (SITESPATH + _site->getName() + path + "jquery-ui.min.css").c_str());
	copyFile((RESPATH + string("jquery-ui.theme.min.css")).c_str(),	(SITESPATH + _site->getName() + path + "jquery-ui.theme.min.css").c_str());
	copyFile((RESPATH + string("jquery-ui.structure.min.css")).c_str(), (SITESPATH + _site->getName() + path + "jquery-ui.structure.min.css").c_str());

	path = "/www/cms/css/images/";
	assertPath((SITESPATH + _site->getName() + path).c_str());
	copyFile((RESPATH + string("ui-bg_glass_20_555555_1x400.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-bg_glass_20_555555_1x400.png").c_str());
	copyFile((RESPATH + string("ui-bg_glass_40_0078a3_1x400.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-bg_glass_40_0078a3_1x400.png").c_str());
	copyFile((RESPATH + string("ui-bg_glass_40_ffc73d_1x400.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-bg_glass_40_ffc73d_1x400.png").c_str());
	copyFile((RESPATH + string("ui-bg_gloss-wave_25_333333_500x100.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-bg_gloss-wave_25_333333_500x100.png").c_str());
	copyFile((RESPATH + string("ui-bg_highlight-soft_80_eeeeee_1x100.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-bg_highlight-soft_80_eeeeee_1x100.png").c_str());
	copyFile((RESPATH + string("ui-bg_inset-soft_25_000000_1x100.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-bg_inset-soft_25_000000_1x100.png").c_str());
	copyFile((RESPATH + string("ui-bg_inset-soft_30_f58400_1x100.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-bg_inset-soft_30_f58400_1x100.png").c_str());
	copyFile((RESPATH + string("ui-icons_4b8e0b_256x240.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-icons_4b8e0b_256x240.png").c_str());
	copyFile((RESPATH + string("ui-icons_222222_256x240.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-icons_222222_256x240.png").c_str());
	copyFile((RESPATH + string("ui-icons_a83300_256x240.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-icons_a83300_256x240.png").c_str());
	copyFile((RESPATH + string("ui-icons_cccccc_256x240.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-icons_cccccc_256x240.png").c_str());
	copyFile((RESPATH + string("ui-icons_ffffff_256x240.png")).c_str(), (SITESPATH + _site->getName() + path + "ui-icons_ffffff_256x240.png").c_str());
	copyFile((RESPATH + string("000000-0.8.png")).c_str(), (SITESPATH + _site->getName() + path + "000000-0.8.png").c_str());
	copyFile((RESPATH + string("ants.gif")).c_str(), (SITESPATH + _site->getName() + path + "ants.gif").c_str());
//	copyFile((RESPATH + string("handle.gif")).c_str(), (SITESPATH + _site->getName() + path + "handle.gif").c_str());

	path = "/www/cms/js/";
	assertPath((SITESPATH + _site->getName() + path).c_str());
	copyFile((RESPATH + string("cmsame.js")).c_str(), (SITESPATH + _site->getName() + path + "cmsame.js").c_str());
	copyFile((RESPATH + string("jquery-ui.min.js")).c_str(), (SITESPATH + _site->getName() + path + "jquery-ui.min.js").c_str());
//	copyFile((RESPATH + string("openpgp.min.js")).c_str(), (SITESPATH + _site->getName() + path + "openpgp.min.js").c_str());
//	copyFile((RESPATH + string("openpgp.worker.min.js")).c_str(), (SITESPATH + _site->getName() + path + "openpgp.worker.min.js").c_str());


}

void TargetCms::writeUIIncludes(ofstream& phpFile, int dir) {

	int i;

	phpFile << "<script src=\"https://code.jquery.com/jquery-3.1.0.min.js\" " <<
		"integrity=\"sha256-cCueBR6CsyA4/9szpPfrX3s49M9vUU5BgtiJj06wt/s=\" " <<
		"crossorigin=\"anonymous\"></script>" << endl_ind <<
		"<script type=\"text/javascript\" src=\"";
/*
	phpFile << "<script src=\"https://code.jquery.com/jquery-1.7.2.min.js\" " <<
		"integrity=\"sha256-R7aNzoy2gFrVs+pNJ6+SokH04ppcEqJ0yFLkNGoFALQ=\" " <<
		"crossorigin=\"anonymous\"></script>" << endl_ind <<
		"<script type=\"text/javascript\" src=\"";
*/
	for(i=0;i<dir;i++) phpFile << "../";

	phpFile << "js/jquery-ui.min.js\"></script>" << endl_ind <<
		"<script type=\"text/javascript\" src=\"";

//	for(i=0;i<dir;i++) phpFile << "../";

//	phpFile << "js/openpgp.min.js\"></script>" << endl_ind <<
//		"<script type=\"text/javascript\" src=\"";

//	for(i=0;i<dir;i++) phpFile << "../";

//	phpFile << "js/openpgp.worker.min.js\"></script>" << endl_ind <<
//		"<script type=\"text/javascript\" src=\"";

	for(i=0;i<dir;i++) phpFile << "../";

	phpFile << "js/cmsame.js\"></script>" << endl_ind <<
		"<link rel=\"stylesheet\" href=\"";

	for(i=0;i<dir;i++) phpFile << "../";

	phpFile << "css/jquery-ui.min.css\" />" << endl_ind <<
		"<link rel=\"stylesheet\" href=\"";

	for(i=0;i<dir;i++) phpFile << "../";

	phpFile << "css/jquery-ui.structure.min.css\" />" << endl_ind <<
		"<link rel=\"stylesheet\" href=\"";

	for(i=0;i<dir;i++) phpFile << "../";

	phpFile << "css/jquery-ui.theme.min.css\" />" << endl_ind <<
		"<link rel=\"stylesheet\" href=\"";

	for(i=0;i<dir;i++) phpFile << "../";

	phpFile << "css/cmsame.css\" />" << endl_ind;

}

//TargetModel - generate PHP middle layer classes

TargetModel::TargetModel(xml_node<>* targetNode, Site* site) : Target(targetNode, site)
{
	//defaults
	_dbHost="localhost";
	_phpVersion=7.0;
	//read the attributes specific to this type of target
	for (xml_attribute<>* attr = targetNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "version") == 0) _version = attr->value();
		else if(strcmp(attr->name(), "dbhost") == 0) _dbHost = attr->value();
		else if(strcmp(attr->name(), "usertable") == 0) _userTablePlural = attr->value();
		else if(strcmp(attr->name(), "phpversion") == 0) {
			_phpVersion = atof(attr->value());
			if(_phpVersion<5.6) {
				_statusMessage = "Target open error: PHP version is not supported.";
				_isOk = false;
				return;
			} else if(_phpVersion<7.0) {
				cout << "Warning: fallback to pseudo-random generation in PHP < 7.0." << endl;
			}
		}
	}


}

TargetModel::~TargetModel() 	{}

void TargetModel::linkup()
{
	_schema = _site->getSchema(_version);
	if (!_schema)
	{
		_statusMessage = "Target linkup error: couldn't find schema \"" + _version + "\".";
		_isOk = false;
		return;
	}
	_userTable = (UserTable*)(_schema->getTableByPlural(_userTablePlural));
	if (!_userTable)
	{
		_statusMessage = "Target linkup error: couldn't find user table \"" + _userTablePlural + "\".";
		_isOk = false;
	}
}

void TargetModel::generate()
{
	if(_site->isVerbose()) cout << "targetmodel\n";
	copyResources();
	_schema->writeModel(_dbHost, _userTable, _phpVersion);
	if(_site->isVerbose()) cout << "generate done\n";
}

void TargetModel::copyResources()
{
	string path = "/model/include/";
	assertPath((SITESPATH + _site->getName() + path).c_str());
	copyFile((RESPATH + string("types.php")).c_str(), (SITESPATH + _site->getName() + path + "types.php").c_str());
	copyFile((RESPATH + string("markdown.php")).c_str(), (SITESPATH + _site->getName() + path + "markdown.php").c_str());
	copyFile((RESPATH + string("mdlite.php")).c_str(), (SITESPATH + _site->getName() + path + "mdlite.php").c_str());
	copyFile((RESPATH + string("cms.php")).c_str(), (SITESPATH + _site->getName() + path + "cms.php").c_str());
	if(_phpVersion<7.0) {
		copyFile((RESPATH + string("crypto_old.php")).c_str(), (SITESPATH + _site->getName() + path + "crypto.php").c_str());
	} else {
		copyFile((RESPATH + string("crypto.php")).c_str(), (SITESPATH + _site->getName() + path + "crypto.php").c_str());
	}
}
