#ifndef __crypto__
#define __crypto__

//c++
#include <string>
#include <random>

using namespace std;

struct blake_ctx {
	uint8_t buf[64];
	uint32_t state[8];
	uint32_t tl;
	uint32_t th;
	size_t ptr;
};

struct md5_ctx {
	uint32_t buf[16];
	uint32_t state[4];
	size_t ptr;
};

class Crypto {
	public:
		static string getRandomPassword(); //16 chars of base64 (112 bits), also used for DB pass and salts
		static string getMnemonicPassword();  //Name+Attitude+Animal
		static bool isStrongPassword(string pw); //same rules as php validation: min 8 chars, at least 2 out of uppercase, lowercase & digit
		static string blake2s128(string data); //better than MD5 for the meantime
		static string md5(string data); //for back compatibility, not very secure

		static random_device rd;

	private:
		static void blake_comp(blake_ctx& ctx, bool last);
		static uint32_t blake_rrot(uint32_t in, size_t bits);

		static void md5_comp(md5_ctx& ctx);

		static const uint32_t blake_init[8];
		static const uint8_t blake_sigma[10][16];
		static const uint8_t blake_seq[8][4];

		static const uint32_t md5_init[4];
		static const uint32_t md5_sine[64];

		static const char* names[];
		static const char* attitudes[];
		static const char* animals[];

		static mt19937 gen;
};

#endif
