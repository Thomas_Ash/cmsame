//c++
#include <string>
#include <sstream>

//c
#include <cstring>
#include <cstdlib>
#include <cmath>

//3p
#include "../../rapidxml/rapidxml.hpp"

//me
#include "common.h"
#include "column.h"
#include "table.h"
#include "permission.h"
#include "schema.h"
#include "target.h"
#include "site.h"
#include "crypto.h"
#include "filters.h"

using namespace std;
using namespace rapidxml;


//////////////
/// COLUMN ///
//////////////

Column::Column(Table* table) : Element()
{
	_table = table;
	_permission = new Permission(_table->getPermission());
}

Column::Column(Table* table, xml_node<>* columnNode, string prefix)
{
	_table = table;
	_permission = new Permission(_table->getPermission());
	_name = "";
	_description = "";
	_type = cmsame::none;
	_x = 0;
	_y = 0;
	_null = true;
	_browse = true;
	_renderMode = cmsame::plain;
	_refTargetName = "";
	_refTarget = NULL;
	_mirrorName="";
	_mirror=NULL;
	_linkTableSingular="";
	_linkTablePlural="";
	_linkTable=NULL;

	string type;

	//attributes
	for (xml_attribute<>* attr = columnNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "name") == 0) _name = prefix+attr->value();
		else if(strcmp(attr->name(), "description") == 0) _description = attr->value();
		else if(strcmp(attr->name(), "type") == 0) {
			type = attr->value();

			if(type == "none") {
				_type = cmsame::none;
			} else if(type == "id") {
				_type = cmsame::id;
			} else if(type == "integer") {
				_type = cmsame::integer;
			} else if(type == "decimal") {
				_type = cmsame::decimal;
			} else if(type == "datetime") {
				_type = cmsame::datetime;
			} else if(type == "bigtext") {
				_type = cmsame::bigtext;
			} else if(type == "text") {
				_type = cmsame::text;
			} else if(type == "password") {
				_type = cmsame::password;
			} else if(type == "tokens") {
				_type = cmsame::tokens;
			} else if(type == "options") {
				_type = cmsame::options;
			} else if(type == "boolean") {
				_type = cmsame::boolean;
			} else if(type == "imageupload") {
				_type = cmsame::imageupload;
			} else if(type == "fileupload") {
				_type = cmsame::fileupload;
			} else {
				_isOk = false;
				_statusMessage = "Column load error: unsupported type \"" + type + "\".";
				return;
			}
		} else if(strcmp(attr->name(), "length") == 0 ||
				strcmp(attr->name(), "precision") == 0 ||
				strcmp(attr->name(), "maxbytes") == 0) _x = atol(attr->value());
		else if(strcmp(attr->name(), "scale") == 0) _y = atol(attr->value());
		else if(strcmp(attr->name(), "browse") == 0) {
			_browse = (strcmp(attr->value(), "false") != 0);
		} else if(strcmp(attr->name(), "null") == 0) {
			_null = (strcmp(attr->value(), "false") != 0);
		} else if(strcmp(attr->name(), "oneof") == 0) {
			_type = cmsame::oneof;
			_refTargetName = attr->value();
		} else if(strcmp(attr->name(), "listof") == 0) {
			_type = cmsame::listof;
			_refTargetName = attr->value();
		} else if(strcmp(attr->name(), "mirror") == 0) {
			_mirrorName = attr->value();
		}
	}

	//child nodes
	for (xml_node<>* node = columnNode->first_node(); node; node = node->next_sibling()) {
		if(strcmp(node->name(), "value") == 0 && node->first_attribute("name")) {
			_values.push_back(new string(node->first_attribute("name")->value()));
		} else if(strcmp(node->name(), "permission") == 0) {
			_permission->apply(node);
		} else if(strcmp(node->name(), "clean") == 0 && node->first_attribute("type")) {
			if(strcmp(node->first_attribute("type")->value(), "trim") == 0)	{
				_cleanSteps.push_back(cmsame::CleanStep(cmsame::trim));
			} else if(strcmp(node->first_attribute("type")->value(), "striptags") == 0) {
				_cleanSteps.push_back(cmsame::CleanStep(cmsame::striptags));
			} else if(strcmp(node->first_attribute("type")->value(), "regex") == 0) {
				if(!node->first_attribute("pattern")||!node->first_attribute("index")) {
					_isOk = false;
					_statusMessage = "Column load error: regex clean must specify pattern and index.";
					return;
				}

				_cleanSteps.push_back(cmsame::CleanStep(cmsame::regex, node->first_attribute("pattern")->value(),
					atoi(node->first_attribute("index")->value())));
			}
		} else if (strcmp(node->name(), "render") == 0 && node->first_attribute("mode")) {
			if(strcmp(node->first_attribute("mode")->value(), "markdown") == 0) {
				_renderMode = cmsame::markdown;
			} else if(strcmp(node->first_attribute("mode")->value(), "mdlite") == 0) {
				_renderMode = cmsame::mdlite;
			}
		} else if (strcmp(node->name(), "mime") == 0 && node->first_attribute("type")) {
			_mimeTypes.push_back(new string(node->first_attribute("type")->value()));
		} else if (strcmp(node->name(), "resize") == 0) {
			ImageSize* resize = new ImageSize(this, node);
			if(!resize->isOk()) {
				_statusMessage = resize->statusMessage();
				_isOk = false;
				break;
			}
			_imageSizes.push_back(resize);
		} else if (strcmp(node->name(), "linktable") == 0) {
			//try to parse w/ regular table ctor
			_linkTable = new Table(node, _table->getSchema());
			if(_linkTable->isOk()&&!_linkTable->isSkeleton()) {
				//FKs get added later at linkup
				_table->getSchema()->addTable(_linkTable);
			} else if(node->first_attribute("singular")&&node->first_attribute("plural"))	{
				delete _linkTable;
				_linkTable=NULL;
				_linkTableSingular=node->first_attribute("singular")->value();
				_linkTablePlural=node->first_attribute("plural")->value();
			} else {
				_isOk = false;
				_statusMessage = "Column load error: linktable must specify singular and plural.";
				return;
			}
		}
	}

	//check
	if(_name.length() < 1)
	{
		_isOk = false;
		_statusMessage = "Column load error: unnamed column.";
	}

	if(_table->getColumn(_name) != NULL)
	{
		_isOk = false;
		_statusMessage = "Column load error: duplicate column name.";
	}

	//type-specific default parameters

	switch(_type)
	{
		case cmsame::decimal:
			if(_x <= 0) _x = 9;
			if(_y <= 0) _y = 2;
			break;

		case cmsame::text:
			if(_x <= 0) _x = 256;
			break;

		case cmsame::none:
		case cmsame::id:
		case cmsame::integer:
		case cmsame::options:
		case cmsame::bigtext:
		case cmsame::password:
		case cmsame::imageupload:
		case cmsame::fileupload:
		case cmsame::tokens:
		case cmsame::boolean:
		case cmsame::datetime:
		case cmsame::oneof:
		case cmsame::listof:
			break;

	}

	//pre linkup
	if(_type==cmsame::imageupload) {
		for(vector<ImageSize*>::iterator imageSize = _imageSizes.begin(); imageSize != _imageSizes.end(); ++imageSize) {
			(*imageSize)->findStartFrom();
			if(!(*imageSize)->isOk()) {
				_statusMessage=(*imageSize)->statusMessage();
				_isOk = false;
				return;
			}
		}
	}

	addSubColumns();
}

Column::Column(Table* table, const char* name, const char* description,
				cmsame::ColumnType type, long x, long y, bool null, bool browse)
{
	_table = table;
	_permission = new Permission(_table->getPermission());
	_name = name;
	_description = description;
	_type = type;
	_x = x;
	_y = y;
	_null = null;
	_browse = browse;
	_refTargetName = "";
	_refTarget = NULL;
	_mirrorName="";
	_mirror=NULL;
	_linkTableSingular="";
	_linkTablePlural="";
	_linkTable=NULL;
	addSubColumns();
}

void Column::addSubColumns() {
	if(_type==cmsame::password) {
		_subColumns.push_back(new Column(_table, (_name + "Salt").c_str(),
			"Salt", cmsame::text, 32, 0, false, false));
	} else if(_type==cmsame::fileupload) {
		_subColumns.push_back(new Column(_table, (_name + "Bytes").c_str(),
			"Bytes", cmsame::integer, 0, 0, true, false));
	} else if(_type==cmsame::imageupload) {
		_subColumns.push_back(new Column(_table, (_name + "Bytes").c_str(),
			"Bytes", cmsame::integer, 0, 0, true, false));
		_subColumns.push_back(new Column(_table, (_name + "Width").c_str(),
			"Width", cmsame::integer, 0, 0, true, false));
		_subColumns.push_back(new Column(_table, (_name + "Height").c_str(),
			"Height", cmsame::integer, 0, 0, true, false));
		for(vector<ImageSize*>::iterator imageSize = _imageSizes.begin(); imageSize != _imageSizes.end(); ++imageSize) {
			_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "Bytes").c_str(),
				((*imageSize)->getName() + "Bytes").c_str(), cmsame::integer, 0, 0, true, false));
			_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "Width").c_str(),
				((*imageSize)->getName() + "Width").c_str(), cmsame::integer, 0, 0, true, false));
			_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "Height").c_str(),
				((*imageSize)->getName() + "Height").c_str(), cmsame::integer, 0, 0, true, false));

			if((*imageSize)->hasStepMode(cmsame::dynamic_free)||(*imageSize)->hasStepMode(cmsame::dynamic_lock)) {
				_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "PreBytes").c_str(),
					((*imageSize)->getName() + "PreBytes").c_str(), cmsame::integer, 0, 0, true, false));
				_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "PreWidth").c_str(),
					((*imageSize)->getName() + "PreWidth").c_str(), cmsame::integer, 0, 0, true, false));
				_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "PreHeight").c_str(),
					((*imageSize)->getName() + "PreHeight").c_str(), cmsame::integer, 0, 0, true, false));
				//TODO store the 4 ints (L, R, T, B) from user too
				//probably can wait until images & files use $cfg instead of hacky set([])
	/*			_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "L").c_str(),
					((*imageSize)->getName() + "L").c_str(), cmsame::integer, 0, 0, true, false));
				_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "R").c_str(),
					((*imageSize)->getName() + "R").c_str(), cmsame::integer, 0, 0, true, false));
				_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "T").c_str(),
					((*imageSize)->getName() + "T").c_str(), cmsame::integer, 0, 0, true, false));
				_subColumns.push_back(new Column(_table, (_name + (*imageSize)->getName() + "B").c_str(),
					((*imageSize)->getName() + "B").c_str(), cmsame::integer, 0, 0, true, false));*/

			}
		}
	} else if(_type==cmsame::text&&_renderMode!=cmsame::plain) {
		//html and stripped html columns

		switch(_renderMode) {
			case cmsame::plain:
				break;
			case cmsame::markdown:
				_subColumns.push_back(new Column(_table, (_name + "MarkdownHtml").c_str(),
					(_name + "MarkdownHtml").c_str(), cmsame::text, _x*4, 0, true, false));
											//should be enough?

				_subColumns.push_back(new Column(_table, (_name + "MarkdownText").c_str(),
					(_name + "MarkdownText").c_str(), cmsame::text, _x, 0, true, false));
				break;
			case cmsame::mdlite:
				_subColumns.push_back(new Column(_table, (_name + "MdHtml").c_str(),
					(_name + "MdHtml").c_str(), cmsame::text, _x*4, 0, true, false));
											//should be enough?

				_subColumns.push_back(new Column(_table, (_name + "MdText").c_str(),
					(_name + "MdText").c_str(), cmsame::text, _x, 0, true, false));

				break;
		}

	} else if(_type==cmsame::bigtext&&_renderMode!=cmsame::plain) {
		//html and stripped html columns

		switch(_renderMode) {
			case cmsame::plain:
				break;
			case cmsame::markdown:
				_subColumns.push_back(new Column(_table, (_name + "MarkdownHtml").c_str(),
					(_name + "MarkdownHtml").c_str(), cmsame::bigtext, 0, 0, true, false));
				_subColumns.push_back(new Column(_table, (_name + "MarkdownText").c_str(),
					(_name + "MarkdownText").c_str(), cmsame::bigtext, 0, 0, true, false));
				break;
			case cmsame::mdlite:
				_subColumns.push_back(new Column(_table, (_name + "MdHtml").c_str(),
					(_name + "MdHtml").c_str(), cmsame::bigtext, 0, 0, true, false));
				_subColumns.push_back(new Column(_table, (_name + "MdText").c_str(),
					(_name + "MdText").c_str(), cmsame::bigtext, 0, 0, true, false));
				break;
		}
	}
}

Column::~Column()
{
	for(vector<string*>::iterator value = _values.begin(); value != _values.end(); ++value) { delete *value; }
	for(vector<string*>::iterator mimeType = _mimeTypes.begin(); mimeType != _mimeTypes.end(); ++mimeType) { delete *mimeType; }
	for(vector<ImageSize*>::iterator imageSize = _imageSizes.begin(); imageSize != _imageSizes.end(); ++imageSize) { delete *imageSize; }
	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) { delete (*column); }
	delete _permission;
}

Column* Column::findByName(const string& name)
{
	if (name == _name) return this;
	return NULL;
}

bool Column::hasType(cmsame::ColumnType type) {
	return (type == _type);
}


const string& Column::getName()
{
	return _name;
}

const string Column::getSqlBaseType() {

	stringstream ss;

	switch(_type)
	{
		case cmsame::none:		break;
		case cmsame::id:		ss << "INT"; break;
		case cmsame::integer:		ss << "INT"; break;
		case cmsame::decimal:		ss << "DECIMAL(" << _x << ", " << _y << ")"; break;
		case cmsame::datetime:		ss << "DATETIME"; break;
		case cmsame::bigtext:		ss << "TEXT"; break;
		case cmsame::text:		ss << "VARCHAR(" << _x << ")"; break;
		case cmsame::password:		ss << "VARCHAR(32)"; break;
		case cmsame::options:		ss << "TINYINT"; break;
		case cmsame::boolean:		ss << "BIT"; break;
		case cmsame::tokens:		ss << "VARCHAR(256)"; break;
		case cmsame::imageupload:	ss << "VARCHAR(256)"; break;
		case cmsame::fileupload:	ss << "VARCHAR(256)"; break;
		case cmsame::oneof:		ss << "INT"; break;
		case cmsame::listof:		break;
	}

	return ss.str();

}

const string Column::getCssClass() {

	stringstream ss;

	switch(_type)
	{
		case cmsame::none:		break;
		case cmsame::id:		ss << "id"; break;
		case cmsame::integer:		ss << "integer"; break;
		case cmsame::decimal:		ss << "decimal"; break;
		case cmsame::datetime:		ss << "datetime"; break;
		case cmsame::bigtext:		ss << "multilinetext"; break;
		case cmsame::text:		ss << "singlelinetext"; break;
		case cmsame::password:		ss << "password"; break;
		case cmsame::options:		ss << "options"; break;
		case cmsame::boolean:		ss << "boolean"; break;
		case cmsame::tokens:		ss << "tokens"; break;
		case cmsame::imageupload:	ss << "imageupload"; break;
		case cmsame::fileupload:	ss << "fileupload"; break;
		case cmsame::oneof:		ss << "oneof"; break;
		case cmsame::listof:		ss << "listof"; break;
	}

	return ss.str();

}
const string Column::getCmsameType() {

	stringstream ss;

	switch(_type)
	{
		case cmsame::none:		break;
		case cmsame::id:		ss << "\\cmsame\\_int"; break;
		case cmsame::integer:		ss << "\\cmsame\\_int"; break;
		case cmsame::decimal:		ss << "\\cmsame\\_real"; break;
		case cmsame::datetime:		ss << "\\cmsame\\_datetime"; break;
		case cmsame::bigtext:		ss << "\\cmsame\\_text"; break;
		case cmsame::text:		ss << "\\cmsame\\_text"; break;
		case cmsame::password:		ss << "\\cmsame\\_password"; break;
		case cmsame::options:		ss << "\\cmsame\\_enum"; break;
		case cmsame::boolean:		ss << "\\cmsame\\_bool"; break;
		case cmsame::tokens:		ss << "\\cmsame\\_text"; break;
		case cmsame::imageupload:	ss << "\\cmsame\\_image"; break;
		case cmsame::fileupload:	ss << "\\cmsame\\_file"; break;
		case cmsame::oneof:		ss << "\\cmsame\\_pointer"; break;
		case cmsame::listof:		ss << "\\cmsame\\_list"; break;
	}

	return ss.str();

}

const string Column::getSqlType() {
	if(_type == cmsame::id) return "INT AUTO_INCREMENT NOT NULL";

	return getSqlBaseType() + (_null ? string(" NULL") : string(" NOT NULL"));
}

const string Column::getSqlName() {
	if(_type == cmsame::oneof) return _name + _refTarget->getSingular() + _refTarget->getIdColumn()->getName();
	return _name;
}

Column* Column::createIdColumn(Table* table)
{
	Column* idColumn = new Column(table, "Id", "Unique Id", cmsame::id, 0, 0, false, false);
	return idColumn;
}

void Column::addValue(string& value) {
	_values.push_back(new string(value));
}

void Column::applyPermission(string appGroups, cmsame::Action action, cmsame::TriState appMode) {
	_permission->apply(appGroups, action, appMode);
}

bool Column::permissionMatches(Permission* other) {
	return ((*other)==(*_permission));
}

bool Column::refTargetIs(Table* table) {
	return(table==_refTarget);
}

bool Column::setRefTarget(Table* refTarget) {
	if(_refTarget!=NULL && _refTarget!=refTarget) return false;
	_refTarget = refTarget;
	return true;
}

bool Column::setMirror(Column* mirror) {
	if(_mirror!=NULL && _mirror!=mirror) return false;
	_mirror = mirror;
	return true;
}

Table* Column::getRefTarget() {
	if(_type!=cmsame::listof&&_type!=cmsame::oneof) return NULL;
	return _refTarget;
}
Column* Column::getMirror() {
	if(_type!=cmsame::listof&&_type!=cmsame::oneof) return NULL;
	return _mirror;
}
Table* Column::getLinkTable() {
	if(_type!=cmsame::listof) return NULL;
	return _linkTable;
}

ImageSize* Column::getImageSizeByName(const string& name) {
	if(_type!=cmsame::imageupload) return NULL;
	for(vector<ImageSize*>::iterator size = _imageSizes.begin(); size != _imageSizes.end(); ++size) {
		if((*size)->getName()==name) return (*size);
	}
	return NULL;
}
/*
void Column::addIncRef(Column* column) {
	_incRefs.push_back(column);
}*/

void Column::linkupRefTarget() {


	_permission->sortGroups();

	//new rules
	//mirroring doesn't care which direction you spec it in the xml
	//both refcols can mirror each other, or either one
	//parser creates mutual relationship either way
	//BUT: a lone listof, or groups >2 (ie A mirrors B which already mirrors C), are not allowed

	//so need 3 passes
	// -resolve reftarget
	// -resolve mirror
	// -check for legality & form linktables

	if(_type==cmsame::oneof) {
		if(_table->getSchema()->getSite()->isVerbose())		cout << "Column[" << _name << "]::linkupRefTarget" << endl;
		_refTarget = _table->getSchema()->getTableBySingular(_refTargetName);
		if(_refTarget==NULL) {
			_isOk = false;
			_statusMessage = "Column linkup error: couldn't find table \"" + _refTargetName + "\" for refcolumn.";
			return;
		}
		if(_table->getSchema()->getSite()->isVerbose())	{
			cout << _name << " points to " << _refTargetName << ", pointer=" << _refTarget << endl;
			cout << "_refTarget->getNameColumn() returns " << _refTarget->getNameColumn() << endl;
		}

		if(!_refTarget->getNameColumn()) {
			_isOk = false;
			_statusMessage = "Column linkup error: table \"" + _refTargetName + "\" must have a namecolumn for refcolumns to point to it.";
			return;
		}
	} else if (_type==cmsame::listof) {
		if(_table->getSchema()->getSite()->isVerbose())		cout << "Column[" << _name << "]::linkupRefTarget" << endl;
		_refTarget = _table->getSchema()->getTableByPlural(_refTargetName);
		if(_refTarget==NULL) {
			_isOk = false;
			_statusMessage = "Column linkup error: couldn't find table \"" + _refTargetName + "\" for refcolumn.";
			return;
		}
		if(_table->getSchema()->getSite()->isVerbose())	{
			cout << _name << " points to " << _refTargetName << ", pointer=" << _refTarget << endl;
			cout << "_refTarget->getNameColumn() returns " << _refTarget->getNameColumn() << endl;
		}

		if(!_refTarget->getNameColumn()) {
			_isOk = false;
			_statusMessage = "Column linkup error: table \"" + _refTargetName + "\" must have a namecolumn for refcolumns to point to it.";
			return;
		}
	}

//	cout << "linkupreftarget ok" << endl;
}

void Column::linkupMirror() {

//	cout << "linkupmirror" << endl;

	if(_type==cmsame::oneof) {
		if(_table->getSchema()->getSite()->isVerbose())		cout << "Column[" << _name << "]::linkupMirror" << endl;

		if(_mirrorName.length() > 0) {
			_mirror = _refTarget->getColumn(_mirrorName);
			if(_mirror==NULL) {
				_isOk = false;
				_statusMessage = "Column linkup error: couldn't find mirror \"" + _mirrorName +
					"\" for refcolumn \"" +_name+ "\".";
				return;
			}
			if(!_mirror->refTargetIs(_table)) {
				_isOk = false;
				_statusMessage = "Column linkup error: mirror \"" + _mirrorName +
					"\" doesn't point to table \""+_table->getSingular()+"\".";

				if(_table->getSchema()->getSite()->isVerbose())		{
					cout << "this: " << this << endl <<
					"_mirror: " << _mirror << endl <<
					"_refTarget: " << _refTarget << endl <<
					"_mirror->_refTarget: " << _mirror->_refTarget << endl;
				}
				return;
			}
			if(!_mirror->setMirror(this)) {
				_isOk = false;
				_statusMessage = "Column linkup error: mirror \"" + _mirrorName + "\" is already mirroring another column.";
				return;
			}
			if(!_mirror->permissionMatches(_permission)) {
				_isOk = false;
				_statusMessage = "Column linkup error: \"" + _name + "\" and \"" + _mirrorName + "\" are mirrors with different permissions.";
				return;
			}
		}
		//don't care if mirroring oneof or listof at this stage; done
	} else if (_type==cmsame::listof) {
		if(_table->getSchema()->getSite()->isVerbose())		cout << "Column[" << _name << "]::linkupMirror" << endl;

		if(_mirrorName.length() > 0) {
			_mirror = _refTarget->getColumn(_mirrorName);
			if(_mirror==NULL) {
				_isOk = false;
				_statusMessage = "Column linkup error: couldn't find mirror \"" + _mirrorName +
					"\" for refcolumn \"" +_name+ "\".";
				return;
			}
			if(!_mirror->refTargetIs(_table)) {
				_isOk = false;
				_statusMessage = "Column linkup error: mirror \"" + _mirrorName +
					"\" doesn't point to table \""+_table->getSingular()+"\".";
				return;
			}
			if(!_mirror->setMirror(this)) {
				_isOk = false;
				_statusMessage = "Column linkup error: mirror \"" + _mirrorName + "\" is already mirroring another column.";
				return;
			}
			if(!_mirror->permissionMatches(_permission)) {
				_isOk = false;
				_statusMessage = "Column linkup error: \"" + _name + "\" and \"" + _mirrorName + "\" are mirrors with different permissions.";
				return;
			}
		}
		//if no mirrorname, don't freak out, could be other direction...
	}

//	cout << "linkupmirror ok" << endl;
}

vector<Table*>* Column::linkupLinkTable() {
		//..but if mirroring another listof, we require a linktable.

	//cout << "linkuplinktable " << _name << endl;

		//when rendering CMS we will need to check for this and use it instead of _refTarget
	Column* oneof;
	string linkdesc;
	vector<Table*>* res = new vector<Table*>();

	if(_type==cmsame::listof) {
		if(_table->getSchema()->getSite()->isVerbose())		cout << "Column[" << _name << "]::linkupLinkTable" << endl;

		if(_mirror) {
			if(_mirror->hasType(cmsame::listof)) {
				if(_linkTable) {
					//
					if(_table->getSchema()->getSite()->isVerbose())	cout<<"make sure it has the two oneofs it needs"<<endl;
					oneof=_linkTable->getColumn(_table->getSingular());
					if(oneof==NULL) {
						oneof=new Column(_linkTable, _table->getSingular().c_str(), _table->getSingular().c_str(),
							cmsame::oneof, 0, 0, false, true);
						oneof->setRefTarget(_table);
						_linkTable->addColumn(oneof);
					} else if(!oneof->hasType(cmsame::oneof)||!oneof->refTargetIs(_table)) {
						_isOk = false;
						_statusMessage = "Column linkup error: linktable \"" + _linkTableSingular + "\" has a reserved column name used for something else.";
						delete res;
						return NULL;
					}
					oneof=_linkTable->getColumn(_refTarget->getSingular());
					if(oneof==NULL) {
						oneof=new Column(_linkTable, _refTarget->getSingular().c_str(), _refTarget->getSingular().c_str(),
							cmsame::oneof, 0, 0, false, true);
						oneof->setRefTarget(_refTarget);
						_linkTable->addColumn(oneof);
					} else if(!oneof->hasType(cmsame::oneof)||!oneof->refTargetIs(_refTarget)) {
						_isOk = false;
						_statusMessage = "Column linkup error: linktable \"" + _linkTableSingular + "\" has a reserved column name used for something else.";
						delete res;
						return NULL;
					}

				} else if(_mirror->getLinkTable()) {
					if(_linkTableSingular.length()>0&&_table->getSchema()->getTableBySingular(_linkTableSingular)!=_mirror->getLinkTable()) {
						_isOk = false;
						_statusMessage = "Column linkup error: \"" + _name + "\" and its mirror \"" + _mirror->getName() +
							"\" have different linktables assigned.";
						delete res;
						return NULL;
					}
					if(_linkTablePlural.length()>0&&_table->getSchema()->getTableByPlural(_linkTablePlural)!=_mirror->getLinkTable()) {
						_isOk = false;
						_statusMessage = "Column linkup error: \"" + _name + "\" and its mirror \"" + _mirror->getName() +
							"\" have different linktables assigned.";
						delete res;
						return NULL;
					}
					if(_table->getSchema()->getSite()->isVerbose())		
						cout<<"using same linktable as mirror."<<endl;

					_linkTable = _mirror->getLinkTable();
				} else {
					if(_linkTableSingular.length() > 0 && _linkTablePlural.length() > 0) {
						if(!_table->getSchema()->getTableBySingular(_linkTableSingular)&&
							!_table->getSchema()->getTableByPlural(_linkTablePlural)) {
							if(_table->getSchema()->getSite()->isVerbose())		
								cout<<"linktable doesnt exist, so create minimal one containing 2 oneofs"<<endl;
							linkdesc = "Link between " + _table->getPlural() + " and " + _refTarget->getPlural();

							_linkTable=new Table(_linkTableSingular, _linkTablePlural, linkdesc, _table->getSchema(), _permission);
//							_table->getSchema()->addTable(_linkTable);
							oneof=new Column(_linkTable, _table->getSingular().c_str(), _table->getSingular().c_str(),
								cmsame::oneof, 0, 0, false, true);
							oneof->setRefTarget(_table);
							_linkTable->addColumn(oneof);
							oneof=new Column(_linkTable, _refTarget->getSingular().c_str(), _refTarget->getSingular().c_str(),
								cmsame::oneof, 0, 0, false, true);
							oneof->setRefTarget(_refTarget);
							_linkTable->addColumn(oneof);
							res->push_back(_linkTable);
						} else if(_table->getSchema()->getTableBySingular(_linkTableSingular)==_table->getSchema()->getTableByPlural(_linkTablePlural)) {
							if(_table->getSchema()->getSite()->isVerbose())		
								cout << "valid ref to existing table."<<endl<<"make sure it has the two oneofs it needs"<<endl;
							_linkTable=_table->getSchema()->getTableBySingular(_linkTableSingular);
							oneof=_linkTable->getColumn(_table->getSingular());
							if(oneof==NULL) {
								oneof=new Column(_linkTable, _table->getSingular().c_str(), _table->getSingular().c_str(),
									cmsame::oneof, 0, 0, false, true);
								oneof->setRefTarget(_table);
								_linkTable->addColumn(oneof);
							} else if(!oneof->hasType(cmsame::oneof)||!oneof->refTargetIs(_table)) {
								_isOk = false;
								_statusMessage = "Column linkup error: linktable \"" + _linkTableSingular + "\" has a reserved column name used for something else.";
								delete res;
								return NULL;
							}
							oneof=_linkTable->getColumn(_refTarget->getSingular());
							if(oneof==NULL) {
								oneof=new Column(_linkTable, _refTarget->getSingular().c_str(), _refTarget->getSingular().c_str(),
									cmsame::oneof, 0, 0, false, true);
								oneof->setRefTarget(_refTarget);
								_linkTable->addColumn(oneof);
							} else if(!oneof->hasType(cmsame::oneof)||!oneof->refTargetIs(_refTarget)) {
								_isOk = false;
								_statusMessage = "Column linkup error: linktable \"" + _linkTableSingular + "\" has a reserved column name used for something else.";
								delete res;
								return NULL;
							}
						} else {
							_isOk = false;
							_statusMessage = "Column linkup error: \"" + _name + "\" has an invalid linktable.";
							delete res;
							return NULL;
						}
					} else {
						_linkTableSingular = _name + "To" + _mirror->getName() + "Link";
						_linkTablePlural = _name + "To" + _mirror->getName() + "Links";
						linkdesc = "Link between " + _table->getPlural() + " and " + _refTarget->getPlural();


						if(_table->getSchema()->getSite()->isVerbose())	cout << "fully implicit linktable \"" << 
							_linkTablePlural << "\" created." << endl;

						_linkTable=new Table(_linkTableSingular, _linkTablePlural, linkdesc, _table->getSchema(), _permission);

						if(_table->getSchema()->getSite()->isVerbose())	cout << "fully implicit linktable \"" << 
							_linkTable->getPlural() << "\" has pointer " << _linkTable << "." << endl;

//						_table->getSchema()->addTable(_linkTable);
						oneof=new Column(_linkTable, _table->getSingular().c_str(), _table->getSingular().c_str(),
							cmsame::oneof, 0, 0, false, true);
						oneof->setRefTarget(_table);
						_linkTable->addColumn(oneof);
						oneof=new Column(_linkTable, _refTarget->getSingular().c_str(), _refTarget->getSingular().c_str(),
							cmsame::oneof, 0, 0, false, true);
						oneof->setRefTarget(_refTarget);
						_linkTable->addColumn(oneof);
						res->push_back(_linkTable);
					}
				}
			}

		} else {
			_isOk = false;
			_statusMessage = "Column linkup error: \"" + _name + "\" is a listof without a mirror.";
			delete res;
			return NULL;
		}
	}
	//cout << "linkuplinktable ok" << endl;

//	if(_table->getSchema()->getSite()->isVerbose() && res->size() > 0) {
//		cout << "Returning " << res->size() << " tables

//	}
	return res;
}

///////////
/// SQL ///
///////////


void Column::writeSqlDefinition(ofstream& sqlFile) {
	if(_type==cmsame::listof) return;
 	sqlFile << _table->getSingular() << getSqlName() << " " << getSqlType() << "," << endl_ind;

	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
		(*column)->writeSqlDefinition(sqlFile);
	}
}

void Column::writeSqlIndex(ofstream& sqlFile) {
	if(_type==cmsame::id)
		sqlFile << "PRIMARY KEY (" << _table->getSingular() << getSqlName() << ")";
	//as yet, nothing needs subcolumns and an index at the same time
}

//used by locate and search procedures
bool Column::writeSqlSelect(ofstream& sqlFile, string group, bool first, string alias) {
	if(!_permission->hasGroup(group, cmsame::VIEW)) return false;
	if(!first) sqlFile << ", ";

	if(_type==cmsame::listof) {
		//count goes here

		sqlFile << "COUNT(" << alias << _name << _refTarget->getPlural() << "." << _refTarget->getSingular() <<
			_refTarget->getIdColumn()->getSqlName() << ") AS " << alias <<
			_table->getSingular() << _name << _refTarget->getSingular() << "Count";

	} else 	if(_type==cmsame::password) {

		sqlFile << "''=============HIDDEN============='' AS " << alias << _table->getSingular() << getSqlName();

		for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
			(*column)->writeSqlSelect(sqlFile, group, false, alias);
		}

	} else {

		sqlFile << alias << _table->getPlural() << "." << _table->getSingular() << getSqlName();

		if(alias!="") sqlFile << " AS " << alias << _table->getSingular() << getSqlName();

		for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
			(*column)->writeSqlSelect(sqlFile, group, false, alias);
		}
	}

	return true;
}

void Column::writeSqlSelectRef(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::VIEW)) return;
	if(_type!=cmsame::listof&&_type!=cmsame::oneof) return;
	sqlFile << "SET SqlSelect = IF(Open" << _name << ", CONCAT(SqlSelect, '";
	_refTarget->writeSqlSelect(sqlFile, group, _name);
 	sqlFile << "'), SqlSelect);" << endl_ind;
}

void Column::writeSqlRefParam(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::VIEW)) return;
	if(_type!=cmsame::listof&&_type!=cmsame::oneof) return;
	sqlFile << ", Open" << _name << " BIT";
}

void Column::writeSqlJoin(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::VIEW)) return;
	if(_type!=cmsame::listof&&_type!=cmsame::oneof) return;
	if(_type==cmsame::oneof) {
		//: no need to join if not opening
		sqlFile << "SET SqlSelect = IF(Open" << _name << ", CONCAT(SqlSelect, ' LEFT JOIN " << _table->getSchema()->getSite()->getSiteName() << "." <<
			_refTarget->getPlural() << " AS " << _name << _refTarget->getPlural() <<
			" ON (" << _name << _refTarget->getPlural() << "." << _refTarget->getSingular() << _refTarget->getIdColumn()->getSqlName() <<
			" = " << _table->getPlural() << "." << _table->getSingular() << getSqlName() << ")";
		//but if we are we need to check if the reftarget has any listof columns
		// they won't be opened but we need to count them.
		_refTarget->writeSqlTailJoin(sqlFile, group, _name);

		sqlFile << "'), SqlSelect);" << endl_ind;
	} else if(_type==cmsame::listof) {
		if(_mirror->hasType(cmsame::oneof)) { // need to join regardless to get count, at least
			sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' LEFT JOIN " << _table->getSchema()->getSite()->getSiteName() << "." <<
				_refTarget->getPlural() << " AS " << _name << _refTarget->getPlural() <<
				" ON (" << _name << _refTarget->getPlural() << "." << _refTarget->getSingular() << _mirror->getSqlName() <<
				" = " << _table->getPlural() << "." << _table->getSingular() << _table->getIdColumn()->getSqlName() <<
				")', IF(Open" << _name << ", '";
		//additionally, if we are opening, we need to check if the reftarget has any listof columns
		// they won't be opened but we need to count them.
		_refTarget->writeSqlTailJoin(sqlFile, group, _name);

		sqlFile << "', ''));" << endl_ind;

		} else if(_mirror->hasType(cmsame::listof)) {
			//double join for many-to-many
			//the cmsame type is a list of the reftarget, not the linktable
			//so always join both even if just to count
			sqlFile << "SET SqlSelect = CONCAT(SqlSelect, ' LEFT JOIN " << _table->getSchema()->getSite()->getSiteName() << "." <<
				_linkTable->getPlural() << " AS " << _name << _linkTable->getPlural() <<
				" ON (" << _name << _linkTable->getPlural() << "." << _linkTable->getSingular() << _table->getSingular() <<
				_table->getSingular() << "Id = " << _table->getPlural() << "." << _table->getSingular() <<
				_table->getIdColumn()->getSqlName() <<
				") LEFT JOIN " <<  _table->getSchema()->getSite()->getSiteName() << "." << _refTarget->getPlural() <<
				" AS " << _name << _refTarget->getPlural() <<
				" ON (" << _name << _linkTable->getPlural() << "." << _linkTable->getSingular() << _refTarget->getSingular() <<
				_refTarget->getSingular() << "Id = " << _name << _refTarget->getPlural() << "." << _refTarget->getSingular() <<
				_refTarget->getIdColumn()->getSqlName() << ")', IF(Open" << _name << ", '";
			//also if opening, plus any listofs belonging to the reftarget

			_refTarget->writeSqlTailJoin(sqlFile, group, _name);

			sqlFile << "', ''));" << endl_ind;
		}
	}
}

void Column::writeSqlTailJoin(ofstream& sqlFile, string group, string alias) {
	if(!_permission->hasGroup(group, cmsame::VIEW)) return;
	if(_type!=cmsame::listof) return;
	if(_mirror->hasType(cmsame::oneof)) { // need to join regardless to get count, at least
		sqlFile << " LEFT JOIN " << _table->getSchema()->getSite()->getSiteName() << "." <<
			_refTarget->getPlural() << " AS " << alias << _name << _refTarget->getPlural() <<
			" ON (" << alias << _name << _refTarget->getPlural() << "." << _refTarget->getSingular() <<
			_mirror->getName() << _table->getSingular() << _table->getIdColumn()->getName() <<
			" = " << alias << _table->getPlural() << "." <<
			_table->getSingular() << _table->getIdColumn()->getName() << ")";
	} else if(_mirror->hasType(cmsame::listof)) {

/*LEFT JOIN MyWebsite.OmNomz AS AnimalDietOmNomz
ON (AnimalDietOmNomz.OmNomAnimalAnimalId = AnimalAnimals.AnimalId)
LEFT JOIN MyWebsite.Foods AS AnimalDietFoods
ON (AnimalDietFoods.FoodId = AnimalDietOmNomz.OmNomFoodFoodId)*/

		sqlFile << " LEFT JOIN " << _table->getSchema()->getSite()->getSiteName() << "." <<
			_linkTable->getPlural() << " AS " << alias << _name << _linkTable->getPlural() <<
			" ON (" << alias << _name << _linkTable->getPlural() << "." << _linkTable->getSingular() <<
			_table->getSingular() << _table->getSingular() <<
			"Id = " << alias << _table->getPlural() << "." << _table->getSingular() <<
			_table->getIdColumn()->getName() <<
			") LEFT JOIN " << _table->getSchema()->getSite()->getSiteName() << "." <<
			_refTarget->getPlural() << " AS " << alias << _name << _refTarget->getPlural() <<
			" ON (" << alias << _name << _refTarget->getPlural() << "." << _refTarget->getSingular() <<
			_refTarget->getIdColumn()->getName() << " = " << alias << _name << _linkTable->getPlural() <<
			"." << _linkTable->getSingular() << _refTarget->getSingular() << _refTarget->getSingular() << "Id)";

	}
}

void Column::writeSqlSearchParam(ofstream& sqlFile) {
	if(_type==cmsame::fileupload||_type==cmsame::imageupload||_type==cmsame::listof) return;
	string name = getSqlName();
	sqlFile << ", Param" << name << " " << getSqlBaseType() << ", Comp" << name << " TINYINT";

//	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
//		sqlFile << ", ";
//		(*column)->writeSqlSearchParam(sqlFile);
//	}

}

void Column::writeSqlSearchWhere(ofstream& sqlFile) {
	if(_type==cmsame::fileupload||_type==cmsame::imageupload||_type==cmsame::listof) return;
	string name = getSqlName();

	switch(_type)
	{
		case cmsame::none:
		case cmsame::fileupload:
		case cmsame::password:
		case cmsame::imageupload:
		case cmsame::listof:
			return;

		case cmsame::decimal:
		case cmsame::id:
		case cmsame::integer:
		case cmsame::options:
		case cmsame::oneof:
			sqlFile << "SET ValueParam = CONCAT('''', Param" << name << ", '''');" << endl_ind;
			break;

		case cmsame::boolean:
			sqlFile << "SET ValueParam = IF(COALESCE(Param" << name << "), '1', '0');" << endl_ind;
			break;

		case cmsame::bigtext:
		case cmsame::text:
		case cmsame::tokens:
		case cmsame::datetime:
			sqlFile << "SET ValueParam = QUOTE(Param" << name << ");" << endl_ind;
			break;
	}





	sqlFile << "SET SqlSelect = CASE COALESCE(Comp" << name << ", 0)" << inc_ind << endl_ind <<
		"WHEN 0 THEN SqlSelect" << endl_ind <<
		"WHEN 1 THEN IF(ISNULL(Param" << name << ")," << inc_ind << endl_ind <<
		"CONCAT(SqlSelect, ' AND " << _table->getPlural() << "." << _table->getSingular() << name << " IS NULL')," << endl_ind <<
		"CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " = ', ValueParam))" << dec_ind << endl_ind <<
		"WHEN 2 THEN IF(ISNULL(Param" << name << ")," << inc_ind << endl_ind <<
		"CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " IS NOT NULL')," << endl_ind <<
		"CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " != ', ValueParam))" << dec_ind << endl_ind <<
		"WHEN 3 THEN CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " < ', ValueParam)" << endl_ind <<
		"WHEN 4 THEN CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " <= ', ValueParam)" << endl_ind <<
		"WHEN 5 THEN CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " > ', ValueParam)" << endl_ind <<
		"WHEN 6 THEN CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " >= ', ValueParam)" << endl_ind <<
		"WHEN 7 THEN CONCAT(SqlSelect, ' AND " << _table->getPlural() << "."  << _table->getSingular() << name << " LIKE ', ValueParam)" << dec_ind << endl_ind <<
		"END;" << endl_ind;


//	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
//		(*column)->writeSqlSearchWhere(sqlFile);
//	}

}

void Column::writeSqlUpdateParam(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::EDIT)) return;
	if(_type==cmsame::listof) return;
	string name = getSqlName();

	sqlFile << ", Param" << name << " " << getSqlBaseType() << ", Move" << name << " BIT";

	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
		(*column)->writeSqlUpdateParam(sqlFile, group);
	}
}

void Column::writeSqlUpdateSet(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::EDIT)) return;
	if(_type==cmsame::listof) return;
	string name = getSqlName();

	sqlFile << "SET ValueParam = IF(ISNULL(Param" << name << "), 'NULL', CONCAT('''', CONCAT(Param" <<
		name << " ,'''')));" << endl_ind << "SET SqlUpdate = IF(COALESCE(Move" << name << ", 0), " <<
		"CONCAT(SqlUpdate, CONCAT(IF(LENGTH(SqlUpdate) = 0,'" << _table->getSingular() << name <<
		" = ', ', " << _table->getSingular() << name << " = '), ValueParam)), SqlUpdate);" << endl_ind;

	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
		(*column)->writeSqlUpdateSet(sqlFile, group);
	}
}

bool Column::writeSqlInsertParam(ofstream& sqlFile, string group, bool first) {
	if(!_permission->hasGroup(group, cmsame::ADD)) return false;
	if(_type==cmsame::listof) return false;
	if(!first) sqlFile << ", ";
	sqlFile << "Param" << getSqlName() << " " << getSqlBaseType();
	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
		first = !(*column)->writeSqlInsertParam(sqlFile, group, first) && first;
	}
	return true;
}
//only used by insert
bool Column::writeSqlName(ofstream& sqlFile, string group, bool first) {
	if(!_permission->hasGroup(group, cmsame::ADD)) return false;
	if(_type==cmsame::listof) return false;
	if(!first) sqlFile << ", ";
	sqlFile << _table->getSingular() << getSqlName();
	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
		first = !(*column)->writeSqlName(sqlFile, group, first) && first;
	}
	return true;
}
//only used by insert
bool Column::writeSqlParam(ofstream& sqlFile, string group, bool first) {
	if(!_permission->hasGroup(group, cmsame::ADD)) return false;
	if(_type==cmsame::listof) return false;
	if(!first) sqlFile << ", ";
	sqlFile << "Param" << getSqlName();
	for(vector<Column*>::iterator column = _subColumns.begin(); column != _subColumns.end(); ++column) {
		first = !(*column)->writeSqlParam(sqlFile, group, first) && first;
	}
	return true;
}

void Column::writeSqlAddForeignKey(ofstream& sqlFile) {
	if(_type==cmsame::oneof)
		sqlFile << "ALTER TABLE " << _table->getPlural() << " ADD CONSTRAINT " << _table->getSingular() <<
			_name << "Constraint FOREIGN KEY (" << _table->getSingular() << getSqlName() <<
			") REFERENCES " << _refTarget->getPlural() << "(" << _refTarget->getSingular() <<
			_refTarget->getIdColumn()->getName() << ");" << endl_ind;
}

void Column::writeSqlDropForeignKey(ofstream& sqlFile) {
	if(_type==cmsame::oneof)
		sqlFile << "ALTER TABLE " << _table->getPlural() << " DROP FOREIGN KEY " << _table->getSingular() <<
			_name << "Constraint;" << endl_ind;

}

void Column::writeSqlRecordInsert(stringstream& name, stringstream& value, string data, Record* record) {
	string pw, un, salt, hash;
	size_t id;
	bool found;
	switch(_type)
	{
		case cmsame::none:
		case cmsame::imageupload:
		case cmsame::fileupload:
		case cmsame::listof:
			break;
		case cmsame::id:
		case cmsame::integer:
		case cmsame::decimal:
		case cmsame::datetime:
			name << _table->getSingular() << _name;
			value << data;
			break;
		case cmsame::oneof:
			name << _table->getSingular() << getSqlName();
			id = _refTarget->getRecordId(data);
			if(id==0) {
				_isOk = false;
				_statusMessage = "Record error: couldn't find a " + _refTarget->getSingular() + " called \"" + data + "\".";
			}
			value << id;
			break;
		case cmsame::bigtext:
		case cmsame::text:
		case cmsame::tokens:
			name << _table->getSingular() << _name;
			value << "'";
			for(string::iterator i=data.begin();i!=data.end();++i){
				if(*i=='\'') value << "''";
				else if (*i=='"') value << "\"\"";
				else value << (*i);
			}
			value << "'";
			break;
		case cmsame::password:
			name << _table->getSingular() << _name << ", " << _table->getSingular() << _name << "Salt";
			un=record->getValue("Username");
			//magic words: 'ask', 'random', 'mnemonic', 'literal:*'
			if(data.compare("ask") == 0) {
				pw=askpw(un);
			} else if(data.compare("random") == 0) {
				pw=choosepw(un, false);
			} else if(data.compare("mnemonic") == 0) {
				pw=choosepw(un, true);
			} else if(data.length() > 8 && data.substr(0,8).compare("literal:") == 0) {
				pw = data.substr(8, data.length()-8);
			} else {
				_isOk = false;
				_statusMessage = "Generate error: invalid password.";
			}
			if(!Crypto::isStrongPassword(pw)) {
				_isOk = false;
				_statusMessage = "Generate error: weak password.";
			}
			salt = Crypto::getRandomPassword();

			switch(((UserTable*)_table)->getHashFunc()) {
				case cmsame::md5:
					_isOk = false;
					_statusMessage = "MD5 is not supported.";
					break;

				case cmsame::blake2s:
					hash = Crypto::blake2s128(salt + pw);
					break;

			}

			value << "'" << hash << "', '" << salt << "'";
			break;
		case cmsame::options:
			name << _table->getSingular() << _name;
			//parse for presets
			found=false;
			for(vector<string*>::iterator preset = _values.begin(); preset != _values.end(); ++preset) {
				if(strcmp(data.c_str(), (*preset)->c_str()) == 0) {
					value << (distance(_values.begin(), preset) + 1);
					found = true;
					break;
				}
			}
			if(!found) {
				_isOk = false;
				_statusMessage = "Generate error: unrecognized value in record.";
			}
			break;
		case cmsame::boolean:
			name << _table->getSingular() << _name;
			if(strcmp(data.c_str(), "true") == 0) {
				value << "1";
			} else if(strcmp(data.c_str(), "false") == 0) {
				value << "0";
			} else {
				_isOk = false;
				_statusMessage = "Generate error: unrecognized value in record.";
			}

			break;
	}
}

void Column::writeSqlCreateCount(ofstream& sqlFile, string& group) {
	if(_type!=cmsame::oneof) return;

	if(!_permission->hasGroup(group, cmsame::VIEW)) return;

	sqlFile << "CREATE PROCEDURE " << _table->getSchema()->getSite()->getSiteName() <<
		"." << group << _table->getSingular() << "CountBy" << _name <<
		"(" << getSqlName() << " INT)" << endl_ind <<
		"BEGIN" << inc_ind << endl_ind <<
		"SELECT COUNT(*) AS Count FROM " <<
		_table->getSchema()->getSite()->getSiteName() << "." << _table->getPlural() << " WHERE " <<
		_table->getSingular() << getSqlName() << " = " << getSqlName() << " OR (" << _table->getSingular() <<
		getSqlName() << " IS NULL AND " << getSqlName() << " IS NULL);" << dec_ind << endl_ind <<
		"END |" << endl_ind;

}

void Column::writeSqlDropCount(ofstream& sqlFile) {
	if(_type!=cmsame::oneof) return;

	for(vector<string*>::iterator group = Permission::getAllGroups().begin(); group != Permission::getAllGroups().end(); ++group) {
		if(!_permission->hasGroup(**group, cmsame::VIEW)) continue;

		sqlFile << "DROP PROCEDURE " << _table->getSchema()->getSite()->getSiteName() <<
			"." << **group << _table->getSingular() << "CountBy" << _name << ";" << endl_ind;

	}
}

void Column::writeGrantExec(ofstream& sqlFile, string& user, string group) {
	if(_type!=cmsame::oneof||!_permission->hasGroup(group, cmsame::VIEW)) return;

	sqlFile << "GRANT EXECUTE ON PROCEDURE " << _table->getSchema()->getSite()->getSiteName() <<
			"." << group << _table->getSingular() << "CountBy" << _name << " TO " << user << ";" << endl_ind;


}

//these functions assume all columns being set to null are nullable
//if not, there will be a validation error stopping the procedure being called
void Column::writeSqlInsertMirror(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::EDIT)) return;
	if(!_permission->hasGroup(group, cmsame::ADD)) return;
	if(_type!=cmsame::oneof) return;
	if(!_mirror) return;
	if(!_mirror->hasType(cmsame::oneof)) return;
	//1. any _other_ records in this table referring to the same id as we are must be nulled
	sqlFile << "UPDATE " << _table->getSchema()->getSite()->getSiteName() << "." << _table->getPlural() <<
		" SET " << _table->getSingular() << getSqlName() << " = NULL WHERE " <<
		_table->getSingular() << getSqlName() << " = Param" << getSqlName() << " AND " <<
		_table->getSingular() << _table->getIdColumn()->getSqlName() << " <> New" <<
		_table->getSingular() << _table->getIdColumn()->getSqlName() << ";" << endl_ind;
	//2. the mirror column in reftarget table must be set to point to the id just generated
	sqlFile << "UPDATE " << _table->getSchema()->getSite()->getSiteName() << "." << _refTarget->getPlural() <<
		" SET " << _refTarget->getSingular() << _mirror->getSqlName() << " = New" << _table->getSingular() <<
		_table->getIdColumn()->getName() << " WHERE " << _refTarget->getSingular() << _refTarget->getIdColumn()->getName() <<
		" = Param" << getSqlName() << ";" << endl_ind;

}
void Column::writeSqlUpdateMirror(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::EDIT)) return;
	if(_type!=cmsame::oneof) return;
	if(!_mirror) return;
	if(!_mirror->hasType(cmsame::oneof)) return;
	//1. any _other_ records in this table referring to the same id as we are must be nulled
	sqlFile << "UPDATE " << _table->getSchema()->getSite()->getSiteName() << "." << _table->getPlural() <<
		" SET " << _table->getSingular() << getSqlName() << " = NULL WHERE " <<
		_table->getSingular() << getSqlName() << " = Param" << getSqlName() << " AND " <<
		_table->getSingular() << _table->getIdColumn()->getSqlName() << " <> Locate" <<
		_table->getIdColumn()->getSqlName() << ";" << endl_ind;
	//2. any other records in the reftarget table referring to the id of the updated record must be nulled
	sqlFile << "UPDATE " << _table->getSchema()->getSite()->getSiteName() << "." << _refTarget->getPlural() <<
		" SET " << _refTarget->getSingular() << _mirror->getSqlName() << " = NULL WHERE " <<
		_refTarget->getSingular() << _mirror->getSqlName() << " = Locate" << _table->getIdColumn()->getSqlName() << ";" << endl_ind;
	//3. the mirror column in the reftarget table must be set to point to the id of the updated record
	sqlFile << "UPDATE " << _table->getSchema()->getSite()->getSiteName() << "." << _refTarget->getPlural() <<
		" SET " << _refTarget->getSingular() << _mirror->getSqlName() << " = Locate" <<
		_table->getIdColumn()->getSqlName() << " WHERE " << _refTarget->getSingular() << _refTarget->getIdColumn()->getName() <<
		" = Param" << getSqlName() << ";" << endl_ind;


}

void Column::writeSqlDeleteMirror(ofstream& sqlFile, string group) {
	if(!_permission->hasGroup(group, cmsame::EDIT)) return;
	if(!_permission->hasGroup(group, cmsame::DELETE)) return;
	if(_type!=cmsame::oneof) return;
	if(!_mirror) return;
	if(!_mirror->hasType(cmsame::oneof)) return;
	//1. any records in the reftarget table referring to the id of the deleted record must be nulled
	sqlFile << "UPDATE " << _table->getSchema()->getSite()->getSiteName() << "." << _refTarget->getPlural() <<
		" SET " << _refTarget->getSingular() << _mirror->getSqlName() << " = NULL WHERE " <<
		_refTarget->getSingular() << _mirror->getSqlName() << " = Locate" << _table->getIdColumn()->getSqlName() << ";" << endl_ind;
}

///////////
// MODEL //
///////////


void Column::writeModelConfig(ofstream& phpFile) {
	if(_type==cmsame::options) {
		phpFile << "['values' => [";
		for(vector<string*>::iterator value = _values.begin(); value != _values.end(); ++value)
		{
			if(value!=_values.begin()) phpFile << ", ";
			phpFile << "'" << **value << "'";
		}
		phpFile << "]]";
	} else if(_type==cmsame::oneof) {
		phpFile << "['type' => '" << _refTarget->getSingular() << "']";
	} else if(_type==cmsame::listof) {
		phpFile << "['type' => '" << _refTarget->getSingular() <<
			"', 'searchtype' => '" << _refTarget->getSingular() <<
			"Search', 'key' => '" << _mirror->getName() << "']";
	} else if(_type==cmsame::fileupload) {
		phpFile << "['maxbytes' => '" << _x << "', 'mime' => [";

		for(vector<string*>::iterator mimeType = _mimeTypes.begin(); mimeType != _mimeTypes.end(); ++mimeType) {
			if(mimeType != _mimeTypes.begin()) phpFile << ", ";
			phpFile << "'" << **mimeType << "'";
		}

		phpFile << "]]";
	} else if(_type==cmsame::imageupload) {
		phpFile << "['maxbytes' => '" << _x << "', 'mime' => [";

		for(vector<string*>::iterator mimeType = _mimeTypes.begin(); mimeType != _mimeTypes.end(); ++mimeType) {
			if(mimeType != _mimeTypes.begin()) phpFile << ", ";
			phpFile << "'" << **mimeType << "'";
		}

		phpFile << "], 'sizes' => [";
		for(vector<ImageSize*>::iterator size = _imageSizes.begin(); size != _imageSizes.end(); ++size)
		{
			if(size!=_imageSizes.begin()) phpFile << "," << endl_ind;
			(*size)->writeModelArray(phpFile);
		}
		phpFile << "]]";
	}
}


void Column::writeModelConstruct(ofstream& phpFile, bool search) {

	if(_type==cmsame::fileupload||_type==cmsame::imageupload||_type==cmsame::listof) {
		if(search) return;
		phpFile << "$this->" << _name << " = new " << getCmsameType() << "(NULL, ";
		writeModelConfig(phpFile);
		phpFile << ");" << endl_ind;
	} else if(_type==cmsame::options) {
		phpFile << "$this->" << _name << " = new " << getCmsameType() << "(NULL, ";
		writeModelConfig(phpFile);
		phpFile << ");" << endl_ind;
		if(search) {
			phpFile << "$this->_Compare['" << _name << "'] = NULL;" << endl_ind <<
				"$this->_Order['" << _name << "'] = NULL;" << endl_ind <<
				"$this->_Priority['" << _name << "'] = NULL;" << endl_ind;
		}
	} else if(_type==cmsame::oneof) {
		if(search) {
			phpFile << "$this->" << getSqlName() << " = new \\cmsame\\_int(NULL);" << endl_ind <<
				"$this->_Compare['" << _name << "'] = NULL;" << endl_ind <<
				"$this->_Order['" << _name << "'] = NULL;" << endl_ind <<
				"$this->_Priority['" << _name << "'] = NULL;" << endl_ind;
		} else {
			phpFile << "$this->" << _name << " = new " << getCmsameType() << "(NULL, ";
			writeModelConfig(phpFile);
			phpFile << ");" << endl_ind;
		}
	} else {
		phpFile << "$this->" << _name << " = new " << getCmsameType() << "(NULL);" << endl_ind;
		if(search) {
			phpFile << "$this->_Compare['" << _name << "'] = NULL;" << endl_ind <<
				"$this->_Order['" << _name << "'] = NULL;" << endl_ind <<
				"$this->_Priority['" << _name << "'] = NULL;" << endl_ind;
		}
	}
}

void Column::writeModelFromPost(ofstream& phpFile, UserTable* userTable) {
	switch(_type)
	{
		case cmsame::none:
		case cmsame::listof:
			break;

		case cmsame::id:
			phpFile << "if(isset($_POST['" << _table->getSingular() << _name << "'])) {" << inc_ind << endl_ind <<
				"$this->" << _name << "->set($_POST['" << _table->getSingular() << _name << "']);" << endl_ind <<
				"$this->load();" << dec_ind << endl_ind << "}" << endl_ind;
			break;

		case cmsame::integer:
		case cmsame::options:
		case cmsame::decimal:
		case cmsame::bigtext:
		case cmsame::text:
		case cmsame::tokens:
		case cmsame::datetime:
			phpFile << "if(isset($_POST['" <<
				_table->getSingular() << _name <<
				"'])) $this->" << _name << "->set($_POST['" <<
				_table->getSingular() << _name <<
				"']);" << endl_ind;
			break;
		case cmsame::boolean:
			phpFile << "if(isset($_POST['" <<
				_table->getSingular() << _name <<
				"'])) $this->" << _name << "->set(true); else $this->"
				<< _name << "->set(false);" << endl_ind;
			break;
		case cmsame::oneof:
			phpFile << "if(isset($_POST['" << _table->getSingular() << getSqlName() <<
				"'])) $this->" << _name << "->set($_POST['" <<
				_table->getSingular() << getSqlName() <<
				"']);" << endl_ind;
			break;

		case cmsame::password: //only used by pw change, not login
			phpFile << "if(" << userTable->getSingular() << "::currentGroup()=='_self'&&isset($_POST['Old" <<
				_table->getSingular() << _name << "'])) {" << inc_ind << endl_ind <<
				//need more creds to authorize
				"$this->old" << _name << " = new \\cmsame\\_password([$this->" << _name << "->val, $this->" << _name << "->salt]);" << endl_ind;
				//now $this->oldPassword has dummy hash and real salt

			//hashfunc
			switch(((UserTable*)_table)->getHashFunc()) {
				case cmsame::md5:
					phpFile << "$hash = md5($this->old" << _name << "->salt . $_POST['Old" <<
						_table->getSingular() << _name << "']);" << endl_ind;
					break;

				case cmsame::blake2s:
					phpFile << "$hash = \\cmsame\\crypto::blake2s128($this->old" << _name << "->salt . $_POST['Old" <<
						_table->getSingular() << _name << "']);" << endl_ind;
					break;

			}

			phpFile << "$this->old" << _name << "->set([$hash, $this->old" << _name << "->salt]);" << dec_ind << endl_ind <<
				//now $this->oldPassword should match DB
				"}" << endl_ind;



			phpFile << "if(isset($_POST['New" << _table->getSingular() << _name << "']) && isset($_POST['Confirm" <<
				_table->getSingular() << _name << "'])";

			phpFile << " && ((!is_null($_POST['New" << _table->getSingular() << _name << "'])&&strlen($_POST['New" <<
				_table->getSingular() << _name << "'])>0)||(isset($_POST['" << 	_table->getSingular() << _name <<
				"Remove'])&&$_POST['" << _table->getSingular() << _name << "Remove']=='Remove'))";
			phpFile << ") {" << inc_ind << endl_ind <<
				"if($_POST['New" << _table->getSingular() << _name <<
				"']==$_POST['Confirm" << _table->getSingular() << _name << "']) {" << inc_ind << endl_ind <<
				"$this->" << _name << "->set($_POST['New" << _table->getSingular() << _name << "']);" << dec_ind << endl_ind <<
				"} else {" << inc_ind << endl_ind <<
				"$this->" << _name << "->set(NULL);" << endl_ind <<
				"$this->" << _name << "->errors = ['Confirmation for @ does not match.'];" << dec_ind << endl_ind <<
				"}" << dec_ind << endl_ind <<
				"};" << endl_ind;

			break;

		case cmsame::imageupload:
		case cmsame::fileupload:
			//cmsfile::fromPost does all the work here
/*			phpFile << "$mt = [";
			for(vector<string*>::iterator mimeType = _mimeTypes.begin(); mimeType != _mimeTypes.end(); ++mimeType) {
				if(mimeType != _mimeTypes.begin()) phpFile << ", ";
				phpFile << "\"" << **mimeType << "\"";
			}
			phpFile << "];" << endl_ind;*/

			phpFile <<  "$this->" << _name << "->set(['" << _table->getSingular() << "', '" <<
				_name << "']);" << endl_ind;


			break;
	}
}



void Column::writeModelFromRow(ofstream& phpFile, UserTable* userTable) {

//	if(_table==userTable) {
//		phpFile << "if(" << userTable->getSingular() << "::hasGroup('";
//		_permission->writeViewGroups(phpFile);
//		phpFile << "') || (" << userTable->getSingular() << "::isLoggedIn() && " <<
//			userTable->getSingular() << "::current()->" << _table->getIdColumn()->getName() <<
//			"->val == $this->" << _table->getIdColumn()->getName() << "->val)) {" << inc_ind << endl_ind;
//	} else {
		phpFile << "if(" << userTable->getSingular() << "::hasGroups('";
		_permission->writeViewGroups(phpFile);
		phpFile << "')) {" << inc_ind << endl_ind;
//	}

	switch(_type)
	{
		case cmsame::none:
			break;

		case cmsame::oneof:
			phpFile << "if($recur&&isset($row[$alias.'" << _name << _refTarget->getSingular() << _refTarget->getIdColumn()->getSqlName() <<
				"'])) {" << inc_ind << endl_ind <<
				"$item = new " << _refTarget->getSingular() << "();" << endl_ind <<
				"$item->fromRow($row, false, '" << _name << "');" << endl_ind <<
				"$this->" << _name << "->set($item);" << dec_ind << endl_ind <<
				"} else $this->" << _name << "->set($row[$alias.'" << _table->getSingular() << getSqlName() << "']);" << endl_ind;

			break;

		case cmsame::listof:
			phpFile << "$this->" << _name << "->set([$row[$alias.'" << _table->getSingular() <<
				_table->getIdColumn()->getSqlName() <<
				 "'], $row[$alias.'" << _table->getSingular() << _name <<
				_refTarget->getSingular() << "Count']]);" << endl_ind;
			break;

		case cmsame::id:
		case cmsame::integer:
		case cmsame::options:
		case cmsame::decimal:
		case cmsame::tokens:
		case cmsame::boolean:
		case cmsame::datetime:
			phpFile << "$this->" << _name << "->set($row[$alias.'" << _table->getSingular() << getSqlName() << "']);" << endl_ind;
			break;
		case cmsame::bigtext:
		case cmsame::text:
			switch(_renderMode) {
				case cmsame::plain:
					phpFile << "$this->" << _name << "->set($row[$alias.'" <<
						_table->getSingular() << getSqlName() << "']);" << endl_ind;
					break;
				case cmsame::markdown:
					phpFile << "$this->" << _name << "->set([$row[$alias.'" <<
						_table->getSingular() << getSqlName() << "'], $row[$alias.'" <<
						_table->getSingular() << getSqlName() << "MarkdownHtml'], $row[$alias.'" <<
						_table->getSingular() << getSqlName() << "MarkdownText']]);" << endl_ind;
					break;
				case cmsame::mdlite:
					phpFile << "$this->" << _name << "->set([$row[$alias.'" <<
						_table->getSingular() << getSqlName() << "'], $row[$alias.'" <<
						_table->getSingular() << getSqlName() << "MdHtml'], $row[$alias.'" <<
						_table->getSingular() << getSqlName() << "MdText']]);" << endl_ind;
					break;
			}
			break;
		case cmsame::imageupload:
		case cmsame::fileupload:
			phpFile << "$this->" << _name << "->set([$row[$alias.'" << _table->getSingular() <<
				_name << "'], $row]);" << endl_ind;
			break;
		case cmsame::password:
			phpFile << "$this->" << _name << "->set([$row[$alias.'" << _table->getSingular() <<
				_name << "'], $row[$alias.'" << _table->getSingular() << _name << "Salt']]);" << endl_ind;
			break;


	}
	phpFile << dec_ind << endl_ind << "} else $this->" << _name << "->set(NULL);" << endl_ind;
}


bool Column::writeModelUpdateParam(ofstream& phpFile, bool first) {
	if(_type==cmsame::listof) return false;
	string name=getSqlName();
	phpFile << (first ? "" : ", ") << "$move" << name << " = true";
	return true;
}

bool Column::writeModelOpenParam(ofstream& phpFile, bool first) {
	if(_type!=cmsame::listof&&_type!=cmsame::oneof) return false;
	phpFile << (first ? "" : ", ") << "$open" << _name << " = " << (_type==cmsame::oneof ? "true" : "false");
	return true;
}
void Column::writeModelOpenValue(ofstream& phpFile, UserTable* userTable) {
	if(_type!=cmsame::listof&&_type!=cmsame::oneof) return;
	phpFile << "if(" << userTable->getSingular() << "::hasGroups('";
	_permission->writeViewGroups(phpFile);
	phpFile << "')) $query .= ($open" << _name << " ? ', 1' : ', 0');" << endl_ind;
	return;
}

void Column::writeModelUpdateValue(ofstream& phpFile, UserTable* userTable) {
	if(_type==cmsame::listof) return;
	string name=getSqlName();
/*
	if(_table==userTable) {
		phpFile << "$move" << _name << " = $move" << _name << " && (" <<
			userTable->getSingular() << "::hasgroup('";
		_permission->writeEditGroups(phpFile);
		phpFile << "') || (" << userTable->getSingular() << "::isLoggedIn() && " <<
			userTable->getSingular() << "::current()->" << _table->getIdColumn()->getName() <<
			"->val == $this->" << _table->getIdColumn()->getName() << "->val));" << endl_ind;
	} else {*/
		phpFile << "if(" << userTable->getSingular() << "::hasGroups('";
		_permission->writeEditGroups(phpFile);
		phpFile << "')) {" << inc_ind << endl_ind;
//	}

	writeModelUpdateFile(phpFile);

	if(_type==cmsame::password) { //special case
		phpFile << "if($this->" << _name << "->val=='=============HIDDEN=============') {" << inc_ind << endl_ind <<
			"$query .= ', NULL, 0, NULL, 0';" << dec_ind << endl_ind <<
			"} else {" << inc_ind << endl_ind <<
			"$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2)" <<
			" . ', ' . ($move" << name <<  " ? 1 : 0);" << endl_ind <<
			"$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2, 'Salt')" <<
			" . ', ' . ($move" << name << " ? 1 : 0);" << dec_ind << endl_ind <<
			"}" << endl_ind;

	} else {//general case

		phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2)" <<
		" . ', ' . ($move" << name <<  " ? 1 : 0);" << endl_ind;
		//extra fields
		if(_type==cmsame::fileupload) {
		phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2, 'Bytes')" <<
			" . ', ' . ($move" << name << " ? 1 : 0);" << endl_ind;
		} else if(_type==cmsame::imageupload) {
			phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2, 'Bytes')" <<
				" . ', ' . ($move" << name << " ? 1 : 0);" << endl_ind;
			phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2, 'Width')" <<
				" . ', ' . ($move" << name << " ? 1 : 0);" << endl_ind;
			phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2, 'Height')" <<
				" . ', ' . ($move" << name << " ? 1 : 0);" << endl_ind;

			for(vector<ImageSize*>::iterator imageSize = _imageSizes.begin(); imageSize != _imageSizes.end(); ++imageSize) {
				phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2, '" <<
					(*imageSize)->getName() << "Bytes')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
				phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name << ", 2, '" <<
					(*imageSize)->getName() << "Width')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
				phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name << ", 2, '" <<
					(*imageSize)->getName() << "Height')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;

				if((*imageSize)->hasStepMode(cmsame::dynamic_free)||(*imageSize)->hasStepMode(cmsame::dynamic_lock)) {
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 2, '" <<
						(*imageSize)->getName() << "PreBytes')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name << ", 2, '" <<
						(*imageSize)->getName() << "PreWidth')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name << ", 2, '" <<
						(*imageSize)->getName() << "PreHeight')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
				}
			}

		} else if(_type==cmsame::text&&_renderMode!=cmsame::plain) {

			switch(_renderMode) {
				case cmsame::plain:
					break;
				case cmsame::markdown:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name <<
						", 2, 'MarkdownSLHtml')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name <<
						", 2, 'MarkdownSLText')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
				break;
				case cmsame::mdlite:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name <<
						", 2, 'MdSLHtml')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name <<
						", 2, 'MdSLText')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
				break;
			}

		} else if(_type==cmsame::bigtext&&_renderMode!=cmsame::plain) {

			switch(_renderMode) {
				case cmsame::plain:
					break;
				case cmsame::markdown:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name <<
						", 2, 'MarkdownHtml')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name <<
						", 2, 'MarkdownText')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
				break;
				case cmsame::mdlite:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name <<
						", 2, 'MdHtml')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << name <<
						", 2, 'MdText')" << " . ', ' . ($move" << _name << " ? 1 : 0);" << endl_ind;
				break;
			}

		}
	}

	phpFile << dec_ind << endl_ind << "}" << endl_ind;

}
/*
void Column::writeModelInsertColumn(ofstream& phpFile) {
	phpFile << _table->getSingular() << _name;
}*/

bool Column::writeModelInsertValue(ofstream& phpFile, bool first) {
	if(_type==cmsame::listof) return false;
//	string name=getSqlName();
	if(_type==cmsame::fileupload)
		//can't ever write file paths on insert; don't know Id yet
		phpFile << "$query .= \"" << (first ? "" : ", ") << "NULL, NULL\";" << endl_ind;
	else if ( _type == cmsame::imageupload) {
		phpFile << "$query .= \"" << (first ? "" : ", ") << "NULL, NULL, NULL, NULL\";" << endl_ind;
		for(vector<ImageSize*>::iterator imageSize = _imageSizes.begin(); imageSize != _imageSizes.end(); ++imageSize) {
			if((*imageSize)->hasStepMode(cmsame::dynamic_free)||(*imageSize)->hasStepMode(cmsame::dynamic_lock))
				phpFile << "$query .= \", NULL, NULL, NULL, NULL, NULL, NULL\";" << endl_ind;
			else	phpFile << "$query .= \", NULL, NULL, NULL\";" << endl_ind;
		}
	} else  {
		phpFile << "$query .= " << (first ? "" : "', ' . ") << "\\cmsame\\getsql::sql($this->" << _name << ");" << endl_ind;
		//extra fields
		if(_type==cmsame::password) {
			phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'Salt');" << endl_ind;
		} else if(_type==cmsame::text&&_renderMode!=cmsame::plain) {
			switch(_renderMode) {
				case cmsame::plain:
					break;
				case cmsame::markdown:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MarkdownSLHtml');" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MarkdownSLText');" << endl_ind;
					break;
				case cmsame::mdlite:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MdSLHtml');" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MdSLText');" << endl_ind;
					break;
			}
		} else if(_type==cmsame::bigtext&&_renderMode!=cmsame::plain) {
			switch(_renderMode) {
				case cmsame::plain:
					break;
				case cmsame::markdown:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MarkdownHtml');" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MarkdownText');" << endl_ind;
					break;
				case cmsame::mdlite:
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MdHtml');" << endl_ind;
					phpFile << "$query .= ', ' . \\cmsame\\getsql::sql($this->" << _name << ", 1, 'MdText');" << endl_ind;
					break;
			}
		}
	}
	return true;
}

void Column::writeModelSearchOrder(ofstream& phpFile) {
	//don't attempt search on files for now
	if(_type == cmsame::fileupload || _type == cmsame::imageupload || _type == cmsame::listof) return;
	string name=_name;
	if(_type==cmsame::oneof) name=getSqlName();
//if($pt->_Priority->_name < $p && $pt.._name > $l) {
//      $p=$pt->.._name;$o='_name';
//      }
	phpFile << "if(array_key_exists('" << name << "', $pt->_Priority) && $pt->_Priority['" << name << "'] < $p && $pt->_Priority['" << name << "'] > $l) {" << inc_ind << endl_ind <<
		"$p=$pt->_Priority['" << name << "']; $o='" << name << "'; }" << dec_ind << endl_ind;
}

void Column::writeModelSearchValue(ofstream& phpFile) {
	//don't attempt search on files for now
	if(_type == cmsame::fileupload || _type == cmsame::imageupload || _type == cmsame::listof) return;
	string name=_name;
	if(_type==cmsame::oneof) name=getSqlName();
	phpFile << "if(array_key_exists('" << name << "', $pt->_Compare) && is_numeric($pt->_Compare['" << name << "'])) {" << inc_ind << endl_ind;
	phpFile << "$query .= \", \" . \\cmsame\\getsql::sql($pt->" << name << ", 2)";
	phpFile << " . \", \" . $pt->_Compare['" << name << "'];" << dec_ind << endl_ind;
	phpFile << "} else $query .= \", NULL, NULL\";" << endl_ind;
	//extra fields
//	if(_type==cmsame::password) {
//		phpFile << "if(!is_null($pt->" <<  _name << "Com)) {" << inc_ind << endl_ind;
//		phpFile << "$query .= \", \" . \\cmsame\\getsql::sql($pt->" << _name << ", 2, 'salt')";
//		phpFile << " . \", \" . $pt->" << _name << "Com;" << dec_ind << endl_ind;
//		phpFile << "} else $query .= \", NULL, NULL\";" << endl_ind;
//	} //else if(_type==cmsame::text&&_renderMode!=cmsame::plain) {
}

//deprecated?
void Column::writeModelCleanText(ofstream& phpFile) {
	if(_type!=cmsame::text && _type!=cmsame::bigtext) return;
	phpFile << "if(!$this->" << _name << "->isnull()) {" << inc_ind << endl_ind;
	for(vector<cmsame::CleanStep>::iterator step = _cleanSteps.begin(); step != _cleanSteps.end(); ++step) {
		switch(step->mode) {
			case cmsame::trim:
				phpFile << "$this->" << _name << "->set(trim($this->" << _name << "->val));" << endl_ind;
				break;
			case cmsame::striptags:
				phpFile << "$this->" << _name << "->set(strip_tags($this->" << _name << "->val));" << endl_ind;
				break;
			case cmsame::regex:
				phpFile << "if(preg_match('" << step->pattern << "', $this->" << _name << "->val, $m)) $this->" << 
					_name << "->set($m[" << step->index <<  "]);" << endl_ind;
				break;
		}
	}
	phpFile << dec_ind << endl_ind << "}" << endl_ind;
}

void Column::writeModelRenderText(ofstream& phpFile) {
	if(_type!=cmsame::text && _type!=cmsame::bigtext) return;
	switch(_renderMode) {
		case cmsame::plain:
			break;
		case cmsame::markdown:
			phpFile << "public function get" << _name << "Markdown() {" << inc_ind << endl_ind <<
				"if($this->" << _name << "->isnull()) return '';" << endl_ind <<
				"$md = new \\cmsame\\markdown();" << endl_ind;

			if(_type==cmsame::bigtext)
				phpFile << "return $md->get_html($this->" << _name << "->val);" << dec_ind << endl_ind;
			else if(_type==cmsame::text)
				phpFile << "return $md->get_html_singleline($this->" << _name << "->val);" << dec_ind << endl_ind;

			phpFile << "}" << endl_ind;
			break;
		case cmsame::mdlite:
			phpFile << "public function get" << _name << "Md() {" << inc_ind << endl_ind <<
				"if($this->" << _name << "->isnull()) return '';" << endl_ind <<
				"$md = new \\cmsame\\mdlite();" << endl_ind;
			if(_type==cmsame::bigtext)
				phpFile << "return $md->get_html($this->" << _name << "->val);" << dec_ind << endl_ind;
			else if(_type==cmsame::text)
				phpFile << "return $md->get_html_singleline($this->" << _name << "->val);" << dec_ind << endl_ind;

			phpFile << "}" << endl_ind;
			break;
	}
}

void Column::writeModelValidate(ofstream& phpFile) {

	switch(_type) {
		case cmsame::none:
			break;

		case cmsame::id:
		case cmsame::listof:
		case cmsame::oneof:
		case cmsame::integer:
		case cmsame::decimal:
			break;
		case cmsame::options:

			if(!_null) phpFile << "if($this->" << _name << "->val == 0) $errors ['" << _table->getSingular() << _name <<
				"'] = \"" << _name << " must not be empty.\";" << endl_ind;

			break;
		case cmsame::bigtext:
		case cmsame::text:
			//text clean

			phpFile << "$buf = $this->" << _name << "->val;" << endl_ind;

			for(vector<cmsame::CleanStep>::iterator step=_cleanSteps.begin();step!=_cleanSteps.end();++step) {
				switch(step->mode) {
					case cmsame::trim:
						phpFile << "$buf = trim($buf);" << endl_ind;
						break;
					case cmsame::striptags:
						phpFile << "$buf = strip_tags($buf);" << endl_ind;
						break;
					case cmsame::regex:
						phpFile << "if(!$this->" << _name << "->isnull()&&!preg_match('" << step->pattern << "', $buf, $m)) $errors ['" << 
							_table->getSingular() << _name << "'] = '" << _name << 
							" is not in the correct format.';" << endl_ind <<
							"else $buf = $m[" << step->index <<  "];" << endl_ind;
						break;
				}

			}

			phpFile << "if($buf!=$this->" << _name << "->val) {" << inc_ind << endl_ind <<
				"$updates['" << _table->getSingular() << _name << "'] = $buf;" << endl_ind <<
				"$this->" << _name << "->set($buf);" << dec_ind << endl_ind <<
				"}" << endl_ind;

			if(!_null) phpFile << "if(!$this->" << _name << "->isnull() && strlen($this->" << _name <<
				"->val) == 0) $errors ['" << _table->getSingular() << _name <<
				"'] = '" << _name << " must not be empty.';" << endl_ind;

			if(_type==cmsame::text) phpFile << "if(!$this->" << _name << "->isnull() && strlen($this->" << 
				_name << "->val) > " << _x << ") $errors['" << _table->getSingular() << _name << 
				"'] = '" << _name << " must be shorter than " << _x << " characters.';" << endl_ind;
			break;
		case cmsame::password:

		case cmsame::imageupload:
		case cmsame::fileupload:
			//don't post the whole file in AJAX validation
			//just check size and MIME type
			phpFile << "if(isset($_POST['" << _table->getSingular() << _name <<
				"UploadValidateSize'])&&intval($_POST['" << _table->getSingular() <<
				_name << "UploadValidateSize'])>" << _x << ") $errors['" << _table->getSingular() << _name <<
				"'] = '" << _name << " must be " << _x << " bytes or smaller.';" << endl_ind;
			phpFile << "if(isset($_POST['" << _table->getSingular() << _name <<
				"UploadValidateType'])&&!in_array($_POST['" << _table->getSingular() <<
				_name << "UploadValidateType'],[";

			for(vector<string*>::iterator mimeType = _mimeTypes.begin(); mimeType != _mimeTypes.end(); ++mimeType) {
				if(mimeType!=_mimeTypes.begin()) phpFile << ",";
				phpFile << "'" << **mimeType << "'";
			}

			phpFile << "])) $errors['" << _table->getSingular() << _name <<
				"'] = '" << _name << " must be one of the allowed file types.';" << endl_ind;
			break;
		case cmsame::tokens:
			//NYI
			break;
		case cmsame::boolean:
			break;
		case cmsame::datetime:

			break;
		default:
			break;


	}

	if(!_null) phpFile << "if($this->" << _name << "->isnull()&&!$this->" << _name << "->errors) $errors [\"" <<
		_table->getSingular() << _name << "\"] = \"" << _name << " must not be empty.\";" << endl_ind;


	phpFile << "if($this->" << _name << "->errors) $errors[\"" << _table->getSingular() << _name <<
		"\"] = str_replace('@', '" << _name << "', $this->" << _name << "->errors[0]);" << endl_ind;


}

//TODO: allow custom defaults in xml
void Column::writeModelDefault(ofstream& phpFile) {
	switch(_type)
	{
		case cmsame::none:
			break;

		case cmsame::id:
		case cmsame::integer:
		case cmsame::options:
		case cmsame::decimal:
		case cmsame::bigtext:
		case cmsame::text:
		case cmsame::password:
		case cmsame::imageupload:
		case cmsame::fileupload:
		case cmsame::tokens:
		case cmsame::boolean:
		case cmsame::listof:
			break;

		case cmsame::oneof:
			phpFile << "if(isset($_GET['" << _table->getSingular() << _name << _refTarget->getSingular() <<
				_refTarget->getIdColumn()->getName() << "'])) {" << inc_ind << endl_ind <<
				"$this->" << _name << "->set(intval($_GET['" <<
				_table->getSingular() << _name << _refTarget->getSingular() <<
				_refTarget->getIdColumn()->getName() << "']));" << endl_ind <<
				//should be possible to omit the open step eventually
				//"$this->" << _name << "->open();" <<
				dec_ind << endl_ind <<
				"}" << endl_ind;
			break;

		case cmsame::datetime:
			phpFile << "$this->" << _name << "->set(new DateTime(''));" << endl_ind;
			break;
	}
}

void Column::writeModelInsertFile(ofstream& phpFile) {
	if(_type != cmsame::fileupload && _type != cmsame::imageupload) return;
	phpFile << "if($this->" << _name << "->needsSync()) $files = true;" << endl_ind;
}
void Column::writeModelUpdateFile(ofstream& phpFile) {
	if(_type != cmsame::fileupload && _type != cmsame::imageupload) return;
	phpFile << "if($this->" << _name <<
		"->needsSync()) {" << inc_ind << endl_ind <<
		"$this->" << _name << "->id = $this->" << _table->getIdColumn()->getName() << "->val;" << endl_ind <<
		"$this->" << _name << "->sync();" << endl_ind <<
		"$this->" << _name << "->cleanup(false);" << dec_ind << endl_ind <<
		"}" << endl_ind;
//		"if(!$this->" << _name << "->isnull() && !$this->" << _name << "->errors) $this->" << _name << "->set(NULL);" << endl_ind;
}
void Column::writeModelDeleteFile(ofstream& phpFile) {
	if(_type != cmsame::fileupload && _type != cmsame::imageupload) return;
	phpFile << "$this->" << _name << "->cleanup(true);" << endl_ind;
}
void Column::writeModelMirror(ofstream& phpFile) {
	if(_type == cmsame::oneof) {
		phpFile << "if($key=='" << _name << "') {" << inc_ind << endl_ind <<
			"$this->" << getSqlName() << "->set($id);" << endl_ind <<
			"$this->_Compare['" << getSqlName() << "']=Compare::Equal;" << dec_ind << endl_ind <<
			"}" << endl_ind;
	}

}

void Column::writeModelCount(ofstream& phpFile, UserTable* userTable) {
	if(_type != cmsame::oneof) return;
	phpFile << "function countBy" << _name << "($" << getSqlName() << ") {" << inc_ind << endl_ind <<
		"if(!" << userTable->getSingular() << "::hasGroups('";

	_permission->writeViewGroups(phpFile);

	phpFile << "')) return;" << endl_ind <<
		"$query='CALL " << _table->getSchema()->getSite()->getSiteName() << ".';" << endl_ind;

	vector<string*> allGroups = Permission::getAllGroups();

	for(vector<string*>::iterator group = allGroups.begin(); group != allGroups.end(); ++group) {
		if(group!=allGroups.begin()) phpFile << "else ";
		phpFile << "if(" << userTable->getSingular() << "::hasGroup('" << **group <<
			"')) $query .= '" << **group << "';" << endl_ind;
	}

	phpFile << "$query .= '" << _table->getSingular() << "CountBy" << _name << "(' . (is_null($" <<
		getSqlName() << ") ? 'NULL' : strval($" << getSqlName() << ")) . ');';" << endl_ind <<
		"$rows = " << _table->getSchema()->getSite()->getSiteName() << "::read_assoc($query);" << endl_ind <<
		"return $rows[0][\"Count\"];" << dec_ind << endl_ind <<
		"}" << endl_ind;
}

/////////
// CMS //
/////////

bool Column::writeCmsIndexCount(ofstream& phpFile, UserTable* userTable, bool first) {
	if(_type!=cmsame::oneof) return false;
	if(!first) phpFile << "else ";

	phpFile << "if(isset($_GET['" << _table->getSingular() << getSqlName() << "'])) $count = " <<
		_table->getSingular() << "::countBy" << _name << "(intval($_GET['" << _table->getSingular() <<
		getSqlName() << "']));" << endl_ind;
	return true;
}

bool Column::writeCmsIndexFilter(ofstream& phpFile, UserTable* userTable, bool first) {
	if(_type!=cmsame::oneof) return false;
	if(_table->getSchema()->getSite()->isVerbose()) cout << "Writing index filter for " << _name << "." << endl;
	if(!first) phpFile << "else ";

	phpFile << "if(isset($_GET['" << _table->getSingular() << getSqlName() << "'])) { " << inc_ind << endl_ind <<
		"$pt->" << getSqlName() << "->set(intval($_GET['" << _table->getSingular() << getSqlName() << "']));" << endl_ind <<
		"$pt->_Compare['" << getSqlName() << "'] = Compare::Equal;" << endl_ind <<
		"$pi = new " << _refTarget->getSingular() << "();" << endl_ind <<
		"$pi->" << _refTarget->getIdColumn()->getName() << "->set(intval($_GET['" << _table->getSingular() << getSqlName() << "']));" << endl_ind <<
		"$pi->load();" << endl_ind <<
		"$fttl = '(" << _name << " = ' . $pi->getName() . ')';" << endl_ind <<
		"$furl = '../" << _refTarget->getPlural() << "/detail.php?" << _refTarget->getSingular() <<
			_refTarget->getIdColumn()->getName() <<	"=' . intval($_GET['" << _table->getSingular() << getSqlName() <<
			"']);" << endl_ind <<
		"$fbtn = 'Back to " << _refTarget->getSingular() << "';" << dec_ind << endl_ind << "}" << endl_ind;

	return true;
}

void Column::writeCmsIndexHeader(ofstream& phpFile, UserTable* userTable) {
	if(!_browse) return;
	if(_type == cmsame::none || _type == cmsame::id) return;
	if(_table->getSchema()->getSite()->isVerbose()) cout << "Writing index table header for " << _name << "." << endl;

	phpFile << "if(" << userTable->getSingular() <<
		"::hasGroups(\"";

	_permission->writeViewGroups(phpFile);

	phpFile	<< "\")) echo '<th>" << _name << "</th>';" << endl_ind;

}

void Column::writeCmsIndexContent(ofstream& phpFile, UserTable* userTable)
{
	if(_table->getSchema()->getSite()->isVerbose()) cout << "Column::writeCmsIndexContent" << endl;

	if(!_browse) return;
	if(_type == cmsame::none || _type == cmsame::id) return;


	if(_table->getSchema()->getSite()->isVerbose()) cout << "Writing index table cell for " << _name << "." << endl;

	phpFile << "if(" << userTable->getSingular() << "::hasGroups(\"";

	_permission->writeViewGroups(phpFile);

	phpFile	<< "\")) echo '<td class=\"data\">' . ";

	switch(_type)
	{
		case cmsame::none:
		case cmsame::id:
			break;

		case cmsame::boolean:
		case cmsame::integer:
		case cmsame::decimal:
		case cmsame::password:
		case cmsame::tokens:
		case cmsame::options:
		case cmsame::datetime:
		case cmsame::bigtext:
		case cmsame::text:
			phpFile << "\\cmsame\\getcms::readonly($item->" << _name << ")";
			break;

		case cmsame::imageupload:
		case cmsame::fileupload:
			phpFile << "\\cmsame\\getcms::linkfile($item->" << _name << ", '../..')";
			break;
		case cmsame::oneof:

			if(_table->getSchema()->getSite()->isVerbose()) {
				cout << "reftarget pointer=" << _refTarget << ", ";
				cout << "singular=" << _refTarget->getSingular() << ", ";
				cout << "plural=" << _refTarget->getPlural() << endl;
			}

			phpFile << "($item->" << _name << "->isnull() ? '(no " << _refTarget->getSingular() <<
				")' : ($item->" << _name << "->isOpen ? \\cmsame\\getcms::readonly($item->" << _name <<
				"->val->" << _refTarget->getNameColumn()->getName() << ") : '(" << _refTarget->getSingular() << ")'))";
			break;
		case cmsame::listof:

			if(_table->getSchema()->getSite()->isVerbose()) {
				cout << "reftarget pointer=" << _refTarget << ", ";
				cout << "singular=" << _refTarget->getSingular() << ", ";
				cout << "plural=" << _refTarget->getPlural() << endl;
			}

			phpFile << "($item->" << _name << "->isnull() ? '(no " << _refTarget->getPlural() <<
				")' : ($item->" << _name << "->count==1?'(1 " << _refTarget->getSingular() <<
				")' : ('(' . strval($item->" << _name << "->count) . ' " << _refTarget->getPlural() << ")')))";
			break;
	}

	phpFile << " . '</td>';" << endl_ind;
	if(_table->getSchema()->getSite()->isVerbose()) cout << "Finished writing index table cell for " << _name << "." << endl;
}

void Column::writeCmsDetailField(ofstream& phpFile, UserTable* userTable)
{
	if(_type==cmsame::none) return;

	if(_table->getSchema()->getSite()->isVerbose()) cout << "Writing detail field for " << _name << "." << endl;

	phpFile << "if(" << userTable->getSingular() << "::hasGroups('";

	_permission->writeEditGroups(phpFile);

	phpFile	<< "')) {" << inc_ind << endl_ind;

	//id has no field-class div wrapper, wheras password has three.
	//all other types have just one so we can generate a generic one
	if(_type!=cmsame::id&&_type!=cmsame::password) {
		phpFile	<< "echo '<div class=\"canedit field " << getCssClass() << "\"><div class=\"label\"><label for=\"" << _table->getSingular() << _name <<
				"\">" << _name << ":</label>";

		if(_description.length()>0) {
			phpFile << "<span>";
			//escape '
			for(size_t i=0;i<_description.size();i++) {
				if(_description[i]=='\'') phpFile << "&#39";
				else phpFile << _description[i];
			}
			phpFile << "</span>";
		}

		phpFile << "</div><div class=\"input\">';" << endl_ind;

	}

	switch(_type)
	{
		case cmsame::none:
			break;
		case cmsame::id:
			phpFile << "echo '<input type=\"hidden\" name=\"" << _table->getSingular() <<
				_name << "\" id=\"" << _table->getSingular() << _name <<
				"\" class=\"id\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name <<
				") . '\" />';" << endl_ind << endl_ind;
			break;

		case cmsame::integer:
			phpFile << "echo '<input type=\"text\" name=\"" << _table->getSingular() <<
				_name << "\" id=\"" << _table->getSingular() << _name <<
				"\" class=\"integer\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '\" />';" << endl_ind;
			break;
		case cmsame::decimal:
			phpFile << "echo '<input type=\"text\" name=\"" << _table->getSingular() <<
				_name << "\" id=\"" << _table->getSingular() << _name <<
				"\" class=\"decimal\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '\" />';" << endl_ind;
			break;
		case cmsame::datetime:
			phpFile << "echo '<input type=\"hidden\" name=\"" << _table->getSingular() <<
					_name << "\" id=\"" << _table->getSingular() << _name <<
					"\" class=\"datetime\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '\" />';" << endl_ind <<
				"echo '<input type=\"text\" class=\"datepart\"/>';" << endl_ind <<
				"echo '<input type=\"text\" class=\"timepart\"/>';" << endl_ind;
			break;

		case cmsame::bigtext:
			if(_renderMode!=cmsame::plain) {

				phpFile << "echo '<div class=\"tabs\" id=\"" << _name << "-tabs\" >';" << endl_ind <<
					"echo '<ul><li><a href=\"#" << _name << "-tabs-comp\">Compose</a></li>';" << endl_ind;

				switch(_renderMode) {
					case cmsame::plain:
						break;
					case cmsame::markdown:
						phpFile << "echo '<li><a href=\"#" << _name << "-tabs-prev\">Preview</a></li>';" << endl_ind;
						break;
					case cmsame::mdlite:
						phpFile << "echo '<li><a href=\"#" << _name << "-tabs-prev\">Preview</a></li>';" << endl_ind;
						break;
				}

				phpFile << "echo '</ul><div id=\"" << _name <<  "-tabs-comp\">';" << endl_ind <<
					"echo '<textarea name=\"" << _table->getSingular() <<
						_name << "\" id=\"" << _table->getSingular() << _name <<
					"\" class=\"bigtext\">' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '</textarea></div>';" << endl_ind;

				switch(_renderMode) {
					case cmsame::plain:
						break;
					case cmsame::markdown:
						phpFile << "echo '<div id=\"" << _name << "-tabs-prev\"><div class=\"preview\"></div></div>';" << endl_ind;
						break;
					case cmsame::mdlite:
						phpFile << "echo '<div id=\"" << _name << "-tabs-prev\"><div class=\"preview\"></div></div>';" << endl_ind;
						break;
				}

				phpFile << "echo '</div>';" << endl_ind;

			} else {

				phpFile << "echo '<textarea name=\"" << _table->getSingular() <<
					_name << "\" id=\"" << _table->getSingular() << _name <<
					"\" class=\"bigtext\">' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '</textarea>';" << endl_ind;

			}

			break;

		case cmsame::text:
			if(_renderMode!=cmsame::plain) {
				phpFile << "echo '<div class=\"tabs\" id=\"" << _name << "-tabs\" >';" << endl_ind <<
					"echo '<ul><li><a href=\"#" << _name << "-tabs-comp\">Compose</a></li>';" << endl_ind;

				switch(_renderMode) {
					case cmsame::plain:
						break;
					case cmsame::markdown:
						phpFile << "echo '<li><a href=\"#" << _name << "-tabs-prev\">Preview</a></li>';" << endl_ind;
						break;
					case cmsame::mdlite:
						phpFile << "echo '<li><a href=\"#" << _name << "-tabs-prev\">Preview</a></li>';" << endl_ind;
						break;
				}

				phpFile << "echo '</ul><div id=\"" << _name <<  "-tabs-comp\">';" << endl_ind;
				phpFile << "echo '<input type=\"text\" name=\"" << _table->getSingular() <<
					_name << "\" id=\"" << _table->getSingular() << _name <<
					"\" class=\"text\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '\" /></div>';" << endl_ind;

				switch(_renderMode) {
					case cmsame::plain:
						break;
					case cmsame::markdown:
						phpFile << "echo '<div id=\"" << _name << "-tabs-prev\"><div class=\"preview\"></div></div>';" << endl_ind;
						break;
					case cmsame::mdlite:
						phpFile << "echo '<div id=\"" << _name << "-tabs-prev\"><div class=\"preview\"></div></div>';" << endl_ind;
						break;
				}

				phpFile << "echo '</div>';" << endl_ind;


			} else {
				phpFile << "echo '<input type=\"text\" name=\"" << _table->getSingular() <<
					_name << "\" id=\"" << _table->getSingular() << _name <<
					"\" class=\"text\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '\" />';" << endl_ind;
			}
			break;

		case cmsame::password:

			phpFile	<< "echo '<div class=\"field\"><div class=\"label\"><label for=\"" <<
				_table->getSingular() << _name << "\">" << _name <<
				":</label>";

			if(_description.length()>0) {
				phpFile << "<span>" << _description << "</span>";

			}

			phpFile << "</div><div class=\"data\">';" << endl_ind;
			phpFile << "echo $item->" << _name << "->isnull() ? '(none)' : '(encrypted)';" << endl_ind <<
				"if(!$item->" << _name << "->isnull()) echo '<input type=\"hidden\"  id=\"" << _table->getSingular() <<
				_name << "Remove\" name=\"" << _table->getSingular() << _name << "Remove\" class=\"doremove\" />';" << endl_ind <<
				"if(!$item->" << _name << "->isnull()) echo '<a class=\"password button btnremove\">Remove</a>';" << endl_ind <<
				"echo '</div>';" << endl_ind;

			//if _self add another pw field for confirmation of old pw
			//initially hidden, shown on any change

			phpFile << "if(" << userTable->getSingular() << "::hasGroup('_self')) {" << inc_ind << endl_ind <<
				"echo '<div class=\"input _selfpw\" style=\"display:none;\">';" << endl_ind <<
				"echo '<input type=\"password\" name=\"Old" << _table->getSingular() <<
				_name << "\" id=\"Old" << _table->getSingular() << _name <<
				"\" class=\"password text\" />';" << endl_ind <<
				"echo '</div>';" << dec_ind << endl_ind << "}" << endl_ind;

			phpFile << "echo '</div>';" << endl_ind;

			phpFile	<< "echo '<div class=\"field\"><div class=\"label\"><label for=\"New" <<
				_table->getSingular() << _name << "\">New " << _name <<
				":</label></div><div class=\"input\">';" << endl_ind;
			phpFile << "echo '<input type=\"password\" name=\"New" << _table->getSingular() <<
				_name << "\" id=\"New" << _table->getSingular() << _name <<
				"\" class=\"password text\" />';" << endl_ind <<
				"echo '</div></div>';" << endl_ind;
			phpFile	<< "echo '<div class=\"field\"><div class=\"label\"><label for=\"Confirm" <<
				_table->getSingular() << _name << "\">Confirm New " << _name <<
				":</label></div><div class=\"input\">';" << endl_ind;
			phpFile << "echo '<input type=\"password\" name=\"Confirm" << _table->getSingular() <<
				_name << "\" id=\"Confirm" << _table->getSingular() << _name <<
				"\" class=\"password text\" />';" << endl_ind <<

				"echo '<div class=\"error\" id=\"" << _table->getSingular() << _name << "Error\"></div>';" << endl_ind <<
				"echo '</div></div><!-- break; -->';" << endl_ind;

			break;

		case cmsame::tokens:
			phpFile << "echo 'tokens';" << endl_ind;
			break;

		case cmsame::options:
			phpFile << "echo '<select name=\"" << _table->getSingular() << 	_name << "\" class=\"select\">';" << endl_ind;// <<
			if(_null) phpFile << "echo '<option value=\"0\">(none)</option>';" << endl_ind;
			phpFile << "foreach(array_keys($item->" << _name << "->fwd) as $i) {" << inc_ind << endl_ind <<
				"echo '<option value=\"' . $i . '\" ' . ($i == $item->" << _name <<
				"->val ? 'selected' : '') . '>' . $item->" << _name << "->fwd[$i] . '</option>';" << dec_ind << endl_ind <<
				"}" << endl_ind << "echo '</select>';" << endl_ind;
			break;

		case cmsame::boolean:
			phpFile << "echo '<input type=\"checkbox\" name=\"" << _table->getSingular() <<
				_name << "\" id=\"" << _table->getSingular() << _name <<
				"\" class=\"boolean\" ' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '/>';" << endl_ind;
			break;
		case cmsame::imageupload:

			//1st block - current file
			phpFile << "echo '<div class=\"current\">';" << endl_ind <<
				//hidden field
				"if($item->" << _name << "->isnull()) { " << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"\" />';" << dec_ind << endl_ind <<
				"} else {" << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" class=\"currentfile\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"' . $item->" << _name <<
				"->getPath() . '\" />';" << dec_ind << endl_ind << "}" << endl_ind <<
				"if(!$item->" << _name << "->isnull()) echo '<input type=\"hidden\"  id=\"" << _table->getSingular() <<
					_name << "Remove\" name=\"" << _table->getSingular() << _name << "Remove\" class=\"doremove\" />';" << endl_ind;
				//then same as index list + remove
			phpFile << "echo \\cmsame\\getcms::editfile($item->" << _name << ", '../..');" << endl_ind;// <<
//				"if(!$item->" << _name << "->isnull()) echo '<a class=\"image button btnremove\">Remove</a>';" << endl_ind;

			//2nd block - html fileupload
			phpFile << "echo '</div><div class=\"upload\">';" << endl_ind <<
				"echo '<input type=\"file\" class=\"uploadfile\" name=\"" << _table->getSingular() << _name <<
				"Upload\" id=\"" << _table->getSingular() << _name << "Upload\" /></div>';" << dec_ind << endl_ind;
			break;


//			phpFile << "echo 'imageupload';" << endl_ind;


//			break;
		case cmsame::fileupload:

			//1st block - current file
			phpFile << "echo '<div class=\"current\">';" << endl_ind <<
				//hidden field
				"if($item->" << _name << "->isnull()) { " << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"\" />';" << dec_ind << endl_ind <<
				"} else {" << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" class=\"currentfile\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"' . $item->" << _name <<
				"->getPath() . '\" />';" << dec_ind << endl_ind << "}" << endl_ind <<
				"if(!$item->" << _name << "->isnull()) echo '<input type=\"hidden\"  id=\"" << _table->getSingular() <<
					_name << "Remove\" name=\"" << _table->getSingular() << _name << "Remove\" class=\"doremove\" />';" << endl_ind;
				//then same as index list
			phpFile << "echo \\cmsame\\getcms::editfile($item->" << _name << ", '../..');" << endl_ind;// <<
//				"if(!$item->" << _name << "->isnull()) echo '<a class=\"file button btnremove\">Remove</a>';" << endl_ind;

			//2nd block - html fileupload
			phpFile << "echo '</div><div class=\"upload\">';" << endl_ind <<
				"echo '<input type=\"file\" class=\"uploadfile\" name=\"" << _table->getSingular() << _name <<
				"Upload\" id=\"" << _table->getSingular() << _name << "Upload\" /></div>';" << dec_ind << endl_ind;
			break;
		case cmsame::oneof:
			phpFile << "echo '<select name=\"" << _table->getSingular() << 	getSqlName() << "\" class=\"select oneof\">';" << endl_ind;// <<
			if(_null) phpFile << "echo '<option value=\"0\">(none)</option>';" << endl_ind;
			phpFile << "$pt = new " << _refTarget->getSingular() << "Search();" << endl_ind <<
				"$res = " << _refTarget->getSingular() << "::search($pt);" << endl_ind <<
				"foreach($res as $i) {" << inc_ind << endl_ind <<
				"$iid=$i->getId();" << endl_ind <<
				"$iname=$i->" << _refTarget->getNameColumn()->getName() << "->val;" << endl_ind <<
				"if(!$item->" << _name << "->isnull() && $iid == $item->" << _name << "->getId()) {" << inc_ind << endl_ind <<
				"echo \"<option value=\\\"$iid\\\" selected>$iname</option>\";" << dec_ind << endl_ind <<
				"} else {" << inc_ind << endl_ind <<
				"echo \"<option value=\\\"$iid\\\">$iname</option>\";" << dec_ind << endl_ind <<
				"}" << dec_ind << endl_ind <<
				"}" << endl_ind << "echo '</select>';" << endl_ind;

			//view/edit button. needs to be dynamic with js
			phpFile << "if(" << userTable->getSingular() <<
				"::hasGroups(\"";
			_refTarget->getPermission()->writeEditGroups(phpFile);
			phpFile	<< 	"\")) {" << inc_ind << endl_ind <<
				"echo '<a href=\"../" << _refTarget->getPlural() << "/detail.php?" << _refTarget->getSingular() <<
				_refTarget->getIdColumn()->getSqlName() << "=' . ($item->" << _name <<
				"->isnull() ? '0' : $item->" << _name <<
				"->getId()) . '\" class=\"button oneof\">Edit</a>';" << dec_ind << endl_ind <<
				"} else if(" << userTable->getSingular() <<
					"::hasGroups(\"";
				_refTarget->getPermission()->writeViewGroups(phpFile);
				phpFile	<< 	"\")) {" << inc_ind << endl_ind <<
				"echo '<a href=\"../" << _refTarget->getPlural() << "/detail.php?" << _refTarget->getSingular() <<
				_refTarget->getIdColumn()->getSqlName() << "=' . ($item->" << _name <<
				"->isnull() ? '0' : $item->" << _name <<
				"->getId()) . '\" class=\"button oneof\">View</a>';" << dec_ind << endl_ind << "}" << endl_ind;

			break;

		case cmsame::listof:
			if(_mirror->hasType(cmsame::oneof)) {
				phpFile << "$count = " << _refTarget->getSingular() << "::countBy" << _mirror->getName() << "($item->" <<
					_table->getIdColumn()->getName() << "->val);" << endl_ind <<
					"if($count==0) echo '(no " << _refTarget->getPlural() << ")';" << endl_ind <<
					"else {" << inc_ind << endl_ind <<
					"if($count==1) echo '(1 " << _refTarget->getSingular() << ")';" << endl_ind <<
					"else echo '(' . $count . ' " << _refTarget->getPlural() << ")';" << endl_ind <<
					"echo '<a href=\"../" << _refTarget->getPlural() << "/index.php?" << _refTarget->getSingular() <<
					_mirror->getSqlName() << "=' . $item->" << _table->getIdColumn()->getName() <<
					"->val . '\" class=\"button\">Browse</a>';" << endl_ind;
				phpFile << dec_ind << endl_ind <<	"}" << endl_ind;

					//'new' button
				phpFile << "if(" << userTable->getSingular() <<
					"::hasGroups(\"";

				_refTarget->getPermission()->writeAddGroups(phpFile);

				phpFile	<< 	"\")) {" << inc_ind << endl_ind <<
					"echo '<a href=\"../" << _refTarget->getPlural() << "/detail.php?" << _refTarget->getSingular() <<
					_mirror->getSqlName() << "=' . $item->" << _table->getIdColumn()->getName() <<
					"->val . '\" class=\"button\">New</a>';" << dec_ind << endl_ind << "}" << endl_ind;

			} else if (_mirror->hasType(cmsame::listof)) { // *=>*, edit
				phpFile << "//table = " << _table->getPlural() << ", refTarget = " << _refTarget->getPlural() << ", linkTable = " << _linkTable->getPlural()  << endl_ind;

				phpFile << "$count = " << _linkTable->getSingular() << "::countBy" << _table->getSingular() << "($item->" <<
					_table->getIdColumn()->getName() << "->val);" << endl_ind <<
					"if($count==0) echo '(no " << _refTarget->getPlural() << ")';" << endl_ind <<
					"else {" << inc_ind << endl_ind <<
					"if($count==1) echo '(1 " << _refTarget->getSingular() << ")';" << endl_ind <<
					"else echo '(' . $count . ' " << _refTarget->getPlural() << ")';" << endl_ind <<
					"echo '<a href=\"../" << _linkTable->getPlural() << "/index.php?" << _linkTable->getSingular() <<
					_table->getSingular() << _table->getSingular() << _table->getIdColumn()->getName() <<
					"=' . $item->" << _table->getIdColumn()->getName() <<
					"->val . '\" class=\"button\">Browse</a>';" << endl_ind;
				phpFile << dec_ind << endl_ind <<	"}" << endl_ind;

					//'new' button
				phpFile << "if(" << userTable->getSingular() <<
					"::hasGroups(\"";

				_linkTable->getPermission()->writeAddGroups(phpFile);

				phpFile	<< 	"\")) {" << inc_ind << endl_ind <<
					"echo '<a href=\"../" << _linkTable->getPlural() << "/detail.php?" << _linkTable->getSingular() <<
					_table->getSingular() << _table->getSingular() << _table->getIdColumn()->getName() <<
					"=' . $item->" << _table->getIdColumn()->getName() <<
					"->val . '\" class=\"button\">New</a>';" << dec_ind << endl_ind << "}" << endl_ind;

			}

			break;

	}

	if(_type!=cmsame::id&&_type!=cmsame::password)
		phpFile << "echo '<div class=\"error\" id=\""<< _table->getSingular() << _name <<
				"Error\"></div></div></div>';" << endl_ind;


	//view only
	phpFile << dec_ind << endl_ind << "} else if(" << userTable->getSingular() <<
		"::hasGroups(\"";

	_permission->writeViewGroups(phpFile);

	phpFile	<< 	"\")) {" << inc_ind << endl_ind;

	if(_type!=cmsame::id) {
		phpFile	<< "echo '<div class=\"readonly field " << getCssClass() <<  "\"><div class=\"label\"><label for=\"" <<
				_table->getSingular() << _name <<
				"\">" << _name << ":</label>";

		if(_description.length()>0) {
			phpFile << "<span>";
			//escape '
			for(size_t i=0;i<_description.size();i++) {
				if(_description[i]=='\'') phpFile << "&#39";
				else phpFile << _description[i];
			}
			phpFile << "</span>";
		}

		phpFile << "</div><div class=\"data\">';" << endl_ind;

	}
	switch(_type)
	{
		case cmsame::password:
		case cmsame::none:
			break;
		case cmsame::id:
			phpFile << "echo '<input type=\"hidden\" name=\"" << _table->getSingular() <<
				_name << "\" id=\"" << _table->getSingular() << _name <<
				"\" class=\"id\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '\" />';" << endl_ind << endl_ind;
			break;

		case cmsame::boolean:
		case cmsame::integer:
		case cmsame::decimal:
		case cmsame::bigtext:
		case cmsame::text:
		case cmsame::tokens:
		case cmsame::datetime:
			phpFile << "echo '<input type=\"hidden\" name=\"" << _table->getSingular() <<
				_name << "\" id=\"" << _table->getSingular() << _name <<
				"\" value=\"' . \\cmsame\\getcms::inputvalue($item->" << _name << ") . '\" />';" << endl_ind << endl_ind;
			phpFile << "echo \\cmsame\\getcms::inputvalue($item->" << _name << ");" << endl_ind;
			break;

		case cmsame::imageupload:
				//hidden field
			phpFile << "if($item->" << _name << "->isnull()) { " << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"\" />';" << dec_ind << endl_ind <<
				"} else {" << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" class=\"currentfile\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"' . $item->" << _name <<
				"->getPath() . '\" />';" << dec_ind << endl_ind << "}" << endl_ind;
			phpFile << "echo \\cmsame\\getcms::linkfile($item->" << _name << ", '../..');" << endl_ind;
			break;
		case cmsame::fileupload:
			phpFile << "if($item->" << _name << "->isnull()) { " << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"\" />';" << dec_ind << endl_ind <<
				"} else {" << inc_ind << endl_ind <<
				"echo '<input type=\"hidden\" class=\"currentfile\" name=\"" << _table->getSingular() << _name <<
				"\" id=\"" << _table->getSingular() << _name << "\" value=\"' . $item->" << _name <<
				"->getPath() . '\" />';" << dec_ind << endl_ind << "}" << endl_ind;
			//either "(no file)" or "foo.jpg (12.3k)"
			phpFile << "echo \\cmsame\\getcms::linkfile($item->" << _name << ", '../..');" << endl_ind;
			break;

		case cmsame::options:
			phpFile << "echo '<input type=\"hidden\" name=\"" << _table->getSingular() <<
				_name << "\" id=\"" << _table->getSingular() << _name <<
				"\" value=\"' . $item->" << _name << "->val . '\" />';" << endl_ind;
			phpFile << "echo \\cmsame\\getcms::readonly($item->" << _name << ");" << endl_ind;
			break;
		case cmsame::oneof:
			phpFile << "echo ($item->" << _name << "->isnull() ? '(no " << _refTarget->getSingular() <<
				")' : ($item->" << _name << "->isOpen ? \\cmsame\\getcms::readonly($item->" << _name <<
				"->val->" << _refTarget->getNameColumn()->getName() << ") : '(" << _refTarget->getSingular() << ")'));" << endl_ind;

			//static view/edit button
			phpFile << "if(!$item->" << _name << "->isnull()) {" << inc_ind << endl_ind;
			phpFile << "if(" << userTable->getSingular() <<
				"::hasGroups(\"";
			_refTarget->getPermission()->writeEditGroups(phpFile);
			phpFile	<< 	"\")) {" << inc_ind << endl_ind <<
				"echo '<a href=\"../" << _refTarget->getPlural() << "/detail.php?" << _refTarget->getSingular() <<
				_refTarget->getIdColumn()->getSqlName() << "=' . $item->" << _name <<
				"->getId() . '\" class=\"button\">Edit</a>';" << dec_ind << endl_ind <<
				"} else if(" << userTable->getSingular() <<
					"::hasGroups(\"";
				_refTarget->getPermission()->writeViewGroups(phpFile);
				phpFile	<< 	"\")) {" << inc_ind << endl_ind <<
				"echo '<a href=\"../" << _refTarget->getPlural() << "/detail.php?" << _refTarget->getSingular() <<
				_refTarget->getIdColumn()->getSqlName() << "=' . $item->" << _name <<
				"->getId() . '\" class=\"button\">View</a>';" << dec_ind << endl_ind <<
				"}" << dec_ind << endl_ind << "}" << endl_ind;

			break;
		case cmsame::listof:
			if(_mirror->hasType(cmsame::oneof)) {
				phpFile << "$count = " << _refTarget->getSingular() << "::countBy" << _mirror->getName() << "($item->" <<
					_table->getIdColumn()->getName() << "->val);" << endl_ind <<
					"if($count==0) echo '(no " << _refTarget->getPlural() << ")';" << endl_ind <<
					"else {" << inc_ind << endl_ind <<
					"if($count==1) echo '(1 " << _refTarget->getSingular() << ")';" << endl_ind <<
					"else echo '(' . $count . ' " << _refTarget->getPlural() << ")';" << endl_ind <<
					"echo '<a href=\"../" << _refTarget->getPlural() << "/index.php?" << _refTarget->getSingular() <<
					_mirror->getSqlName() << "=' . $item->" << _table->getIdColumn()->getName() <<
					"->val . '\" class=\"button\">Browse</a>';" << dec_ind << endl_ind <<
					"}" << endl_ind;
			} else if (_mirror->hasType(cmsame::listof)) { // *=>*, view
				phpFile << "//table = " << _table->getPlural() << ", refTarget = " << _refTarget->getPlural() << ", linkTable = " << _linkTable->getPlural()  << endl_ind;

				phpFile << "$count = " << _linkTable->getSingular() << "::countBy" << _table->getSingular() << "($item->" <<
					_table->getIdColumn()->getName() << "->val);" << endl_ind <<
					"if($count==0) echo '(no " << _refTarget->getPlural() << ")';" << endl_ind <<
					"else {" << inc_ind << endl_ind <<
					"if($count==1) echo '(1 " << _refTarget->getSingular() << ")';" << endl_ind <<
					"else echo '(' . $count . ' " << _refTarget->getPlural() << ")';" << endl_ind <<
					"echo '<a href=\"../" << _linkTable->getPlural() << "/index.php?" << _linkTable->getSingular() <<
					_table->getSingular() << _table->getSingular() << _table->getIdColumn()->getName() <<
					"=' . $item->" << _table->getIdColumn()->getName() <<
					"->val . '\" class=\"button\">Browse</a>';" << dec_ind << endl_ind <<
					"}" << endl_ind;
			}
			break;

	}

	if(_type!=cmsame::id)
		phpFile	<< "echo '</div></div>';" << endl_ind;


	phpFile << dec_ind << endl_ind << "}" << endl_ind;

}

void Column::writeCmsAJAXPreview(bool requireSSL) {
	if(_type!=cmsame::text && _type!=cmsame::bigtext) return;
	switch(_renderMode) {
		case cmsame::plain:
			break;
		case cmsame::markdown:
		case cmsame::mdlite:
			if(!Target::assertPath((SITESPATH + _table->getSchema()->getSite()->getName() + "/www/cms/" +
				_table->getPlural() + "/").c_str())) {
		                _statusMessage = "Target generate error: couldn't create directory.";
       	        		_isOk = false;
		                return;
	        	}

			string phpFileName = Target::nativePath(SITESPATH +
				_table->getSchema()->getSite()->getName() + "/www/cms/" +
				_table->getPlural() + "/Preview" + _name + "Markdown.php");
		        ofstream phpFile;
       			phpFile.open(phpFileName.c_str(), ios_base::trunc);

		        if(!phpFile.good()) {
		                _statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
               			_isOk = false;
		                if (phpFile.is_open()) phpFile.close();
		                return;
       			}

			phpFile << "<?php" << inc_ind << endl_ind;
			if(requireSSL) phpFile << "if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on'){" <<
				"header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);die();}" << endl_ind;
			phpFile << "require_once('cmsame/" << _table->getSchema()->getSite()->getName() << "/" <<
				_table->getSchema()->getSite()->getSiteName() << ".php');" << endl_ind <<
				"if(!isset($_POST[\"" << _name << "\"])) die();" << endl_ind <<
				"$txt = $_POST[\"" << _name << "\"];" << endl_ind;


			for(vector<cmsame::CleanStep>::iterator step = _cleanSteps.begin(); step != _cleanSteps.end(); ++step) {
				switch(step->mode) {
					case cmsame::trim:
						phpFile << "$txt = trim($txt);" << endl_ind;
						break;
					case cmsame::striptags:
						phpFile << "$txt = strip_tags($txt);" << endl_ind;
						break;
					case cmsame::regex:
						phpFile << "if(preg_match('" << step->pattern << "', $txt, $m)) $txt = $m[" << 
							step->index <<  "];" << endl_ind;
						break;
				}
			}

			if(_renderMode==cmsame::markdown) {
				phpFile << "$md = new \\cmsame\\markdown();" << endl_ind;
			} else if(_renderMode==cmsame::mdlite) {
				phpFile << "$md = new \\cmsame\\mdlite();" << endl_ind;
			}

			if(_type==cmsame::bigtext)
				phpFile << "echo $md->get_html($txt);" << dec_ind << endl_ind;
			else if(_type==cmsame::text)
				phpFile << "echo $md->get_html_singleline($txt);" << dec_ind << endl_ind;

			phpFile << "?>" << endl_ind;


			phpFile.close();
			break;
	}
}

void Column::writeCmsAJAXResize(bool requireSSL, UserTable* userTable) {
	if(_type!=cmsame::imageupload) return;
	for(vector<ImageSize*>::iterator imageSize = _imageSizes.begin(); imageSize != _imageSizes.end(); ++imageSize) {
		if((*imageSize)->hasStepMode(cmsame::dynamic_free)||(*imageSize)->hasStepMode(cmsame::dynamic_lock)) {
			if(!Target::assertPath((SITESPATH + _table->getSchema()->getSite()->getName() + "/www/cms/" +
				_table->getPlural() + "/").c_str())) {
		                _statusMessage = "Target generate error: couldn't create directory.";
       	        		_isOk = false;
		                return;
	        	}

			string phpFileName = Target::nativePath(SITESPATH +
				_table->getSchema()->getSite()->getName() + "/www/cms/" +
				_table->getPlural() + "/Resize" + _name + (*imageSize)->getName() + ".php");
		        ofstream phpFile;
       			phpFile.open(phpFileName.c_str(), ios_base::trunc);

		        if(!phpFile.good()) {
		                _statusMessage = "Target generate error: couldn't open \"" + phpFileName + "\".";
               			_isOk = false;
		                if (phpFile.is_open()) phpFile.close();
		                return;
       			}

			phpFile << "<?php" << inc_ind << endl_ind;

			if(requireSSL) phpFile << "if(!isset($_SERVER['HTTPS']) || $_SERVER['HTTPS'] != 'on'){" <<
				"header('Location: https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']);die();}" << endl_ind;
			phpFile << "require_once('cmsame/" << _table->getSchema()->getSite()->getName() << "/" <<
				_table->getSchema()->getSite()->getSiteName() << ".php');" << endl_ind;

			phpFile << "$item = new " << _table->getSingular() << ";" << endl_ind <<
				"if(isset($_GET['" << _table->getSingular() << _table->getIdColumn()->getName() <<
				"']) && is_numeric($_GET['" << _table->getSingular() << _table->getIdColumn()->getName() <<
				"'])) $item->" << _table->getIdColumn()->getName() << "->set($_GET['" <<
					_table->getSingular() << _table->getIdColumn()->getName() << "']);" << endl_ind <<
				"else die();";

			phpFile << "if(!$item->load() || !file_exists(\\cmsame\\_var::$home . $item->" << _name << "->getPath('" <<
				(*imageSize)->getName() << "Pre'))) die();" << endl_ind;

			//get size of pre img and browser window(via ajax querystring)

			phpFile << "$wx=intval($_GET['Width']);" << endl_ind <<
				"$wy=intval($_GET['Height']);" << endl_ind <<
				"$ix=$item->" << _name << "->s_width['" << (*imageSize)->getName() << "Pre'];" << endl_ind <<
				"$iy=$item->" << _name << "->s_height['" << (*imageSize)->getName() << "Pre'];" << endl_ind <<
				"if($ix>($wx*0.8)) {" << inc_ind << endl_ind <<
				"$scale=($wx*0.8)/$ix;" << endl_ind <<
				"$ix *= $scale; $iy *= $scale;" << dec_ind << endl_ind <<
				"}" << endl_ind <<
				"if($iy>($wy*0.8)) {" << inc_ind << endl_ind <<
				"$scale=($wy*0.8)/$iy;" << endl_ind <<
				"$ix *= $scale; $iy *= $scale;" << dec_ind << endl_ind <<
				"}" << endl_ind <<
				"$scale = $ix/$item->" << _name << "->s_width['" << (*imageSize)->getName() << "Pre'];" << endl_ind <<
				"$left=($wx-$ix)/2-16;" << endl_ind <<
				"$top=($wy-$iy)/2-32;" << endl_ind;

			phpFile << "echo '<div id=\"lightbox\" style=\"left:' . $left . 'px;top:' . $top . 'px;\">';" << endl_ind <<
				"echo '<div class=\"image\">';" << endl_ind <<
				"echo '<div class=\"cropbox\" style=\"left:0px;top:0px;right:0px;bottom:0px;\">';" << endl_ind <<
				"echo '<div class=\"handle h_t h_l h_tl\"><div><div></div></div></div>';" << endl_ind <<
				"echo '<div class=\"handle h_t h_tc\"><div><div></div></div></div>';" << endl_ind <<
				"echo '<div class=\"handle h_t h_r h_tr\"><div><div></div></div></div>';" << endl_ind <<
				"echo '<div class=\"handle h_l h_cl\"><div><div></div></div></div>';" << endl_ind <<
				"echo '<div class=\"handle h_r h_cr\"><div><div></div></div></div>';" << endl_ind <<
				"echo '<div class=\"handle h_b h_l h_bl\"><div><div></div></div></div>';" << endl_ind <<
				"echo '<div class=\"handle h_b h_bc\"><div><div></div></div></div>';" << endl_ind <<
				"echo '<div class=\"handle h_b h_r h_br\"><div><div></div></div></div>';" << endl_ind <<
				"echo '</div>';" << endl_ind <<
				"echo '<img src=\"' . $item->" << _name <<  "->getUrl('../..', '" << (*imageSize)->getName() <<
				"Pre') . '\" style=\"width:' . $ix . 'px;height:' . $iy . 'px;\"/>';" << endl_ind <<
				"echo '</div><div class=\"buttons\">';" << endl_ind <<
				"printf('<span>%d%%</span>', $scale*100);" << endl_ind <<
				"echo '<a class=\"button btnresizecancel\" href=\"#\">Cancel</a>';" << endl_ind <<
				"echo '<a class=\"button btnresizedone\" href=\"#\">Done</a>';" << endl_ind <<
				"echo '<input type=\"hidden\" class=\"scale\" value=\"' . $scale . '\" />';" << endl_ind <<
				"echo '<input type=\"hidden\" class=\"prefix\" value=\"" << _table->getSingular() <<
					_name << (*imageSize)->getName() << "\" />';" << endl_ind;

			if((*imageSize)->hasStepMode(cmsame::dynamic_lock)) {
				double aspect = (double)((*imageSize)->dynamicX()) / (*imageSize)->dynamicY();
				phpFile << "echo '<input type=\"hidden\" class=\"aspect\" value=\"" <<
					aspect << "\" />';" << endl_ind;
			}

			phpFile << "echo '<input type=\"hidden\" class=\"min_x\" value=\"" << (*imageSize)->dynamicX() << "\" />';" << endl_ind <<
				"echo '<input type=\"hidden\" class=\"min_y\" value=\"" << (*imageSize)->dynamicY() << "\" />';" << endl_ind <<
				"echo '</div></div>';" << endl_ind;

			phpFile << dec_ind << endl_ind << "?>" << endl_ind;

			phpFile.close();
		}
	}
}

//Section

Section::Section(Table* table, xml_node<>* sectionNode, string prefix)
{
	_table = table;
	_permission = NULL; //new Permission(_table->getPermission());
	_name = "";
	_description = "";
	_type = cmsame::none;
	_x = 0;
	_y = 0;
	_null = true;
	_browse = true;

	//attributes
	for (xml_attribute<>* attr = sectionNode->first_attribute(); attr; attr = attr->next_attribute())
	{
		if(strcmp(attr->name(), "name") == 0) _name = prefix+attr->value();
		else if(strcmp(attr->name(), "description") == 0) _description = attr->value();

	}

	Column* column;

	//child nodes
	for (xml_node<>* node = sectionNode->first_node(); node; node = node->next_sibling())
	{
		if(strcmp(node->name(), "column") == 0)
		{
			column = new Column(_table, node, _name);
//			column->appendPrefix(_name);
			if(!column->isOk())
			{
				_isOk = false;
				_statusMessage = column->statusMessage();
			}
			_columns.push_back(column);
		}
		else if(strcmp(node->name(), "section") == 0)
		{
			column = new Section(_table, node, _name);
//			column->appendPrefix(_name);
			if(!column->isOk())
			{
				_statusMessage = column->statusMessage();
				_isOk = false;
				break;
			}
			_columns.push_back(column);
		}/*
		else if(strcmp(node->name(), "permission") == 0)
		{
			_permission->apply(node);
		}	*/
	}
}

Section::~Section()
{
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
		 delete (*column);
}

Column* Section::findByName(const string& name)
{
	Column* findColumn = NULL;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		findColumn = (*column)->findByName(name);
		if(findColumn) return findColumn;
	}
	return NULL;
}

bool Section::hasType(cmsame::ColumnType type) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if((*column)->hasType(type)) return true;
	}
	return false;
}

void Section::linkupRefTarget() {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->linkupRefTarget();
	}
}

void Section::linkupMirror() {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->linkupMirror();
	}
}

vector<Table*>* Section::linkupLinkTable() {
	vector<Table*>* sres = new vector<Table*>();
	vector<Table*>* res = NULL;
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		res = (*column)->linkupLinkTable();
		if(!(*column)->isOk()) {
			_isOk = false;
			_statusMessage = (*column)->statusMessage();
			if(res) delete res;
			delete sres;
			return NULL;
		}
		sres->insert(sres->end(), res->begin(), res->end());
		delete res;
	}
	return sres;
}


//SQL
void Section::writeSqlDefinition(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
//		if(column != _columns.begin()) sqlFile << "," << endl_ind;
		(*column)->writeSqlDefinition(sqlFile);
	}
}

void Section::writeSqlIndex(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlIndex(sqlFile);
	}
}

bool Section::writeSqlSelect(ofstream& sqlFile, string group, bool first, string alias) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlSelect(sqlFile, group, first && column!=_columns.begin(), alias);
	}
	return true;
}

void Section::writeSqlSelectRef(ofstream& sqlFile, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlSelectRef(sqlFile, group);
	}
}
void Section::writeSqlRefParam(ofstream& sqlFile, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlRefParam(sqlFile, group);
	}
}
void Section::writeSqlJoin(ofstream& sqlFile, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlJoin(sqlFile, group);
	}
}
void Section::writeSqlTailJoin(ofstream& sqlFile, string group, string alias) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlTailJoin(sqlFile, group, alias);
	}
}

void Section::writeSqlSearchParam(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		//if(column != _columns.begin()) sqlFile << ", ";
		(*column)->writeSqlSearchParam(sqlFile);
	}
}

void Section::writeSqlSearchWhere(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlSearchWhere(sqlFile);
	}
}

void Section::writeSqlUpdateParam(ofstream& sqlFile, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
//		if(column != _columns.begin()) sqlFile << ", ";
		(*column)->writeSqlUpdateParam(sqlFile, group);
	}
}
void Section::writeSqlUpdateSet(ofstream& sqlFile, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlUpdateSet(sqlFile, group);
	}
}

bool Section::writeSqlInsertParam(ofstream& sqlFile, string group, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
//		if(column != _columns.begin()) sqlFile << ", ";
		(*column)->writeSqlInsertParam(sqlFile, group, first && column!=_columns.begin());
	}
	return true; ///hmmmmm. hastype(anything except listof)
}

bool Section::writeSqlName(ofstream& sqlFile, string group, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
//		if(column != _columns.begin()) sqlFile << ", ";
		(*column)->writeSqlName(sqlFile, group, first && column!=_columns.begin());
	}
	return true;
}

bool Section::writeSqlParam(ofstream& sqlFile, string group, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlParam(sqlFile, group, first && column!=_columns.begin());
	}
	return true;
}

void Section::writeSqlAddForeignKey(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlAddForeignKey(sqlFile);
	}
}

void Section::writeSqlDropForeignKey(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlDropForeignKey(sqlFile);
	}
}

void Section::writeSqlCreateCount(ofstream& sqlFile, string& group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlCreateCount(sqlFile, group);
	}
}

void Section::writeSqlDropCount(ofstream& sqlFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlDropCount(sqlFile);
	}
}

void Section::writeGrantExec(ofstream& sqlFile, string& user, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeGrantExec(sqlFile, user, group);
	}
}
void Section::writeSqlInsertMirror(ofstream& sqlFile, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlInsertMirror(sqlFile, group);
	}
}
void Section::writeSqlUpdateMirror(ofstream& sqlFile, string group) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeSqlUpdateMirror(sqlFile, group);
	}
}
void Section::writeSqlDeleteMirror(ofstream& sqlFile, string group) {
}
//MODEL

void Section::writeModelConstruct(ofstream& phpFile, bool search)
{
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeModelConstruct(phpFile, search); }
}

void Section::writeModelFromPost(ofstream& phpFile, UserTable* userTable)
{
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeModelFromPost(phpFile, userTable); }
}

void Section::writeModelFromRow(ofstream& phpFile, UserTable* userTable)
{
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeModelFromRow(phpFile, userTable); }
}
/*
void Section::writeModelLoad(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeModelLoad(phpFile); }
}*/

bool Section::writeModelUpdateParam(ofstream& phpFile, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelUpdateParam(phpFile, first && column==_columns.begin());
	}
	return true;
}

bool Section::writeModelOpenParam(ofstream& phpFile, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelOpenParam(phpFile, first && column==_columns.begin());
	}
	return true;
}

void Section::writeModelOpenValue(ofstream& phpFile, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelOpenValue(phpFile, userTable);
	}
}
void Section::writeModelUpdateValue(ofstream& phpFile, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelUpdateValue(phpFile, userTable);
	}
}
/*
void Section::writeModelInsertColumn(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		if(column != _columns.begin()) phpFile << ", ";
		(*column)->writeModelInsertColumn(phpFile);
	}
}*/

bool Section::writeModelInsertValue(ofstream& phpFile, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
//		if(column != _columns.begin()) phpFile << "$query .= \", \";" << endl_ind;
		(*column)->writeModelInsertValue(phpFile, first && column==_columns.begin());
	}
	return true;
}

void Section::writeModelSearchOrder(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelSearchOrder(phpFile);
	}
}
void Section::writeModelSearchValue(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelSearchValue(phpFile);
	}
}

void Section::writeModelCleanText(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelCleanText(phpFile);
	}
}

void Section::writeModelRenderText(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelRenderText(phpFile);
	}
}

void Section::writeModelValidate(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelValidate(phpFile);
	}
}

void Section::writeModelDefault(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelDefault(phpFile);
	}
}
void Section::writeModelInsertFile(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelInsertFile(phpFile);
	}
}
void Section::writeModelUpdateFile(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelUpdateFile(phpFile);
	}
}
void Section::writeModelDeleteFile(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelDeleteFile(phpFile);
	}
}

void Section::writeModelMirror(ofstream& phpFile) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelMirror(phpFile);
	}
}

void Section::writeModelCount(ofstream& phpFile, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column) {
		(*column)->writeModelCount(phpFile, userTable);
	}
}

//CMS
bool Section::writeCmsIndexCount(ofstream& phpFile, UserTable* userTable, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeCmsIndexCount(phpFile, userTable, first && column==_columns.begin()); }
	return hasType(cmsame::oneof);
}
bool Section::writeCmsIndexFilter(ofstream& phpFile, UserTable* userTable, bool first) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeCmsIndexFilter(phpFile, userTable, first && column==_columns.begin()); }
	return hasType(cmsame::oneof);
}

void Section::writeCmsIndexHeader(ofstream& phpFile, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeCmsIndexHeader(phpFile, userTable); }
}

void Section::writeCmsIndexContent(ofstream& phpFile, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeCmsIndexContent(phpFile, userTable); }
}

void Section::writeCmsDetailField(ofstream& phpFile, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeCmsDetailField(phpFile, userTable); }
}
void Section::writeCmsAJAXPreview(bool requireSSL) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeCmsAJAXPreview(requireSSL); }
}
void Section::writeCmsAJAXResize(bool requireSSL, UserTable* userTable) {
	for(vector<Column*>::iterator column = _columns.begin(); column != _columns.end(); ++column)
	{ (*column)->writeCmsAJAXResize(requireSSL, userTable); }
}
