CC		= g++
SRCPATH		= cmsame/src/
OBJPATH		= cmsame/obj/
BINPATH		= cmsame/bin/
CXXFLAGS 	= -Wall -g -std=c++11
LDFLAGS 	= $(CXXFLAGS)
LDFLAGSWIN 	= -static $(CXXFLAGS)
ALLOBJS		= $(OBJPATH)main.o $(OBJPATH)site.o $(OBJPATH)target.o $(OBJPATH)schema.o $(OBJPATH)table.o $(OBJPATH)column.o $(OBJPATH)element.o $(OBJPATH)permission.o $(OBJPATH)record.o $(OBJPATH)crypto.o $(OBJPATH)filters.o

all: win32 linux

win32: $(BINPATH)cmsame.exe
linux: $(BINPATH)cmsame

$(BINPATH)cmsame: $(ALLOBJS)
	$(CC) $(LDFLAGS) -o $(BINPATH)cmsame $(ALLOBJS) -lncurses -ltinfo

$(BINPATH)cmsame.exe: $(ALLOBJS)
	$(CC) $(LDFLAGSWIN) -o $(BINPATH)cmsame.exe $(ALLOBJS) -lpdcurses

$(OBJPATH)main.o: $(SRCPATH)main.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)site.o: $(SRCPATH)site.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)target.o: $(SRCPATH)target.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)schema.o: $(SRCPATH)schema.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)table.o: $(SRCPATH)table.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)column.o: $(SRCPATH)column.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)element.o: $(SRCPATH)element.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)permission.o: $(SRCPATH)permission.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)record.o: $(SRCPATH)record.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)crypto.o: $(SRCPATH)crypto.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

$(OBJPATH)filters.o: $(SRCPATH)filters.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

clean-linux:
	rm -f $(OBJPATH)*.o
	rm -f $(BINPATH)cmsame

clean-win32:
	del $(subst /,\,$(OBJPATH))*.o
	del $(subst /,\,$(BINPATH))cmsame.exe

.PHONY: clean-linux clean-win32
