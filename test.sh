#!/bin/sh
rm -r sites/example/scripts
rm -r sites/example/model
rm -r sites/example/www/cms
cmsame/bin/cmsame example
rm -r sites/example/www/upload/*
chown www-data:www-data sites/example/www/upload
chmod 0700 sites/example/www/upload
chown www-data:www-data sites/example/model/keys
chmod 0700 sites/example/model/keys
service apache2 restart
mysql -p

